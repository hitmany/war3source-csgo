#include <sdktools>
#include "W3SIncs/War3Source_Interface"
#include <war3_vip>

public Plugin:myinfo = 
{
    name = "VIP system - Warcraft",
    author = "HiTmAnY",
    description = "[LP] Warcraft XP - 777 donut system",
    version = "1.5",
};

public OnPluginStart()
{
	HookEvent("player_spawn", Event_PlayerSpawn, EventHookMode_Post);
	HookEvent("player_death", hookPlayerDie, EventHookMode_Post);
}

public Action:Event_PlayerSpawn(Handle:hEvent, const String:szName[], bool:bDontBroadcast)
{
    new client = GetClientOfUserId(GetEventInt(hEvent, "userid"));
    if(IsPlayerAlive(client) && GetPlayerPremium(client))
    {
		if(VIPGetPlayerarmour(client))
		{
			GivePlayerItem(client, "item_assaultsuit");
			SetEntProp(client, Prop_Send, "m_ArmorValue", 100, 1);
		}
		//GivePlayerItem(client, "weapon_decoy");
		if(VIPGetPlayergrenade(client))
		{
			GivePlayerItem(client, "weapon_molotov");
			GivePlayerItem(client, "weapon_hegrenade");
			GivePlayerItem(client, "weapon_flashbang");
			GivePlayerItem(client, "weapon_smokegrenade");
		}
    }
}

public Action:hookPlayerDie(Handle:event, const String:name[], bool:dontBroadcast)
{
	new attacker = GetEventInt(event, "attacker");
	new id =  GetClientOfUserId(attacker);

	new Hp 	= 20;
	if(id > 0)
	{
		new CurrentHp = GetClientHealth(id);
		new Max = War3_GetMaxHP(id);
		
		if(CurrentHp == Max)
			return Plugin_Handled;
		
		if(GetPlayerPremium(id) && VIPGetPlayerhp(id))
		{	
			if((CurrentHp + Hp) > Max)
			{		
				SetEntProp(id, Prop_Send, "m_iHealth", Max, 1);
				
				PrintHintText(id, "+20 HP");
			} else {
				SetEntProp(id, Prop_Send, "m_iHealth", Hp + CurrentHp, 1);
				
				PrintHintText(id, "+20 HP");
			}	
		}
	}
	return Plugin_Continue;
	
}