#pragma semicolon 1

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"

//test commit
public Plugin:myinfo = 
{
    name = "War3Source - Race - Undead Scourge",
    author = "War3Source Team",
    description = "The Undead Scourge race for War3Source"
};

new thisRaceID;

new Float:SuicideBomberRadius[7] = {0.0, 250.0, 280.0, 280.0, 280.0, 280.0, 300.0}; 
new Float:SuicideBomberDamage[7] = {0.0, 166.0, 200.0, 266.0, 266.0, 266.0, 350.0};
new Float:SuicideBomberDamageTF[7] = {0.0, 133.0, 175.0, 250.0, 250.0, 250.0, 300.0}; 

new Float:UnholySpeed[7] = {1.0, 1.15, 1.18, 1.21, 1.24, 1.27, 1.3};
new Float:LevitationGravity[7] = {1.0, 0.8, 0.75, 0.7, 0.65, 0.6, 0.5};
new Float:VampirePercent[7] = {0.0, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2};

new Float:lastshot[MAXPLAYERSCUSTOM];
new speed_level;

new SKILL_LEECH, SKILL_SPEED, SKILL_LOWGRAV, SKILL_SUICIDE;

public OnPluginStart()
{
    LoadTranslations("w3s.race.undead.phrases");
	CreateTimer(1.0, ReturnSpeed, _, TIMER_REPEAT);
}

public OnWar3LoadRaceOrItemOrdered(num)
{
    if(num == 10)
    {
        thisRaceID = War3_CreateNewRaceT("undead");
        SKILL_LEECH = War3_AddRaceSkillT(thisRaceID, "VampiricAura", false, 6, "20%");
        SKILL_SPEED = War3_AddRaceSkillT(thisRaceID, "UnholyAura", false, 6, "20%");
        SKILL_LOWGRAV = War3_AddRaceSkillT(thisRaceID, "Levitation", false, 6, "0.5");
        SKILL_SUICIDE = War3_AddRaceSkillT(thisRaceID, "SuicideBomber", true, 6); 
        
        War3_CreateRaceEnd(thisRaceID);
        
        War3_AddSkillBuff(thisRaceID, SKILL_LEECH, fVampirePercent, VampirePercent);
        War3_AddSkillBuff(thisRaceID, SKILL_SPEED, fMaxSpeed, UnholySpeed);
        War3_AddSkillBuff(thisRaceID, SKILL_LOWGRAV, fLowGravitySkill, LevitationGravity);
    }
}

public OnUltimateCommand(client, race, bool:pressed)
{
    if(pressed && War3_GetRace(client) == thisRaceID && IsPlayerAlive(client) && !Silenced(client))
    {
        new ult_level = War3_GetSkillLevel(client, race, SKILL_SUICIDE);
        ult_level > 0 ? ForcePlayerSuicide(client) : W3MsgUltNotLeveled(client);
    }
}

public OnWar3EventDeath(victim, attacker)
{
    new race = W3GetVar(DeathRace);
    new skill = War3_GetSkillLevel(victim, thisRaceID, SKILL_SUICIDE);
    if(race == thisRaceID && skill > 0 && !Hexed(victim))
    {
        decl Float:fVictimPos[3];
        GetClientAbsOrigin(victim, fVictimPos);
        
        War3_SuicideBomber(victim, fVictimPos, GameTF() ? SuicideBomberDamageTF[skill] : SuicideBomberDamage[skill], SKILL_SUICIDE, SuicideBomberRadius[skill]);        
    } 
}

public OnW3TakeDmgBulletPre(victim,attacker,Float:damage)
{
	if(ValidPlayer(victim)&&ValidPlayer(attacker))
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			if(War3_GetRace(attacker)==thisRaceID)
			{
				lastshot[attacker] = GetGameTime();
				War3_SetBuff(attacker,fMaxSpeed,thisRaceID,1.00);
			}
		}
	}
}

public Action:ReturnSpeed(Handle:timer,any:zclient)
{
    for(new client=1; client <= MaxClients; client++)
    {
        if(ValidPlayer(client, true))
        {
            if(War3_GetRace(client) == thisRaceID)
            {
                if(lastshot[client] + 1.0 < GetGameTime())
				{
					speed_level=War3_GetSkillLevel(client,thisRaceID,SKILL_SPEED);
					if(speed_level>0)
					{
						War3_SetBuff(client,fMaxSpeed,thisRaceID,UnholySpeed[speed_level]);
						W3ReapplySpeed(client);
					}
				}
            }
        }
        
    }
}

public OnClientPutInServer(client)
{
	lastshot[client] = 0;
}