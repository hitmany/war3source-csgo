/**
 * File: War3Source_BladeMaster.sp
 * Description: The Blademaster race for War3Source.
 * Author(s): [Oddity]TeacherCreature
 */
 
#pragma semicolon 1
 
#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdkhooks>
#include <sdktools>
#include <sdktools_functions>
#include <sdktools_tempents>
#include <sdktools_tempents_stocks>
 
new thisRaceID;
new SKILL_WINDWALK,SKILL_MIRRORIMAGE, SKILL_CRITICAL,ULT_BLADESTORM;
 
//skill 1
new Float:WindWalkTime[9]={0.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0};
new Float:WindWalkspeed[9]={1.0,1.075,1.1,1.13,1.18,1.2,1.3,1.4,1.5};
new Float:AbilityCooldownTime[9]={0.0,35.0,33.0,30.0,28.0,25.0,28.0,20.0,15.0};
new bool:bWinded[66];
new String:ww_on[]="npc/scanner/scanner_nearmiss1.wav";
new String:ww_off[]="npc/scanner/scanner_nearmiss2.wav";
 
//skill 2
//new String:mImage[256];
new Float:MirrorImageTimer[9]={0.0,1.0,1.4,1.8,2.2,2.6,3.0,3.4,3.8};
new Float:MirrorImageChance[9]={0.0,0.4,0.06,0.010,0.15,0.2,0.25,0.3,0.4};
 
//skill 3
new Float:CriticalPercent[9]={0.0,0.06,0.08,0.10,0.12,0.14,0.15,0.17,0.2};
new Float:CriticalChance[9]={0.0,0.4,0.06,0.08,0.10,0.15,0.25,0.3,0.4};
 
//ultimate
new Handle:ultCooldownCvar;
new OverloadDuration=30; //HIT TIMES, DURATION DEPENDS ON TIMER
new OverloadRadius=200;
new OverloadDamagePerHit[9]={0,8,9,10,11,12,13,14,15};
new Float:OverloadDamageIncrease[9]={1.0,1.015,1.018,1.021,1.024,1.027,1.030,1.033,1.035};
////
new UltimateZapsRemaining[66];
new Float:PlayerDamageIncrease[66];
new BeamSprite,HaloSprite,SmokeSprite;
new String:ultsnd[256];

new UserMsg:g_FadeUserMsgId;
 
public Plugin:myinfo =
{
        name = "War3Source Race - Blademaster",
        author = "[Oddity]TeacherCreature",
        description = "The Blademaster race for War3Source.",
        version = "1.0.0.0",
        url = "warcraft-source.net"
}
 
public OnPluginStart()
{
        //HookEvent("round_start",RoundStartEvent);
        ultCooldownCvar=CreateConVar("war3_bmaster_ult_cooldown","30","Cooldown time for BMaster ult Bladestorm.");
		g_FadeUserMsgId = GetUserMessageId("Fade");
}
 
public OnWar3LoadRaceOrItemOrdered(num)
{
        if(num==200)
        {
                thisRaceID=War3_CreateNewRace("Мастер клинка","bmaster");
                SKILL_WINDWALK=War3_AddRaceSkill(thisRaceID,"Дуновение Ветра","Абилити. Невидимость и скорость.",false,8);
                SKILL_MIRRORIMAGE=War3_AddRaceSkill(thisRaceID,"Зеркальное Отражение","Экран противника затуманивается\n зеленым ядом",false,8);
                SKILL_CRITICAL=War3_AddRaceSkill(thisRaceID,"Критический Удар","Шанс дополнительного урона",false,8);
                ULT_BLADESTORM=War3_AddRaceSkill(thisRaceID,"Шторм Лезвий","Враги рядом \nс вами получают урон",true,8);
                War3_CreateRaceEnd(thisRaceID);
        }
 
}
 
public OnClientPutInServer(client)
{
        SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
}
 
public Action:OnWeaponCanUse(client, weapon)
{
        if (War3_GetRace(client)== thisRaceID)
        {
                /*decl String:name[64];
                GetEdictClassname(weapon, name, sizeof(name));
                //if(StrEqual(name, "weapon_knife", false))
                if (IsEquipmentMelee(name))
                        return Plugin_Continue;
                return Plugin_Handled;*/
        }
        return Plugin_Continue;
}
 
public OnMapStart()
{
        BeamSprite=PrecacheModel("materials/sprites/lgtning.vmt");
        HaloSprite=PrecacheModel("materials/sprites/halo01.vmt");
        SmokeSprite=PrecacheModel("sprites/smoke.vmt");
		War3_AddSoundFolder(ultsnd, sizeof(ultsnd), "bmaster/swiftbladespin.mp3");
		War3_AddCustomSound(ultsnd);
		War3_PrecacheSound(ww_on);
		War3_PrecacheSound(ww_off);
}
 
public OnWar3EventSpawn(client)
{
        new race=War3_GetRace(client);
        if(race==thisRaceID)
        {
                UltimateZapsRemaining[client]=0;
                bWinded[client]=false;
                SetEntityRenderMode(client , RENDER_NORMAL);
				War3_SetBuff(client, fMaxSpeed, thisRaceID, 1.0);
				//War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
        }
}
 
public OnAbilityCommand(client,ability,bool:pressed)
{
        if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client))
        {
                new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_WINDWALK);
                if(skill_level>0)
                {
                        if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_WINDWALK,true))
                        {
								SetEntityRenderMode(client , RENDER_NONE);
								//War3_SetBuff(client,fInvisibilitySkill,thisRaceID,0.0);
								War3_SetBuff(client, fMaxSpeed, thisRaceID, WindWalkspeed[skill_level]);
                                CreateTimer(WindWalkTime[skill_level],RemoveInvis,client);
                                War3_CooldownMGR(client,AbilityCooldownTime[skill_level],thisRaceID,SKILL_WINDWALK,_,_);
                                PrintHintText(client,"Дуновение Ветра: Невидимость + Скорость");
                                bWinded[client]=true;
                                EmitSoundToAll(ww_on,client);
                        }
                }
        }
}
 
public Action:RemoveInvis(Handle:t,any:client)
{
        if(bWinded[client]==true)
        {
			SetEntityRenderMode(client , RENDER_NORMAL);
			//War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
			War3_SetBuff(client, fMaxSpeed, thisRaceID, 1.0);
			bWinded[client]=false;
			PrintHintText(client,"Дуновение Ветра: Завершено");
			EmitSoundToAll(ww_off,client);
			War3_SetBuff(client, fMaxSpeed, thisRaceID, 1.0);
        }
}
 
public CooldownUltimate(client)
{
        War3_CooldownMGR(client,GetConVarFloat(ultCooldownCvar),thisRaceID,ULT_BLADESTORM,_,_);
}
 
public Action:RemoveInvis2(Handle:t,any:client)
{
	if(bWinded[client]==true)
	{
		SetEntityRenderMode(client , RENDER_NORMAL);
		//War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
		bWinded[client]=false;
	}
}


stock FadeClient(target, amount)
{
	new color[4] = { 0, 0, 0, 0 };
	color[0] = 0;
	color[1] = 255;
	color[2] = 51;
	color[3] = amount;
	new targets[2];
	targets[0] = target;
	
	new flags;
		if (amount == 0)
			flags = (0x0001 | 0x0010);
		else
			flags = (0x0002 | 0x0008);

	new Handle:message = StartMessageEx(g_FadeUserMsgId, targets, 1);
    PbSetInt(message, "duration", 768);
    PbSetInt(message, "hold_time", 1536);
	PbSetInt(message, "flags", flags);
    PbSetColor(message, "clr", color);
    EndMessage();
	CreateTimer(2.0, UnFadeClient, target);
}

public Action:UnFadeClient(Handle:timer,any:target)
{
	FadeClient(target, 0);
}
 
public OnWar3EventPostHurt(victim, attacker, Float:damage, const String:weapon[32], bool:isWarcraft)
{
        if(ValidPlayer(victim,true)&&ValidPlayer(attacker,true)&&GetClientTeam(victim)!=GetClientTeam(attacker)&&attacker!=victim)
        {
                new vteam=GetClientTeam(victim);
                new ateam=GetClientTeam(attacker);
                if(vteam!=ateam)
                {
                        new race_attacker=War3_GetRace(attacker);
                        new race_victim=War3_GetRace(victim);
                        // mirror image
                        new skill_mimage=War3_GetSkillLevel(victim,race_victim,SKILL_MIRRORIMAGE);
                        if(race_victim==thisRaceID)
                        {
                                if(GetRandomFloat(0.0,1.0)<=MirrorImageChance[skill_mimage])
                               {    //War3_SetBuff(victim,fInvisibilitySkill,thisRaceID,0.0);
                                        //new Float:pos[3];
                                        //GetClientAbsOrigin(victim,pos);
                                        //GetClientModel(victim,mImage[victim],256);
                                        //TE_SetupGlowSprite(pos,mImage[victim], 4.0, 1.0, 205);
                                       
                                        //SmokeSprite=sprites/smoke.vmt
										new CurrentHp = GetClientHealth(victim);
										new Max = War3_GetMaxHP(victim);
										SetEntityRenderMode(victim , RENDER_NONE);
										//War3_SetBuff(victim,fInvisibilitySkill,thisRaceID,0.0);
										new Float:pos[3];
										GetClientAbsOrigin(victim,pos);
										//pos[0] = pos[0] - 200;
										pos[2] = pos[2] + 100;
										TE_SetupSmoke(pos,SmokeSprite,1000.0,1);
										TE_SendToAll();
                                        CreateTimer(1.0,Smoked,victim);
                                        CreateTimer(1.5,Smoked,victim);
                                        CreateTimer(2.0,Smoked,victim);
                                        CreateTimer(2.5,Smoked,victim);
                                        CreateTimer(3.0,Smoked,victim);
                                        CreateTimer(MirrorImageTimer[skill_mimage],RemoveInvis2,victim);
                                        PrintHintText(victim,"Зеркальное Отражение. +25HP");
                                        SetEntityHealth(victim,GetClientHealth(victim)+25);
										if((CurrentHp + 25) > Max)
										{		
											SetEntProp(victim, Prop_Send, "m_iHealth", Max, 1);
										} 
										else 
										{
											SetEntProp(victim, Prop_Send, "m_iHealth", 25 + CurrentHp, 1);
										}
                                        bWinded[victim]=true;
								}
                        }
                        if(race_attacker==thisRaceID)
                        {
                                //windwalk turn off
                                if(bWinded[attacker]==true)
                                {
                                        //War3_SetBuff(attacker,fInvisibilitySkill,thisRaceID,1.0);
										SetEntityRenderMode(attacker , RENDER_NORMAL);
                                        bWinded[attacker]=false;
                                        PrintHintText(attacker,"Дуновение Ветра Завершено");
                                        EmitSoundToAll(ww_off,attacker);
                                }
                                //critical
                                new skill_cs_attacker=War3_GetSkillLevel(attacker,race_attacker,SKILL_CRITICAL);
                                if(skill_cs_attacker>0)
                                {
                                        if(GetRandomFloat(0.0,1.0)<=CriticalChance[skill_cs_attacker] && !W3GetBuffHasTrue(victim,bImmunitySkills))
                                        {
                                                new Float:percent=CriticalPercent[skill_cs_attacker];
                                                new health_take=RoundFloat(damage*percent);
												health_take=health_take+RoundFloat(damage);
                                                if(War3_DealDamage(victim,health_take,attacker,_,"bmastercrit",W3DMGORIGIN_SKILL,W3DMGTYPE_PHYSICAL,true))
                                                {      
                                                        PrintHintText(attacker,"Крит: +%d",health_take);
                                                        PrintHintText(victim,"Мастер клинка: Урон -%d",health_take);
                                                       
                                                        /*War3_DamageModPercent(percent);
                                                        PrintToConsole(attacker,"%.1fX Critical ! ",percent+1.0);
                                                        PrintHintText(attacker,"Critical !",percent+1.0);
                                                        PrintToConsole(victim,"Received %.1fX Critical Dmg!",percent+1.0);
                                                        PrintHintText(victim,"Received Critical Dmg!");*/
                                                       
                                                        FadeClient(victim,255);
                                                }
                                        }
                                }
                        }
                }
        }
}
 
public Action:Smoked(Handle:h,any:victim)
{
        new Float:pos[3];
        GetClientAbsOrigin(victim,pos);
        TE_SetupSmoke(pos,SmokeSprite,1000.0,1);
        TE_SendToAll();
}
 
public OnUltimateCommand(client,race,bool:pressed)
{
        if(race==thisRaceID && pressed && IsPlayerAlive(client))
        {
                //if(
               
                new skill=War3_GetSkillLevel(client,race,ULT_BLADESTORM);
                if(skill>0)
                {
                        if(War3_SkillNotInCooldown(client,thisRaceID,ULT_BLADESTORM,true))
                        {
                                UltimateZapsRemaining[client]=OverloadDuration;
                                if(War3_GetGame()==Game_CS){
                                        UltimateZapsRemaining[client]=OverloadDuration*2;
                                }
                                EmitSoundToAll(ultsnd,client);
                                PlayerDamageIncrease[client]=1.0;
                                War3_CooldownMGR(client,GetConVarFloat(ultCooldownCvar),thisRaceID,ULT_BLADESTORM,_,_);
                                PrintHintText(client,"Шторм Лезвий");
                                CreateTimer(War3_GetGame()==Game_CS?0.25:0.5,UltimateLoop,GetClientUserId(client)); //damage
                        }
                }
                else
                {
                        PrintHintText(client,"Ультимейт не прокачен");
                }
        }
}
public Action:UltimateLoop(Handle:timer,any:userid)
{
        new attacker=GetClientOfUserId(userid);
        if(ValidPlayer(attacker) && UltimateZapsRemaining[attacker]>0&&IsPlayerAlive(attacker))
        {
                UltimateZapsRemaining[attacker]--;
                new Float:pos[3];
                new Float:otherpos[3];
                GetClientEyePosition(attacker,pos);
                new team = GetClientTeam(attacker);
                new lowesthp=99999;
                new besttarget=0;
 
                for(new i=1;i<=MaxClients;i++){
                        if(ValidPlayer(i,true)){
                               
                                if(GetClientTeam(i)!=team&&!W3GetBuffHasTrue(i,bImmunityUltimates)){
                                        GetClientEyePosition(i,otherpos);
                                        if(War3_GetGame()==Game_CS){
                                                otherpos[2]-=20;
                                        }
                                        //PrintToChatAll("%d distance %f",i,GetVectorDistance(pos,otherpos));
                                        if(GetVectorDistance(pos,otherpos)<OverloadRadius){
                                               
                                                //TE_SetupBeamPoints(pos,otherpos,BeamSprite,HaloSprite,0,35,0.15,6.0,5.0,0,1.0,{255,255,255,100},20);
                                                //TE_SendToAll();
                                               
                                                new Float:distanceVec[3];
                                                SubtractVectors(otherpos,pos,distanceVec);
                                                new Float:angles[3];
                                                GetVectorAngles(distanceVec,angles);
                                               
                                                TR_TraceRayFilter(pos, angles, MASK_PLAYERSOLID, RayType_Infinite, CanHitThis,attacker);
                                                new ent;
                                                if(TR_DidHit(_))
                                                {
                                                        ent=TR_GetEntityIndex(_);
                                                        //PrintToChatAll("trace hit: %d      wanted to hit player: %d",ent,i);
                                                }
                                               
                                                if(ent==i&&GetClientHealth(i)<lowesthp){
                                                        besttarget=i;
                                                        lowesthp=GetClientHealth(i);
                                                }
                                        }
                                }
                        }
                }
                if(besttarget>0){
                        pos[2]-=20.0;
                       
                        GetClientEyePosition(besttarget,otherpos);
                        otherpos[2]-=20.0;
                        TE_SetupBeamPoints(pos,otherpos,BeamSprite,HaloSprite,0,35,0.15,6.0,5.0,0,1.0,{255,000,255,255},20);
                        TE_SendToAll();
                        War3_DealDamage(besttarget,OverloadDamagePerHit[War3_GetSkillLevel(attacker,thisRaceID,ULT_BLADESTORM)],attacker,_,"bladestorm");
                        PlayerDamageIncrease[attacker]*=OverloadDamageIncrease[War3_GetSkillLevel(attacker,thisRaceID,ULT_BLADESTORM)];
                        PrintHintText(besttarget,"Мастер Клинка: Атакован Штормом Лезвий");
                }
                CreateTimer(War3_GetGame()==Game_CS?0.25:0.5,UltimateLoop,GetClientUserId(attacker)); //damage
        }
        else
        {
                UltimateZapsRemaining[attacker]=0;
        }
}
 
public bool:CanHitThis(entity, mask, any:data)
{
        if(entity == data)
        {// Check if the TraceRay hit the itself.
                return false; // Don't allow self to be hit
        }
        return true; // It didn't hit itself
}