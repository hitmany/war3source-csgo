	//www.wcs-lagerhaus.de
/*
 Race: Traxex - The Drow Ranger
 Author(s): Vulpone&Revan
 Version: 1.0
 */


#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>

new SKILL_ARROWS, SKILL_MARKMAN, SKILL_TRUESHOT, ULT_SILENCE;
new thisRaceID;
new Handle:ultCooldownCvar;
new BeamSprite,HaloSprite;
new bool:bUlt[MAXPLAYERS];

new Float:Restriction[MAXPLAYERS];

//Skill #1: arrows
new Float:FrostChance[7]={0.0,0.010,0.15,0.2,0.25,0.3,0.4};
new Float:AttackFrost[7]={1.0, 0.95, 0.92, 0.90, 0.86, 0.80, 0.70};
new Float:MoveFrost[7]={1.0, 0.92, 0.90, 0.86, 0.80, 0.70, 0.50};

//Skill #2: Markmanship
new Float:speed[7]={1.0, 1.15, 1.20, 1.22, 1.25, 1.29, 1.30};
new Float:grav[7]={1.0, 0.9, 0.8, 0.75, 0.73, 0.70, 0.60};

//Skill #3: Trueshot Aura
new Float:ExtraAura[7]={0.0, 0.14, 0.16, 0.2, 0.25, 0.28, 0.35};
new Float:AuraChance=0.17;
new Float:AuraRange[7]={0.0, 150.0, 180.0, 200.0, 250.0, 300.0, 350.0};

//Skill #4:Silence
new Float:SilenceRadius[7]={0.0, 500.0, 500.0, 500.0, 500.0, 500.0, 500.0};
new Float:SilenceDuration[7]={0.0, 4.0, 7.0, 9.0, 11.0, 14.0, 15.0};

new UserMsg:g_FadeUserMsgId;
new Float:durationsilence;



//coding starts HERE

public Plugin:myinfo = 
{
	name = "War3Source Race - Traxex",
	author = "Vulpone",
	description = "The Traxex race for War3Source",
	version = "1.0",
	url = "www.wcs-lagerhaus.de"
};

public OnPluginStart()
{
	LoadTranslations("w3s.race.bmaster.phrases");
	ultCooldownCvar=CreateConVar("war3_trax_ult_cooldown","15.0","Ultimate colldown for silence");
	g_FadeUserMsgId = GetUserMessageId("Fade");
}	

public OnWar3LoadRaceOrItemOrdered(num)
{
	if( num == 200)
	{
		thisRaceID=War3_CreateNewRaceT("bmaster");
		SKILL_ARROWS=War3_AddRaceSkillT(thisRaceID,"FrostArrows",false,6);
		SKILL_MARKMAN=War3_AddRaceSkillT(thisRaceID,"Marksmanship",false,6);
		SKILL_TRUESHOT=War3_AddRaceSkillT(thisRaceID,"TrueshotAura",false, 6);
		ULT_SILENCE=War3_AddRaceSkillT(thisRaceID,"Silence",true,6); 
		War3_CreateRaceEnd(thisRaceID);
	}
}


public OnMapStart() 
{
	PrecacheModel("particle/particle_smokegrenade1.vmt", true);
    AddFileToDownloadsTable("particle/particle_smokegrenade1.vmt");
	BeamSprite=PrecacheModel("materials/sprites/laserbeam.vmt");
	HaloSprite=PrecacheModel("materials/sprites/glow.vmt");
	//AddFileToDownloadsTable("sprites/plasmabeam.vmt");
	//AddFileToDownloadsTable("materials/sprites/halo01.vmt");

	AddFileToDownloadsTable("sound/war3source/traxex/icehit.mp3");
	FakePrecacheSound( "*war3source/traxex/icehit.mp3" );
	AddFileToDownloadsTable("sound/war3source/traxex/bowhit.mp3");
	FakePrecacheSound( "*war3source/traxex/bowhit.mp3" );
	AddFileToDownloadsTable("sound/war3source/traxex/touch.mp3");
	FakePrecacheSound( "*war3source/traxex/touch.mp3" );
}
stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}
public OnWar3EventSpawn(client)
{
	if(War3_GetRace(client) == thisRaceID)
	{
		InitPassiveSkills(client);
	}
	if(War3_GetRace(client) != thisRaceID)
	{
		War3_SetBuff(client, fMaxSpeed, thisRaceID, 1.0);
		War3_SetBuff(client, fLowGravitySkill, thisRaceID, 1.0);
	}
	
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, 1.0 );
	}
}

public InitPassiveSkills(client)
{
	if(ValidPlayer(client, true))
	{
		new skilllvl = War3_GetSkillLevel(client, thisRaceID, SKILL_MARKMAN);
		if( skilllvl > 0)
		{
			War3_SetBuff(client, fMaxSpeed, thisRaceID, speed[skilllvl]);
			War3_SetBuff(client, fLowGravitySkill, thisRaceID, grav[skilllvl]);
		}
	}
}

public OnW3TakeDmgBulletPre(victim,attacker,Float:damage)
{
	new skilllvl = War3_GetSkillLevel(attacker, thisRaceID, SKILL_ARROWS);
	new skillaura = War3_GetSkillLevel(attacker, thisRaceID, SKILL_TRUESHOT);
	new race_attacker=War3_GetRace(attacker);
	new String:sWeaponName[64];
	GetClientWeapon(attacker, sWeaponName, sizeof(sWeaponName));
	if (StrEqual(sWeaponName, "weapon_xm1014", false) || StrEqual(sWeaponName, "weapon_nova", false) || StrEqual(sWeaponName, "weapon_sawedoff", false) || StrEqual(sWeaponName, "weapon_mag7", false)) 
	{
		return Plugin_Continue;
	}
	if(ValidPlayer(attacker, true) && ValidPlayer(victim, true))
	{
		new vteam = GetClientTeam( victim );
		new ateam = GetClientTeam( attacker );
		if(vteam != ateam) {
			//arrows frost
			if(skilllvl > 0 && race_attacker == thisRaceID)
			{
				if(GetRandomFloat(0.01, 1.0) <= FrostChance[skilllvl])
				{
					if(!W3HasImmunity(victim, Immunity_Skills))
					{
						CreateTimer(3.0, Timer_RemoveAll, victim);
						War3_SetBuff(victim,fSlow,thisRaceID,MoveFrost[skilllvl]);
						//War3_SetBuff(victim,fAttackSpeed,thisRaceID,AttackFrost[skilllvl]);
						W3FlashScreen(victim,RGBA_COLOR_BLUE);
						PrintHintText(attacker, "%T","Frost Arrows activated!", attacker);
						EmitSoundToAll("*war3source/traxex/icehit.mp3", attacker);
						EmitSoundToAll("*war3source/traxex/icehit.mp3", victim);
						new Float:myVec[3];
						GetClientAbsOrigin(attacker, myVec);
						new Float:endVec[3];
						GetClientAbsOrigin(victim, endVec);
						
						endVec[2] += 30;
						myVec[2] += 30;
						
						TE_SetupBeamPoints(myVec, endVec, BeamSprite, HaloSprite, 0, 15, 1.4, 5.0, 10.0, 1, 2.0, {175,201,255,222}, 20);
						TE_SendToAll();
						
					}
				}
			}
			//trueshot aura
			if(skillaura > 0 && GetRandomFloat(0.01, 1.0) <= AuraChance )
			{
				for(new client = 1; client <= MaxClients; client++)
				{
					if(ValidPlayer(client,true)) {
						if(War3_GetRace(client) == thisRaceID && GetClientTeam(client) == ateam)
						{
							new Float:AttackerPos[3];
							GetClientAbsOrigin(attacker, AttackerPos);
							new Float:iVec[3];
							GetClientAbsOrigin(client, iVec);
							new Float:radius = AuraRange[skillaura];
							if(GetVectorDistance(iVec, AttackerPos) <= radius)
							{
								if(!W3HasImmunity(victim, Immunity_Skills))
								{
									War3_DamageModPercent(ExtraAura[skillaura]+1.0);
									EmitSoundToAll("*war3source/traxex/bowhit.mp3", victim);
									EmitSoundToAll("*war3source/traxex/bowhit.mp3", attacker);
									W3FlashScreen(victim,{255, 0, 0, 255},0.4,0.6,FFADE_IN);
								}
							}
							else
							{
								War3_DamageModPercent(1.0);
							}
						}
					}
				}
			}
		}
	}
	
	return Plugin_Continue;
}

stock FadeClient(target, amount)
{
	new color[4] = { 0, 0, 0, 0 };
	color[0] = 255;
	color[1] = 0;
	color[2] = 0;
	color[3] = amount;
	new targets[2];
	targets[0] = target;
	
	new flags;
		if (amount == 0)
			flags = (0x0001 | 0x0010);
		else
			flags = (0x0002 | 0x0008);

	new Handle:message = StartMessageEx(g_FadeUserMsgId, targets, 1);
    PbSetInt(message, "duration", 768);
    PbSetInt(message, "hold_time", 1536);
	PbSetInt(message, "flags", flags);
    PbSetColor(message, "clr", color);
    EndMessage();
	CreateTimer(2.0, UnFadeClient, target);
}

public Action:UnFadeClient(Handle:timer,any:target)
{
	if(ValidPlayer(target, true))
	{
		FadeClient(target, 0);
	}
}

public Action:Timer_RemoveAll(Handle:timer, any:victim)
{
	if(ValidPlayer(victim, false))
	{
		War3_SetBuff(victim,fSlow,thisRaceID,1.0);
		//War3_SetBuff(victim,fAttackSpeed,thisRaceID,1.0);
	}
}

public CooldownUltimate(client)
{
        War3_CooldownMGR(client,GetConVarFloat(ultCooldownCvar),thisRaceID,ULT_SILENCE,_,_);
}

public OnUltimateCommand(client,race,bool:pressed)
{
	new skillult = War3_GetSkillLevel(client, thisRaceID, ULT_SILENCE);
	if(ValidPlayer(client, true) && skillult > 0 && race == thisRaceID && pressed)
	{
		if(War3_SkillNotInCooldown(client,thisRaceID,ULT_SILENCE,true ))
		{
			new Float:client_location[3];
			GetClientAbsOrigin(client,client_location);
			//client_location[2]-=5;
			new Float:radius = SilenceRadius[skillult];
			new radiusint = RoundToFloor(SilenceRadius[skillult]);
			new Float:radiusSize = SilenceRadius[skillult];
			bUlt[client] = true;
			CreateTimer(SilenceDuration[skillult], Timer_UltGone, client);
			CreateTimer(0.1,Effects,client);
			
			
			
			
			while(bUlt[client] == true && Restriction[client] < GetGameTime() - 2.0)
			{
				TE_SetupBeamRingPoint(client_location, 10.0, SilenceRadius[skillult], BeamSprite, HaloSprite, 0, 20, 1.0, 10.0, 1.0, {173,90,255,225}, 40, 0);
				TE_SendToAll(1.0);
				
				TE_SetupBeamRingPoint(client_location, 10.0, SilenceRadius[skillult], BeamSprite, HaloSprite, 0, 20, 1.0, 10.0, 1.0, {173,90,255,225}, 40, 0);
				TE_SendToAll(1.5);
			
				TE_SetupBeamRingPoint(client_location, 10.0, SilenceRadius[skillult], BeamSprite, HaloSprite, 0, 20, 1.0, 10.0, 1.0, {173,90,255,225}, 40, 0);
				TE_SendToAll(SilenceDuration[skillult]-2.0);
			
			
				TE_SetupBeamRingPoint(client_location, 10.0, SilenceRadius[skillult], BeamSprite, HaloSprite, 0, 20, 1.0, 10.0, 1.0, {173,90,255,225}, 40, 0);
				TE_SendToAll(SilenceDuration[skillult]-1.0);
				
				Restriction[client]=GetGameTime();
				
			}
			
			
			new Enviorment = CreateEntityByName("env_smokestack");    
			if(Enviorment)
			{
				DispatchKeyValue( Enviorment, "SmokeMaterial", "particle/particle_smokegrenade1.vmt" );
				DispatchKeyValue( Enviorment, "RenderColor", "128 0 255" );
				//DispatchKeyValue( Enviorment, "RenderColor", "0 154 255" );
				DispatchKeyValue( Enviorment, "SpreadSpeed", "100" );
				DispatchKeyValue( Enviorment, "RenderAmt", "200" );
				DispatchKeyValueFloat( Enviorment, "JetLength", radius );
				DispatchKeyValue( Enviorment, "RenderMode", "0" );
				DispatchKeyValue( Enviorment, "Initial", "0" );
				DispatchKeyValue( Enviorment, "Speed", "44" );
				DispatchKeyValue( Enviorment, "Rate", "50" );
				DispatchKeyValueFloat( Enviorment, "BaseSpread", 33.0 );
				DispatchKeyValueFloat( Enviorment, "StartSize", 0.0 );
				DispatchKeyValueFloat( Enviorment, "EndSize", radiusSize );
				DispatchKeyValueFloat( Enviorment, "Twist", 22.0 );
				DispatchSpawn(Enviorment);
				TeleportEntity(Enviorment, client_location, NULL_VECTOR, NULL_VECTOR);
				AcceptEntityInput(Enviorment, "TurnOn");
				CreateTimer( 3.0, Timer_TurnOffEntity, Enviorment );
				CreateTimer( 3.5, Timer_RemoveEntity, Enviorment );
			}
			
			
			War3_CooldownMGR(client,GetConVarFloat(ultCooldownCvar),thisRaceID,ULT_SILENCE,_,_);
			
			for(new i = 1; i <= MaxClients; i++)
			{
				if(ValidPlayer(i, false))
				{
					if(GetClientTeam(i) != GetClientTeam(client) && !Silenced(client) && !W3HasImmunity(i, Immunity_Ultimates))
					{
						new Float:iVec[3];
						GetClientAbsOrigin(client, iVec);
						
						new Float:enemyVec[3];
						GetClientAbsOrigin(i, enemyVec);
						
						
						enemyVec[2] += 35;
						iVec[2] += 30;
							
						TE_SetupBeamPoints(iVec, enemyVec, BeamSprite, HaloSprite, 0, 15, 1.4, 5.0, 10.0, 1, 2.0, {173,90,255,255}, 20);
						TE_SendToAll(0.0);
						

						enemyVec[2] -= 35;
						iVec[2] -= 30;
						durationsilence = SilenceDuration[skillult];

						if(GetVectorDistance(iVec, enemyVec) <= radius)
						{
							CreateTimer(durationsilence, Timer_SilenceGone, i);
							War3_SetBuff(i,bSilenced,thisRaceID,true);
							PrintHintText(i,"%T","Your skills are disabled by Traxex!", i);
							//PrintToChatAll("SilenceDuration %f",SilenceDuration[skillult]);
						}
					}
				}
			}
		}
	}		
}


public Action:Timer_UltGone(Handle:timer, any:client)
{
	bUlt[client] = false;
}

public Action:Timer_SilenceGone(Handle:timer, any:client)
{
	War3_SetBuff(client,bSilenced,thisRaceID,false);
	PrintHintText(client,"Your skills enabled!",client);
}
	

public Action:Effects(Handle:t,any:client)
{
	if(bUlt[client] != false)
	{
		EmitSoundToAll("*war3source/traxex/touch.mp3",client);
		CreateTimer(1.6,Effects,client);
	}

}

public Action:Timer_TurnOffEntity( Handle:timer, any:edict )
{
	if (edict > 0 && IsValidEdict(edict))
	AcceptEntityInput( edict, "TurnOff" );
}

public Action:Timer_RemoveEntity( Handle:timer, any:edict )
{
	if (edict > 0 && IsValidEdict(edict))
	AcceptEntityInput( edict, "Kill" );
}