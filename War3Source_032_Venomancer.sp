#pragma semicolon 1    ///WE RECOMMEND THE SEMICOLON

#include <sourcemod>
#include <sdktools>
#include "W3SIncs/War3Source_Interface"

public Plugin:myinfo = 
{
    name = "War3Source - Race - Venomancer",
    author = "Aurora",
    description = "The Venomancer race for War3Source."
};

new thisRaceID;
new SKILL_PDAGGER, SKILL_PWARD, SKILL_APOISON, ULT_PNOVA;

// This skill does damage when the venomancer shoots someone. Initial damage followed by a DOT
new Float:DaggerFreq = 1.0; // How often the poison on the dagger deals damage
// Cooldown on Dagger
new Float:DaggerCooldown = 4.0;
// Dagger Initial Damage
new DaggerDamage[]={0,1,2,4,6,8,10};
new DaggerDOT[]={0,2,4,6,8,10,12};
//new VictimDaggerTicks[MAXPLAYERSCUSTOM];
//new DaggeredBy[MAXPLAYERSCUSTOM];

// Same as shamans ward
// Max Wards
new MaximumWards[]={0,2,3,4,5,6,7}; 
// Ward Damage
new WardDamage[]={0,1,2,3,5,7,10};

// Poison stacking effect, applied on hit
new Float:PoisonFreq = 1.0; // How often the poison ticks per second
new PoisonDamage[]={0,1,2,3,4,5,6}; // How much damage does a stack do?
new MaxPoisonStacks = 3; // How many stacks can the attacker dish out?
new VictimPoisonStacks[MAXPLAYERSCUSTOM]; // How many stacks does the victim have?
new VictimPoisonTicks[MAXPLAYERSCUSTOM];
new PoisonedBy[MAXPLAYERSCUSTOM]; // Who was the victim poisoned by?


new Float:PNovaFreq = 1.0;// How often the poison ticks per second
new PNovaDamage[] = {0, 15, 20, 25, 30, 35, 45}; //Total Poison Nova Damage
new VictimNovaTicks[MAXPLAYERSCUSTOM];
new PNovaBy[MAXPLAYERSCUSTOM];
new PNovaTicks = 6;
new PNovaDamagePerTick[]={0,1,3,4,6,8,10};
// The rest of the variables will be modified inside OnPluginStart() as the functions were being picky    

new Handle:ultCooldownCvar;
new ultmaxdistance[] = {0, 300, 400, 500, 600, 700, 800};
public OnPluginStart()
{
    //CreateTimer(DaggerFreq, DaggerStackTimer , _, TIMER_REPEAT);  // Stacking Dagger DoT Timer
    CreateTimer(PoisonFreq, PoisonStackTimer , _, TIMER_REPEAT);  // Stacking Poison DoT Timer
    CreateTimer(PNovaFreq, PoisonNovaTimer, _, TIMER_REPEAT); // Poison Nova Timer
    LoadTranslations("w3s.race.venom.phrases");
    ultCooldownCvar=CreateConVar("war3_venom_ult_cooldown","30","Cooldown time for ult.");
   
    // The number of ticks is determined by how often the damage ticks within the duration of the poisoning
    // For example, if you have the ult tick once every half-second (.5) and the duration is 4 seconds,
    // it would tick 8 times (4 / .5 = 8). We round this to the nearest number    
    
    // We then set the damage per tick because the timer doesn't use the duration variable, it uses
    // the tick variable
}

public OnWar3LoadRaceOrItemOrdered(num){

    if(num==210)
    {
        thisRaceID=War3_CreateNewRaceT("venom");
        SKILL_PDAGGER=War3_AddRaceSkillT(thisRaceID,"PoisonDagger",false,6);
        SKILL_PWARD=War3_AddRaceSkillT(thisRaceID,"PlagueWard",false,6);
        SKILL_APOISON=War3_AddRaceSkillT(thisRaceID,"PoisonSting",false,6);
        ULT_PNOVA=War3_AddRaceSkillT(thisRaceID,"PoisonNova",true,6);
        War3_CreateRaceEnd(thisRaceID); ///DO NOT FORGET THE END!!!
    }
}

public OnMapStart()
{
    PrecacheModel("particle/particle_smokegrenade1.vmt", true);
    AddFileToDownloadsTable("particle/particle_smokegrenade1.vmt");

    AddFileToDownloadsTable("sound/war3source/venom/dagger.mp3");
	FakePrecacheSound( "*war3source/venom/dagger.mp3" );
	AddFileToDownloadsTable("sound/war3source/venom/ult.mp3");
	FakePrecacheSound( "*war3source/venom/ult.mp3" );
	AddFileToDownloadsTable("sound/war3source/venom/ward.mp3");
	FakePrecacheSound( "*war3source/venom/ward.mp3" );
	AddFileToDownloadsTable("sound/war3source/venom/sting.mp3");
	FakePrecacheSound( "*war3source/venom/sting.mp3" );
	
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public OnWar3EventDeath(client)
{
    if (ValidPlayer(client))
    {    
		VictimPoisonStacks[client] = 0;  // Remove Poison Stacks
		VictimPoisonTicks[client] = 0;
		VictimNovaTicks[client] = 0;
	}
}

public Action:PoisonStackTimer(Handle:h,any:data)
{
    new attacker;
    new damage;
    new skill;
    for(new i = 1; i <= MaxClients; i++) // Iterate over all clients
    {
        if(ValidPlayer(i, true))
        {
            if(VictimPoisonTicks[i] > 0)
            {
                attacker = PoisonedBy[i];
                skill = War3_GetSkillLevel(attacker, thisRaceID, SKILL_APOISON);
                damage = VictimPoisonStacks[i] * PoisonDamage[skill]; // Number of stacks on the client * damage of the attacker
                    
                if(War3_GetGame() == Game_TF)
                {
                    War3_DealDamage(i, damage, attacker, _, "bleed_kill"); // Bleeding Icon
                }
                else
                {
                    if(GameCS() && GetClientHealth(i) > damage){ //cs damages slows....
                        SetEntityHealth(i, GetClientHealth(i) - damage);
                    }
                    else{
                        War3_DealDamage(i, damage,attacker, _, "PoisonSting"); // Generic skill name
                    }
                }
                PrintHintText(attacker,"%T","Poison Sting did {amount} damage", attacker, damage);
                PrintHintText(i,"%T","Poison Sting did {amount} damage to you", i, damage);
                EmitSoundToAll("*war3source/venom/sting.mp3",i);
                VictimPoisonTicks[i]--;
            }
        }
    }
}                


/*public Action:DaggerStackTimer(Handle:h,any:data)
{
    new attacker;
    new damage;
    new skill;
    for(new i = 1; i <= MaxClients; i++) // Iterate over all clients
    {
        if(ValidPlayer(i, true))
        {
            if(VictimDaggerTicks[i] > 0)
            {
                attacker = DaggeredBy[i];
                skill = War3_GetSkillLevel(attacker, thisRaceID, SKILL_PDAGGER);
                damage = DaggerDOT[skill];
                    
                if(War3_GetGame() == Game_TF)
                {
                    War3_DealDamage(i, damage, attacker, _, "bleed_kill"); // Bleeding Icon
                }
                else
                {
                    if(GameCS() && GetClientHealth(i) > damage){ //cs damages slows....
                        SetEntityHealth(i, GetClientHealth(i) - damage);
                    }
                    else{
                        War3_DealDamage(i, damage,attacker, _, "PoisonDagger"); // Generic skill name
                    }
                }
				War3_ChatMessage(attacker,"%T","Poison Dagger did {amount} damage", attacker, damage);
                War3_ChatMessage(i,"%T","Poison Dagger did {amount} damage to you", i, damage);
                VictimDaggerTicks[i]--;
            }
        }
    }
}  */      

public Action:PoisonNovaTimer(Handle:h,any:data)
{
    new attacker;
    new damage;
    new skill;
    for(new i = 1; i <= MaxClients; i++) // Iterate over all clients
    {
        if(ValidPlayer(i, true))
        {
            if(VictimNovaTicks[i] > 0)
            {
                attacker = PNovaBy[i];
                skill = War3_GetSkillLevel(attacker, thisRaceID, ULT_PNOVA);
                damage = PNovaDamagePerTick[skill];
                    
                if(War3_GetGame() == Game_TF)
                {
                    War3_DealDamage(i, damage, attacker, _, "bleed_kill"); // Bleeding Icon
                }
                else
                {
                    if(GameCS() && GetClientHealth(i) > damage){ //cs damages slows....
                        SetEntityHealth(i, GetClientHealth(i) - damage);
                    }
                    else{
                        War3_DealDamage(i, damage,attacker, _, "PoisonNova"); // Generic skill name
                    }
                }
                PrintHintText(attacker,"%T","Poison Nova did {amount} damage", attacker, damage);
                VictimNovaTicks[i]--;
            }
        }
    }
}  

public OnW3TakeDmgBullet(victim,attacker,Float:damage){
    if(ValidPlayer(victim, true) && ValidPlayer(attacker, false) && GetClientTeam(victim) != GetClientTeam(attacker))
    {
         if(War3_GetRace(attacker) == thisRaceID)
        {
		 // Apply Poison Dagger      		 
           if(War3_SkillNotInCooldown(attacker, thisRaceID, SKILL_PDAGGER, true))
            {
                new skilllvl2 = War3_GetSkillLevel(attacker, thisRaceID, SKILL_PDAGGER);
				new DaggerDOTtext = DaggerDOT[skilllvl2];
                if(skilllvl2 > 0 && !Hexed(attacker) && !W3HasImmunity(victim,Immunity_Skills) && !Silenced(attacker))
                {
                    War3_CooldownMGR(attacker, DaggerCooldown, thisRaceID, SKILL_PDAGGER, true, true);
                    
                    // Initial Damage from dagger
                    new daggerDamage = DaggerDamage[skilllvl2];
                    EmitSoundToAll("*war3source/venom/dagger.mp3",victim);
                    PrintHintText(attacker,"%T","Dagger did {amount} damage", attacker, DaggerDOTtext);
                    PrintHintText(victim,"%T","Dagger hit you for {amount} damage", victim, DaggerDOTtext);
                    if(War3_GetGame()==Game_TF)
                    {
                        War3_DealDamage(victim,DaggerDOTtext,attacker,_,"bleed_kill"); // Bleeding Icon
                    }
                    else
                    {
                        if(GameCS() && GetClientHealth(victim) > DaggerDOTtext){ //cs damages slows....
                            SetEntityHealth(victim, GetClientHealth(victim) - DaggerDOTtext);
                        }
                        else{
                            War3_DealDamage(victim, DaggerDOTtext, attacker, _, "PoisonDagger"); // Generic skill name
                        }
                    }  
                }
           }
            // Apply Poison Sting
            new skilllvl = War3_GetSkillLevel(attacker, thisRaceID, SKILL_APOISON);
            if(skilllvl > 0 && !Hexed(attacker) && !W3HasImmunity(victim, Immunity_Skills) && !Silenced(attacker) && War3_SkillNotInCooldown(attacker, thisRaceID, SKILL_APOISON, true))
            {

                if(VictimPoisonStacks[victim] < MaxPoisonStacks)
                {
                    VictimPoisonStacks[victim]++; //stack if less than max stacks
                }
                
                VictimPoisonTicks[victim] = 3 ; //always three ticks                
                PoisonedBy[victim] = attacker;
            }
            
                   
        }
    }
}

public OnAbilityCommand(client,ability,bool:pressed)
{
    if(War3_GetRace(client )== thisRaceID && ability == 0 && pressed && IsPlayerAlive(client))
    {
        new skill_level = War3_GetSkillLevel(client, thisRaceID, SKILL_PWARD);
        if(skill_level > 0)
        {
            if(!Silenced(client) && War3_GetWardCount(client) < MaximumWards[skill_level])
            {
                new iTeam=GetClientTeam(client);
                new bool:conf_found=false;
                if(War3_GetGame()==Game_TF)
                {
                    new Handle:hCheckEntities=War3_NearBuilding(client);
                    new size_arr=0;
                    if(hCheckEntities!=INVALID_HANDLE)
                        size_arr=GetArraySize(hCheckEntities);
                    for(new x=0;x<size_arr;x++)
                    {
                        new ent=GetArrayCell(hCheckEntities,x);
                        if(!IsValidEdict(ent)) continue;
                        new builder=GetEntPropEnt(ent,Prop_Send,"m_hBuilder");
                        if(builder>0 && ValidPlayer(builder) && GetClientTeam(builder)!=iTeam)
                        {
                            EmitSoundToAll("*war3source/venom/ward.mp3",client);
                            conf_found=true;
                            break;                            
                        }
                    }
                    if(size_arr>0)
                        CloseHandle(hCheckEntities);
                }
                if(conf_found)
                {
                    W3MsgWardLocationDeny(client);
                }
                else
                {
                    if(War3_IsCloaked(client))
                    {
                        W3MsgNoWardWhenInvis(client);
                        return;
                    }
                    new Float:location[3];
                    GetClientAbsOrigin(client, location);
                    War3_CreateWardMod(client, location, 60, 300.0, 0.5, "damage", SKILL_PWARD, WardDamage);
                    W3MsgCreatedWard(client,War3_GetWardCount(client),MaximumWards[skill_level]);
                    EmitSoundToAll("*war3source/venom/ward.mp3",client);
                }
            }
            else
            {
                W3MsgNoWardsLeft(client);
            }    
        }
    }
}


public OnUltimateCommand(client,race,bool:pressed)
{
    if(race==thisRaceID && pressed && ValidPlayer(client,true) &&!Silenced(client) && !Hexed(client) && War3_SkillNotInCooldown(client,thisRaceID,ULT_PNOVA,true))
    {
        new ult_level = War3_GetSkillLevel(client,race,ULT_PNOVA);
        if(ult_level > 0)        
        {
        
            new caster_team = GetClientTeam(client);
            new Float:start_pos[3];
            GetClientAbsOrigin(client,start_pos);
            new distance = ultmaxdistance[ult_level] + 1;
			new target=0;
            
            for(new x = 1; x <= MaxClients; x++)
            {
                if(ValidPlayer(x,true) && caster_team != GetClientTeam(x) && !W3HasImmunity(x,Immunity_Ultimates))
                {
                    new Float:this_pos[3];
                    GetClientAbsOrigin(x, this_pos);
                    new Float:dist_check = GetVectorDistance(start_pos, this_pos);
                    if(dist_check <= distance)
                    {
                        EmitSoundToAll("*war3source/venom/ult.mp3",client);
                        VictimNovaTicks[x] = PNovaTicks;
						PNovaBy[x]=client;				
                        PrintHintText(x,"%T","Poison Nova has affected you!", x);   
						target=x;					
                    }
                }
            }
			
			if(target<=0)
			{
				W3MsgNoTargetFound(client,distance);
				return;
			}
			else
			{
				new Float:radius = float(distance);
				new Enviorment = CreateEntityByName("env_smokestack");    
				if(Enviorment)
				{
				DispatchKeyValue( Enviorment, "SmokeMaterial", "particle/particle_smokegrenade1.vmt" );
				DispatchKeyValue( Enviorment, "RenderColor", "0 255 51" );
				//DispatchKeyValue( Enviorment, "RenderColor", "0 154 255" );
				DispatchKeyValue( Enviorment, "SpreadSpeed", "300" );
				DispatchKeyValue( Enviorment, "RenderAmt", "50" );
				DispatchKeyValueFloat( Enviorment, "JetLength", radius );
				DispatchKeyValue( Enviorment, "RenderMode", "0" );
				DispatchKeyValue( Enviorment, "Initial", "0" );
				DispatchKeyValue( Enviorment, "Speed", "44" );
				DispatchKeyValue( Enviorment, "Rate", "50" );
				DispatchKeyValueFloat( Enviorment, "BaseSpread", 33.0 );
				DispatchKeyValueFloat( Enviorment, "StartSize", 0.0 );
				DispatchKeyValueFloat( Enviorment, "EndSize", radius );
				DispatchKeyValueFloat( Enviorment, "Twist", 22.0 );
				DispatchSpawn(Enviorment);
				TeleportEntity(Enviorment, start_pos, NULL_VECTOR, NULL_VECTOR);
				AcceptEntityInput(Enviorment, "TurnOn");
				CreateTimer( 3.0, Timer_TurnOffEntity, Enviorment );
				CreateTimer( 3.5, Timer_RemoveEntity, Enviorment );
				}
			}

            War3_CooldownMGR(client,GetConVarFloat(ultCooldownCvar),thisRaceID,ULT_PNOVA,true,true);
            
        }
        else
        {
            W3MsgUltNotLeveled(client);
        }
    }
}

public Action:Timer_TurnOffEntity( Handle:timer, any:edict )
{
	if (edict > 0 && IsValidEdict(edict))
	AcceptEntityInput( edict, "TurnOff" );
}

public Action:Timer_RemoveEntity( Handle:timer, any:edict )
{
	if (edict > 0 && IsValidEdict(edict))
	AcceptEntityInput( edict, "Kill" );
}
