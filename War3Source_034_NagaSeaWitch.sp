 /**
* File: War3Source_NagaSeaWitch.sp
* Description: The Naga Sea Witch unit for War3Source.
* Author(s): [Oddity]TeacherCreature
*/

#pragma semicolon 1

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdkhooks>
#include <sdktools>
#include <sdktools_functions>
#include <sdktools_tempents>
#include <sdktools_tempents_stocks>

new thisRaceID;

//new Handle:ultCooldownCvar;

//skill 1
new bool:bForked[66];
new ForkedDamage[7]={0, 10, 15, 18, 20, 22, 25};

//skill 2
new Float:FrostArrow[7]={0.00,0.85,0.80,0.78,0.75,0.70,0.65};

//skill 3
new BeamSprite,HaloSprite;
new ShieldSprite;
new MoneyOffsetCS;
new String:lightningSound[]="*war3source/manashield/thunder.mp3";
new String:Cast[]="*war3source/manashield/cast.mp3";
new String:Imp1[]="*war3source/manashield/impact_1.mp3";
new String:Imp2[]="*war3source/manashield/impact_2.mp3";
new String:Imp3[]="*war3source/manashield/impact_3.mp3";
new String:State[]="*war3source/manashield/state.mp3";
new String:Exp[]="*war3source/manashield/state_expire.mp3";
new String:Tornado[]="*war3source/manashield/tornado.mp3";
new MSmultiplier[6]={0,124,112,100,88,50};
new MSEVadePerc[6]={0,0.40,0.45,0.55,0.58,0.60};
new ManaShield[66];
new maxshield=120;
new ManaShieldbyLevel[7]={0,5,7,9,11,12,13};
new Float:MSreducer[7]={0.0,0.90,0.70,0.50,0.30,0.20,0.10};

//skill 4
new m_vecBaseVelocity; //offsets
new TornadoSprite;
new Float:Cooldown[7]={0.0, 32.0, 30.0, 28.0, 26.0, 24.0, 22.0};

new SKILL_FORKED, SKILL_FROSTARROW, SKILL_MANASHIELD, ULT_TORNADO;

public Plugin:myinfo = 
{
	name = "War3Source Race - Naga Sea Witch",
	author = "[Oddity]TeacherCreature",
	description = "The Naga Sea Witch race for War3Source.",
	version = "1.0.6.3",
	url = "warcraft-source.net"
}

public OnPluginStart()
{
	CreateTimer(6.0,mana,_,TIMER_REPEAT);
	HookEvent("round_start",RoundStartEvent);
	m_vecBaseVelocity = FindSendPropOffs("CBasePlayer","m_vecBaseVelocity");
	MoneyOffsetCS=FindSendPropInfo("CCSPlayer","m_iAccount");
	//ultCooldownCvar=CreateConVar("war3_naga_tornado_cooldown","30.0","Cooldown for Tornado");
	LoadTranslations("w3s.race.naga.phrases.txt");
	HookEvent("player_spawn",PlayerSpawnEvent);
	RegConsoleCmd("naga",CheckShield);
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==220)
	{
		thisRaceID=War3_CreateNewRaceT("naga");
		SKILL_FORKED=War3_AddRaceSkillT(thisRaceID,"ForkedLight",false,6);
		SKILL_FROSTARROW=War3_AddRaceSkillT(thisRaceID,"FrostArrows",false,6);
		SKILL_MANASHIELD=War3_AddRaceSkillT(thisRaceID,"ManaShield",false,6);
		ULT_TORNADO=War3_AddRaceSkillT(thisRaceID,"Tornado",true,6); 
		War3_CreateRaceEnd(thisRaceID);
	}
}

public Action:CheckShield(client, args)
{
	if(War3_GetRace(client)==thisRaceID)
	{
		PrintToChat(client, "%d", ManaShield[client]);
	}
}

/*public OnEntityCreated(entity, const String:classname[])
{
	if(!IsValidEntity(entity))
		return;
	
	if(StrContains(classname, "weapon_", false) || StrContains(classname, "item_", false))
	{
		SDKHook(entity, SDKHook_SpawnPost, Spawn); 
	}
}

public Action:Spawn(entity, classname)
{
	
	new client =   GetEntPropEnt(entity, Prop_Data, "m_hOwner");
	PrintToServer( "client = %d", client );
	if(War3_GetRace(client)==thisRaceID)
	{
		//AcceptEntityInput(entity, "Kill");
		//RemoveEdict(entity);
		//AcceptEntityInput( entity, "stop" );  
		//AcceptEntityInput(entity, "Kill");
		AcceptEntityInput(entity, "Kill"); 
		PrintToServer( "kill" );
		return Plugin_Handled;
	}
	return Plugin_Continue;
}  
public OnClientPutInServer(client)
{
    SDKHook(client,SDKHook_WeaponDrop,Event_WeaponDrop);
}

public Action:Event_WeaponDrop(client,weapon) 
{
	if (IsValidEdict(weapon)) 
	{
		if(War3_GetRace(client)==thisRaceID)
		{
			AcceptEntityInput( weapon, "Kill" );
		}
	}
	
	return Plugin_Continue; 
}

public Action:CS_OnCSWeaponDrop(owner, weaponIndex) 
{ 
		if(IsClientInGame(owner) && IsPlayerAlive(owner))
		{
			if(War3_GetRace(owner)==thisRaceID)
			{
				decl String:sWeapon[64]; 
				GetEntityClassname(weaponIndex, sWeapon, sizeof(sWeapon)); 
				if(StrEqual(sWeapon[7], "scout"))
				PrintToChat(owner, "Выбрасывать запрещено"); 
				return Plugin_Handled; 
			}
		}
	return Plugin_Continue; 
}*/

/*public Action:CS_OnBuyCommand(client, const String:chWeapon[])
{
    if(War3_GetRace(client) == thisRaceID) 
	{
        if(!(StrEqual(chWeapon, "ssg08") || StrEqual(chWeapon, "nova") || StrEqual(chWeapon, "glock") || StrEqual(chWeapon, "p250") || StrEqual(chWeapon, "fiveseven") || StrEqual(chWeapon, "deagle") || StrEqual(chWeapon, "elite") || StrEqual(chWeapon, "hkp2000") || StrEqual(chWeapon, "tec9") || StrEqual(chWeapon, "hegrenade") || StrEqual(chWeapon, "flashbang") || StrEqual(chWeapon, "molotov") || StrEqual(chWeapon, "decoy")))
		{
		return Plugin_Handled; // Block the buy.
		}
    }
    
    return Plugin_Continue; // Continue as normal.
}  

public OnRaceChanged(client,oldrace,newrace)
{
	if(newrace == thisRaceID)
	{
		War3_WeaponRestrictTo(client,thisRaceID,"weapon_knife,weapon_ssg08,weapon_nova,weapon_glock,weapon_p250,weapon_fiveseven,weapon_deagle,weapon_elite,weapon_hkp2000,weapon_tec9,weapon_hegrenade,weapon_flashbang,weapon_molotov,weapon_decoy");
	}
	if(newrace!=thisRaceID)
	{
		War3_WeaponRestrictTo(client,thisRaceID,"");
	}
}*/

public OnMapStart()
{
	ShieldSprite=PrecacheModel("materials/sprites/strider_blackball.vmt");
	TornadoSprite=PrecacheModel("materials/sprites/lgtning.vmt");
	AddFileToDownloadsTable("materials/sprites/strider_blackball.vmt");
	AddFileToDownloadsTable("materials/sprites/strider_blackball.vtf");
	AddFileToDownloadsTable("materials/sprites/lgtning.vmt");
	AddFileToDownloadsTable("materials/sprites/lgtning.vtf");
	AddFileToDownloadsTable("sound/war3source/manashield/cast.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/impact_1.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/impact_2.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/impact_3.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/state.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/state_expire.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/tornado.mp3");
	AddFileToDownloadsTable("sound/war3source/manashield/thunder.mp3");
	FakePrecacheSound("*war3source/manashield/thunder.mp3");
	FakePrecacheSound("*war3source/manashield/cast.mp3");
	FakePrecacheSound("*war3source/manashield/impact_1.mp3");
	FakePrecacheSound("*war3source/manashield/impact_2.mp3");
	FakePrecacheSound("*war3source/manashield/impact_3.mp3");
	FakePrecacheSound("*war3source/manashield/state.mp3");
	FakePrecacheSound("*war3source/manashield/state_expire.mp3");
	FakePrecacheSound("*war3source/manashield/tornado.mp3");
	
	BeamSprite=War3_PrecacheBeamSprite(); 
    HaloSprite=War3_PrecacheHaloSprite(); 
	
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public PlayerSpawnEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
    new userid=GetEventInt(event,"userid");
    new client=GetClientOfUserId(userid);
    if(client>0)
    {
        if(War3_GetRace(client)==thisRaceID)
        {
			new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_MANASHIELD);
			if(skill_level>0)
			{
				ManaShield[client] = maxshield;
			}
        }
    }
    
}

/*public OnWar3EventDeath(victim,attacker)
{
	new race=War3_GetRace(victim);
	if(race==thisRaceID)
	{
		SetMoney(victim,0);
	}
}

public OnWar3EventSpawn(client)
{
	new race=War3_GetRace(client);
	if(race==thisRaceID)
	{
		new String:sWeaponName[64];
		GetClientWeapon(client, sWeaponName, sizeof(sWeaponName));
		if (StrEqual(sWeaponName, "weapon_ssg08", false) || StrEqual(sWeaponName, "weapon_nova", false)) 
		{ 
		}
		else
		{
			GivePlayerItem(client, "weapon_ssg08");
		}
	}
}*/

public RoundStartEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i)&&War3_GetRace(i)==thisRaceID)
		{
			new skill=War3_GetSkillLevel(i,thisRaceID,SKILL_MANASHIELD);
			if(skill>0)
			{
				//bMShield[i]=false;
				W3ResetPlayerColor(i, thisRaceID);
			}
		}
	}
}

public OnWar3EventPostHurt(victim, attacker, Float:damage, const String:weapon[32], bool:isWarcraft)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new race_attacker=War3_GetRace(attacker);
			new skill_level=War3_GetSkillLevel(attacker,thisRaceID,SKILL_FROSTARROW);
			// Frost Arrow
			if(race_attacker==thisRaceID && skill_level>0 && !Silenced(attacker))
			{
				if(!Silenced(attacker)&&War3_SkillNotInCooldown(attacker,thisRaceID,SKILL_FROSTARROW) && !W3HasImmunity(victim,Immunity_Skills))
				{					
					/*if (strcmp(weapon, "weapon_ssg08") == 0)
					{
						War3_CooldownMGR(attacker,1.0,thisRaceID,SKILL_FROSTARROW,_,_);
					}
					else if (strcmp(weapon, "weapon_nova") == 0)
					{
						War3_CooldownMGR(attacker,3.0,thisRaceID,SKILL_FROSTARROW,_,_);
					}
					else
					{*/
						War3_CooldownMGR(attacker,5.0,thisRaceID,SKILL_FROSTARROW,_,_);
					//}
					War3_SetBuff(victim,fSlow,thisRaceID,FrostArrow[skill_level]);
					War3_SetBuff(victim,fAttackSpeed,thisRaceID,FrostArrow[skill_level]);
					W3FlashScreen(victim,RGBA_COLOR_RED);
					CreateTimer(0.5,unfrost,victim);
					PrintHintText(attacker,"%T","Frost Arrow!",attacker);
					PrintHintText(victim,"%T","You have been hit by a Frost Arrow",victim);
				}
			}
		}
	}
}

public OnW3TakeDmgBulletPre(victim, attacker, Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new race_victim=War3_GetRace(victim);
			if(race_victim==thisRaceID && !W3HasImmunity(attacker, Immunity_Skills))
			{
				new rnd = GetRandomInt(1,3);
				if(rnd==1)
				{
					//EmitSoundToAll(Imp1,attacker);
					EmitSoundToAll(Imp1,victim);
				}
				if(rnd==2)
				{
					//EmitSoundToAll(Imp2,attacker);
					EmitSoundToAll(Imp2,victim);
				}
				if(rnd==3)
				{
					//EmitSoundToAll(Imp3,attacker);
					EmitSoundToAll(Imp3,victim);
				}				
				new Float:pos[3];
				GetClientAbsOrigin(victim,pos);
				pos[2]+=35;
				TE_SetupGlowSprite(pos, ShieldSprite, 0.1, 1.0, 130);
				TE_SendToAll(); 
				new skill_mana=War3_GetSkillLevel(victim,thisRaceID,SKILL_MANASHIELD);
				new ddamage = RoundToFloor(damage);
				if(ManaShield[victim] > ddamage)
				{
					War3_DamageModPercent(MSEVadePerc[skill_mana]);
					ManaShield[victim] =  RoundToFloor(ManaShield[victim] - ddamage*0.8);
				}
				else if(ManaShield[victim] > 0)
				{
					ManaShield[victim] = 0;
					War3_DamageModPercent(MSEVadePerc[skill_mana]);
					StopSound(victim, SNDCHAN_AUTO,State);
					EmitSoundToAll(Exp,victim);
					PrintHintText(victim,"%T","Mana Shield: Depleted!",victim);
				}
				/*new money=GetMoney(victim);
				new ddamage=RoundFloat(damage*MSmultiplier[skill_mana]);
				if(money>=ddamage)
				{
					War3_DamageModPercent(0.0);
					new new_money;
					new_money=money-ddamage;
					SetMoney(victim,new_money);
				}
				else
				{
					StopSound(victim, SNDCHAN_AUTO,State);
					EmitSoundToAll(Exp,victim);
					War3_DamageModPercent(MSreducer[skill_mana]);
					bMShield[victim]=false;
					PrintHintText(victim,"%T","Mana Shield: Depleted!",victim);
				}*/
			}
		}
	}
}

/*stock GetMoney(player)
{
	return GetEntData(player,MoneyOffsetCS);
}

stock SetMoney(player,money)
{
	SetEntData(player,MoneyOffsetCS,money);
}*/

public Action:unfrost(Handle:timer,any:client)
{
	War3_SetBuff(client,fSlow,thisRaceID,1.0);
	War3_SetBuff(client,fAttackSpeed,thisRaceID,1.0);
}

public bool:TargetCheck(client)
{
	if(bForked[client]||W3HasImmunity(client,Immunity_Skills))
	{
		return false;
	}
	return true;
}

public OnAbilityCommand(client,ability,bool:pressed)
{
	if(!Silenced(client))
	{
		if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client))
		{
			new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_FORKED);
			if(skill_level>0)
			{
				if(!Silenced(client)&&War3_SkillNotInCooldown(client,thisRaceID,SKILL_FORKED,true))
				{
					new targets;
					new targetlist[3];
					for(new i=0;i<3;i++){
						new target = War3_GetTargetInViewCone(client,800.0,false,23.0,TargetCheck);
						new skill=War3_GetSkillLevel(client,thisRaceID,SKILL_FORKED);
						if(target>0&&!W3HasImmunity(target,Immunity_Skills))
						{
							bForked[target]=true;
							new Float:start_pos[3];
							GetClientAbsOrigin(client,start_pos);
							War3_DealDamage(target,ForkedDamage[skill],client,DMG_ENERGYBEAM,"nagalightning");
							PrintHintText(target,"Hit by Forked Lightning",target,War3_GetWar3DamageDealt());
							start_pos[2]+=30.0; // offset for effect
							new Float:target_pos[3];
							GetClientAbsOrigin(target,target_pos);
							target_pos[2]+=30.0;
							//TE_SetupBeamPoints(start_pos,target_pos,TornadoSprite,TornadoSprite,0,35,2.0,40.0,40.0,0,40.0,{255,255,255,255},40);
							TE_SetupBeamPoints(start_pos,target_pos,BeamSprite,HaloSprite,0,35,2.0,25.0,25.0,0,10.0,{255,255,255,255},40);
							TE_SendToAll();
							EmitSoundToAll( lightningSound , target,_,SNDLEVEL_TRAIN);
							EmitSoundToAll( lightningSound , client,_,SNDLEVEL_TRAIN);
							War3_CooldownMGR(client,11.0,thisRaceID,SKILL_FORKED,_,_);
							targetlist[targets]=target;
							targets++;
						}
					}
					if(targets==0){
						PrintHintText(client,"%T","No valid targets",client);
					}
					for(new i=0;i<3;i++){
						bForked[targetlist[i]]=false;
					}
				}
			}
			else
			{
				PrintHintText(client,"%T","Level Forked Lightning First",client);
			}
		}
	}
	else
	{
		PrintHintText(client,"%T","Silenced: Can not cast",client);
	}
}

public Action:shieldsoundloop(Handle:timer,any:client)
{
	EmitSoundToAll(State,client,SNDCHAN_AUTO);
}

public Action:mana(Handle:timer,any:client)
{
	new skill_level,newshield;
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i,true))
		{
			if(War3_GetRace(i)==thisRaceID)
			{
				skill_level=War3_GetSkillLevel(i,thisRaceID,SKILL_MANASHIELD);
				if(skill_level>0)
				{
					newshield=ManaShield[i]+ManaShieldbyLevel[skill_level];
					if(newshield<maxshield)
					{
						ManaShield[i] = newshield;
					}
					else
					{
						ManaShield[i] = maxshield;
					}
				}
			}
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && ValidPlayer(client,true))
	{
		new ult_level=War3_GetSkillLevel(client,race,ULT_TORNADO);
		if(ult_level>0)		
		{
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT_TORNADO,true)) 
			{
				new Float:pos[3];
				new Float:lookpos[3];
				War3_GetAimEndPoint(client,lookpos);
				GetClientAbsOrigin(client,pos);
				pos[1]+=60.0;
				pos[2]+=60.0;
				TE_SetupBeamPoints(pos, lookpos, TornadoSprite,TornadoSprite, 0, 5, 2.0,15.0,19.0, 2, 10.0, {54,66,120,100}, 60); 
				TE_SendToAll();
				pos[1]-=120.0;
				TE_SetupBeamPoints(pos, lookpos, TornadoSprite,TornadoSprite, 0, 5, 2.0,15.0,19.0, 2, 10.0, {54,66,120,100}, 60);
				TE_SendToAll();
				new target = War3_GetTargetInViewCone(client,400.0,false,300.0);
				if(target>0&&!W3HasImmunity(target,Immunity_Ultimates))
				{
						if(!Silenced(client))
						{
							new Float:targpos[3];
							GetClientAbsOrigin(target,targpos);
							TE_SetupBeamRingPoint(targpos, 20.0, 80.0,TornadoSprite,TornadoSprite, 0, 5, 2.6, 20.0, 0.0, {54,66,120,100}, 10,FBEAM_HALOBEAM);
							TE_SendToAll();
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 40.0, 100.0,TornadoSprite,TornadoSprite, 0, 5, 2.4, 20.0, 0.0, {54,66,120,100}, 10,FBEAM_HALOBEAM);
							TE_SendToAll();
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 60.0, 120.0,TornadoSprite,TornadoSprite, 0, 5, 2.2, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 80.0, 140.0,TornadoSprite,TornadoSprite, 0, 5, 2.0, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();	
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 100.0, 160.0,TornadoSprite,TornadoSprite, 0, 5, 1.8, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();	
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 120.0, 180.0,TornadoSprite,TornadoSprite, 0, 5, 1.6, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();	
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 140.0, 200.0,TornadoSprite,TornadoSprite, 0, 5, 1.4, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();	
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 160.0, 220.0,TornadoSprite,TornadoSprite, 0, 5, 1.2, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();	
							targpos[2]+=20.0;
							TE_SetupBeamRingPoint(targpos, 180.0, 240.0,TornadoSprite,TornadoSprite, 0, 5, 1.0, 20.0, 0.0, {54,66,120,100}, 10, FBEAM_HALOBEAM);
							TE_SendToAll();
							//EmitSoundToAll(Tornado,client);
							new Float:vec[3];
							GetClientAbsOrigin(target,vec);
							EmitSoundToAll(Tornado,_,_,_,_,_,_,_,vec,_,_,_);
							new Float:velocity[3];
							velocity[2]+=500.0;
							SetEntDataVector(target,m_vecBaseVelocity,velocity,true);
							CreateTimer(0.1,nado1,target);
							CreateTimer(0.4,nado2,target);
							CreateTimer(0.9,nado3,target);
							CreateTimer(1.4,nado4,target);
							War3_DealDamage(target,40,client,DMG_GENERIC,"tornado");
							//War3_CooldownMGR(client,GetConVarFloat(ultCooldownCvar),thisRaceID,ULT_TORNADO,_,_,_,"Tornado");
							War3_CooldownMGR(client,Cooldown[ult_level],thisRaceID,ULT_TORNADO,_,_);
						}
						else
						{
							PrintHintText(client,"%T","Silenced: Can not cast",client);
						}
				}
				else
				{
					PrintHintText(client,"%T","No valid targets",client);
				}
			}
		}
		else
		{
			PrintHintText(client,"%T","Level Tornado First",client);
		}
	}
}

public Action:nado1(Handle:timer,any:client)
{
	new Float:velocity[3];
	velocity[2]+=4.0;
	velocity[0]-=600.0;
	SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
}
public Action:nado2(Handle:timer,any:client)
{
	new Float:velocity[3];
	velocity[2]+=4.0;
	velocity[1]-=600.0;
	SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
}

public Action:nado3(Handle:timer,any:client)
{
	new Float:velocity[3];
	velocity[2]+=4.0;
	velocity[0]+=600.0;
	SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
}

public Action:nado4(Handle:timer,any:client)
{
	new Float:velocity[3];
	velocity[2]+=4.0;
	velocity[1]+=600.0;
	SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
}
