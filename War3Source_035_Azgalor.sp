//Azgalor - Pit Lord
//From War3Source - http://www.dotastrategy.com/hero-91-AzgalorPitLord.html
//THANKS TO:
//Ownz & Pimpin - War3Source
#include <sourcemod>
#include <sdktools_functions>
#include <sdktools_sound>
#include "W3SIncs/War3Source_Interface"
new DamageStorm[7]={10,15,18,22,24,26,29};
new MaximumDamage[7]={15,18,22,24,26,28,30};
new Float:Chance[7]={0.05,0.06,0.07,0.09,0.11,0.13,0.15};
new Float:RadiusStorm[7]={0.00,0.20,0.22,0.24,0.26,0.28,0.30};
new Float:PitSlow[7]={0.00,0.80,0.77,0.75,0.72,0.7,0.65};
new Float:PitMaxDistance[7]={0.00,350.0,400.0,450.0,500.0,600.0,800.0};
new Float:ult_delay[7]={0.0,6.5,6.0,5.5,4.0,3.0,1.95};
new DamageFire1[7]={30,31,32,34,36,38,40};
new DamageFire2[7]={32,35,37,40,42,43,45};
new addhp[7]={0,10,15,19,21,23,25};
//new bool:bIsTarget[MAXPLAYERS];
//WARD(Fire) BY OWNZ & PIMPINJUICE
#define MAXWARDS 64*4
#define WARDRADIUS 80
#define WARDBELOW -2.0
#define WARDABOVE 160.0
#define WARDDAMAGE 6
//new CurrentWardCount[MAXPLAYERS];
//new WardStartingArr[]={0,1,1,1,2}; 
new Float:WardLocation[MAXWARDS][3]; 
//new PitOwner[MAXWARDS];
new FlameOwner[MAXWARDS];
new Float:LastThunderClap[MAXPLAYERS];
new CurrentFlameCount[MAXPLAYERS];
new Handle:ultCooldownCvar_SPAWN;
new Handle:ultCooldownCvar;
new Handle:SFXCvar;
new thisRaceID, SKILL_FIRE, SKILL_PIT, SKILL_IGNITE, ULT_RIFT;
new String:burnsnd[]="*weapons/hegrenade/explode5.mp3";
new String:catchsnd[]="*war3source/azgalor/fire.mp3";
new String:pitsnd[]="*war3source/azgalor/teleport_postblast_thunder1.mp3";
new String:riftsnd[]="*war3source/azgalor/irifle_fire2.mp3";
new String:ignitesnd[]="*war3source/azgalor/gascan_ignite1.mp3";

//temp ents
new BeamSprite, HaloSprite, FireSprite, Explosion, HydraSprite, SimpleFire, Particle, Ventilator;

public Plugin:myinfo = 
{
	name = "War3Source Race - Pit Lord[DotA]",
	author = "Revan",
	description = "One of the many regents of Lord Archimonde - DotA",
	version = "1.0.0.5",
	url = "www.wcs-lagerhaus.de"
};

public OnPluginStart()
{
	HookEvent("round_end",RoundEvent);
	//CreateTimer(0.14,DeadlyPit,_,TIMER_REPEAT);
	CreateTimer(0.25,Flame,_,TIMER_REPEAT);
	ultCooldownCvar_SPAWN=CreateConVar("war3_azgalor_ult_cooldown_spawn","30","(Azgalor)Pit Lord's Ultimate Cooldown on spawn.");
	ultCooldownCvar=CreateConVar("war3_azgalor_ult_cooldown","25","(Azgalor)Pit Lord's Ultimate Cooldown.");
	SFXCvar=CreateConVar("war3_azgalor_hugefx_enable","1","Enable/Disable distracting and revealing sfx");
	LoadTranslations("w3s.race.azgalor.phrases.txt");
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==230)
	{
		thisRaceID = War3_CreateNewRaceT( "azgalor" );
		SKILL_FIRE = War3_AddRaceSkillT( thisRaceID, "FireStorm", false, 6 );	
		SKILL_PIT = War3_AddRaceSkillT( thisRaceID, "PitofMalice", false, 6 );	
		SKILL_IGNITE = War3_AddRaceSkillT( thisRaceID, "Expulsion", false, 6 );
		ULT_RIFT = War3_AddRaceSkillT( thisRaceID, "DarkRift", true, 6 );
		W3SkillCooldownOnSpawn( thisRaceID, ULT_RIFT, GetConVarFloat(ultCooldownCvar_SPAWN), _);
		War3_CreateRaceEnd( thisRaceID );
	}
}

public OnAutoConfigsBuffered() {
	//AddFileToDownloadsTable("weapons/hegrenade/explode5.mp3");
	AddFileToDownloadsTable("sound/war3source/azgalor/fire.mp3");
	AddFileToDownloadsTable("sound/war3source/azgalor/teleport_postblast_thunder1.mp3");
	AddFileToDownloadsTable("sound/war3source/azgalor/irifle_fire2.mp3");
	AddFileToDownloadsTable("sound/war3source/azgalor/gascan_ignite1.mp3");
	BeamSprite=War3_PrecacheBeamSprite();
	HaloSprite=War3_PrecacheHaloSprite();
	FireSprite=PrecacheModel("materials/sprites/glow07.vmt");
	SimpleFire=PrecacheModel("materials/sprites/flatflame.vmt");
	//Explosion=PrecacheModel("materials/sprites/floorfire4_.vmt");
	HydraSprite=PrecacheModel("materials/sprites/hydragutbeam.vmt");
	Particle=PrecacheModel("materials/particle/fire.vmt");
	Ventilator=PrecacheModel("materials/decals/decalmetalvent006a.vmt");
	AddFileToDownloadsTable("materials/sprites/glow07.vmt");
	AddFileToDownloadsTable("materials/sprites/flatflame.vmt");
	//AddFileToDownloadsTable("materials/sprites/floorfire4_.vmt");
	AddFileToDownloadsTable("materials/sprites/hydragutbeam.vmt");
	AddFileToDownloadsTable("materials/particle/fire.vmt");
	AddFileToDownloadsTable("materials/sprites/scanner.vmt");
	AddFileToDownloadsTable("materials/decals/decalmetalvent006a.vmt");
	AddFileToDownloadsTable("materials/sprites/glow07.vtf");
	AddFileToDownloadsTable("materials/sprites/flatflame.vtf");
	//AddFileToDownloadsTable("materials/sprites/floorfire4_.vtf");
	AddFileToDownloadsTable("materials/sprites/hydragutbeam.vtf");
	AddFileToDownloadsTable("materials/particle/fire.vtf");
	AddFileToDownloadsTable("materials/sprites/scanner.vtf");
	AddFileToDownloadsTable("materials/decals/decalmetalvent006a.vtf");
	FakePrecacheSound(catchsnd);
	FakePrecacheSound(burnsnd);
	FakePrecacheSound(ignitesnd);
	FakePrecacheSound(riftsnd);
	FakePrecacheSound(pitsnd);
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public CreateFlame(client,target)
{
	for(new i=0;i<MAXWARDS;i++)
	{
		if(FlameOwner[i]==0)
		{
			FlameOwner[i]=client;
			GetClientAbsOrigin(target,WardLocation[i]);
			break;
		}
	}
}

public RemoveFlames(client)
{
	for(new i=0;i<MAXWARDS;i++)
	{
		if(FlameOwner[i]==client)
		{
			FlameOwner[i]=0;
		}
	}
	CurrentFlameCount[client]=0;
}

public Action:Flame(Handle:timer,any:userid)
{
	new client;
	for(new i=0;i<MAXWARDS;i++)
	{
		if(FlameOwner[i]!=0)
		{
			client=FlameOwner[i];
			if(!ValidPlayer(client,true))
			{
				FlameOwner[i]=0;
				--CurrentFlameCount[client];
			}
			else
			{
				FlameLoop(client,i);
			}
		}
	}
}

public FlameLoop(owner,wardindex)
{
	new ownerteam=GetClientTeam(owner);
	new Float:start_pos[3];
	new Float:end_pos[3];
	new Float:tempVec1[]={0.0,0.0,WARDBELOW};
	new Float:tempVec2[]={0.0,0.0,WARDABOVE};
	AddVectors(WardLocation[wardindex],tempVec1,start_pos);
	AddVectors(WardLocation[wardindex],tempVec2,end_pos);
	//TE_SetupGlowSprite(start_pos,SimpleFire,0.26,1.00,212);
	//TE_SendToAll();
	new Float:BeamXY[3];
	for(new x=0;x<3;x++) BeamXY[x]=start_pos[x];
	new Float:BeamZ= BeamXY[2];
	BeamXY[2]=0.0;
	
	
	new Float:VictimPos[3];
	new Float:tempZ;
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i,true)&& GetClientTeam(i)!=ownerteam )
		{
			GetClientAbsOrigin(i,VictimPos);
			tempZ=VictimPos[2];
			VictimPos[2]=0.0;
			      
			if(GetVectorDistance(BeamXY,VictimPos) < WARDRADIUS)
			{
				if(tempZ>BeamZ+WARDBELOW && tempZ < BeamZ+WARDABOVE)
				{
					if(W3HasImmunity(i,Immunity_Skills))
					{
						W3MsgSkillBlocked(i,_,"Expulsion");
					}
					else
					{
						W3FlashScreen(i,{0,0,0,255});
						if(War3_DealDamage(i,WARDDAMAGE,owner,DMG_BULLET,"flame",_,W3DMGTYPE_MAGIC))
						{
							if(LastThunderClap[i]<GetGameTime()-2){
								EmitSoundToAll(ignitesnd,i);
								LastThunderClap[i]=GetGameTime();
							}
						}
					}
				}
			}
		}
	}
}

public Action:StopSlow( Handle:timer, any:client )
{
	War3_SetBuff(client,fSlow,thisRaceID,1.0);
	new Float:startpos[3];
	GetClientAbsOrigin(client,startpos);
	TE_SetupBeamRingPoint(startpos,120.1,20.0,BeamSprite,BeamSprite,0,15,1.20,27.0,12.0,{255,120,120,255},0,0);
	TE_SendToAll();
}

public OnRaceChanged(client,oldrace,newrace)
{
	if(newrace!=thisRaceID)
	{
		RemoveFlames(client);
	}
}

public RoundEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	for(new x=1;x<=64;x++)
	{
		new race = War3_GetRace(x);
		if (race == thisRaceID)
		{
			RemoveFlames(x);
			//bIsTarget[x]=true;
		}
		/*else
		{
			//bIsTarget[x]=false;
		}*/
	}
}

public OnWar3EventSpawn(client)
{
	new user_race = War3_GetRace(client);
	if (user_race == thisRaceID)
	{ 
		RemoveFlames(client);
		new Float:iVec[3];
		GetClientAbsOrigin(client, Float:iVec);
		new Float:iVec2[3];
		GetClientAbsOrigin(client, Float:iVec2);
		iVec[2]+=100;
		iVec2[2]+=100;
		TE_SetupBeamRingPoint(iVec,20.0,75.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(0.3);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,75.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(0.6);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,75.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(0.9);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,75.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(1.2);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,75.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(1.5);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,75.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(1.8);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,120.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(2.1);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,120.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(2.1);
		iVec[2]-=10;
		TE_SetupBeamRingPoint(iVec,20.0,120.0,HaloSprite,HaloSprite,0,15,0.4,15.0,2.0,{255,120,120,255},0,0);
		TE_SendToAll(2.4);
		TE_SetupGlowSprite(iVec,SimpleFire,3.0,1.00,212);
		TE_SendToAll();
		//TE_SetupDynamicLight(iVec,255,28,28,10,30.0,2.2,2.2);
		//TE_SendToAll();
		TE_SetupDynamicLight(iVec,255,0,0,12,80.0,2.8,1.0);
		TE_SendToAll(2.4);
	}
}

public OnW3TakeDmgBullet(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new String:sWeaponName[64];
			GetClientWeapon(attacker, sWeaponName, sizeof(sWeaponName));
			if (StrEqual(sWeaponName, "weapon_xm1014", false) || StrEqual(sWeaponName, "weapon_nova", false) || StrEqual(sWeaponName, "weapon_sawedoff", false) || StrEqual(sWeaponName, "weapon_mag7", false)) 
			{
				return Plugin_Continue;
			}
			new race_attacker=War3_GetRace(attacker);
			new skill_level=War3_GetSkillLevel(attacker,thisRaceID,SKILL_FIRE);
			if(race_attacker==thisRaceID && skill_level>0 )
			{
				if(GetRandomFloat(0.0,1.0)<=Chance[skill_level] && !W3HasImmunity(victim,Immunity_Skills))
				{
					new Float:spos[3];
					new Float:epos[3];
					GetClientAbsOrigin(victim,epos);
					GetClientAbsOrigin(attacker,spos);
					epos[2]+=35;
					spos[2]+=100;
					/*if(GetConVarBool(SFXCvar))
					{
						TE_SetupBeamPoints(spos, epos, BeamSprite, BeamSprite, 0, 35, 1.0, 10.0, 10.0, 0, 10.0, {255,25,25,255}, 30);
						TE_SendToAll();
					}*/
					new damage1=DamageStorm[skill_level];
					new damage2=MaximumDamage[skill_level];
					new Float:radius=RadiusStorm[skill_level];
					DoFire(attacker,victim,radius,damage1,damage2,true);
					
					W3FlashScreen(victim,RGBA_COLOR_RED);
				
					//bIsTarget[victim]=true;
					//CreateTimer( 0.10, Timer_DeSelect, victim );

					EmitSoundToAll(burnsnd,victim);
					//PrintHintText(attacker,"Fire Storm");
				}
			}
		}
	}
	
	return Plugin_Continue;
}

//extra wagante effekte
stock TE_SetupDynamicLight(const Float:vecOrigin[3], r,g,b,iExponent,Float:fRadius,Float:fTime,Float:fDecay)
{
    TE_Start("Dynamic Light");
    TE_WriteVector("m_vecOrigin",vecOrigin);
    TE_WriteNum("r",r);
    TE_WriteNum("g",g);
    TE_WriteNum("b",b);
    TE_WriteNum("exponent",iExponent);
    TE_WriteFloat("m_fRadius",fRadius);
    TE_WriteFloat("m_fTime",fTime);
    TE_WriteFloat("m_fDecay",fDecay);
}

//DoFire(angreifer, getroffener, radius, schaden
public DoFire(attacker,victim,Float:radius,damage,maxdmg,bool:showmsg)
{
	if(ValidPlayer(victim,true)&&ValidPlayer(attacker,true)){
		//if(IsPlayerAlive(victim)&&IsPlayerAlive(attacker));
		//{
			if(War3_GetRace(attacker)==thisRaceID && War3_GetRace(victim)!=War3_GetRaceIDByShortname("azgalor"))
			{
				new Float:StartPos[3];
				new Float:EndPos[3];
				
				GetClientAbsOrigin( attacker, StartPos );
				GetClientAbsOrigin( victim, EndPos );

				StartPos[2]+=100;
				TE_SetupGlowSprite(StartPos,FireSprite,3.0,0.80,212);
				TE_SendToAll();
				//TE_SetupBeamRingPoint(StartPos,74.0,76.0,HaloSprite,HaloSprite,0,15,3.45,280.0,2.0,{255,77,77,255},0,0);
				//TE_SendToAll();
				TE_SetupDynamicLight(StartPos,255,80,80,10,radius,3.30,2.2);
				TE_SendToAll();
				W3FlashScreen(attacker,RGBA_COLOR_RED);
				EmitSoundToClient(attacker, catchsnd);
				DoExplosion(damage,maxdmg,attacker,victim);
			}
		//}
	}
}

public OnWar3EventDeath(victim,attacker)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new race_attacker=War3_GetRace(attacker);
			new skill_level=War3_GetSkillLevel(attacker,thisRaceID,SKILL_IGNITE);
			if(race_attacker==thisRaceID && skill_level>0 )
			{
				new Float:Vec[3];
				GetClientAbsOrigin(victim,Vec);
				new Float:Vec2[3];
				GetClientAbsOrigin(victim,Vec2);
				new Float:Vec3[3];
				GetClientAbsOrigin(attacker,Vec3);
				Vec2[2]+=100;
				Vec3[2]+=45;
				TE_SetupGlowSprite( Vec, SimpleFire, 1.0 , 1.6 , 195);
				TE_SendToAll();
				if(GetConVarBool(SFXCvar))
				{
					/*TE_SetupExplosion(Vec, Explosion, 6.5, 1, 4, 0, 0);
					TE_SendToAll();
					TE_SetupExplosion(Vec, Explosion, 6.5, 1, 4, 0, 0);
					TE_SendToAll();*/
					//TE_SetupBeamPoints( Vec, Vec3, HydraSprite, HaloSprite, 0, 1, 2.3, 45.0, 2.0, 0, 3.0, { 255, 0, 0, 255 }, 1 );
					//TE_SendToAll();
				}
				EmitSoundToClient(attacker, burnsnd);

				new damage1=DamageFire1[skill_level];
				new damage2=DamageFire2[skill_level];
				//bIsTarget[victim]=true;
				//CreateTimer( 0.10, Timer_DeSelect, victim );
				IgniteExplosion(damage1,damage2,attacker,victim);
				CreateFlame(attacker,victim);
				CreateTimer(1.0, Timer_Extinguish, attacker);
				TE_Start("Bubbles");
				TE_WriteVector("m_vecMins", Vec);
				TE_WriteVector("m_vecMaxs", Vec2);
				TE_WriteFloat("m_fHeight", 228.0);
				TE_WriteNum("m_nModelIndex", Particle);
				TE_WriteNum("m_nCount", 35);
				TE_WriteFloat("m_fSpeed", 0.5);
				TE_SendToAll();
			}
		}
	}
}
public DoExplosion(magnitude,maxdmg,client,target)
{
	//Destination = Owner
	//Vec = Fireball
	//Origin = Victim
	new Float:Destination[3];
	GetClientAbsOrigin(client,Destination);
	new AttackerTeam = GetClientTeam(client);
	//TE_SetupBeamRingPoint(Destination,1.0,9000.0,HaloSprite,HaloSprite,0,15,2.8,10.0,2.0,{255,120,120,255},0,0);
	//TE_SendToAll();
	War3_DealDamage(target,3,client,DMG_BULLET,"firestorm");
	//schaden simlurieren und spieler schmokeln lassen
	for(new i=1;i<=MaxClients;i++)
		{
			if(ValidPlayer(i,true) && GetClientTeam(i)!=AttackerTeam)
			{
				//bIsTarget[i]=true;
				//CreateTimer( 0.10, Timer_DeSelect, i );
				new Float:Vec[3];
				GetClientAbsOrigin(target,Vec);
				new Float:Origin[3];
				GetClientAbsOrigin(i,Origin);
				Vec[0] += GetRandomFloat( -150.0, 150.0 );
				Vec[1] += GetRandomFloat( -150.0, 150.0 );
				Vec[2] += 10.0;
				if(GetConVarBool(SFXCvar))
				{
					/*TE_SetupExplosion(Vec, Explosion, 6.5, 1, 4, 0, 0);
					TE_SendToAll();
					TE_SetupExplosion(Vec, Explosion, 6.5, 1, 4, 0, 0);
					TE_SendToAll(0.18);*/
					Destination[2] += 100.0;
					//TE_SetupBeamPoints( Vec, Destination, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 255, 11, 11, 255 }, 1 );
					//TE_SendToAll();
				}
				if(GetVectorDistance(Origin,Vec) < 100.0)
				{
					new magdmg = GetRandomInt(magnitude,maxdmg);
					PrintToConsole(client,"FireStorm hit a target and damaged him for %d damage",magdmg);
					IgniteEntity(i, 2.0);
					EmitSoundToClient(i, ignitesnd);
					W3FlashScreen(i,RGBA_COLOR_RED);
					War3_ShakeScreen(i);
					//TODO 1 - may add explosion sounds?
					War3_DealDamage(i,magdmg,client,DMG_BULLET,"firestorm");
					PrintToConsole(i,"hit by a firestorm");
					PrintHintText(client,"%T","Firestorm was successfully",client);
				}
			}
		}
}
//tagmismatch = float / int epic failed
//loosing identation = 
public IgniteExplosion(mindmg,maxdmg,client,target)
{
	new Float:Destination[3];
	GetClientAbsOrigin(target,Destination);
	new AttackerTeam = GetClientTeam(client);
	EmitSoundToClient(client, catchsnd);
	//PrintToChatAll("взрыв создан");
	for(new i=1;i<=MaxClients;i++)
		{
			if(ValidPlayer(i,true)&&GetClientTeam(i)!=AttackerTeam)
			{
				new Float:Vec[3];
				GetClientAbsOrigin(i,Vec);
				if(GetVectorDistance(Destination,Vec)<=200.0)
				{
					//CreateTimer( 0.10, Timer_DeSelect, i );
					//new Float:dir[3]={0.0,0.0,-90.0};
					new magdmg = GetRandomInt(mindmg,maxdmg);
					PrintToConsole(client,"[Notice] Expulsion dealing %i damage",magdmg);
					War3_ChatMessage(i, "%T", "Hit by Expulsion", i);
					IgniteEntity(i, 2.0);
					W3FlashScreen(i,RGBA_COLOR_RED);
					War3_ShakeScreen(i);
					//TODO 1 - may add explosion sounds?
					War3_DealDamage(i,magdmg,client,DMG_BULLET,"expulsion");
					if(GetConVarBool(SFXCvar))
					{
						//TE_SetupExplosion(Vec, Explosion, 6.5, 1, 4, 0, 0);
						//TE_SendToAll();
						
						//TE_SetupBeamRingPoint( Vec,65.0,75.0,HaloSprite,HaloSprite,0,15,12.20,100.0,2.0,{255,0,0,255},30,0);
						//TE_SendToAll();
						TE_SetupBeamRingPoint( Vec,200.0,9000.0,SimpleFire,HaloSprite,0,15,3.45,20.0,2.0,{255,0,0,255},0,0);
						TE_SendToAll();
					}
				}
			}
		}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && IsPlayerAlive(client))
	{
		new skill=War3_GetSkillLevel(client,race,ULT_RIFT);
		if(skill>0)
		{
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT_RIFT,true)&&!Silenced(client))
			{
				new Float:startpos[3];
				new Float:targetpos[3];
				GetClientAbsOrigin(client,startpos);
				GetClientAbsOrigin(client,targetpos);
				targetpos[2]+=850;
				TE_SetupBeamPoints(startpos, targetpos, BeamSprite, BeamSprite, 0, 5, 10.0, 65.0, 5.5, 2, 0.2, {255,128,35,255}, 70);  
				TE_SendToAll();
				TE_SetupBeamPoints(startpos, targetpos, BeamSprite, BeamSprite, 0, 5, 8.0, 65.0, 5.5, 2, 0.2, {255,128,35,240}, 70);  //do it twice so it disappears more smoothly
				TE_SendToAll();
				CreateTimer(ult_delay[skill], Timer_Rift, client);
				//PrintToChat(client,"Rift in %f seconds.",delay[skill]);
				War3_ChatMessage(client,"%T","Rift in {amount} seconds.",client,RoundToFloor(ult_delay[skill]));
				EmitSoundToAll(riftsnd,client);
				new Float:ult_cooldown=GetConVarFloat(ultCooldownCvar);
				War3_CooldownMGR(client,ult_cooldown,thisRaceID,ULT_RIFT,_,_);
			}
		}
		else
		{
			W3MsgUltNotLeveled(client);
		}
	}
}

public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID && pressed && IsPlayerAlive(client))
	{
		new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_PIT);
		if(skill_level>0)
		{
			if(!Silenced(client))
			{
				if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_PIT,true))
				{
					new Float:startpos[3];
					new Float:targetpos[3];
					War3_GetAimEndPoint(client,targetpos);
					GetClientAbsOrigin(client,startpos);
					startpos[2]+=45;
					//new Float:maxdist=PitMaxDistance[skill_level];
					new target = War3_GetTargetInViewCone(client,PitMaxDistance[skill_level],false,23.0);
					if(target>0 && !W3HasImmunity(target,Immunity_Skills))
					{
						TE_SetupBeamRingPoint(targetpos,120.1,120.0,FireSprite,HaloSprite,0,15,3.50,1.0,50.0,{255,230,230,255},0,0);
						TE_SendToAll();
						EmitSoundToAll(burnsnd,target);
						EmitSoundToAll(riftsnd,client);
						W3FlashScreen(client,{10,10,15,228}, 0.65, 0.8, FFADE_OUT);
						new ddmg = GetRandomInt(1,10);
						War3_DealDamage(target,ddmg,client,DMG_ENERGYBEAM,"pit_of_malice",W3DMGORIGIN_SKILL,W3DMGTYPE_TRUEDMG);
						W3FlashScreen(target,{10,10,15,255}, 1.08, 1.3, FFADE_OUT);
						//War3_CooldownMGR(client,12.0,thisRaceID,SKILL_PIT,_,_,_,"Pit of Malice");
						War3_CooldownMGR(client,12.0,thisRaceID,SKILL_PIT,true,true);
						new Float:slowmotion=PitSlow[skill_level];
						War3_SetBuff(target,fSlow,thisRaceID,slowmotion);
						CreateTimer( 3.0, StopSlow, target );
						//TE_SetupDynamicLight(targetpos,255,255,100,110,88.0,1.00,5.0);
						//TE_SendToAll();
						PrintToConsole(client,"damaged enemy (%i -hp)",ddmg);
						PrintHintText(client,"%T","Pit Of Malice : Hit Target",client);
						PrintHintText(target,"%T","Slowed down by Pit Of Malice",target);
						War3_CooldownMGR(client,15.0,thisRaceID,SKILL_PIT,true,true);
					}
					else
					{
						PrintHintText(client,"%T","No Valid Target in {amount} Feed found",client,RoundToFloor(PitMaxDistance[skill_level]/10.0));
					}
				}
			}	
		}
	}
}

public Action:Timer_Rift(Handle:timer, any:client)
{
	new skill=War3_GetSkillLevel(client,thisRaceID,ULT_RIFT);
	if(skill>0)
	{
		new Float:iVec[3];
		GetClientAbsOrigin(client,iVec);  
		War3_SpawnPlayer(client,true);
		//War3_SpawnPlayer(client,false);
		//thanks to Ownz ( War3_SpawnPlayer(client,bool:ignore_dead_check=false) )
		TE_SetupGlowSprite( iVec, BeamSprite, 3.5 , 1.5 , 150);
		TE_SendToAll();
		TE_SetupBeamRingPoint( iVec,1.0,75.0,HaloSprite,HaloSprite,0,15,16.0,280.0,2.0,{255,0,0,255},0,0);
		TE_SendToAll();
		TE_SetupBeamFollow(client,SimpleFire,0,0.4,10.0,20.0,20,{250,250,250,255});
		TE_SendToAll();
		TE_SetupEnergySplash(iVec, iVec,false);
		TE_SendToAll();
		SetEntityHealth(client,GetClientHealth(client)+addhp[skill]);
		War3_ChatMessage(client, "%T","Dark Rift : A Rift opens, gained {amount} HP",client,addhp[skill]);
		EmitSoundToAll(riftsnd, client);
		new Float:ult_cooldown=GetConVarFloat(ultCooldownCvar);
		War3_CooldownMGR(client,ult_cooldown,thisRaceID,ULT_RIFT,_,_);
	}
}

/*ublic Action:Timer_DeSelect(Handle:timer, any:client)
{
	if(ValidPlayer(client,true))
	{
		bIsTarget[client]=false;
	}
}*/

public Action:Timer_Extinguish(Handle:timer, any:client)
{
	if(ValidPlayer(client,true))
	{
		RemoveFlames(client);
	}
}