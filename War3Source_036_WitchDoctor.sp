/**
* File: War3Source_CustomRace_Witch_Doctor.sp
* Description: The Witch Doctor race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx
*/

#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

// War3Source stuff
new thisRaceID;

// Chance/Data Arrays
// skill 1
new Float:GetSpellChance[7] = { 0.0, 0.12, 0.18, 0.22, 0.25, 0.28, 0.33 };
new bool:bSmokeAttached[MAXPLAYERS];
new spells[MAXPLAYERS + 1];
new smoke[MAXPLAYERS];

// skill 2
#define MAXWARDS 64*5 //on map LOL
#define WARDDAMAGE 20
#define WARDBELOW -2.0 // player is 60 units tall about (6 feet)
#define WARDABOVEWITCH 140.0
#define WARDHEALTH 1

new WardRadius[] = { 60, 75, 90, 100, 105, 110, 120 };
new WardStartingArr[] = { 1, 2, 2, 3, 4, 4, 5 };
new Float:LastThunderClap[MAXPLAYERS];
new Float:WardLocation[MAXWARDS][3];
new Float:LastWardRing[MAXWARDS];
new Float:LastWardClap[MAXWARDS];
new CurrentWardCount[MAXPLAYERS];
new WardOwner[MAXWARDS];
new WardType[MAXWARDS];

// skill 3
new Float:FlickChance[7] = { 0.0, 0.12, 0.18, 0.22, 0.26, 0.3, 0.33 };

// skill 4
new bool:bMagicAttached[MAXPLAYERS];
new ClientWardType[MAXPLAYERS];
new magic[MAXPLAYERS];

new BlueBlackLargeBeamSprite, LightningSprite, TPBeamSprite, PurpleGlowSprite;
new SKILL_SPELL, SKILL_WARD, SKILL_FLICK, ULT_BOOK;

new String:wind_snippet2[]="*war3source/witch/wind_snippet2.mp3";
new String:hole_hit2[]="*war3source/witch/hole_hit2.mp3";

new flick_check[MAXPLAYERS];
new helicwardcount[MAXPLAYERS];

public Plugin:myinfo = 
{
	name = "War3Source Race - Witch Doctor",
	author = "xDr.HaaaaaaaXx",
	description = "The Witch Doctor race for War3Source.",
	version = "1.0",
	url = ""
};

public OnPluginStart()
{
	CreateTimer( 0.14, CalcWards, _, TIMER_REPEAT );
}

public OnMapStart()
{
	FakePrecacheSound(wind_snippet2);
	FakePrecacheSound(hole_hit2);
	PurpleGlowSprite = PrecacheModel( "sprites/purpleglow1.vmt" );
	LightningSprite = PrecacheModel( "materials/sprites/laserbeam.vmt" );
	BlueBlackLargeBeamSprite = PrecacheModel( "effects/blueblacklargebeam.vmt" );
	AddFileToDownloadsTable("materials/sprites/war3source/physbeam.vtf");
	AddFileToDownloadsTable("materials/sprites/war3source/physbeam.vmt");
	AddFileToDownloadsTable("materials/sprites/war3source/tp_beam001.vtf");
	AddFileToDownloadsTable("materials/sprites/war3source/tp_beam001.vmt");
	TPBeamSprite = PrecacheModel( "sprites/war3source/tp_beam001.vmt" );
	PrecacheModel( "sprites/war3source/physbeam.vmt" );
	PrecacheModel( "materials/particle/fire.vmt" );
	PrecacheModel( "effects/combinemuzzle2.vmt" );
	AddFileToDownloadsTable( "sound/war3source/witch/wind_snippet2.mp3" );
	AddFileToDownloadsTable( "sound/war3source/witch/hole_hit2.mp3" );
	LoadTranslations("w3s.race.witchdoctor.phrases.txt");
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==240)
	{
		thisRaceID = War3_CreateNewRaceT( "witchdoctor" );
		SKILL_SPELL = War3_AddRaceSkillT( thisRaceID, "SpellPoints", false, 6 );
		SKILL_WARD = War3_AddRaceSkillT( thisRaceID, "SpellPower", false, 6 );
		SKILL_FLICK = War3_AddRaceSkillT( thisRaceID, "FlickeringCloak", false, 6 );
		ULT_BOOK = War3_AddRaceSkillT( thisRaceID, "BookofSpells", false, 6 );	
		War3_CreateRaceEnd( thisRaceID );
	}
}

public OnClientPutInServer( client )
{
	spells[client] = 1;
	ClientWardType[client] = 0;
}

public OnWar3PlayerAuthed( client )
{
	LastThunderClap[client] = 0.0;
}

public OnWar3EventSpawn( client )
{
	if( War3_GetRace( client ) == thisRaceID && War3_GetSkillLevel( client, thisRaceID, SKILL_WARD ) > 0 )
	{
		AttachSpawnEffect( client );
		SetEntityRenderMode(client, RENDER_NORMAL);
		new skill_book = War3_GetSkillLevel( client, thisRaceID, ULT_BOOK );
		if(skill_book > 1)
		{
			if(spells[client] == 0)
			{
				spells[client] = GetRandomInt(1,2);
			}
		}
		RemoveWards( client );
	}
	if( War3_GetRace( client ) == thisRaceID)
	{
		flick_check[client] = 0;
		helicwardcount[client] = 0;
		SetEntityRenderMode(client,RENDER_NORMAL);
	}
}

public OnWar3EventDeath( victim, attacker )
{
	W3ResetAllBuffRace( victim, thisRaceID );
}

public OnRaceChanged(client,oldrace,newrace)
{
	if( newrace != thisRaceID )
	{
		RemoveWards( client );
		W3ResetAllBuffRace( client, thisRaceID );
		flick_check[client] = 0;
		helicwardcount[client] = 0;
		SetEntityRenderMode(client,RENDER_NORMAL);
	}
}

public OnWar3EventPostHurt(victim, attacker, Float:damage, const String:weapon[32], bool:isWarcraft)
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( victim ) == thisRaceID )
		{
			new skill_level = War3_GetSkillLevel( victim, thisRaceID, SKILL_FLICK );
			if( !Hexed( victim, false ) && GetRandomFloat( 0.0, 1.0 ) <= FlickChance[skill_level] && !flick_check[victim] )
			{
				SetEntityRenderMode(victim, RENDER_NONE);
				flick_check[victim] = 24;
				W3FlashScreen(victim,{0,255,0,50},0.5,_,FFADE_OUT);
				CreateTimer( 0.2, StopFlick, victim );
			}
		}
	}
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_spell = War3_GetSkillLevel( attacker, thisRaceID, SKILL_SPELL );
			if( !Hexed( attacker, false ) && W3Chance( GetSpellChance[skill_spell] * W3ChanceModifier( attacker ) ) )
			{
				spells[attacker]++;
				if( spells[attacker] > 20 )
				{
					spells[attacker] = 20;
				}
				else
				{
					War3_ChatMessage( attacker,"%T","You have Gained 1 Spell Point! Now you have {int} spells!", attacker, spells[attacker] );
				}
				AttachSmoke( attacker );
			}
		}
	}
}

public Action:StopFlick( Handle:timer, any:client )
{
	if( ValidPlayer( client, true ) )
	{
		if(flick_check[client])
		{
			flick_check[client]--;
			if(IsOdd(flick_check[client]))
			{
				SetEntityRenderMode(client,RENDER_NORMAL);
			}
			else
			{
				SetEntityRenderMode(client,RENDER_NONE);
			}
			CreateTimer( 0.2, StopFlick, client );
		}
		else
		{
			SetEntityRenderMode(client,RENDER_NORMAL);
		}
	}
}

stock bool:IsOdd(num)
{
    return (num & 1);
}

public OnAbilityCommand( client, ability, bool:pressed )
{
	if( War3_GetRace( client ) == thisRaceID && ability == 0 && pressed && IsPlayerAlive( client ) )
	{
		if(((helicwardcount[client] < 2) && (ClientWardType[client] == 4)) || ClientWardType[client] != 4)
		{
			new skill_level = War3_GetSkillLevel( client, thisRaceID, SKILL_WARD );
			if( skill_level > 0 )
			{
				if( !Silenced( client ) && CurrentWardCount[client] < WardStartingArr[skill_level] && spells[client] > 0 )
				{
					if(ClientWardType[client] == 4)
					{
						helicwardcount[client]++;
					}
					spells[client]--;
					CreateWard( client );
					CurrentWardCount[client]++;
					W3MsgCreatedWard( client, CurrentWardCount[client], WardStartingArr[skill_level] );
				}
				else
				{
					W3MsgNoWardsLeft( client );
				}
			}
		}
	}
	if( War3_GetRace( client ) == thisRaceID && ability == 1 && pressed && IsPlayerAlive( client ) )
	{
		if(((helicwardcount[client] < 2) && (ClientWardType[client] == 4)) || ClientWardType[client] != 4)
		{
			new skill_level = War3_GetSkillLevel( client, thisRaceID, SKILL_WARD );
			if( skill_level > 0 )
			{
				new Float:ClientPos[3];
				GetClientAbsOrigin( client, ClientPos );
				for( new i = 0; i < MAXWARDS; i++ )
				{
					if( WardOwner[i] == client )
					{
						if( GetVectorDistance( ClientPos, WardLocation[i] ) <= 60 )
						{
							LastWardClap[i] = 0.0;
							LastWardRing[i] = 0.0;
							WardOwner[i] = 0;
							WardType[i] = 0;
							CurrentWardCount[client]--;
						}
					}
				}
			}
		}
	}
}

public CreateWard( client )
{
	for( new i = 0; i < MAXWARDS; i++ )
	{
		if( WardOwner[i] == 0 && ClientWardType[client] != 0 )
		{
			WardOwner[i] = client;
			GetClientAbsOrigin( client, WardLocation[i] );
			WardType[i] = ClientWardType[client];
			if( WardType[i] == 1 )
				War3_ChatMessage( client,"%T","You planted True Sight Ward", client, spells[client] );
			if( WardType[i] == 2 )
				War3_ChatMessage( client,"%T","You planted Stasis Trap", client, spells[client] );
			if( WardType[i] == 3 )
				War3_ChatMessage( client,"%T","You planted Damage Trap", client, spells[client] );
			if( WardType[i] == 4 )
				War3_ChatMessage( client,"%T","You planted Healing Ward", client, spells[client] );
			break;
		}
	}
}

public RemoveWards( client )
{
	for( new i = 0; i < MAXWARDS; i++ )
	{
		if( WardOwner[i] == client )
		{
			LastWardClap[i] = 0.0;
			LastWardRing[i] = 0.0;
			WardOwner[i] = 0;
			WardType[i] = 0;
		}
	}
	CurrentWardCount[client] = 0;
}

public Action:CalcWards( Handle:timer, any:userid )
{
	new client;
	for( new i = 0; i < MAXWARDS; i++ )
	{
		if( WardOwner[i] != 0 )
		{
			client = WardOwner[i];
			if( !ValidPlayer( client, true ) )
			{
				WardOwner[i] = 0;
				--CurrentWardCount[client];
				WardType[i] = 0;
			}
			else
			{
				WardEffectAndDamage( client, i );
			}
		}
	}
}

public WardEffectAndDamage( owner, wardindex )
{
	new WARDRADIUS = WardRadius[War3_GetSkillLevel( owner, thisRaceID, SKILL_WARD )];
	new ownerteam = GetClientTeam( owner );
	new beamcolor[] = { 25, 0, 255, 255 };
	if( ownerteam == 2 )
	{
		beamcolor[0] = 255;
		beamcolor[1] = 0;
		beamcolor[2] = 25;
		beamcolor[3] = 128;
	}
	
	new Float:start_pos[3];
	new Float:end_pos[3];
	new Float:tempVec1[] = { 0.0, 0.0, WARDBELOW };
	new Float:tempVec2[] = { 0.0, 0.0, WARDABOVEWITCH };
	
	AddVectors( WardLocation[wardindex], tempVec1, start_pos );
	AddVectors( WardLocation[wardindex], tempVec2, end_pos );
	
	if( WardType[wardindex] == 1 )
	{
		if( LastWardRing[wardindex] < GetGameTime() - 0.25 )
		{
			LastWardRing[wardindex] = GetGameTime();
			TE_SetupBeamRingPoint( start_pos, 20.0, float( WARDRADIUS * 2 ), LightningSprite, LightningSprite, 0, 15, 1.0, 20.0, 1.0, { 255, 255, 255, 100 }, 5, FBEAM_ISACTIVE );
			TE_SendToAll();
		}
		
		TE_SetupBeamPoints( start_pos, end_pos, LightningSprite, LightningSprite, 0, GetRandomInt( 30, 100 ), 0.17, 20.0, 20.0, 0, 0.0, beamcolor, 0 );
		TE_SendToAll();
		
		TE_SetupGlowSprite( end_pos, PurpleGlowSprite, 1.0, 1.0, 50 );
		TE_SendToAll();
	}
	if( WardType[wardindex] == 2 )
	{
		if( LastWardRing[wardindex] < GetGameTime() - 0.25 )
		{
			LastWardRing[wardindex] = GetGameTime();
			TE_SetupBeamRingPoint( start_pos, 20.0, float( WARDRADIUS * 2 ), LightningSprite, LightningSprite, 0, 15, 1.0, 20.0, 1.0, { 25, 25, 255, 100 }, 5, FBEAM_ISACTIVE );
			TE_SendToAll();
		}
		
		TE_SetupBeamPoints( start_pos, end_pos, LightningSprite, LightningSprite, 0, GetRandomInt( 30, 100 ), 0.17, 20.0, 20.0, 0, 0.0, beamcolor, 0 );
		TE_SendToAll();
		
		TE_SetupGlowSprite( end_pos, PurpleGlowSprite, 1.0, 1.25, 50 );
		TE_SendToAll();
	}
	if( WardType[wardindex] == 3 )
	{
		if( LastWardRing[wardindex] < GetGameTime() - 0.25 )
		{
			LastWardRing[wardindex] = GetGameTime();
			TE_SetupBeamRingPoint( start_pos, 20.0, float( WARDRADIUS * 2 ), LightningSprite, LightningSprite, 0, 15, 1.0, 20.0, 1.0, { 255, 25, 25, 100 }, 5, FBEAM_ISACTIVE );
			TE_SendToAll();
		}
		
		TE_SetupBeamPoints( start_pos, end_pos, LightningSprite, LightningSprite, 0, GetRandomInt( 30, 100 ), 0.17, 20.0, 20.0, 0, 0.0, beamcolor, 0 );
		TE_SendToAll();
		
		TE_SetupGlowSprite( end_pos, PurpleGlowSprite, 1.0, 1.25, 50 );
		TE_SendToAll();
	}
	if( WardType[wardindex] == 4 )
	{
		if( LastWardRing[wardindex] < GetGameTime() - 0.25 )
		{
			LastWardRing[wardindex] = GetGameTime();
			TE_SetupBeamRingPoint( start_pos, 20.0, float( WARDRADIUS * 2 ), LightningSprite, LightningSprite, 0, 15, 1.0, 20.0, 1.0, { 25, 255, 25, 255 }, 5, FBEAM_ISACTIVE );
			TE_SendToAll( 0.1 );
		}
		
		TE_SetupBeamPoints( start_pos, end_pos, LightningSprite, LightningSprite, 0, GetRandomInt( 30, 100 ), 0.17, 20.0, 20.0, 0, 0.0, beamcolor, 0 );
		TE_SendToAll();

		TE_SetupGlowSprite( end_pos, PurpleGlowSprite, 1.0, 1.25, 50 );
		TE_SendToAll();
	}
	
	new Float:BeamXY[3];
	for( new x = 0; x < 3; x++ ) BeamXY[x] = start_pos[x];
	new Float:BeamZ = BeamXY[2];
	BeamXY[2] = 0.0;
	
	new Float:VictimPos[3];
	new Float:tempZ;
	if( WardType[wardindex] == 1 )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( ValidPlayer( i, true ) && GetClientTeam(i) != ownerteam )
			{
				GetClientAbsOrigin( i, VictimPos );
				tempZ = VictimPos[2];
				VictimPos[2] = 0.0;
				
				if( GetVectorDistance( BeamXY, VictimPos ) <= WARDRADIUS )
				{
					if( tempZ > BeamZ + WARDBELOW && tempZ < BeamZ + WARDABOVEWITCH )
					{
						if( !W3HasImmunity( i, Immunity_Skills ) )
						{
							War3_SetBuff( i, bInvisibilityDenyAll, thisRaceID, true );
							if(GetEntityRenderMode(i) == RENDER_NONE)
							{
								SetEntityRenderMode(i,RENDER_NORMAL);
							}
						}
						else
						{
							War3_SetBuff( i, bInvisibilityDenyAll, thisRaceID, false );
						}
					}
					else
					{
						War3_SetBuff( i, bInvisibilityDenyAll, thisRaceID, false );
					}
				}
				else
				{
					War3_SetBuff( i, bInvisibilityDenyAll, thisRaceID, false );
				}
			}
		}
	}
	if( WardType[wardindex] == 2 )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( ValidPlayer( i, true ) && GetClientTeam(i) != ownerteam )
			{
				GetClientAbsOrigin( i, VictimPos );
				tempZ = VictimPos[2];
				VictimPos[2] = 0.0;
				
				if( GetVectorDistance( BeamXY, VictimPos ) < WARDRADIUS )
				{
					if( tempZ > BeamZ + WARDBELOW && tempZ < BeamZ + WARDABOVEWITCH )
					{
						if( !W3HasImmunity( i, Immunity_Skills ) )
						{
							if( LastWardClap[wardindex] < GetGameTime() - 1.0 )
							{
								War3_SetBuff( i, fSlow, thisRaceID, 0.5 );
								
								CreateTimer( 2.0, StopSlow, i );
								
								LastWardClap[i] = GetGameTime();
							}
						}
					}
				}
			}
		}
	}
	if( WardType[wardindex] == 3 )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( ValidPlayer( i, true ) && GetClientTeam(i) != ownerteam )
			{
				GetClientAbsOrigin( i, VictimPos );
				tempZ = VictimPos[2];
				VictimPos[2] = 0.0;
				
				if( GetVectorDistance( BeamXY, VictimPos ) < WARDRADIUS )
				{
					if( tempZ > BeamZ + WARDBELOW && tempZ < BeamZ + WARDABOVEWITCH )
					{
						if( !W3HasImmunity( i, Immunity_Skills ) )
						{
							if( LastWardClap[wardindex] < GetGameTime() - 1.0 )
							{
								new DamageScreen[4];
								new Float:pos[3];
								
								GetClientAbsOrigin( i, pos );
								
								DamageScreen[0] = beamcolor[0];
								DamageScreen[1] = beamcolor[1];
								DamageScreen[2] = beamcolor[2];
								DamageScreen[3] = 50;
								
								W3FlashScreen( i, DamageScreen );
								
								War3_DealDamage( i, WARDDAMAGE, owner, DMG_ENERGYBEAM, "wards", _, W3DMGTYPE_MAGIC );
								
								pos[2] += 40;
								
								TE_SetupBeamPoints( start_pos, pos, LightningSprite, LightningSprite, 0, 0, 1.0, 10.0, 20.0, 0, 0.0, { 255, 150, 70, 255 }, 0 );
								TE_SendToAll();
								
								LastWardClap[wardindex] = GetGameTime();
							}
						}
					}
				}
			}
		}
	}
	if( WardType[wardindex] == 4 )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( ValidPlayer( i, true ) && GetClientTeam(i) == ownerteam )
			{
				GetClientAbsOrigin( i, VictimPos );
				tempZ = VictimPos[2];
				VictimPos[2] = 0.0;
				
				if( GetVectorDistance( BeamXY, VictimPos ) < WARDRADIUS )
				{
					if( tempZ > BeamZ + WARDBELOW && tempZ < BeamZ + WARDABOVEWITCH )
					{
						if( GetClientHealth( i ) != War3_GetMaxHP( i ) )
						{							
							War3_HealToMaxHP( i, WARDHEALTH );
						}
					}
				}
			}
		}
	}
}

public Action:StopSlow( Handle:timer, any:client )
{
	if( ValidPlayer( client, true ) )
	{
		War3_SetBuff( client, fSlow, thisRaceID, 1.0 );
	}
}

public OnUltimateCommand( client, race, bool:pressed )
{
	if( ValidPlayer( client, false ) && pressed && race == thisRaceID )
	{
		new skill_book = War3_GetSkillLevel( client, race, ULT_BOOK );
		if( skill_book > 0 )
		{
			DoSpellMenu( client );
			if( ValidPlayer( client, true ) )
			{
				AttachMagic( client );
			}
		}
	}
}

public DoSpellMenu( client )
{
	new Handle:spellMenu = CreateMenu( War3Source_Spell_Selected );
	
	SetMenuExitButton( spellMenu, true );
	
	new String:title[64];
	
	Format( title, 64, "%T", "Book of Spells You have {int} spells", client, spells[client] );
	
	SetMenuTitle( spellMenu, title );
	
	new level = War3_GetSkillLevel( client, thisRaceID, ULT_BOOK );
	
	new String:sight[64] = "True Sight Ward";
	new String:stasis[64] = "Stasis Trap";
	new String:damage[64] = "�amage Trap";
	new String:heal[64] = "Healing Ward";
	
	if( ClientWardType[client] == 1 )
	{
		Format( sight, 64, "%T", "True Sight Ward", client );
	}
	if( ClientWardType[client] == 2 )
	{
		Format( stasis, 64, "%T", "Stasis Trap", client );
	}
	if( ClientWardType[client] == 3 )
	{
		Format( damage, 64, "%T", "Damage Trap", client );
	}
	if( ClientWardType[client] == 4 )
	{
		Format( heal, 64, "%T", "Healing Ward", client );
	}
	
	AddMenuItem( spellMenu, "", sight,  ITEMDRAW_DEFAULT );
	AddMenuItem( spellMenu, "", stasis, ( level > 1 ) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED );
	AddMenuItem( spellMenu, "", damage, ( level > 2 ) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED );
	AddMenuItem( spellMenu, "", heal,   ( level > 3 ) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED );
	
	DisplayMenu( spellMenu, client, 15 );
}

public War3Source_Spell_Selected( Handle:menu, MenuAction:action, client, selection )
{
	if( action == MenuAction_Select )
	{
		if( selection == 0 )
		{
			ClientWardType[client] = 1;
			War3_ChatMessage( client,"%T","True Sight Ward",client );
		}
		else if( selection == 1 )
		{
			ClientWardType[client] = 2;
			War3_ChatMessage( client,"%T","Stasis Trap",client );
		}
		else if( selection == 2 )
		{
			ClientWardType[client] = 3;
			War3_ChatMessage( client,"%T","Damage Trap",client );
		}
		else if( selection == 3 )
		{
			ClientWardType[client] = 4;
			War3_ChatMessage( client,"%T","Healing Ward",client );
		}
	}
	if( action == MenuAction_End )
	{
		CloseHandle( menu );
	}
}

stock AttachSmoke( client )
{
	if( !bSmokeAttached[client] )
	{
		smoke[client] = CreateEntityByName( "env_smokestack" );
		bSmokeAttached[client] = true;
		
		if( IsValidEdict( smoke[client] ) && IsClientInGame( client ) )
		{
			decl Float:fPos[3], Float:fAng[3] = { 0.0, 0.0, 0.0 };
			GetEntPropVector( client, Prop_Send, "m_vecOrigin", fPos );
			fPos[2] += 38;
			//Set Key Values
			DispatchKeyValueVector( smoke[client], "Origin", fPos );
			DispatchKeyValueVector( smoke[client], "Angles", fAng );
			DispatchKeyValueFloat( smoke[client], "BaseSpread", 50.0 );
			DispatchKeyValueFloat( smoke[client], "StartSize", 5.0 );
			DispatchKeyValueFloat( smoke[client], "EndSize", 1.0 );
			DispatchKeyValueFloat( smoke[client], "Twist", 500.0 );
			
			DispatchKeyValue( smoke[client], "SmokeMaterial", "particle/fire.vmt" );
			DispatchKeyValue( smoke[client], "RenderColor", "100 100 220" );
			DispatchKeyValue( smoke[client], "SpreadSpeed", "10" );
			DispatchKeyValue( smoke[client], "RenderAmt", "100" );
			DispatchKeyValue( smoke[client], "JetLength", "100" );
			DispatchKeyValue( smoke[client], "RenderMode", "18" );
			DispatchKeyValue( smoke[client], "Initial", "0" );
			DispatchKeyValue( smoke[client], "Speed", "50" );
			DispatchKeyValue( smoke[client], "Rate", "200" );
			DispatchSpawn( smoke[client] );
			
			//Set Entity Inputs
			SetVariantString( "!activator" );
			AcceptEntityInput( smoke[client], "SetParent", client, smoke[client], 0 );
			AcceptEntityInput( smoke[client], "TurnOn" );
			CreateTimer( 3.0, StopSmoke, client );
		}
		else
		{
			LogError( "Failed to create env_smokestack!" );
		}
	}
}

public Action:StopSmoke( Handle:timer, any:client )
{
	bSmokeAttached[client] = false;
	AcceptEntityInput( smoke[client], "TurnOff" );
	AcceptEntityInput( smoke[client], "Kill" );
}

stock AttachMagic( client )
{
	if( !bMagicAttached[client] )
	{
		magic[client] = CreateEntityByName( "env_smokestack" );
		bMagicAttached[client] = true;
		
		if( IsValidEdict( magic[client] ) && IsClientInGame( client ) )
		{
			decl Float:fPos[3];
			GetEntPropVector( client, Prop_Send, "m_vecOrigin", fPos );
			fPos[2] += 38;
			//Set Key Values
			DispatchKeyValueVector( magic[client], "Origin", fPos );
			DispatchKeyValueFloat( magic[client], "BaseSpread", 11.0 );
			DispatchKeyValueFloat( magic[client], "StartSize", 16.0 );
			DispatchKeyValueFloat( magic[client], "EndSize", 1.0 );
			DispatchKeyValueFloat( magic[client], "Twist", 50.0 );
			
			DispatchKeyValue( magic[client], "SpreadSpeed", "40" );
			DispatchKeyValue( magic[client], "Speed", "60" );
			DispatchKeyValue( magic[client], "Rate", "50" );
			DispatchKeyValue( magic[client], "JetLength", "150" );
			DispatchKeyValue( magic[client], "RenderColor", "115 79 183" );
			DispatchKeyValue( magic[client], "RenderAmt", "200" );
			DispatchKeyValue( magic[client], "SmokeMaterial", "effects/combinemuzzle2.vmt" );
			DispatchSpawn( magic[client] );
			
			//Set Entity Inputs
			SetVariantString( "!activator" );
			AcceptEntityInput( magic[client], "SetParent", client, magic[client], 0 );
			AcceptEntityInput( magic[client], "TurnOn" );
			CreateTimer( 8.0, StopMagic, client );
			EmitSoundToAll( wind_snippet2, client );
		}
		else
		{
			LogError( "Failed to create env_smokestack!" );
		}
	}
}

public Action:StopMagic( Handle:timer, any:client )
{
	bMagicAttached[client] = false;
	//AcceptEntityInput( magic[client], "TurnOff" );
	AcceptEntityInput( magic[client], "Kill" );
}

stock AttachSpawnEffect( client )
{
	new Float:pos1[3];
	new Float:pos2[3];
	
	GetClientAbsOrigin( client, pos1 );
	GetClientAbsOrigin( client, pos2 );
	
	pos2[2] += 1110;
	
	TE_SetupBeamPoints( pos1, pos2, LightningSprite, LightningSprite, 0, 100, 3.0, 1.0, 1.0, 10, 1.0, { 255, 255, 255, 255 }, 100 );
	TE_SendToAll();
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 2.2 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 2.2 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 2.0 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 2.0 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 1.8 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 1.8 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 1.4 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 1.4 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 1.2 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 1.2 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 1.0 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 1.0 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 0.8 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 0.8 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 0.6 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 0.6 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 0.4 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 0.4 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll( 0.2 );
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll( 0.2 );
	
	pos1[2] += 110;
	
	TE_SetupBeamRingPoint( pos1, 199.0, 150.0, TPBeamSprite, TPBeamSprite, 0, 0, 2.0, 23.0, 0.0, { 15, 5, 255, 255 }, 9, FBEAM_ISACTIVE );
	TE_SendToAll();
	
	TE_SetupBeamPoints( pos1, pos2, BlueBlackLargeBeamSprite, BlueBlackLargeBeamSprite, 0, 0, 2.0, 5.0, 5.0, 0, 0.0, { 20, 15, 255, 255 }, 0 );
	TE_SendToAll();
	
	EmitSoundToAll( hole_hit2, client );
}