/**
* File: War3Source_Firelord.sp
* Description: Firelord race of warcraft.
* Author: Lucky 
*/
 
#pragma semicolon 1
 
#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
#include <sdktools_functions>
#include <sdktools_tempents>
#include <sdktools_tempents_stocks>
#include <sdkhooks>
#include <emitsoundany>

new thisRaceID;
new g_offsCollisionGroup;

new BeingBurnedBy[MAXPLAYERSCUSTOM];
new Float:BurnsRemaining[MAXPLAYERSCUSTOM]; //burn count for victims

//Soulburn
new Float:SouldDur[]={0.0,4.0,5.0,6.0,6.5,7.0,8.0};
new Float:SouldCD[]={0.0,30.0,25.0,22.0,20.0,18.0,15.0};
//new String:soulburn_sound[256];
new BurnedBy[MAXPLAYERS];
new Float:SoulTimer[MAXPLAYERS];

new String:soulburn_sound[256]; //="war3source/firelord/soulburn.mp3";
new String:volcano_sound[256]; //="war3source/firelord/volcano.mp3";

//Summon Lava Spawn
new Float:SummonCD[]={0.0,90.0,80.0,75.0,70.0,65.0,60.0};

//Incinerate
new Float:IncinCD[]={0.0,8.0,7.5,7.0,6.5,6.0,5.0};
new IncinDmg[MAXPLAYERS];
new Float:IncinTimer[MAXPLAYERS];
//new String:incinerate_sound[]="war3source/firelord/incinerate.wav";

//Volcano
new Float:VolcanoTime[]={0.0,5.0,6.0,7.0,8.0,9.0,10.0};
new Float:VolcanoTimer[MAXPLAYERS];
new Float:VolcanoLocation[MAXPLAYERS][3];
new VolcanoCLIENT[MAXPLAYERS];
new g_iExplosionModel;
//new String:volcano_sound[256];

new BeamSprite,HaloSprite;

//Skills & Ultimate
new SKILL_SOUL, SKILL_SUMMON, SKILL_INCINERATE, ULT_VOLCANO;

new MyWeaponsOffset,AmmoOffset;

public Plugin:myinfo = 
{
	name = "War3Source Race - Firelord",
	author = "Lucky",
	description = "Firelord race of warcraft",
	version = "alpha",
	url = ""
}

public OnPluginStart()
{
//	CreateTimer(1.0,Soulburn,_,TIMER_REPEAT);
	CreateTimer(0.5,Volcano,_,TIMER_REPEAT);
	CreateTimer(3.0,Incinerate,_,TIMER_REPEAT);
	g_offsCollisionGroup = FindSendPropOffs("CBaseEntity", "m_CollisionGroup");	
	HookEvent("round_end", RoundEndEvent );
	LoadTranslations("w3s.race.firelord.phrases");
	
	MyWeaponsOffset=FindSendPropOffs("CBaseCombatCharacter","m_hMyWeapons");
    AmmoOffset=FindSendPropOffs("CBasePlayer","m_iAmmo");
}

public OnMapStart()
{
	//War3_AddSoundFolder(soulburn_sound, sizeof(incinerate_sound), "firelord/soulburn.mp3");
    //War3_AddSoundFolder(volcano_sound, sizeof(volcano_sound), "firelord/volcano2.mp3");
	
	AddFileToDownloadsTable("sound/war3source/firelord/volcano3.mp3");
	FakePrecacheSound( "*war3source/firelord/volcano3.mp3" );
	
	AddFileToDownloadsTable("sound/war3source/firelord/summon.mp3");
	FakePrecacheSound( "*war3source/firelord/summon.mp3" );
	
	//War3_AddCustomSound(soulburn_sound);
	//War3_AddCustomSound(volcano_sound);
	
	//War3_PrecacheSound(soulburn_sound);
	//War3_PrecacheSound(volcano_sound);
	//War3_PrecacheSound(incinerate_sound);
	g_iExplosionModel=PrecacheModel("materials/sprites/glow07.vmt");
	War3_PrecacheParticle("molotov_explosion");
	
	BeamSprite=War3_PrecacheBeamSprite(); 
    HaloSprite=War3_PrecacheHaloSprite();
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==270)
	{
		thisRaceID=War3_CreateNewRaceT("firelord");
	SKILL_SOUL=War3_AddRaceSkillT(thisRaceID,"1",false,6);
	SKILL_SUMMON=War3_AddRaceSkillT(thisRaceID,"2",false,6);
	SKILL_INCINERATE=War3_AddRaceSkillT(thisRaceID,"3",false,6);
	ULT_VOLCANO=War3_AddRaceSkillT(thisRaceID,"4",true,6);
	War3_CreateRaceEnd(thisRaceID);
	W3SkillCooldownOnSpawn(thisRaceID,SKILL_SUMMON,20.0,_);
	}

}

public OnRaceChanged(client, oldrace, newrace)
{
	if(newrace != thisRaceID){
		//War3_WeaponRestrictTo(client,thisRaceID,"");
		W3ResetAllBuffRace(client,thisRaceID);
	}
	
	if(newrace == thisRaceID){
		//War3_WeaponRestrictTo(client,thisRaceID,"weapon_elite,weapon_knife,weapon_defuse,weapon_c4");
		if(ValidPlayer(client,true)){
		//GivePlayerItem(client, "weapon_elite");
		}
	}
	
}

public OnWar3EventSpawn(client)
{	
	if(War3_GetRace(client)==thisRaceID)
	{
		//GivePlayerItem(client, "weapon_elite");
		VolcanoTimer[client]=0.0;
		IncinTimer[client]=0.0;
		IncinDmg[client]=0;
		
	}
	BurnsRemaining[client]=0.0;

	
}

public OnWar3EventDeath(victim,attacker)
{
	new race_victim=War3_GetRace(victim);
	if(race_victim==thisRaceID){
		IncinTimer[victim]=0.0;
	}
}

public OnW3TakeDmgBullet(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim){
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		
		if(vteam!=ateam){
			new race_attacker=War3_GetRace(attacker);
			new skill_incinerate=War3_GetSkillLevel(attacker,thisRaceID,SKILL_INCINERATE);
			if(race_attacker==thisRaceID && skill_incinerate>0)
			{
				if(War3_SkillNotInCooldown(attacker,thisRaceID,SKILL_INCINERATE,true))
				{
					War3_CooldownMGR(attacker,IncinCD[skill_incinerate],thisRaceID,SKILL_INCINERATE);
					CreateBlinkParticle(victim, "env_fire_small");
					
					War3_DealDamage(victim,5,attacker,DMG_BURN,"weapon_molotov",_,W3DMGTYPE_MAGIC);
					//W3EmitSoundToAll(incinerate_sound,victim);
				}
				if(IncinTimer[attacker]>0)
				{
					War3_DealDamage(victim,IncinDmg[attacker],attacker,DMG_BULLET,"Incinerate");
				}
				IncinTimer[attacker]+=2.0;
				if(IncinDmg[attacker]<20){
					IncinDmg[attacker]++;
				}
			}
		}
	}
}

public OnAbilityCommand(client,ability,bool:pressed)
{
	if(!Silenced(client))
	{
		new skill_soul=War3_GetSkillLevel(client,thisRaceID,SKILL_SOUL);
		new skill_summon=War3_GetSkillLevel(client,thisRaceID,SKILL_SUMMON);
		
		if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client)){
			if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_SOUL,true))
			{
				if(skill_soul>0)
				{
					new target = War3_GetTargetInViewCone(client,700.0,false,20.0);
					
					if(target>0)
					{
						if(!W3HasImmunity(target,Immunity_Skills))
						{
						new skill_level = War3_GetSkillLevel( client, thisRaceID, SKILL_SOUL );
						War3_CooldownMGR(client,SouldCD[skill_level],thisRaceID,SKILL_SOUL);
						BeingBurnedBy[target]=GetClientUserId(client);
						BurnsRemaining[target]=SouldDur[skill_level];
						CreateTimer(1.0,BurnLoop,GetClientUserId(target));
						W3EmitSoundToAll(soulburn_sound,target);
						W3EmitSoundToAll(soulburn_sound,client);
						
						new Float:start_pos[3];
						GetClientAbsOrigin(client,start_pos);
						start_pos[2]+=30.0; // offset for effect
						decl Float:target_pos[3];
						GetClientAbsOrigin(target,target_pos);
						target_pos[2]+=30.0;
						TE_SetupBeamPoints(start_pos,target_pos,BeamSprite,HaloSprite,0,35,1.0,20.0,20.0,0,10.0,{247,0,255,255},40);
						TE_SendToAll();
						
						PrintHintText(target,"Hit by Soul");
						//War3_DealDamage(target,7,client,DMG_BULLET,"Soulburn");
						
						//CreateTimer(SouldDur[skill_soul],Undo,target);
						
							//BurnedBy[target]=client;
							//SoulTimer[target]=SouldDur[skill_soul];
							//CreateTimer(1.0,Poison,target);
						//War3_DealDamage(target,7,BurnedBy[target],DMG_BULLET,"Soulburn");
						//CreateTimer(SouldDur[skill_soul], poisonoff, target);
						}
					}
					else
					{
						PrintHintText(client,"%t", "no target found");
					}
				}
				else
				{
					PrintHintText(client,"%t", "Level Blizzard first");
				}	
			}
		}
		
		if(War3_GetRace(client)==thisRaceID && ability==1 && pressed && IsPlayerAlive(client)){
			if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_SUMMON,true)){
				if(skill_summon>0){
					new Float:position111[3];
					War3_CachedPosition(client,position111);
					position111[2]+=5.0;
					new targets[MAXPLAYERS];
					new foundtargets;
					for(new ally=1;ally<=MaxClients;ally++){
						if(ValidPlayer(ally)){
							new ally_team=GetClientTeam(ally);
							new client_team=GetClientTeam(client);
							if(!IsPlayerAlive(ally) && ally_team==client_team){
								targets[foundtargets]=ally;
								foundtargets++;
							}
						}
					}
					new target;
					if(foundtargets>0){
						target=targets[GetRandomInt(0, foundtargets-1)];
						if(target>0){
							War3_CooldownMGR(client,SummonCD[skill_summon],thisRaceID,SKILL_SUMMON);
							new Float:ang[3];
							new Float:pos[3];
							RespawnPlayer(target);
							GetClientEyeAngles(client,ang);
							GetClientAbsOrigin(client,pos);
							TeleportEntity(target,pos,ang,NULL_VECTOR);
							SetEntData(target, g_offsCollisionGroup, 2, 4, true);
							SetEntData(client, g_offsCollisionGroup, 2, 4, true);
							CreateTimer(3.0,normal,target);
							CreateTimer(3.0,normal,client);
							
							pos[2]+=15.0;
							//TE_SetupBeamRingPoint(pos, 20.0, 100.0, BeamSprite, HaloSprite, 0, 5, 0.5, 10.0, 1.0, {255,0,0,133}, 60, 0);
							TE_SetupBeamRingPoint(pos, 20.0, 200.0, BeamSprite, HaloSprite, 0, 5, 0.5, 10.0, 1.0, {255,0,0,133}, 60, 0);
							TE_SendToAll();
							
							EmitSoundToAll("*war3source/firelord/summon.mp3",client);
						}
					}
					else
					{
						PrintHintText(client,"%t","There are no Lava Spawns to rez");
					}
				}
				else
				{
					PrintHintText(client,"%t", "Level your Summon first");
				}
			}
		}
		
	}
	else
	{
		PrintHintText(client,"%t","Silenced: Can not cast");
	}
	
}

public bool:IsBurningFilter(client)
{
	return (BurnsRemaining[client]<=0);
}

public PlayerDeathEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	new userid=GetEventInt(event,"userid");
	new victim=GetClientOfUserId(userid);
	if(victim>0)
	{
		BurnsRemaining[victim]=0.0;
	}
}


public RoundEndEvent( Handle:event, const String:name[], bool:dontBroadcast )
{
	new userid=GetEventInt(event,"userid");
	new victim=GetClientOfUserId(userid);
	if( BurnsRemaining[victim]>=1 )
	{
		BurnsRemaining[victim]=0.0;
	}
}

public Action:BurnLoop(Handle:timer,any:userid)
{
	new victim=GetClientOfUserId(userid);
	new attacker=GetClientOfUserId(BeingBurnedBy[victim]);
	if(victim>0 && attacker>0 && BurnsRemaining[victim]>0 && IsClientInGame(victim) && IsClientInGame(attacker) && IsPlayerAlive(victim))
	{
		BurnsRemaining[victim]--;
		new damage = 5;
		War3_DealDamage(victim,damage,attacker,DMG_BURN,"weapon_molotov",_,W3DMGTYPE_MAGIC);

		CreateBlinkParticle(victim, "env_fire_small");
		CreateTimer(1.0,BurnLoop,userid);
	}
}

public Action:Undo(Handle:timer,any:victim)
{
	War3_SetBuff(victim,bSilenced,thisRaceID,false);
	//Poison[victim]=false;
}

public Action:normal(Handle:timer,any:client)
{
	if(ValidPlayer(client,true))
	{
		new Float:end_dist=50.0;
		new Float:end_pos[3];
		GetClientAbsOrigin(client,end_pos);
		for(new i=1;i<=MaxClients;i++)
		{
			if(ValidPlayer(i,true)&&i!=client)
			{
				new Float:pos[3];
				GetClientAbsOrigin(i,pos);
				new Float:dist=GetVectorDistance(end_pos,pos);
				if(dist<=end_dist)
				{
					CreateTimer(1.0,normal,client);
					break;
				}
				else{
					SetEntData(client, g_offsCollisionGroup, 5, 4, true);
				}
			}
		}
	}
}

public Action:Poison(Handle:timer,any:victim)
{
	if(SoulTimer[victim]>0){
		//SoulTimer--;
		//War3_DealDamage(victim,5,BurnedBy[victim],DMG_BULLET,"Soulburn");
		//CreateTimer(SouldDur[skill_soul], War3_DealDamage(victim,7,BurnedBy[victim],DMG_BULLET,"Soulburn") , victim);
		//CreateTimer(SouldDur[skill_soul], poisonoff , victim);
	}
}

public Action:poisonoff(Handle:timer,any:victim)
{
	if(SoulTimer[victim]>0){
		//SoulTimer--;
		War3_DealDamage(victim,0,BurnedBy[victim],DMG_BULLET,"Soulburn");
		//CreateTimer(SouldDur[skill_soul], War3_DealDamage(victim,7,BurnedBy[victim],DMG_BULLET,"Soulburn") , victim);
		//CreateTimer(SouldDur[skill_soul], poisonoff , victim);
	}
}


public Action:Volcano(Handle:timer,any:userid)
{
	for(new x=1;x<=MaxClients;x++){
		if(ValidPlayer(x)){
			if(War3_GetRace(x)==thisRaceID){
				new client=VolcanoCLIENT[x];
				new Float:victimPos[3];
				if(VolcanoLocation[client][0]==0.0&&VolcanoLocation[client][1]==0.0&&VolcanoLocation[client][2]==0.0){
				}
				else 
				{
					if(VolcanoTimer[client]>1.0)
					{
						//PrintToChatAll("Volcano 387");
						VolcanoTimer[client]--;
						new ownerteam=GetClientTeam(client);
						TE_SetupGlowSprite(VolcanoLocation[client],g_iExplosionModel,0.6,1.9,255);
						TE_SendToAll();
						decl Float:ranPos[3];
						for(new i= 1; i<=10;++i)
						{
							ranPos[1]=GetRandomFloat((VolcanoLocation[client][1]-400.0),(VolcanoLocation[client][1]+400.0));
							ranPos[0]=GetRandomFloat((VolcanoLocation[client][0]-400.0),(VolcanoLocation[client][0]+400.0));
							ranPos[2]=VolcanoLocation[client][2];
							TE_SetupGlowSprite(ranPos,g_iExplosionModel,0.6,1.0,255);
							TE_SendToAll();
							ThrowAwayParticle("molotov_explosion", ranPos, 0.6);
						}
						for (new i=1;i<=MaxClients;i++){
							if(ValidPlayer(i,true)&& GetClientTeam(i)!=ownerteam){
								GetClientAbsOrigin(i,victimPos);
								if(GetVectorDistance(VolcanoLocation[client],victimPos)<800.0)
								{
									if(!W3HasImmunity(i,Immunity_Ultimates)){
										//War3_ShakeScreen(i,3.0,50.0,40.0);
										War3_DealDamage(i,6,client,DMG_BULLET,"Volcano");
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

public Action:Incinerate(Handle:timer,any:userid)
{
	for(new client=1;client<=MaxClients;client++){
		if(ValidPlayer(client,true)){
			if(War3_GetRace(client)==thisRaceID){
				new skill_incinerate=War3_GetSkillLevel(client,thisRaceID,SKILL_INCINERATE);

				if(skill_incinerate>0){
					if(IncinTimer[client]>0.0){
						IncinTimer[client]--;
					}
					else
					{
						IncinTimer[client]==0.0;
						IncinDmg[client]=0;
					}
				}
			}
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && ValidPlayer(client,true)){
		if(!Silenced(client)){
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT_VOLCANO,true)){
				new ult_volcano=War3_GetSkillLevel(client,thisRaceID,ULT_VOLCANO);
				if(ult_volcano>0){
					EmitSoundToAll("*war3source/firelord/volcano3.mp3",client);
					War3_CooldownMGR(client,30.0,thisRaceID,ULT_VOLCANO);
					War3_CachedPosition(client,VolcanoLocation[client]);
					TE_SetupGlowSprite(VolcanoLocation[client],g_iExplosionModel,2.0,1.9,255);
					TE_SendToAll();
					VolcanoTimer[client]=VolcanoTime[ult_volcano];
					VolcanoCLIENT[client]=client;
					//CreateTimer((VolcanoTime[ult_volcano]/2),Stop,client);
				}
				else
				{
					PrintHintText(client,"%t", "Level your Volcano first");
				}
			}
		}	
		else
		{
			PrintHintText(client,"%t", "Silenced: Can not cast");
		}
	}
}
/*
public Action:Stop(Handle:timer,any:client)
{
	StopSound(client,SNDCHAN_AUTO,volcano_sound);
}*/

public RespawnPlayer(client)
{
	War3_SpawnPlayer(client);
	new Float:pos[3];
	new Float:ang[3];
	War3_CachedAngle(client,ang);
	War3_CachedPosition(client,pos);
	// cool, now remove their weapons besides knife and c4 
	for(new slot=0;slot<10;slot++)
	{
		new ent=GetEntDataEnt2(client,MyWeaponsOffset+(slot*4));
		if(ent>0 && IsValidEdict(ent))
		{
			new String:ename[64];
			GetEdictClassname(ent,ename,64);
			if(StrEqual(ename,"weapon_c4") || StrEqual(ename,"weapon_knife"))
			{
				continue; // don't think we need to delete these
			}
			W3DropWeapon(client,ent);
			UTIL_Remove(ent);
		}
	}
	// restore iAmmo
	for(new ammotype=0;ammotype<32;ammotype++)
	{
		SetEntData(client,AmmoOffset+(ammotype*4),War3_CachedDeadAmmo(client,ammotype),4);
	}
	// give them their weapons
	for(new slot=0;slot<10;slot++)
	{
		new String:wep_check[64];
		War3_CachedDeadWeaponName(client,slot,wep_check,64);
		//PrintToChatAll("zz %s",wep_check);
		if(!StrEqual(wep_check,"weapon_c4") && !StrEqual(wep_check,"weapon_knife"))
		{
			new wep_ent=GivePlayerItem(client,wep_check);
			if(wep_ent>0) 
			{
				///dont set clip
				//SetEntData(wep_ent,Clip1Offset,War3_CachedDeadClip1(client,slot),4);
			}
		}
	}
	War3_ChatMessage(client,"Reincarnated via Fire Lord team mate",client);
}

stock CreateBlinkParticle(ent, String:particleType[])
{
	new particle = CreateEntityByName("info_particle_system");

	decl String:name[64];

	if (IsValidEdict(particle))
	{
		new Float:position[3];
		GetEntPropVector(ent, Prop_Send, "m_vecOrigin", position);
		position[2]+=30.0;
		TeleportEntity(particle, position, NULL_VECTOR, NULL_VECTOR);
		GetEntPropString(ent, Prop_Data, "m_iName", name, sizeof(name));
		DispatchKeyValue(particle, "targetname", "tf2particle");
		DispatchKeyValue(particle, "parentname", name);
		DispatchKeyValue(particle, "effect_name", particleType);
		DispatchSpawn(particle);
		SetVariantString(name);
		AcceptEntityInput(particle, "SetParent", particle, particle, 0);
		ActivateEntity(particle);
		AcceptEntityInput(particle, "start");
		decl String:Buffer[64];
        Format(Buffer, sizeof(Buffer), "Client%d", ent);
        DispatchKeyValue(ent, "targetname", Buffer);
        SetVariantString(Buffer);
        AcceptEntityInput(particle, "SetParent");  
		CreateTimer(1.0, DeleteLevelupParticle, particle);
	}
}

public Action:DeleteLevelupParticle(Handle:timer, any:particle)
{
	if (IsValidEntity(particle))
	{
		new String:classN[64];
		GetEdictClassname(particle, classN, sizeof(classN));
		if (StrEqual(classN, "info_particle_system", false))
		{
			RemoveEdict(particle);
		}
	}
}
