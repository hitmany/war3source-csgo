#pragma semicolon 1
 
#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
#include <sdktools_functions>
#include <sdktools_tempents>
#include <sdktools_tempents_stocks>
#include <sdkhooks>
#include <emitsoundany>

new thisRaceID;

//Flashlight
new bool:player_inuse[MAXPLAYERS+1];
new bool:flashlight_enabled[MAXPLAYERS+1];
new bool:claw_owner[MAXPLAYERS+1];
new flashbattery[MAXPLAYERS+1];

new player_colored[MAXPLAYERS+1];
new player_colored_left[MAXPLAYERS+1];

//Rage
new Float:RageChance[]={0.0,0.01,0.02,0.03,0.04,0.05,0.06};

//Claw
new clawdamage=6;
new Float:durationsilence=8.0;
new Float:ClawCooldown[]={0.0,60.0,55.0,50.0,45.0,40.0,30.0};

//Ward
new Float:WardChance[]={0.0,0.01,0.03,0.05,0.06,0.08,0.1};
new Float:WardFireSpeed=250.0;

//Ultimate
new Float:UltimateFireSpeed=1000.0;
new Float:FireballsNumber[]={0.0,0,2,3,5,8,10};


//Skills & Ultimate
new SKILL_RAGE, SKILL_CLAW, SKILL_WARD, ULT_FIREBALL;

//Offsets
new m_OffsetClrRender=-1;

new String:fire_expsnd[]="*diablomod.net/fire_explode.mp3";
new String:fireballsnd[]="*diablomod.net/firelaunch2.mp3";
new player_fireball_sprite[2049];
new fireball_count[MAXPLAYERS+1];

/*new String:death_flash[]="*war3source/death/death_flash.mp3";
new String:death_rage[]="*war3source/death/death_rage.mp3";
new String:death_ult[]="*war3source/death/death_ult.mp3";
new String:death_ward[]="*war3source/death/death_ward.mp3";*/

new Laser;

public Plugin:myinfo = 
{
	name = "War3Source Race - Deathwing",
	author = "hitmany",
	description = "Deathwing race of warcraft",
	version = "alpha",
	url = ""
}

public OnPluginStart()
{
	LoadTranslations("w3s.race.deathwing.phrases");
	
	CreateTimer(0.2,TimerEveryQuarSecond,_,TIMER_REPEAT);
	CreateTimer(1.0,TimerEverySecond,_,TIMER_REPEAT);
}

public OnMapStart()
{
	FakePrecacheSound("items/flashlight1.wav");
	
	FakePrecacheSound("weapons/hegrenade/explode5.wav");
	FakePrecacheSound(fire_expsnd);
	AddFileToDownloadsTable("sound/diablomod.net/fire_explode.mp3");
	FakePrecacheSound(fireballsnd);
	AddFileToDownloadsTable("sound/diablomod.net/firelaunch2.mp3");
	
	/*FakePrecacheSound(death_flash);
	FakePrecacheSound(death_rage);
	FakePrecacheSound(death_ult);
	FakePrecacheSound(death_ward);
	AddFileToDownloadsTable("sound/war3source/death/death_flash.mp3");
	AddFileToDownloadsTable("sound/war3source/death/death_rage.mp3");
	AddFileToDownloadsTable("sound/war3source/death/death_ult.mp3");
	AddFileToDownloadsTable("sound/war3source/death/death_ward.mp3");*/
	
	m_OffsetClrRender=FindSendPropOffs("CBaseAnimating","m_clrRender");
}

public OnAutoConfigsBuffered()
{
	FakePrecacheSound("items/flashlight1.wav");
	
	FakePrecacheSound("weapons/hegrenade/explode5.wav");
	FakePrecacheSound(fire_expsnd);
	AddFileToDownloadsTable("sound/diablomod.net/fire_explode.mp3");
	FakePrecacheSound(fireballsnd);
	AddFileToDownloadsTable("sound/diablomod.net/firelaunch2.mp3");
	
	PrecacheModel("materials/sprites/diablo/fireball.vmt");
	PrecacheModel("materials/sprites/diablo/fireball_small.vmt");
	
	AddFileToDownloadsTable("materials/sprites/diablo/fireball.vmt");
	AddFileToDownloadsTable("materials/sprites/diablo/fireball.vtf");
	
	AddFileToDownloadsTable("materials/sprites/diablo/fireball_small.vmt");
	AddFileToDownloadsTable("materials/sprites/diablo/fireball_small.vtf");
	
	Laser=PrecacheModel("materials/sprites/laserbeam.vmt");
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==270)
	{
		thisRaceID=War3_CreateNewRaceT("deathwing");
		SKILL_RAGE=War3_AddRaceSkillT(thisRaceID,"1",false,6);
		SKILL_CLAW=War3_AddRaceSkillT(thisRaceID,"2",false,6);
		SKILL_WARD=War3_AddRaceSkillT(thisRaceID,"3",false,6);
		ULT_FIREBALL=War3_AddRaceSkillT(thisRaceID,"4",true,6);
		War3_CreateRaceEnd(thisRaceID);
		W3SkillCooldownOnSpawn( thisRaceID, ULT_FIREBALL, 1, _);
	}

}

public OnW3TakeDmgBullet(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim){
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		
		if(vteam!=ateam)
		{
			new skill_rage_level=War3_GetSkillLevel(victim,thisRaceID,SKILL_RAGE);
			new skill_ward_level=War3_GetSkillLevel(attacker,thisRaceID,SKILL_WARD);
			if(War3_GetRace(victim)==thisRaceID && skill_rage_level>0)
			{
				if(!W3HasImmunity(attacker,Immunity_Skills)&&!Hexed(victim,false))
				{
					if(GetRandomFloat(0.0,1.0)<=RageChance[skill_rage_level])
					{
						//EmitSoundToAll(death_rage, victim);
						FakeClientCommandEx(attacker, "drop");
					}
				}
			}
			if(War3_GetRace(attacker)==thisRaceID && skill_ward_level>0)
			{
				if(!W3HasImmunity(victim,Immunity_Skills)&&!Hexed(attacker,false))
				{
					if(GetRandomFloat(0.0,1.0)<=WardChance[skill_ward_level])
					{
						//EmitSoundToAll(death_ult, victim);
						item_fireball(attacker, 2, victim);
					}
				}
			}
		}
	}
}

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public OnClientDisconnect(client)
{
	SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}



public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon) 
{
	if (IsPlayerAlive(client) && !IsFakeClient(client))
	{
		new fCurFlags = GetEntityFlags(client);
		if (!player_inuse[client] && buttons & IN_USE)
		{
			if(War3_GetRace(client)==thisRaceID)
			{
				new skill_claw_level=War3_GetSkillLevel(client,thisRaceID,SKILL_CLAW);
				if(skill_claw_level>0)
				{
					if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_CLAW,true))
					{
						War3_CooldownMGR(client,ClawCooldown[skill_claw_level],thisRaceID,SKILL_CLAW);
						ToggleFlashlight(client);
						flashbattery[client] = 8;
					}
				}
			}
			player_inuse[client] = true;
		}
		else if(player_inuse[client] && !(buttons & IN_USE))
		{
			player_inuse[client] = false;
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && ValidPlayer(client,true)){
		if(!Silenced(client)){
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT_FIREBALL,true)){
				new ult_fire=War3_GetSkillLevel(client,thisRaceID,ULT_FIREBALL);
				if(ult_fire>0){
					item_fireball(client, 1, 0);
					War3_CooldownMGR(client,30.0,thisRaceID,ULT_FIREBALL);
					//EmitSoundToAll(death_ward, client);
					if(ult_fire>1)
					{
						fireball_count[client]=FireballsNumber[ult_fire];
						CreateTimer(0.5, Timer_Fireball, client);
					}
				}
				else
				{
					PrintHintText(client,"%t", "Level your Elementium Bolt first");
				}
			}
		}	
		else
		{
			PrintHintText(client,"%t", "Silenced: Can not cast");
		}
	}
}

public Action:Timer_Fireball(Handle:timer, any:client)
{
	item_fireball(client, 3, 0);
	fireball_count[client]--;
	if(fireball_count[client]>0)
	{
		CreateTimer(0.5, Timer_Fireball, client);
	}
}

ToggleFlashlight(client) 
{	
	SetEntProp(client, Prop_Send, "m_fEffects", GetEntProp(client, Prop_Send, "m_fEffects") ^ 4);
	//EmitSoundToAll("items/flashlight1.wav", client);
	//EmitSoundToAll(death_flash, client);
	
	new flash = GetEntProp(client, Prop_Send, "m_fEffects");
	if(flash == 0)
	{
		flashlight_enabled[client] = false;
	}
	else
	{
		flashlight_enabled[client] = true;
	}
}

public Action:TimerEveryQuarSecond(Handle:timer,any:userid)
{
	for(new client=1;client<=MaxClients;client++)
	{
		if(ValidPlayer(client,true))
		{
			if(flashlight_enabled[client])
			{
				new target = GetClientAimTarget(client,false);
				if(ValidPlayer(target, true))
				{
					if(GetClientTeam(client) != GetClientTeam(target))
					{
						if(!W3HasImmunity(target,Immunity_Skills))
						{
							if(!player_colored[target])
							{
								new colorpick = GetRandomInt(1,3);
								new r, g, b;
								switch(colorpick)
								{
									case 1:
									{
										r = GetRandomInt(245,255);
										g = GetRandomInt(0,255);
									}
									case 2:
									{
										g = GetRandomInt(245,250);
										b = GetRandomInt(0,240);
									}
									case 3:
									{
										b = GetRandomInt(245,255);
										r = GetRandomInt(0,255);
									}
								}
								SetPlayerColored(target,r,g,b,255,8);
								War3_DealDamage(target,clawdamage,claw_owner[target],DMG_BURN,"deathclaw",_,W3DMGTYPE_MAGIC);
								claw_owner[target]=client;	
								CreateTimer(durationsilence, Timer_SilenceGone, target);
								War3_SetBuff(target,bSilenced,thisRaceID,true);
								War3_SetBuff(target, bInvisibilityDenyAll, thisRaceID, true );
							}
						}
					}
				}
			}
		}
	}
}

stock SetPlayerColored(client,r,g,b,alpha,time)
{
	if(player_colored[client] == 0)
	{
		player_colored[client] = 1;
	}
	W3SetPlayerColor(client,thisRaceID,r,g,b,alpha,GLOW_ULTIMATE);	
	player_colored_left[client] = time;
}

stock ResetPlayerRGB(client)
{
	W3ResetPlayerColor(client, thisRaceID);
	SetEntityRenderMode(client,RENDER_NORMAL);
}

stock AddInFrontOf(const Float:vecOrigin[3], const Float:vecAngle[3], Float:units, Float:output[3])
{
	decl Float:vecView[3];
	GetAngleVectors(vecAngle, vecView, NULL_VECTOR, NULL_VECTOR);
    
	output[0] = vecView[0] * units + vecOrigin[0];
	output[1] = vecView[1] * units + vecOrigin[1];
	output[2] = vecView[2] * units + vecOrigin[2];
}

stock bool:IsOdd(num)
{
    return (num & 1);
}

public Action:TimerEverySecond(Handle:timer,any:userid)
{
	for(new client=1;client<=MaxClients;client++)
	{
		if(ValidPlayer(client,true))
		{
			if(flashlight_enabled[client])
			{
				if(flashbattery[client] == 0)
				{
					ToggleFlashlight(client);
				}
				else
				{
					flashbattery[client]--;
				}
			}
			if(player_colored[client] == 1)
			{
				if(ValidPlayer(claw_owner[client]))
				{
					if(player_colored_left[client] < 1)
					{
						ResetPlayerRGB(client);
						player_colored[client] = 0;
						W3FlashScreen(client,RGBA_COLOR_BLUE,1.0,1.0,0x0010);
						claw_owner[client] = 0;
						War3_SetBuff(client, bInvisibilityDenyAll, thisRaceID, false );
					}
					else
					{
						player_colored_left[client]--;
						War3_DealDamage(client,clawdamage,claw_owner[client],DMG_BURN,"deathclaw",_,W3DMGTYPE_MAGIC);
						new colorpick = GetRandomInt(1,3);
						new r, g, b;
						switch(colorpick)
						{
							case 1:
							{
								r = GetRandomInt(245,255);
								g = GetRandomInt(0,255);
							}
							case 2:
							{
								g = GetRandomInt(245,250);
								b = GetRandomInt(0,240);
							}
							case 3:
							{
								b = GetRandomInt(245,255);
								r = GetRandomInt(0,255);
							}
						}
						W3SetPlayerColor(client,thisRaceID,r,g,b,255,GLOW_ULTIMATE);						
						if(IsOdd(player_colored_left[client]))
						{
						
						}
						else
						{
							W3FlashScreen(client,RGBA_COLOR_RED,1.0,1.0,0x0010);
						}
					}
				}
				else
				{
					ResetPlayerRGB(client);
					player_colored[client] = 0;
					W3FlashScreen(client,RGBA_COLOR_BLUE,1.0,1.0,0x0010);
					claw_owner[client] = 0;
					War3_SetBuff(client, bInvisibilityDenyAll, thisRaceID, false );
				}
			}
		}
	}
}

public Action:Timer_SilenceGone(Handle:timer, any:client)
{
	War3_SetBuff(client,bSilenced,thisRaceID,false);
	PrintHintText(client,"%T","Your skills enabled!",client);
}

public Action:OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype, &weapon, Float:damageForce[3], Float:damagePosition[3])
{
	if(ValidPlayer(victim))
	{
		new String:targetname[60];
		GetEntPropString(inflictor, Prop_Data, "m_iName", targetname, sizeof(targetname), 0);
		if(StrEqual(targetname, "fireball", false))
		{	
			if(W3HasImmunity(victim,Immunity_Skills))
			{
				return Plugin_Handled;
			}
			else if(victim == attacker)
			{
				return Plugin_Handled;
			}
			else
			{
				damage = 100.0;				
				return Plugin_Changed;
			}
		}
		if(StrEqual(targetname, "fireball2", false))
		{	
			if(victim == attacker)
			{
				return Plugin_Handled;
			}
			else
			{
				damage = 15.0;				
				return Plugin_Changed;
			}
		}
		if(StrEqual(targetname, "fireball3", false))
		{	
			if(victim == attacker)
			{
				return Plugin_Handled;
			}
			else
			{
				damage = 25.0;				
				return Plugin_Changed;
			}
		}
	}
	
	return Plugin_Continue;
}

public item_fireball(client, type, target)
{
//1 - big, 2 - small
	
	if (IsPlayerAlive(client))
	{
		new fireball = CreateEntityByName("env_sprite_oriented");
		new smokeEnt = CreateEntityByName("smokegrenade_projectile");
		player_fireball_sprite[smokeEnt] = fireball;
		if(IsValidEntity(fireball) && IsValidEntity(smokeEnt))
		{
			new Float:Clientpos[3];
			GetClientAbsOrigin(client,Clientpos);
			
			if(type == 1)
			{
				DispatchKeyValue(fireball, "model", "materials/sprites/diablo/fireball.vmt");  
			}
			else
			{
				DispatchKeyValue(fireball, "model", "materials/sprites/diablo/fireball_small.vmt");
			}
			
			DispatchKeyValue(fireball, "spawnflags", "1");
			DispatchKeyValue(fireball, "rendermode", "5");
			
			SetEntPropEnt(fireball, Prop_Send, "m_hOwnerEntity", client);
			SetEntPropEnt(smokeEnt, Prop_Send, "m_hOwnerEntity", client);
			
			DispatchKeyValue(smokeEnt, "spawnflags", "1");
			DispatchKeyValue(smokeEnt, "rendermode", "0");
			DispatchKeyValue(smokeEnt, "renderfx", "0");
			DispatchKeyValue(smokeEnt, "rendercolor", "0 0 0");
			DispatchKeyValue(smokeEnt, "renderamt", "0");

			decl Float:vecOrigin[3];
			decl Float:vecAngles[3];
			decl Float:vecVeloc[3];
			decl Float:end[3];
			
			GetClientEyePosition(client, vecOrigin);
			GetClientEyeAngles(client, vecAngles);
			
			AddInFrontOf(vecOrigin, vecAngles, 40.0, vecOrigin);
			
			GetAngleVectors(vecAngles, vecVeloc, end, NULL_VECTOR);
			ScaleVector(end, 5.0);
			AddVectors(vecOrigin, end, vecOrigin);
			
			if(type == 1)
			{
				ScaleVector(vecVeloc, UltimateFireSpeed);
			}
			else
			{
				ScaleVector(vecVeloc, WardFireSpeed);
			}
			decl Float:baseVel[3];
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", baseVel);
			AddVectors(vecVeloc, baseVel, vecVeloc);
			/*GetClientEyeAngles( client, fAng );
			GetAngleVectors(fAng, fVel, NULL_VECTOR, NULL_VECTOR);
			ScaleVector(fVel, 500.0);
			GetEntPropVector(client, Prop_Data, "m_vecVelocity", fPVel);
			AddVectors(fVel, fPVel, fVel);*/
			Clientpos[2] = Clientpos[2]+50;
			DispatchSpawn(smokeEnt);
			DispatchSpawn(fireball);
			SetEntityRenderMode(smokeEnt, RENDER_NONE);
			SetEntityMoveType(smokeEnt, MOVETYPE_FLY);
			
			// knive elasticity
			SetEntPropFloat(smokeEnt, Prop_Send, "m_flElasticity", 0.0);
			// gravity
			SetEntProp(smokeEnt, Prop_Data, "m_nNextThinkTick", -1);
			if(type == 1)
			{
				SetEntPropFloat(smokeEnt, Prop_Data, "m_flGravity", 0.0);
			}
			else
			{
				SetEntPropFloat(smokeEnt, Prop_Data, "m_flElasticity", 5.0);
				SetEntPropFloat(smokeEnt, Prop_Data, "m_flGroundSpeed", 100.0);
				SetEntProp(smokeEnt, Prop_Send, "m_nSolidType", 2);
			}
			SetVariantString("!activator");
			AcceptEntityInput(fireball, "SetParent", smokeEnt);
			EmitAmbientSound( fireballsnd, Clientpos, _, SNDLEVEL_TRAIN  );
			TeleportEntity(smokeEnt, vecOrigin, vecAngles, vecVeloc);
			if(type == 1)
			{
				SDKHook(smokeEnt, SDKHook_Touch, OnStartTouch);
			}
			else if(type == 3)
			{
				SDKHook(smokeEnt, SDKHook_Touch, OnStartTouch3);
			}
			else
			{
				SDKHook(smokeEnt, SDKHook_Touch, OnStartTouch2);
				new Handle:DataPack;  
				CreateDataTimer(0.1, Timer_Seek, DataPack, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
				WritePackCell(DataPack, fireball); 
				WritePackCell(DataPack, smokeEnt); 
				WritePackCell(DataPack, client); 
				WritePackCell(DataPack, target);

			}
		}
	}
}

public Action:Timer_Seek(Handle:timer, Handle:pack)
{
	ResetPack(pack); 
	new fireball = ReadPackCell(pack);
	new grenade = ReadPackCell(pack);
	new thrower = ReadPackCell(pack);
	new target = ReadPackCell(pack);
	if(!ValidPlayer(target, true))
		return Plugin_Stop;

	if(!ValidPlayer(target,true))
	{
	//	target = FindClosestTarget(thrower);
	//	if(!IsValidClient(target))
	//		return Plugin_Stop;
		OnStartTouch2(grenade, grenade);
	}

	new floay:targetPos[3];
	new floay:nadePos[3];
	new floay:direction[3];
	GetEntPropVector(grenade, Prop_Send, "m_vecOrigin", nadePos);
	GetClientAbsOrigin(target, targetPos);
	targetPos[2] += 30.0;
	SubtractVectors(targetPos, nadePos, direction);

	NormalizeVector(direction, direction);
	ScaleVector(direction, 280.0);
	TeleportEntity(grenade, NULL_VECTOR, NULL_VECTOR, direction);

	if(GetVectorDistance(targetPos, nadePos) <= 30.0)
	{
	//	SetEntProp(grenade, Prop_Data, "m_nNextThinkTick", 1);
	//	SDKHooks_TakeDamage(grenade, grenade, grenade, 1.0);
	//	return Plugin_Stop;
		OnStartTouch2(grenade, target);
		KillTimer(timer);
	}

	return Plugin_Continue;
}

public OnStartTouch(caller, client)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	if(owner!=client)
	{		
		new ignore = 0;
		if(IsValidEntity(caller))
		{
			new String:classname[64];
			GetEntityClassname(client, classname, sizeof(classname));
			if((StrContains(classname, "trigger", false) == 0) || (StrContains(classname, "info_", false) == 0) || (StrContains(classname, "env_", false) == 0) || (StrContains(classname, "game_", false) == 0) || (StrContains(classname, "logic_", false) == 0)  || (StrContains(classname, "point_", false) == 0) || StrEqual(classname, "func_buyzone", false) || StrEqual(classname, "func_bomb_target", false) || StrEqual(classname, "func_hostage_rescue", false) || StrEqual(classname, "func_no_defuse", false) || StrEqual(classname, "func_illusionary", false))
			{
				ignore = 1;
			}
		}
		if(ignore == 0)
		{
			new explode = CreateEntityByName("env_explosion");
			new String:StringDamage[3];
			Format(StringDamage, sizeof(StringDamage), "%d", 100);
			DispatchKeyValue(explode, "targetname", "fireball");
			SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", owner);
			
			DispatchSpawn(explode);
			SetEntProp(explode, Prop_Data, "m_iMagnitude", 150);
			SetEntProp(explode, Prop_Data, "m_iRadiusOverride", 150);
			new Float:position[3];
			GetEntPropVector(caller, Prop_Send, "m_vecOrigin", position);
			TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
			AcceptEntityInput(explode, "Explode");
			CreateLevelupParticle(caller, "explosion_molotov_air");
			AcceptEntityInput(caller, "DispatchEffect");
			AcceptEntityInput(caller, "Kill");
			AcceptEntityInput(player_fireball_sprite[caller], "Kill");
			AcceptEntityInput(explode, "kill");
			EmitAmbientSound( fire_expsnd, position, _, SNDLEVEL_TRAIN  );
			TE_SetupSparks(position, NULL_VECTOR, 2500, 500);
			TE_SendToAll();
		}
	}
}

public OnStartTouch2(caller, client)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	if(owner!=client)
	{		
		new ignore = 0;
		if(IsValidEntity(caller))
		{
			new String:classname[64];
			GetEntityClassname(client, classname, sizeof(classname));
			if((StrContains(classname, "trigger", false) == 0) || (StrContains(classname, "info_", false) == 0) || (StrContains(classname, "env_", false) == 0) || (StrContains(classname, "game_", false) == 0) || (StrContains(classname, "logic_", false) == 0)  || (StrContains(classname, "point_", false) == 0) || StrEqual(classname, "func_buyzone", false) || StrEqual(classname, "func_bomb_target", false) || StrEqual(classname, "func_hostage_rescue", false) || StrEqual(classname, "func_no_defuse", false) || StrEqual(classname, "func_illusionary", false))
			{
				ignore = 1;
			}
		}
		if(ignore == 0)
		{
			new explode = CreateEntityByName("env_explosion");
			new String:StringDamage[3];
			Format(StringDamage, sizeof(StringDamage), "%d", 15);
			DispatchKeyValue(explode, "targetname", "fireball2");
			SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", owner);
			
			DispatchSpawn(explode);
			SetEntProp(explode, Prop_Data, "m_iMagnitude", 100);
			SetEntProp(explode, Prop_Data, "m_iRadiusOverride", 100);
			new Float:position[3];
			GetEntPropVector(caller, Prop_Send, "m_vecOrigin", position);
			TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
			AcceptEntityInput(explode, "Explode");
			CreateLevelupParticle(caller, "explosion_molotov_air");
			AcceptEntityInput(caller, "DispatchEffect");
			AcceptEntityInput(caller, "Kill");
			AcceptEntityInput(player_fireball_sprite[caller], "Kill");
			AcceptEntityInput(explode, "kill");
			EmitAmbientSound( fire_expsnd, position, _, SNDLEVEL_TRAIN  );
			TE_SetupSparks(position, NULL_VECTOR, 2500, 500);
			TE_SendToAll();
		}
	}
}

public OnStartTouch3(caller, client)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	if(owner!=client)
	{		
		new ignore = 0;
		if(IsValidEntity(caller))
		{
			new String:classname[64];
			GetEntityClassname(client, classname, sizeof(classname));
			if((StrContains(classname, "trigger", false) == 0) || (StrContains(classname, "info_", false) == 0) || (StrContains(classname, "env_", false) == 0) || (StrContains(classname, "game_", false) == 0) || (StrContains(classname, "logic_", false) == 0)  || (StrContains(classname, "point_", false) == 0) || StrEqual(classname, "func_buyzone", false) || StrEqual(classname, "func_bomb_target", false) || StrEqual(classname, "func_hostage_rescue", false) || StrEqual(classname, "func_no_defuse", false) || StrEqual(classname, "func_illusionary", false))
			{
				ignore = 1;
			}
		}
		if(ignore == 0)
		{
			new explode = CreateEntityByName("env_explosion");
			new String:StringDamage[3];
			Format(StringDamage, sizeof(StringDamage), "%d", 25);
			DispatchKeyValue(explode, "targetname", "fireball3");
			SetEntPropEnt(explode, Prop_Send, "m_hOwnerEntity", owner);
			
			DispatchSpawn(explode);
			SetEntProp(explode, Prop_Data, "m_iMagnitude", 100);
			SetEntProp(explode, Prop_Data, "m_iRadiusOverride", 100);
			new Float:position[3];
			GetEntPropVector(caller, Prop_Send, "m_vecOrigin", position);
			TeleportEntity(explode, position, NULL_VECTOR, NULL_VECTOR);
			AcceptEntityInput(explode, "Explode");
			CreateLevelupParticle(caller, "explosion_molotov_air");
			AcceptEntityInput(caller, "DispatchEffect");
			AcceptEntityInput(caller, "Kill");
			AcceptEntityInput(player_fireball_sprite[caller], "Kill");
			AcceptEntityInput(explode, "kill");
			EmitAmbientSound( fire_expsnd, position, _, SNDLEVEL_TRAIN  );
			TE_SetupSparks(position, NULL_VECTOR, 2500, 500);
			TE_SendToAll();
		}
	}
}

stock CreateLevelupParticle(ent, String:particleType[])
{
	new particle = CreateEntityByName("info_particle_system");

	decl String:name[64];

	if (IsValidEdict(particle))
	{
		new Float:position[3];
		GetEntPropVector(ent, Prop_Send, "m_vecOrigin", position);
		TeleportEntity(particle, position, NULL_VECTOR, NULL_VECTOR);
		GetEntPropString(ent, Prop_Data, "m_iName", name, sizeof(name));
		DispatchKeyValue(particle, "targetname", "tf2particle");
		DispatchKeyValue(particle, "parentname", name);
		DispatchKeyValue(particle, "effect_name", particleType);
		DispatchSpawn(particle);
		SetVariantString(name);
		AcceptEntityInput(particle, "SetParent", particle, particle, 0);
		ActivateEntity(particle);
		AcceptEntityInput(particle, "start");
		CreateTimer(2.0, DeleteLevelupParticle, particle);
	}
}

public Action:DeleteLevelupParticle(Handle:timer, any:particle)
{
	if (IsValidEntity(particle))
	{
		new String:classN[64];
		GetEdictClassname(particle, classN, sizeof(classN));
		if (StrEqual(classN, "info_particle_system", false))
		{
			RemoveEdict(particle);
		}
	}
}