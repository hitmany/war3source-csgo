
//#undef REQUIRE_EXTENSIONS
//#undef AUTOLOAD_EXTENSIONS

#include "W3SIncs/War3Source_Interface"
//#include "W3SIncs/War3Source_Shopitems"
#include <dbi>  

new Handle:XPMultiplierCVar;

new Handle:db;
new bool:IsAPremium[ MAXPLAYERS + 1 ];
new tome;

public Plugin:myinfo = 
{
    name = "W3S - Addon Premium XP",
    author = "HiTmAnY",
    description = "XP multiplier for premium",
    version = "1.2",
};

public OnPluginStart()
{
	XPMultiplierCVar = CreateConVar("war3_premium_type1_multi","2.0","XP multiplier for premium level 1");
    SQL_Init_Connect(db);
    SQL_FastQuery(db, "SET NAMES \"UTF8\"");
	tome = War3_GetItemIdByShortname("tome");
}

public SQL_Init_Connect(&Handle:DbHNDL)
{
    // Errormessage Buffer
	  new String:Error[255];
	
	  // COnnect to the DB
    DbHNDL = SQL_Connect("shop", true, Error, sizeof(Error));
        
   if(DbHNDL == INVALID_HANDLE)
	{
		SetFailState(Error);
	}
  
  return Plugin_Handled;
}

public SQL_ErrorCheckCallBack(Handle:owner, Handle:hndl, const String:error[], any:id)
{
	// This is just an errorcallback for function who normally don't return any data
	if(IsClientConnected(id))
	{
		new authid[32];
		GetClientAuthString(id, authid, 32);
		
		if(hndl == INVALID_HANDLE)
		{
			LogToFile ("conn_error.log"," %s", error);
			SetFailState("Query failed! %s", error);
		}
		if(SQL_GetRowCount(hndl) > 0)
		{
			IsAPremium[ id ] = 1;
		}
	}
}

public OnClientPutInServer(id) 
{
	new authid[32];
	GetClientAuthString(id, authid, 32);
	//LogToFile ("all.log","Name: %s | SteamID: %s | IP: %s", name, authid, ip);
	new String:Query[255];
	Format(Query, sizeof(Query), "SELECT `id` FROM  `vip_frankfurt` WHERE  `steamid` LIKE  '%s';", authid);
	
	 // Send our Query to the Function
	//LogToFile ("check.log"," %s", Query);
	if(IsClientConnected(id))
	{
		SQL_TQuery(db, SQL_ErrorCheckCallBack, Query, id);
	}
  
 return Plugin_Handled;
}


public OnClientDisconnect(client)
{
   IsAPremium[ client ] = 0;
}

public OnWar3Event(W3EVENT:event,client)
{
        if(event==OnPreGiveXPGold)
        {
            new xp = W3GetVar(EventArg2);
            
            if((IsAPremium[ client ] == 1))
            {
                W3SetVar(EventArg2,RoundToFloor(xp * GetConVarFloat(XPMultiplierCVar)));
            }
        }
		/*if(event==OnPostGiveXPGold)
        {
			War3_SetOwnsItem(client,tome,false);
		}*/
}
