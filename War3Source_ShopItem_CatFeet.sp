/**
* File: War3Source_ShopItem_CatFeet.sp
* Description: Shopmenu Item for war3source - silence player's footsteps.
* Author(s): Remy Lebeau
*/

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include "W3SIncs/War3Source_Interface"

new thisItem;
new Handle:sv_footsteps;

public Plugin:myinfo= {
	name="War3Source Shopitem - Cat's Feet",
	author="Remy Lebeau",
	description="War3Source",
	version="1.0.1",
	url="sevensinsgaming.com"
};

public OnPluginStart()
{
	LoadTranslations("w3s.item.amulet.phrases");
	AddNormalSoundHook(OnNormalSoundPlayed);
	sv_footsteps = FindConVar("sv_footsteps");
	
	for(new i = 1; i <= MaxClients; i++)
    {
        if(IsClientInGame(i) && !IsFakeClient(i))    OnClientPutInServer(i);
    }
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if(!IsFakeClient(client))
	{
		if (IsPlayerAlive(client))
		{
			if(War3_GetOwnsItem(client, thisItem))
			{
				SendConVarValue(client, sv_footsteps, "0");
			}
		}
	}
}


public OnWar3LoadRaceOrItemOrdered2(num)
{
	if(num==110)
	{
		thisItem=War3_CreateShopItemT("amulet",3,5000);
	}	
}

public OnItemPurchase(client,item)
{
	if(item==thisItem&&ValidPlayer(client))
	{
		War3_SetOwnsItem(client,item,true);
		War3_ChatMessage(client,"%T","Your footsteps are silent.",client);
	}
}

public Action:OnNormalSoundPlayed(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags)
{	
	if (0 < entity <= MaxClients)
	{
		if(StrContains(sample, "physics") != -1 || StrContains(sample, "footsteps") != -1)
		{
			// Player not ninja, play footsteps
			if(!War3_GetOwnsItem(entity, thisItem))
			{
				numClients = 0;

				for(new i = 1; i <= MaxClients; i++)
				{
					if(IsClientInGame(i) && !IsFakeClient(i))
					{
						clients[numClients++] = i;
					}
				}

				if(StrContains(sample, "suit_") == -1) EmitSound(clients, numClients, sample, entity);
				
				//return Plugin_Changed;
			}
			return Plugin_Stop;
		}
	}
	return Plugin_Continue;
}

public OnClientPutInServer(client)
{
    if(!IsFakeClient(client))        SendConVarValue(client, sv_footsteps, "0");
}
/*
public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
    if(ValidPlayer (client, true))
    {
        if(buttons & (IN_FORWARD | IN_BACK | IN_MOVELEFT | IN_MOVERIGHT | IN_JUMP) && !(buttons & IN_USE) && War3_GetOwnsItem(client, thisItem) && GetEntProp(client, Prop_Send, "m_fFlags") & FL_ONGROUND)
        {
            SetEntProp(client, Prop_Send, "m_fFlags", GetEntProp(client, Prop_Send, "m_fFlags") & ~FL_ONGROUND);
        }
    }
    return Plugin_Continue;
}  
*/

public OnWar3EventDeath(victim){
	if(War3_GetOwnsItem(victim,thisItem)){
		War3_SetOwnsItem(victim,thisItem,false);
	}
}