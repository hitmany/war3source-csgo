/**
 * 00. Includes
 * 01. Globals
 * 02. Forwards
 * 03. Events
 * 04. Functions
 */
 
// 00. Includes     
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdktools_functions>
#include <sdkhooks>

// 01. Globals
#define GOVIP_MAINLOOP_INTERVAL 0.1
#define GOVIP_MAXPLAYERS 64
#define MAX_AMMO_LENGTH 50

enum VIPState {
	VIPState_WaitingForMinimumPlayers = 0,
	VIPState_Playing
};

new CurrentVIP = 0;
new LastVIP = 0;
new VIPState:CurrentState = VIPState_WaitingForMinimumPlayers;
new Handle:CVarMinCT = INVALID_HANDLE;
new Handle:CVarMinT = INVALID_HANDLE;
new MyWeaponsOffset = 0;
new Handle:RescueZones = INVALID_HANDLE;
new bool:RoundComplete = false;
new const g_iaGrenadeOffsets[] = {15, 17, 16, 14, 18, 17};

//Sounds
new String:vipsnd[]="*hitmany_vip/vip.mp3";
new String:elimsnd[]="*hitmany_vip/elim.mp3";

// 02. Forwards
public OnPluginStart() {
	CVarMinCT = CreateConVar("govip_min_ct", "2", "Minimum number of CTs to play GOVIP");
	CVarMinT = CreateConVar("govip_min_t", "1", "Minimum number of Ts to play GOVIP");
	
	CurrentState = VIPState_WaitingForMinimumPlayers;
	
	CreateTimer(GOVIP_MAINLOOP_INTERVAL, GOVIP_MainLoop, INVALID_HANDLE, TIMER_REPEAT);

	HookEvent("round_start", Event_RoundStart);
	HookEventEx("player_death", Event_PlayerDeath);
	//HookEventEx("player_spawn", Event_PlayerSpawn);
	
	MyWeaponsOffset = FindSendPropOffs("CBaseCombatCharacter", "m_hMyWeapons");
	
	RescueZones = CreateArray();
	
	RoundComplete = false;
	
	for(new i=1;i<=MaxClients;i++)
	{
		if(IsClientInGame(i) && !IsFakeClient(i) && IsClientAuthorized(i))
		{
			OnClientPutInServer(i);
		}
	}
	
	AddFileToDownloadsTable("sound/hitmany_vip/vip.mp3");
	FakePrecacheSound(vipsnd);
	FakePrecacheSound(elimsnd);
	AddFileToDownloadsTable("sound/hitmany_vip/elim.mp3");
}

public OnAutoConfigsBuffered()
{
	AddFileToDownloadsTable("sound/hitmany_vip/vip.mp3");
	FakePrecacheSound(vipsnd);
	FakePrecacheSound(elimsnd);
	AddFileToDownloadsTable("sound/hitmany_vip/elim.mp3");
}

stock FakePrecacheSound( const String:szPath[] )
{
	AddToStringTable( FindStringTable( "soundprecache" ), szPath );
}

public OnClientPutInServer(client) {
	SDKHook(client, SDKHook_WeaponCanUse, OnWeaponCanUse);
}

public OnClientDisconnect(client) {
	if(CurrentState != VIPState_Playing || client != CurrentVIP || RoundComplete) {
		return;
	}
	
	RoundComplete = true;
	
	LastVIP = CurrentVIP;
	
	CurrentVIP = 0;
	
	PrintToChatAll("%s", "[GO:VIP] ВИП вышел, раунд заканчивается в ничью.");
	
	CS_TerminateRound(5.0, CSRoundEnd_Draw);
}

public OnMapStart() {
	new String:buffer[512];
	
	new trigger = -1;
	while((trigger = FindEntityByClassname(trigger, "trigger_multiple")) != -1) {
		GetEntPropString(trigger, Prop_Data, "m_iName", buffer, sizeof(buffer));
		if(StrContains(buffer, "vip_rescue_zone", false) == 0) {
			SDKHook(trigger, SDKHook_Touch, TouchRescueZone);
		}
	}
	
	ClearArray(RescueZones);
	
	GetCurrentMap(buffer, sizeof(buffer));
	
	new Handle:kv = CreateKeyValues("RescueZones");
	
	new String:path[1024];
	BuildPath(Path_SM, path, sizeof(path), "configs/rescue_zones.cfg");
	
	FileToKeyValues(kv, path);
	
	if(KvJumpToKey(kv, buffer)) {
		KvGotoFirstSubKey(kv);
		
		do {
			new Float:radius = KvGetFloat(kv, "radius", 200.0);
		
			KvGetString(kv, "coords", buffer, sizeof(buffer));
			new String:coords[3][128];
			ExplodeString(buffer, " ", coords, 3, 128);

			PrintToServer("[GO:VIP] Loading rescue zone at [%s, %s, %s] with radius of %f units.", coords[0], coords[1], coords[2], radius);
						
			new Handle:rescueZone = CreateArray();
			PushArrayCell(rescueZone, radius);
			PushArrayCell(rescueZone, StringToFloat(coords[0]));
			PushArrayCell(rescueZone, StringToFloat(coords[1]));
			PushArrayCell(rescueZone, StringToFloat(coords[2]));
			
			PushArrayCell(RescueZones, rescueZone);
		} while (KvGotoNextKey(kv));
	}	
	
	CloseHandle(kv);
}

// 03. Events
public Action:Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
	RoundComplete = false;
	
	CurrentVIP = GetRandomPlayerOnTeam(CS_TEAM_CT, LastVIP);
	
	if(CurrentState != VIPState_Playing) {
		return Plugin_Continue;
	}
	
	for (new i = 1; i <= MaxClients; i++) {
		if (IsClientInGame(i) && IsPlayerAlive(i)) {
			new iWeapon = GetPlayerWeaponSlot(i, 4);
			if (iWeapon != -1 && IsValidEdict(iWeapon)) {
				decl String:szClassName[64];
				GetEdictClassname(iWeapon, szClassName, sizeof(szClassName));
				if (StrEqual(szClassName, "weapon_c4", false)) {
					RemovePlayerItem(i, iWeapon);
					RemoveEdict(iWeapon);
				}
			}
    	}
    }
		
	RemoveMapObj();
		
	if(CurrentVIP == 0 || !IsValidPlayer(CurrentVIP)) {
		return Plugin_Continue;
	}
	
	new String:VIPName[128];
	GetClientName(CurrentVIP, VIPName, sizeof(VIPName));
	
	PrintToChatAll("[GO:VIP] \"%s\" ВИП, CT защищайте его от Террористов!", VIPName);
	CreateTimer(3.0, GiveVIPW, CurrentVIP);
	StripWeapons(CurrentVIP);
	RemoveNades(CurrentVIP);
	
	new String:buffer[512];
	
	new trigger = -1;
	while ((trigger = FindEntityByClassname(trigger, "trigger_multiple")) != -1) { 
		GetEntPropString(trigger, Prop_Data, "m_iName", buffer, sizeof(buffer));
		if(StrContains(buffer, "vip_rescue_zone", false) == 0) {
			SDKHook(trigger, SDKHook_Touch, TouchRescueZone);
		}
	}
	
	return Plugin_Continue;
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast) {
	if(CurrentState != VIPState_Playing) {
		return Plugin_Continue;
	}
	
	new attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	
	new userid = GetEventInt(event, "userid");
	new client = GetClientOfUserId(userid);
	
	if(client != CurrentVIP || RoundComplete) {
		return Plugin_Continue;
	}
	
	RoundComplete = true;
	
	CS_TerminateRound(5.0, CSRoundEnd_TerroristWin);
	
	PrintToChatAll("%s", "[GO:VIP] VIP погиб, Террористы победили!");
	
	LastVIP = CurrentVIP;
	
	CurrentVIP = 0;
	
	new currency =  GetEntProp(attacker, Prop_Send, "m_iAccount");
	new newCurrency = currency + 2500;
	if(newCurrency > 16000)
	{
		newCurrency = 16000;
	}
	SetEntProp(attacker, Prop_Send, "m_iAccount", newCurrency);
	
	for(new i=1;i<=MaxClients;i++)
	{
		 if(ValidPlayer(i)&&GetClientTeam(i)==2)
		 {
			currency =  GetEntProp(i, Prop_Send, "m_iAccount");
			newCurrency = currency + 3500;
			if(newCurrency > 16000)
			{
				newCurrency = 16000;
			}
			SetEntProp(i, Prop_Send, "m_iAccount", newCurrency);
			EmitSoundToClient(i,elimsnd, SOUND_FROM_PLAYER, SNDCHAN_STATIC);
		}
	}
	
	return Plugin_Continue;
}

stock bool:ValidPlayer(client,bool:check_alive=false,bool:alivecheckbyhealth=false) {
  if(client>0 && client<=MaxClients && IsClientConnected(client) && IsClientInGame(client))
  {
    if(check_alive && !IsPlayerAlive(client))
    {
      return false;
    }
    if(alivecheckbyhealth&&GetClientHealth(client)<1) {
      return false;
    }
    return true;
  }
  return false;
}

/*public Action:Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast) {
	if(CurrentState != VIPState_Playing) {
		PrintToChatAll("notplaying");
		return Plugin_Continue;
	}
	
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client == CurrentVIP) 
	{
		PrintToChatAll("init vip");
		
	}
	//new String:VIPWeapon[256];
	//GetConVarString(CVarVIPWeapon, VIPWeapon, sizeof(VIPWeapon));

	
	return Plugin_Continue;
}*/

public Action:GiveVIPW(Handle:timer, any:client)
{
	if (IsClientInGame(client) && IsPlayerAlive(client))
	{
		StripWeapons(client);
		RemoveNades(client);
		
		//new index = CreateEntityByName("weapon_usp_silencer");
		
		GivePlayerItem(client, "weapon_usp_silencer");
		//Weapon_SetPrimaryClip(weapon, 24);
		new hClientWeapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
		if (hClientWeapon)
		{
			SetPlayerWeaponAmmo(client, hClientWeapon, 12, 12);
		}
		
		SetEntProp(client, Prop_Send, "m_bHasHelmet", 1);
		SetEntProp(client, Prop_Data, "m_ArmorValue", 200, 1);  
		GivePlayerItem(client, "weapon_knife");
		
		for(new i=1;i<=MaxClients;i++)
		{
			 if(ValidPlayer(i,true)&&GetClientTeam(i)==3)
			 {
				EmitSoundToClient(i,vipsnd, SOUND_FROM_PLAYER, SNDCHAN_STATIC);
			}
		}
		PrintHintText(client, "[GO:VIP] Вы ВИП. Бегите к вертолету");
		PrintToChat(client, "[GO:VIP] Вы ВИП. Вы должны выжить и добежать до вертолета!");
	}
	
	return Plugin_Continue;
}

stock SetPlayerWeaponAmmo(client, weaponEnt, clip = -1, ammo = -1)
{
    if (weaponEnt == INVALID_ENT_REFERENCE)
        return;

    if (clip != -1)
        SetEntProp(weaponEnt, Prop_Send, "m_iClip1", clip);

    if (ammo != -1) {
        new iOffset = FindDataMapOffs(client, "m_iAmmo") + (Weapon_GetPrimaryAmmoType(weaponEnt) * 4);
        SetEntData(client, iOffset, ammo, 4, true);

        if (GetEngineVersion() == Engine_CSGO) {
            SetEntProp(weaponEnt, Prop_Send, "m_iPrimaryReserveAmmoCount", ammo);
        }
    }
}  

stock Weapon_GetPrimaryAmmoType(weapon)
{
    return GetEntProp(weapon, Prop_Data, "m_iPrimaryAmmoType");
}

stock lib_GetSecondaryAmmo(weapon)
{
    return GetEntProp(weapon, Prop_Send, "m_iPrimaryReserveAmmoCount");
}

public Action:CS_OnBuyCommand(client, const String:weapon[])
{
    if(client == CurrentVIP) 
	{
		PrintHintText(client, "[GO:VIP] ВИП не может покупать оружие");
		PrintToChat(client, "[GO:VIP] ВИП не может покупать оружие")
		return Plugin_Handled; // Block the buy.
	}
	new team = GetClientTeam(client);
	//TERR
	if(team == 2) 
	{
        if(StrEqual(weapon, "weapon_xm1014", false) || StrEqual(weapon, "weapon_nova", false) || StrEqual(weapon, "weapon_sawedoff", false) || StrEqual(weapon, "weapon_mag7", false) || StrEqual(weapon, "weapon_mac10", false)  || StrEqual(weapon, "weapon_mp7", false)  || StrEqual(weapon, "weapon_mp9", false) || StrEqual(weapon, "weapon_p90", false) || StrEqual(weapon, "weapon_bizon", false) || StrEqual(weapon, "weapon_ump45", false) || StrEqual(weapon, "weapon_ssg08", false) || StrEqual(weapon, "weapon_scar20", false) || StrEqual(weapon, "weapon_g3sg1", false) || StrEqual(weapon, "weapon_m249", false) || StrEqual(weapon, "weapon_negev", false))
		{
			PrintHintText(client, "[GO:VIP] Тер. не могут покупать это");
			PrintToChat(client, "[GO:VIP] Тер. не могут покупать дробовики, полуавтоматы, пулеметы, скорострелки")
			return Plugin_Handled; // Block the buy.
		}
    }
	//CT
	if(team == 3) 
	{
        if(StrEqual(weapon, "weapon_awp", false) || StrEqual(weapon, "weapon_ssg08", false))
		{
			PrintHintText(client, "[GO:VIP] КТ не могут покупать AWP и скаут");
			PrintToChat(client, "[GO:VIP] КТ не могут покупать AWP и скаут")
			return Plugin_Handled; // Block the buy.
		}
    }
    
    return Plugin_Continue; // Continue as normal.
}  

// 04. Functions
public Action:GOVIP_MainLoop(Handle:timer) {
	new CTCount = GetTeamClientCount(CS_TEAM_CT);
	new TCount = GetTeamClientCount(CS_TEAM_T);
	
	if(CurrentState == VIPState_WaitingForMinimumPlayers) {
		if(CTCount >= GetConVarInt(CVarMinCT) && TCount >= GetConVarInt(CVarMinT)) {
			CurrentState = VIPState_Playing;
			PrintToChatAll("%s", "[GO:VIP] Игра началась!");
			return Plugin_Continue;
		}
	}
	else if(CurrentState == VIPState_Playing) {
		if(TCount < GetConVarInt(CVarMinT) || CTCount < GetConVarInt(CVarMinCT)) {
			CurrentState = VIPState_WaitingForMinimumPlayers;
			PrintToChatAll("%s", "[GO:VIP] ВИП режим отключен, ожидаем игроков.");
			return Plugin_Continue;
		}
		
		if(CurrentVIP == 0) {
			RoundComplete = true;
				
			CurrentVIP = GetRandomPlayerOnTeam(CS_TEAM_CT, LastVIP);
			
			CS_TerminateRound(1.0, CSRoundEnd_GameStart); 
		}
		else if(!RoundComplete && IsValidPlayer(CurrentVIP)) {
			new Float:vipOrigin[3];
			GetClientAbsOrigin(CurrentVIP, vipOrigin);
			
			new rescueZoneCount = GetArraySize(RescueZones);
			
			for(new rescueZoneIndex = 0; rescueZoneIndex < rescueZoneCount; rescueZoneIndex++) {
				new Handle:rescueZone = GetArrayCell(RescueZones, rescueZoneIndex);
				
				new Float:rescueZoneOrigin[3];
				rescueZoneOrigin[0] = GetArrayCell(rescueZone, 1);
				rescueZoneOrigin[1] = GetArrayCell(rescueZone, 2);
				rescueZoneOrigin[2] = GetArrayCell(rescueZone, 3);
				
				new Float:rescueZoneRadius = GetArrayCell(rescueZone, 0);
				
				if(GetVectorDistance(rescueZoneOrigin, vipOrigin) <= rescueZoneRadius) {
					RoundComplete = true;
					
					LastVIP = CurrentVIP;
					
					CurrentVIP = 0;
					
					CS_TerminateRound(5.0, CSRoundEnd_CTWin);
					
					PrintToChatAll("%s", "[GO:VIP] ВИП сбежал, CT победили.");
					
					new currency =  GetEntProp(LastVIP, Prop_Send, "m_iAccount");
					new newCurrency = currency + 2500;
					if(newCurrency > 16000)
					{
						newCurrency = 16000;
					}
					SetEntProp(LastVIP, Prop_Send, "m_iAccount", newCurrency);
					
					for(new i=1;i<=MaxClients;i++)
					{
						 if(ValidPlayer(i)&&GetClientTeam(i)==3)
						 {
							currency =  GetEntProp(i, Prop_Send, "m_iAccount");
							newCurrency = currency + 3500;
							if(newCurrency > 16000)
							{
								newCurrency = 16000;
							}
							SetEntProp(i, Prop_Send, "m_iAccount", newCurrency);
						}
					}
					
					break;
				}
			}
		}
	}
	
	return Plugin_Continue;
}

public Action:OnWeaponCanUse(client, weapon2) {
	if(CurrentState != VIPState_Playing)
	{
		return Plugin_Continue;
	}
	new String:weapon[256];
	
	GetEntityClassname(weapon2, weapon, sizeof(weapon));
	new team = GetClientTeam(client);
	//TERR
	if(team == 2) 
	{
        if(StrEqual(weapon, "weapon_xm1014", false) || StrEqual(weapon, "weapon_nova", false) || StrEqual(weapon, "weapon_sawedoff", false) || StrEqual(weapon, "weapon_mag7", false) || StrEqual(weapon, "weapon_mac10", false) || StrEqual(weapon, "weapon_mp7", false)  || StrEqual(weapon, "weapon_mp9", false) || StrEqual(weapon, "weapon_p90", false) || StrEqual(weapon, "weapon_bizon", false) || StrEqual(weapon, "weapon_ump45", false) || StrEqual(weapon, "weapon_ssg08", false) || StrEqual(weapon, "weapon_scar20", false) || StrEqual(weapon, "weapon_g3sg1", false) || StrEqual(weapon, "weapon_m249", false) || StrEqual(weapon, "weapon_negev", false))
		{
			return Plugin_Handled; // Block the buy.
		}
    }
	//CT
	if(team == 3) 
	{
        if(StrEqual(weapon, "weapon_awp", false) || StrEqual(weapon, "weapon_ssg08", false))
		{
			return Plugin_Handled; // Block the buy.
		}
    }
	
	if(client == CurrentVIP) 
	{	 
		if(!(StrEqual(weapon, "weapon_knife") || StrEqual(weapon, "weapon_hkp2000")))
		{
			return Plugin_Handled;
		}
	}
	
	return Plugin_Continue;
}

public Action:Command_JoinTeam(client, const String:command[], argc)  {
	if(CurrentState != VIPState_Playing || client != CurrentVIP) {
		return Plugin_Continue;
	}
	
	PrintToChat(client, "%s", "[GO:VIP] ВИП не может менять команду.");
	return Plugin_Handled;
}

bool:IsValidPlayer(client) {
	if(!IsClientConnected(client) || !IsClientInGame(client)) {
		return false;
	}
	
	return true;
}

GetRandomPlayerOnTeam(team, ignore = 0) {
	new teamClientCount = GetTeamClientCount(team);
	
	if(teamClientCount <= 0) {
		return 0;
	}
	
	new client;
	
	do {
	    client = GetRandomInt(1, MaxClients);
	} while((teamClientCount > 1 && client == ignore) || !IsClientInGame(client) || GetClientTeam(client) != team);
	
	return client;
}

stock RemoveMapObj() {
	decl maxent,i;
	decl String:Class[65];
	maxent = GetMaxEntities();
	for (i=0;i<=maxent;i++)
	{
		if(IsValidEdict(i) && IsValidEntity(i))
		{
			GetEdictClassname(i, Class, sizeof(Class));
			if(StrContains("func_bomb_target_hostage_entity_func_hostage_rescue",Class) != -1)
			{
				RemoveEdict(i);
			}
		}
	}
}


StripWeapons(client) {
    new weaponID;
    
	for(new i = 0; i < 4; i++)
	{
		new weapon = GetPlayerWeaponSlot(client, i);
		if(weapon != -1)
		{
			RemovePlayerItem(client, weapon);
			RemoveEdict(weapon);
		}
	}
}

stock RemoveNades(iClient)
{
    while(RemoveWeaponBySlot(iClient, 3))
	{
	
	}
    for(new i = 0; i < 6; i++)
	{
        SetEntProp(iClient, Prop_Send, "m_iAmmo", 0, _, g_iaGrenadeOffsets[i]);
	}
}

stock bool:RemoveWeaponBySlot(iClient, iSlot)
{
    new iEntity = GetPlayerWeaponSlot(iClient, iSlot);
    if(IsValidEdict(iEntity)) {
        RemovePlayerItem(iClient, iEntity);
        AcceptEntityInput(iEntity, "Kill");
        return true;
    }
    return false;
}  

public TouchRescueZone(trigger, client) {
	if(!ValidPlayer(client,true)) {
		return;
	} 
	
    if(CurrentState != VIPState_Playing || client != CurrentVIP || RoundComplete) {
    	return;
    }
    
    RoundComplete = true;
    
    CS_TerminateRound(5.0, CSRoundEnd_CTWin);
    
    LastVIP = CurrentVIP;
    
	CurrentVIP = 0;
	
	PrintToChatAll("[GO:VIP] ВИП сбежал, CT победили.");
}