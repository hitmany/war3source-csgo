/**
* File: War3Source_CustomRace_Barrel.sp
* Description: The Barrel race for war3source
* Author: Don Revan
*/

#pragma semicolon 1

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
new Float:barrelspeed[]={1.0,1.1,1.15,1.17,1.20,1.22};
new Float:barrelchance[]={0.0, 0.6, 0.75, 0.80, 0.90, 0.100};
new Float:jump[6]={0.0,1.20,1.25,1.30,1.35,1.42};
new Float:extime[6]={0.0,6.00,5.25,4.50,3.35,2.20};
new Float:exradius[6]={0.0,100.0,120.0,150.0,160.0,180.0};
new exdamage[6]={0,30,35,40,45,50};// float type(old) : ew Float:exdamage[6]={0.0,40.00,65.0,70.0,95.0,115.0};
new String:Sound1[]="war3source/particle_suck1.wav";
new String:ignitesnd[]="ambient/fire/gascan_ignite1.wav";
new thisRaceID;
new BeamSprite,HaloSprite,BlackBall;
new SKILL_1,SKILL_2,SKILL_ABILITY,ULT_1;
new m_vecVelocity_0, m_vecVelocity_1, m_vecBaseVelocity;
new Handle:UltCooldownCvar = INVALID_HANDLE;
new Handle:Health = INVALID_HANDLE;
public Plugin:myinfo = { name = "War3Source Race - Special Barrel", author = "Don Revan", description = "The Barrel race for war3source", version = "1.0", url = "www.wcs-lagerhaus.de" };

public OnPluginStart()
{
	HookEvent("player_jump",PlayerJumpEvent);
	HookEvent("player_falldamage",PlayerHurt,EventHookMode_Pre);
	m_vecVelocity_0 = FindSendPropOffs("CBasePlayer","m_vecVelocity[0]");
	m_vecVelocity_1 = FindSendPropOffs("CBasePlayer","m_vecVelocity[1]");
	m_vecBaseVelocity = FindSendPropOffs("CBasePlayer","m_vecBaseVelocity");
	UltCooldownCvar=CreateConVar("war3_barrel_ultimate_cooldown","15","[W3S] Cooldown timer for barrels duplicate ultimate.");
	Health = CreateConVar("war3_barrel_ultimate_health", "400", "[W3S] Ammount of health for each duplicated barrel.");
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==010){
		thisRaceID=War3_CreateNewRace("Barrel", "barrel");
		SKILL_1=War3_AddRaceSkill(thisRaceID,"Oildrum", "Raises Speed and Longjump",false,5);
		SKILL_2=War3_AddRaceSkill(thisRaceID,"UnBreakable","60-100% Chance to Ignore Fall Damage",false,5);
		SKILL_ABILITY=War3_AddRaceSkill(thisRaceID,"Explosive Barrel","Spawns an Explosive Barrel at your aimed position(ability)",false,5);
		ULT_1=War3_AddRaceSkill(thisRaceID,"Duplicate","Spawns an Barrel at your aimed position(ultimate)",true,1);
		War3_CreateRaceEnd(thisRaceID);
	}
}

public OnMapStart()
{
	PrecacheModel("models/props_c17/oildrumchunk01a.mdl");
	PrecacheModel("models/props_c17/oildrumchunk01b.mdl");
	PrecacheModel("models/props_c17/oildrumchunk01c.mdl");
	PrecacheModel("models/props_c17/oildrumchunk01d.mdl");
	PrecacheModel("models/props_c17/oildrumchunk01e.mdl");
	PrecacheModel("models/props_c17/oildrum001.mdl");
	PrecacheModel("models/props_c17/oildrum001_explosive.mdl");
	BlackBall=PrecacheModel("sprites/strider_blackball.spr");
	BeamSprite=PrecacheModel("materials/sprites/lgtning.vmt");
	HaloSprite=PrecacheModel("materials/sprites/halo01.vmt");
	War3_PrecacheSound(Sound1);
	War3_PrecacheSound(ignitesnd);
}

CreateBarrel(client,bool:xplosivemdl=false)
{
	decl String:Name[64];
	Format(Name, sizeof(Name), "Barrel_%i",client);
	decl Float:Endpoint[3];
	War3_GetAimEndPoint(client,Endpoint);
	new entity = CreateEntityByName("prop_physics_override");
	//if(GetClientTeam(client)==3){
	if(xplosivemdl)
	SetEntityModel(entity, "models/props_c17/oildrum001_explosive.mdl");
	else
	SetEntityModel(entity, "models/props_c17/oildrum001.mdl");
	/*}
	else{
		SetEntityModel(entity, "models/props_c17/oildrum002.mdl");
	}*/
	SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", client);
	DispatchKeyValue(entity, "StartDisabled", "false");
	DispatchKeyValue(entity, "targetname", Name);
	DispatchKeyValue(entity, "disableshadows", "1");
	DispatchKeyValue(entity, "physdamagescale", "10");
	DispatchSpawn(entity);
	AcceptEntityInput(entity, "Wake");
	TeleportEntity(entity, Endpoint, NULL_VECTOR, NULL_VECTOR);
	new health = GetConVarInt(Health);
	if (health > 0)
	SetEntProp(entity, Prop_Data, "m_iHealth", health);
	else
	PrintToServer("[SM] : Failed to set barrel health!");

	PrintHintText(client,"Barrel spawned!");
	return entity;
}

CreateExplosiveBarrel(const Float:radius, magdmg, const Float:timer, client)
{
	new entity = CreateBarrel(client,true); //blub:normales barrel mit explosive mdl erstellen
	DispatchKeyValue(entity, "physdamagescale", "100");
	DispatchKeyValue(entity, "spawnflags", "256");
	AcceptEntityInput(entity, "Ignite");
	HookSingleEntityOutput(entity, "OnTakeDamage", BarrelTakeDamage, true);
	new Handle:datapack = CreateDataPack();
	WritePackCell(datapack, GetClientUserId(client));
	WritePackCell(datapack, magdmg);
	WritePackCell(datapack, entity);
	WritePackFloat(datapack, radius);
	CreateTimer(timer, TimerData_Explode, datapack, TIMER_FLAG_NO_MAPCHANGE & TIMER_DATA_HNDL_CLOSE);
	PrintHintText(client,"Explosive Barrel spawned!");
}

public BarrelTakeDamage(const String:output[], caller, activator, Float:delay)
{
	new owner = GetEntPropEnt(caller, Prop_Send, "m_hOwnerEntity");
	if(ValidPlayer(owner,false)) {
		new slevel = War3_GetSkillLevel( owner, thisRaceID, SKILL_ABILITY );
		if (slevel > 0){
			new Handle:datapack = CreateDataPack();
			WritePackCell(datapack, GetClientUserId(owner));
			WritePackCell(datapack, exdamage[slevel]);
			WritePackCell(datapack, caller);
			WritePackFloat(datapack, exradius[slevel]);
			CreateTimer(0.1, TimerData_Explode, datapack, TIMER_FLAG_NO_MAPCHANGE & TIMER_DATA_HNDL_CLOSE);
		}
	}
}

public Action:TimerData_Explode(Handle:timer, Handle:datapack){
	ResetPack(datapack);
	new client = GetClientOfUserId(ReadPackCell(datapack));
	new damage = ReadPackCell(datapack);
	new entity = ReadPackCell(datapack);
	new Float:radius = ReadPackFloat(datapack);
	new AttackerTeam = GetClientTeam(client);
	new Float:fPos[3],Float:fPos2[3];
	if(IsValidEdict(entity))
	{
		GetEntPropVector(entity, Prop_Send, "m_vecOrigin", fPos);
		AcceptEntityInput(entity, "Kill"); //we got position lets kill the barrel
		TE_SetupExplosion(fPos, BeamSprite, 6.5, 1, 4, 0, 0);
		TE_SendToAll();
		EmitSoundToAll(ignitesnd, SOUND_FROM_WORLD, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, 100, -1, fPos, NULL_VECTOR, true, 0.0);
		for(new i=1;i<=MaxClients;i++)
		{
			if(ValidPlayer(i,true) && GetClientTeam(i)!=AttackerTeam && !W3HasImmunity(i,Immunity_Skills))
			{
				GetClientAbsOrigin(i,fPos2);
				if(GetVectorDistance(fPos,fPos2) <= radius)
				{
					//Just simulate a explosion O.O
					IgniteEntity(i, 1.0);
					//EmitSoundToAll(i, ignitesnd);
					W3FlashScreen(i,RGBA_COLOR_RED);
					War3_ShakeScreen(i);
					War3_DealDamage(i,damage,client,DMG_BULLET,"env_explosion");
					//don't know env_explosion is the valve explosion entity may you want to use another classname... just uses this for clear-i-ness.
				}
			}
		}
	}
}


/*CreateExplosiveBarrel(const Float:radius, const Float:magnitude, const Float:timer, client)
{
	decl String:Name[64];
	Format(Name, sizeof(Name), "ExplosiveBarrel_%i",client);
	decl Float:Endpoint[3];
	War3_GetAimEndPoint(client,Endpoint);
	new entity = CreateEntityByName("prop_physics_override");
	SetEntityModel(entity, "models/props_c17/oildrum001_explosive.mdl");
	DispatchKeyValue(entity, "StartDisabled", "false");
	DispatchKeyValue(entity, "targetname", Name);
	DispatchKeyValue(entity, "disableshadows", "1");
	DispatchKeyValue(entity, "physdamagescale", "100");
	DispatchKeyValueFloat(entity, "exploderadius", radius);
	DispatchKeyValueFloat(entity, "explodedamage", magnitude);
	DispatchKeyValue(entity, "spawnflags", "256");
	//DispatchKeyValue(entity, "OnHealthChanged", "!self,Break,,0,-1");
	//DispatchKeyValue(entity, "OnHealthChanged", "!self,Kill,,0,-1");
	//CreateTimer(timer,barrel_addoutput,entity);
	DispatchSpawn(entity);
	AcceptEntityInput(entity, "Wake");
	//AcceptEntityInput(entity, "Ignite");
	TeleportEntity(entity, Endpoint, NULL_VECTOR, NULL_VECTOR);
	PrintHintText(client,"Explosive Barrel spawned!");
	SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", client);
	SetEntPropEnt(entity, Prop_Send, "m_hThrower", client);
}

public Action:barrel_addoutput(Handle:timer,any:ent)
{
	if(ent>0&& IsValidEntity(ent))
	{
		AcceptEntityInput(ent, "Break");
		AcceptEntityInput(ent, "Kill");
	}
}*/

public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client))
	{
		new slevel = War3_GetSkillLevel( client, thisRaceID, SKILL_ABILITY );
		if (ability==0 && slevel > 0){
			if(!Silenced(client)&&War3_SkillNotInCooldown( client, thisRaceID, SKILL_ABILITY, true ))
			{
				War3_CooldownMGR( client, GetRandomFloat(10.0,12.0), thisRaceID, SKILL_ABILITY, _, _ );
				CreateExplosiveBarrel(exradius[slevel], exdamage[slevel], extime[slevel], client);
			}
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && IsPlayerAlive(client) && pressed)
	{
		new skill_level=War3_GetSkillLevel(client,race,ULT_1);
		if(skill_level>0)
		{
			if(!Silenced(client)&&War3_SkillNotInCooldown(client,thisRaceID,ULT_1,true)){
				War3_CooldownMGR(client,GetConVarFloat(UltCooldownCvar),thisRaceID,ULT_1,_,true);
				CreateBarrel(client);
			}
		}
	}
}

public OnRaceChanged( client, oldrace, newrace )
{
	if(newrace != thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"");
		War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);

	}
	if(newrace == thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"weapon_usp;weapon_knife");
		if(ValidPlayer(client,true)){
			new ent = GivePlayerItem(client, "weapon_usp");
			SetEntityRenderColor(ent, 120, 255, 120, 180);
			PassiveSkills(client);
		}
	}
}

public OnWar3EventSpawn(client)
{
	if(War3_GetRace(client)==thisRaceID){
		new ent = GivePlayerItem(client, "weapon_usp");
		SetEntityRenderColor(ent, 120, 255, 120, 180); 
		if(IsPlayerAlive(client)){
			//if(GetClientTeam(client)==3){
			SetEntityModel(client, "models/props_c17/oildrum001.mdl");
			/*}
			else{
				SetEntityModel(client, "models/props_c17/oildrum002.mdl");
			}*/
			PassiveSkills(client);
			new Float:position[ 3 ];
			GetClientAbsOrigin( client, Float:position );
			TE_SetupGlowSprite(position, BlackBall, 1.0, 0.6, 200);
			TE_SendToAll();
		}
	}
}

public PlayerHurt(Handle:event,const String:name[],bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(ValidPlayer(client,true)&&War3_GetRace(client)==thisRaceID)
	{
		new slevel=War3_GetSkillLevel(client,thisRaceID,SKILL_2);
		if(slevel>0)
		{
			if(GetRandomFloat(0.0,1.0) < barrelchance[slevel] && !Hexed(client))
			{ 
				new falldamage = GetEventInt(event, "damage");
				if(falldamage > 0)
				{
					PrintToChat(client,"UnBreakable prevented %i falldamage",falldamage);
					SetEntityHealth(client,GetClientHealth(client)+falldamage);
					new Float:position[ 3 ];
					GetClientAbsOrigin( client, Float:position );
					TE_SetupGlowSprite(position, BlackBall, 1.0, 0.6, 200);
					TE_SendToAll();
				}
			}
		}
	}
}

public PassiveSkills(client){
	if(ValidPlayer(client,true))
	{
		new skill1=War3_GetSkillLevel(client,thisRaceID,SKILL_1);
		if(skill1)
		{
			new Float:speed=barrelspeed[skill1];
			War3_SetBuff(client,fMaxSpeed,thisRaceID,speed);
			if(IsPlayerAlive(client)){
				new Float:client_location[ 3 ];
				GetClientAbsOrigin(client,client_location);
				client_location[2]+=60;
				TE_SetupBeamRingPoint(client_location, 120.0, 60.0, BeamSprite, HaloSprite, 0, 15, 3.5, 10.0, 10.0, {255,255,255,255}, 120, 0);
				TE_SendToAll();
			}
		}
	}
}

public PlayerJumpEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	new client=GetClientOfUserId(GetEventInt(event,"userid"));
	new race=War3_GetRace(client);
	if (race==thisRaceID)
	{
		new SKILL_LEVEL=War3_GetSkillLevel(client,race,SKILL_1);
		if (SKILL_LEVEL){
			if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_1))
			{
				new Float:velocity[3]={0.0,0.0,0.0};
				velocity[0]= GetEntDataFloat(client,m_vecVelocity_0);
				velocity[1]= GetEntDataFloat(client,m_vecVelocity_1);
				velocity[0]*=jump[SKILL_LEVEL]*0.30;
				velocity[1]*=jump[SKILL_LEVEL]*0.30;
				SetEntDataVector(client,m_vecBaseVelocity,velocity,true);
				War3_CooldownMGR(client,1.5,thisRaceID,SKILL_1,_,_);
				EmitSoundToAll(Sound1,client);
			}
		}
	}
}