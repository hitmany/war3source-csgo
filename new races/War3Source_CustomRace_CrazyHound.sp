/**
* File: War3Source_Crazy_Hound.sp
* Description: The Crazy Hound race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx
*/

//vulp-edit: invisibility lowered, gravity lowered, health lowered, damage lowered.

#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

// War3Source stuff
new thisRaceID;

// Chance/Data Arrays
new Float:HoundGravity[5] = { 1.0, 0.80, 0.70, 0.60, 0.50 };
new Float:HoundSpeed[5] = { 1.0, 1.35, 1.45, 1.55, 1.65 };
new Float:DamageMultiplier[5] = { 0.0, 0.20, 0.35, 0.45, 0.50 };
new Float:HoundInvis[5] = { 1.0, 0.55, 0.44, 0.42, 0.35 };
new MaxHP[5] = { 50, 75, 100, 125, 150 };

new SKILL_GRAV, SKILL_SPEED, SKILL_DMG, SKILL_HP, SKILL_INVIS;

public Plugin:myinfo = 
{
	name = "Crazy Hound",
	author = "xDr.HaaaaaaaXx",
	description = "The Crazy Hound race for War3Source.",
	version = "1.0.0.0",
	url = ""
};

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "Crazy Hound", "crhound" );
	
	SKILL_GRAV = War3_AddRaceSkill( thisRaceID, "Strong Legs", "Low Gravity", false );
	SKILL_SPEED = War3_AddRaceSkill( thisRaceID, "Four Legs", "More Speed", false );
	SKILL_DMG = War3_AddRaceSkill( thisRaceID, "Claws of the Hound", "More Knife damage", false );
	SKILL_HP = War3_AddRaceSkill( thisRaceID, "Additional Blood", "More Health", false );
	SKILL_INVIS = War3_AddRaceSkill( thisRaceID, "Skills of the Chameleon", "Get a little Invis", false );
	
	War3_CreateRaceEnd( thisRaceID );
}

public InitPassiveSkills( client )
{
	if( War3_GetRace( client ) == thisRaceID )
	{
		new skill_levi = War3_GetSkillLevel( client, thisRaceID, SKILL_GRAV );
		new Float:gravity = HoundGravity[skill_levi];
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, gravity );
		
		new skill_speed = War3_GetSkillLevel( client, thisRaceID, SKILL_SPEED );
		new Float:speed = HoundSpeed[skill_speed];
		War3_SetBuff( client, fMaxSpeed, thisRaceID, speed );
		
		new skill_hp = War3_GetSkillLevel( client, thisRaceID, SKILL_HP );
		SetEntityHealth( client, 50 );
		War3_SetMaxHP( client, MaxHP[skill_hp] );
		if( War3_GetMaxHP( client ) > GetClientHealth( client ) )
		{
			War3_HealToMaxHP( client, ( War3_GetMaxHP( client ) - GetClientHealth( client ) ) );
		}
		
		new skill_invis = War3_GetSkillLevel( client, thisRaceID, SKILL_INVIS );
		new Float:invis = HoundInvis[skill_invis];
		War3_SetBuff( client, fInvisibilitySkill, thisRaceID, invis );
	}
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_WeaponRestrictTo( client, thisRaceID, "" );
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, 1.0 );
		War3_SetBuff( client, fInvisibilitySkill, thisRaceID, 1.0 );
	}
	else
	{
		War3_WeaponRestrictTo( client, thisRaceID, "weapon_knife" );
		if( IsPlayerAlive( client ) )
		{
			InitPassiveSkills( client );
		}
	}
}

public OnSkillLevelChanged( client, race, skill, newskilllevel )
{
	InitPassiveSkills( client );
}

public OnWar3EventSpawn( client )
{
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		InitPassiveSkills( client );
		War3_WeaponRestrictTo( client, thisRaceID, "weapon_knife" );
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_level = War3_GetSkillLevel( attacker, thisRaceID, SKILL_DMG );
			if( !Hexed( attacker, false ) && skill_level > 0 )
			{
				//War3_DealDamage( victim, RoundToFloor( damage * DamageMultiplier[skill_level] ), attacker, DMG_BULLET, "weapon_knife" ); //if killed with this skill just show the knife icon
				DealDamageWrapper(victim,attacker,RoundToFloor( damage * DamageMultiplier[skill_level] ),"weapon_knife");
				W3FlashScreen( victim, RGBA_COLOR_RED );
			}
		}
	}
}