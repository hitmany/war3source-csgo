/**
* File: War3Source_Dean_Winchester.sp
* Description: The Dean Winchester race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx
*/

#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdktools_stocks>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

#define KNIFEHIT_SOUND "weapons/knife/knife_hit3.wav"

// War3Source stuff
new thisRaceID, SKILL_COLT, SKILL_WATER, SKILL_RESP, SKILL_EVADE, ULT_DUEL;

// Chance/Data Arrays
// skill 1
new Float:DamageMultiplier[6] = {0.0,0.33,0.66,0.88,0.90}; 
new PhysRingSprite;

// skill 2
new Float:WaterChance[6] = { 0.0, 0.1, 0.15, 0.2, 0.25, 0.3 };
new String:WaterSound[] = "ambient/wind/wind_snippet2.wav";
new FunnelSprite;

// skill 3
new Float:SpawnChance[6] = { 0.0, 0.15, 0.20, 0.25, 0.30, 0.35 };
//new Float:aimtime[6]={0.0,3.0,4.5,5.0,5.5,6.0};
new Float:death_pos[MAXPLAYERS][3];

// skill 4
new Float:EvadeChance[6] = { 0.0, 0.05, 0.09, 0.14, 0.18, 0.20 };
new PupilSprite;

// skill 5
new Float:UltimateCooldown[6] = { 0.0,40.0,38.0,35.0,30.0,26.0};//{ 0.0,45.0,43.0,41.0,49.0,48.0};
//new Float:ClientTargetPos[MAXPLAYERS][3];
//new Float:ClientPos[MAXPLAYERS][3];
//new ClientTarget[MAXPLAYERS];
//new bool:bClientInDuel[MAXPLAYERS];

new CurrentRound=0;

// Other
new m_iFOV,m_vecBaseVelocity;
new HaloSprite;

public Plugin:myinfo = 
{
	name = "War3Source Race - Dean Winchester",
	author = "xDr.HaaaaaaaXx&Revan",
	description = "The Dean Winchester race for War3Source.",
	version = "1.5[wcsl]",
	url = "www.war3source.com"
};

public OnPluginStart()
{
	//HookEvent( "player_death", PlayerDeathEvent );
	HookEvent( "round_start", EventRoundStart);
	m_iFOV = FindSendPropInfo( "CBasePlayer", "m_iFOV" );
	m_vecBaseVelocity=FindSendPropOffs( "CBasePlayer", "m_vecBaseVelocity" );
	if(m_iFOV == -1 || m_vecBaseVelocity==-1) {
		SetFailState("error while sendpropoffs searching!");
	}
}

public OnMapStart()
{
	PhysRingSprite = PrecacheModel( "sprites/lgtning.vmt" );
	PupilSprite = PrecacheModel( "materials/sprites/orangeglow1.vmt" );
	FunnelSprite = PrecacheModel( "materials/effects/strider_bulge_dudv.vmt" );
	HaloSprite = PrecacheModel( "materials/sprites/halo01.vmt" );
	//Glow = PrecacheModel( "effects/redflare.vmt" );
	PrecacheSound(WaterSound);
	PrecacheSound(KNIFEHIT_SOUND);
	CurrentRound=0;
}

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "Dean Winchester", "winchesterd" );
	
	SKILL_COLT = War3_AddRaceSkill( thisRaceID, "Colt", "Cause more damage with your Colt", false, 5 );
	SKILL_WATER = War3_AddRaceSkill( thisRaceID, "Holy Water", "Disort your targets! (10 - 30 % chance) and gain more health on spawn(10 - 50 HP)", false, 5 );
	SKILL_RESP = War3_AddRaceSkill( thisRaceID, "Contract With a Demon", "Respawn yourself (15 -35 % chance)", false, 5 );
	SKILL_EVADE = War3_AddRaceSkill( thisRaceID, "Rabbit Foot", "Get Luck and Change to Evade Bullets (11 - 20 % chance)", false, 5 );
	ULT_DUEL = War3_AddRaceSkill( thisRaceID, "Demon Trap", "Set's up a Demon Trap at your aiming pos", true, 1 );
	
	W3SkillCooldownOnSpawn( thisRaceID, ULT_DUEL, 15.0, true);
	
	War3_CreateRaceEnd( thisRaceID );
}

public EventRoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
	CurrentRound++;
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
		War3_WeaponRestrictTo( client, thisRaceID, "" );
	}
	else
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.16 );
		War3_WeaponRestrictTo( client, thisRaceID, "weapon_knife,weapon_deagle" );
		if( IsPlayerAlive( client ) )
		{
			StripWeaponFromClient( client );
			GivePlayerItem( client, "weapon_deagle" );
			GivePlayerItem( client, "weapon_knife" );
		}
	}
}
new bool:bEvadedLastShot[MAXPLAYERS]=false;
public OnWar3EventSpawn( client )
{
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		bEvadedLastShot[client]=false;
		StripWeaponFromClient( client );
		GivePlayerItem( client, "weapon_deagle" );
		GivePlayerItem( client, "weapon_knife" );
		new skill = War3_GetSkillLevel( client, thisRaceID, SKILL_WATER );
		if(skill>0) {
			new health = 10*skill;
			new maxhp = GetClientHealth(client)+health;
			SetEntityHealth(client,maxhp);
			War3_SetMaxHP(client,maxhp);
			PrintToChat(client,"\x04[Holy Water]\x03Gained additional %i health",health);
		}
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_colt = War3_GetSkillLevel( attacker, thisRaceID, SKILL_COLT );
			if( !Hexed( attacker, false ) && GetRandomFloat( 0.0, 1.0 ) <= 0.15 )
			{
				if( !W3HasImmunity( victim, Immunity_Skills ) )
				{
					War3_DealDamage( victim, RoundToFloor( damage * DamageMultiplier[skill_colt] ), attacker, DMG_BULLET, "weapon_deagle" );
				
					W3PrintSkillDmgHintConsole( victim, attacker, War3_GetWar3DamageDealt(), SKILL_COLT );
					
					new Float:pos[3];
					
					GetClientAbsOrigin( victim, pos );
					
					pos[2] += 15;
					
					TE_SetupGlowSprite( pos, PhysRingSprite, 0.8, 1.0, 180 );
					TE_SendToAll();
					TE_SetupBeamRingPoint(pos,20.0,240.0,PhysRingSprite,HaloSprite,0,20,1.0,20.0,0.0,{255,180,70,100},10,0);
					TE_SendToAll();
					TE_SetupDynamicLight( pos, 255,240,20,2,120.0,1.2,1.2);
					TE_SendToAll();
				}
			}
		}
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_water = War3_GetSkillLevel( attacker, thisRaceID, SKILL_WATER );
			if( !Hexed( attacker, false ) && GetRandomFloat( 0.0, 1.0 ) <= WaterChance[skill_water] )
			{
				if( !W3HasImmunity( victim, Immunity_Skills ))
				{
					new Float:pos[3];
					
					GetClientAbsOrigin( victim, pos );
					
					TE_SetupGlowSprite( pos, FunnelSprite, 2.0, 0.8, 255 );
					TE_SendToAll();
					
					W3FlashScreen( victim, { 215, 25, 251, 75 }, 0.3, 0.3 );
					
					EmitSoundToAll( WaterSound, attacker );
					EmitSoundToAll( WaterSound, victim );
					
					SetEntData( victim, m_iFOV, 500 );
					CreateTimer( 0.1, StopFov, victim );					
				}
			}
		}
	}
}

public Action:StopFov( Handle:timer, any:client )
{
	SetEntData( client, m_iFOV, 0 );
}

public PlayerDeathEvent( Handle:event, const String:name[], bool:dontBroadcast )
{
	new client = GetClientOfUserId( GetEventInt( event, "userid" ) );
	if( War3_GetRace( client ) == thisRaceID && client != 0 )
	{
		new skill_spawn = War3_GetSkillLevel( client, thisRaceID, SKILL_RESP );
		if( skill_spawn > 0 && GetRandomFloat( 0.0, 1.0 ) <= SpawnChance[skill_spawn] )
		{
			GetClientAbsOrigin( client, death_pos[client] );
			
			CreateTimer( 2.0, Spawn, client );
			CreateTimer( 2.1, Teleport, client );
		}
	}
}

public Action:Spawn( Handle:timer, any:client )
{
	if(ValidPlayer(client,false)) {
		War3_SpawnPlayer( client );
		PrintToChat( client, "\x04[War3Source] \x01You have ecaped from Hell!" );
	}
}

public Action:Teleport( Handle:timer, any:client )
{
	if(ValidPlayer(client,true)) {
		TeleportEntity( client, death_pos[client], NULL_VECTOR, NULL_VECTOR );
		TE_SetupDynamicLight(death_pos[client], 255,20,20,7,120.0,3.0,2.0);
		TE_SendToAll();
		TE_SetupBeamRingPoint(death_pos[client],20.0,200.0,PhysRingSprite,HaloSprite,0,20,1.2,35.0,3.0,{255,50,40,100},10,0);
		TE_SendToAll();
	}
}

stock GetClientEyeAx(client) {
	decl Float:fAngles[3];
	GetClientEyeAngles(client, fAngles);
	new ax = 1;
	if(fAngles[1]<150 && fAngles[1]>25) {
		ax = 0;
		fAngles[1]=90.0;
	}
	else if(fAngles[1]>-150 && fAngles[1]<-25) {
		ax = 0;
		fAngles[1]=90.0;
	}
	return ax;
}

public OnW3TakeDmgBulletPre( victim, attacker, Float:damage )
{
	if( IS_PLAYER( victim ) && IS_PLAYER( attacker ) && victim > 0 && attacker > 0 && attacker != victim )
	{
		new vteam = GetClientTeam( victim );
		new ateam = GetClientTeam( attacker );
		if( vteam != ateam )
		{
			new race_victim = War3_GetRace( victim );
			new skill_level = War3_GetSkillLevel( victim, thisRaceID, SKILL_EVADE );
			if( race_victim == thisRaceID && skill_level > 0 && GetRandomFloat( 0.0, 1.0 ) <= EvadeChance[skill_level] )
			{
				if( !W3HasImmunity( attacker, Immunity_Skills ) )
				{
					if(!bEvadedLastShot[victim]) { //avoid double evading
						War3_DamageModPercent( 0.0 );
						W3MsgEvaded( victim, attacker );
						
						new Float:pos[3];
						
						GetClientAbsOrigin( victim, pos );
						
						pos[2] += 15;
						
						TE_SetupGlowSprite( pos, PupilSprite, 3.0, 3.0, 255 );
						TE_SendToAll();
						TE_SetupDynamicLight( pos, 255,80,80,2,120.0,1.2,1.3);
						TE_SendToAll();
						bEvadedLastShot[victim]=true;
					}
					else
					bEvadedLastShot[victim]=false;
				}
				else
				{
					W3MsgEnemyHasImmunity( victim, true );
				}
			}
		}
	}
}

/*public OnAbilityCommand(client,ability,bool:pressed) {
	if(War3_GetRace(client)==thisRaceID && pressed && IsPlayerAlive(client)) {
		if(!Silenced(client)) {
			new ult_level = War3_GetSkillLevel( client, thisRaceID, SKILL_RESP );
			if( ult_level > 0 )
			{
				if( War3_SkillNotInCooldown( client, thisRaceID, SKILL_RESP, true ) )
				{
					new target=War3_GetTargetInViewCone(client,1200.0,false,80.0);
					if(target > 0)
					{
						if(!W3HasImmunity(target,Immunity_Skills) && GetClientTeam(target) != GetClientTeam(client))
						{
							War3_CooldownMGR( client, 3.0, thisRaceID, SKILL_RESP, _, _ );
							CreateTimer(aimtime[ult_level], Timer_FinishAim, client);
							decl Handle:pack;
							CreateDataTimer(0.05, Timer_AimLoop, pack);
							WritePackCell(pack, client);
							WritePackCell(pack, target);
							WritePackCell(pack, CurrentRound);
						}
						else {
							PrintHintText(client,"Unable to target demon!");
							War3_CooldownMGR( client, 3.0, thisRaceID, SKILL_RESP, _, _ );
						}
					}
					else {
						War3_CooldownMGR( client, 3.0, thisRaceID, SKILL_RESP, _, _ );
						PrintHintText(client,"You need to face a demon");
					}
				}
			}
		}
	}
}*/

/*public Action:Timer_AimLoop( Handle:timer, Handle:data )
{
	ResetPack(data);
	new client=ReadPackCell(data);
	new target=ReadPackCell(data);
	new round=ReadPackCell(data);
	if(client>0&&target>0&&round>0) {
		if(round==CurrentRound) {
			if(ValidPlayer(client,true) && ValidPlayer(target,true) && bAiming[client]==true) {
				if(!W3HasImmunity(target,Immunity_Skills)) {
					SetAim( client, target, 0.0 );
					PrintCenterText(target,"Contract with a Demon affects you!");
				}
				else {
					PrintCenterText(client,"Contract with a Demon blocked!");
				}
				decl Handle:pack;
				CreateDataTimer(0.05, Timer_AimLoop, pack);
				WritePackCell(pack, client);
				WritePackCell(pack, target);
				WritePackCell(pack, round);
			}
		}
	}
}

public Action:Timer_FinishAim( Handle:timer, any:client ) bAiming[client]=false;*/

public OnUltimateCommand( client, race, bool:pressed )
{
	if( race == thisRaceID && pressed && IsPlayerAlive( client ) && !Silenced( client ) )
	{
		new ult_level = War3_GetSkillLevel( client, race, ULT_DUEL );
		if( ult_level > 0 )
		{
			if( War3_SkillNotInCooldown( client, thisRaceID, ULT_DUEL, true ) )
			{
				//Duel( client, ult_level );
				//CreateTimer( 1.0, Timer_TrapLoop, client );
				decl Float:aimVec[3],Float:usrPos[3];
				War3_GetAimEndPoint(client, aimVec);
				GetClientAbsOrigin(client, usrPos);
				if(GetVectorDistance(usrPos,aimVec) <= 1200.0) {
					decl Handle:pack;
					CreateDataTimer(0.1, Timer_TrapLoop, pack);
					WritePackCell(pack, client);
					WritePackCell(pack, CurrentRound);
					WritePackCell(pack, GetClientEyeAx(client));
					WritePackFloat(pack, aimVec[0]);
					WritePackFloat(pack, aimVec[1]);
					WritePackFloat(pack, aimVec[2]);
					PrintHintText(client,"Demon Trap was created!");
					War3_CooldownMGR( client, UltimateCooldown[ult_level], thisRaceID, ULT_DUEL, _, _ );
				}
				else {
					PrintHintText(client,"Target location is too far away!");
				}
			}
		}
		else
		{
			W3MsgUltNotLeveled( client );
		}
	}
}

public Action:Timer_TrapLoop( Handle:timer, Handle:data )
{
	decl Float:origin[3];
	ResetPack(data);
	new client=ReadPackCell(data);
	new round=ReadPackCell(data);
	new ax=ReadPackCell(data);
	origin[0]=ReadPackFloat(data);
	origin[1]=ReadPackFloat(data);
	origin[2]=ReadPackFloat(data);
	if (client>0 && ValidPlayer(client,true))
	{
		if(round==CurrentRound) {
			new ult_level=War3_GetSkillLevel(client,thisRaceID,ULT_DUEL);
			if(ult_level>0)
			{
				//CreateTimer(0.1, Timer_MidnightLoop, client );
				decl Handle:pack;
				CreateDataTimer(0.1, Timer_TrapLoop, pack);
				WritePackCell(pack, client);
				WritePackCell(pack, round);
				WritePackCell(pack, ax);
				WritePackFloat(pack, origin[0]);
				WritePackFloat(pack, origin[1]);
				WritePackFloat(pack, origin[2]);
				new team = GetClientTeam(client);
				for(new t=1;t<=MaxClients;t++)
				{
					if(ValidPlayer(t,true))
					{
						new Float:VictimPos[3];
						GetClientAbsOrigin(t,VictimPos);
						if(GetVectorDistance(VictimPos,origin) < 150.0 && GetClientTeam(t)!=team)
						{
							if(W3HasImmunity( t, Immunity_Ultimates ) || W3HasImmunity( t, Immunity_Wards ))
							{
								PrintCenterText(t,"You blocked a Skill!");
							}
							else
							{
								if(GetRandomInt(0,2)==2) {
									War3_DealDamage( t, 1, client, DMG_DISSOLVE, "demontrap", _, W3DMGTYPE_MAGIC);
									EmitSoundToAll(KNIFEHIT_SOUND, t, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, SNDPITCH_NORMAL);
								}
								TE_SetupBeamPoints(origin,VictimPos,PupilSprite,HaloSprite,0,20,1.1,15.0,10.0,1,1.0,{255,140,140,220},20);
								TE_SendToAll();
								origin[2]+=35;
								PushClientToVector( t, origin, 0.1 );
								origin[2]-=35;
								W3FlashScreen(t,{155,40,40,120},0.1);
							}
						}					
					}
				}
				TE_SetupGlowSprite( origin, PupilSprite, 0.4, 0.3, 255 );
				TE_SendToAll();
				if(team==2)
				TE_SetupBeamRingPoint(origin,18.0,160.0,HaloSprite,HaloSprite,0,10,0.2,40.0,1.0,{180,180,255,255},8,FBEAM_HALOBEAM);
				else
				TE_SetupBeamRingPoint(origin,18.0,160.0,HaloSprite,HaloSprite,0,10,0.2,40.0,1.0,{255,190,190,255},8,FBEAM_HALOBEAM);
				TE_SendToAll();
				//cruzi
				decl Float:origin2[3];
				origin2[0]=origin[0];
				origin2[1]=origin[1];
				origin2[2]=origin[2]+80;				
				TE_SetupBeamPoints( origin, origin2, PhysRingSprite, HaloSprite, 0, 10, 0.2, 2.5, 2.5, 0, 1.0, { 255, 255, 255, 255 }, 5 );
				TE_SendToAll();
				origin2[2]-=30;
				origin[2]+=50;
				origin[ax]+=40;
				TE_SetupBeamPoints( origin, origin2, PhysRingSprite, HaloSprite, 0, 10, 0.2, 2.5, 2.5, 0, 1.0, { 255, 255, 255, 255 }, 5 );
				TE_SendToAll();
				origin[ax]-=80;
				TE_SetupBeamPoints( origin, origin2, PhysRingSprite, HaloSprite, 0, 10, 0.2, 2.5, 2.5, 0, 1.0, { 255, 255, 255, 255 }, 5 );
				TE_SendToAll();
			}
		}
	}
}

public Action:PushClientToVector( victim, Float:pos1[3], Float:power )
{
	new Float:pos2[3], Float:main_origin[3], Float:velo1[3], Float:velo2[3];
	GetClientAbsOrigin( victim, pos2 );

	main_origin[0] = pos1[0] - pos2[0], main_origin[1] = pos1[1] - pos2[1], main_origin[2] = pos1[2] - pos2[2];
	velo1[0] += 0, velo1[1] += 0, velo1[2] += 300;
	
	velo2[0] = main_origin[0] * ( 25 * power );
	velo2[1] = main_origin[1] * ( 25 * power );
	velo2[2] = main_origin[2] * ( 25 * power );
	
	SetEntDataVector( victim, m_vecBaseVelocity, velo1, true );
	SetEntDataVector( victim, m_vecBaseVelocity, velo2, true );
}

stock TE_SetupDynamicLight(const Float:vecOrigin[3], r,g,b,iExponent,Float:fRadius,Float:fTime,Float:fDecay)
{
	TE_Start("Dynamic Light");
	TE_WriteVector("m_vecOrigin",vecOrigin);
	TE_WriteNum("r",r);
	TE_WriteNum("g",g);
	TE_WriteNum("b",b);
	TE_WriteNum("exponent",iExponent);
	TE_WriteFloat("m_fRadius",fRadius);
	TE_WriteFloat("m_fTime",fTime);
	TE_WriteFloat("m_fDecay",fDecay);
}

stock StripWeaponFromClient( client )
{
	for( new slot = 0; slot < 10; slot++ )
	{		
		new wpn = GetPlayerWeaponSlot( client, slot );
		new String:ename[64];
		if ( IsValidEdict( wpn ) )
			GetEdictClassname( wpn, ename, 64 );
		if( wpn > 0 && ( !StrEqual( ename, "weapon_c4" ) || !StrEqual( ename, "weapon_knife" ) ) )
		{
			RemovePlayerItem( client, wpn );
		}
	}
}

stock SetAim( client, aim_at, Float:add )
{
	new Float:pos1[3];
	new Float:pos2[3];
	new Float:vecang[3];
	new Float:ang[3];
	
	GetClientAbsOrigin( client, pos1 );
	GetClientAbsOrigin( aim_at, pos2 );
	
	pos2[2] += add;
	
	SubtractVectors( pos2, pos1, vecang );
	
	GetVectorAngles( vecang, ang );
	
	ang[2] = 0.0;
	
	TeleportEntity( client, NULL_VECTOR, ang, NULL_VECTOR );
}

/*stock bool:HasImmunity(client) {
	if(W3HasImmunity(client, Immunity_Ultimates) || W3HasImmunity(client, Immunity_Skills)) {
		return true;
	}
	return false;
}

PickTarget(client) {
	if( GetClientTeam( client ) == TEAM_T )
		ClientTarget[client] = GetRandomPlayer( "#ct", true );
	if( GetClientTeam( client ) == TEAM_CT )
		ClientTarget[client] = GetRandomPlayer( "#t", true );
}

Action:Duel( client, ult_level)
{
	if( GetClientTeam( client ) == TEAM_T )
		ClientTarget[client] = GetRandomPlayer( "#ct", true );
	if( GetClientTeam( client ) == TEAM_CT )
		ClientTarget[client] = GetRandomPlayer( "#t", true );
	
	if( !ValidPlayer(ClientTarget[client],true) ) {
		PrintHintText( client, "Ultimate failed - could not catch a demon!" );
	}
	else
	{
		bClientInDuel[ClientTarget[client]] = true;
		bClientInDuel[client] = true;
		
		new String:ClientTargetName[32];
		new String:ClientName[32];
		
		GetClientName( ClientTarget[client], ClientTargetName, 32 );
		GetClientName( client, ClientName, 32 );
		
		//PrintToChat( ClientTarget[client], "\x04: %s \x03has challanged you to a duel \x04Fight in 3 seconds", ClientName );
		PrintToChat( ClientTarget[client], "\x04: %s \x03caught you in his demon trap - \x04Fight in 3 seconds", ClientName );
		//PrintToChat( client, "\x03[Demon Trap]: You have Challanged \x04%s \x03to a Duel", ClientTargetName );
		PrintToChat( client, "\x03[Demon Trap]: \x04%s \x03was caught in your trap!", ClientTargetName );
		PrintHintText(ClientTarget[client],"You've entered a Demon Trap!");
		PrintCenterText(ClientTarget[client],"Prepare to face %s in 3 seconds..",ClientName);
		GetClientAbsOrigin( ClientTarget[client], ClientTargetPos[client] );
		GetClientAbsOrigin( client, ClientPos[client] );

		CreateTimer( 3.0, TeleportClient, client );
		CreateTimer( 3.1, FreezeClient, client );
		CreateTimer( 3.1, GiveDeagle, client );
		CreateTimer( 10.0, TeleportClientBack, client );
		
		War3_CooldownMGR( client, UltimateCooldown[ult_level], thisRaceID, ULT_DUEL, _, _ );
	}
}

public Action:TeleportClient( Handle:timer, any:client )
{
	if(ValidPlayer(client,false)) 
	{
		new Float:target_pos[3] = { 0.0, 300.0, 900.0 };
		new Float:client_pos[3] = { 0.0, 0.0, 900.0 };
		
		TE_SetupBeamRingPoint(client_pos, 1.0, 900.0, PupilSprite, PupilSprite, 0, 5, 10.0, 80.0, 1.0, {255,255,255,250}, 60, 0);
		TE_SendToAll();
		
		TE_SetupGlowSprite( target_pos, Glow, 5.0, 3.5, 255 );
		TE_SendToClient(client);
		TE_SetupGlowSprite( client_pos, Glow, 5.0, 3.5, 255 );
		TE_SendToClient(ClientTarget[client]);
		
		TeleportEntity( ClientTarget[client], target_pos, NULL_VECTOR, NULL_VECTOR );
		TeleportEntity( client, client_pos, NULL_VECTOR, NULL_VECTOR );
		
		PrintCenterTextAll( "DEMON TRAP" );
	}
}

public Action:FreezeClient( Handle:timer, any:client )
{
	War3_SetBuff( ClientTarget[client], bNoMoveMode, thisRaceID, true );
	War3_SetBuff( client, bNoMoveMode, thisRaceID, true );
	
	SetAim( ClientTarget[client], client, 0.0 );
	SetAim( client, ClientTarget[client], 0.0 );
}

public Action:GiveDeagle( Handle:timer, any:client )
{
	GivePlayerItem( ClientTarget[client], "weapon_deagle" );
	GivePlayerItem( client, "weapon_deagle" );
	
	GivePlayerItem( ClientTarget[client], "weapon_knife" );
	GivePlayerItem( client, "weapon_knife" );
}

public Action:TeleportClientBack( Handle:timer, any:client )
{
	TeleportEntity( ClientTarget[client], ClientTargetPos[client], NULL_VECTOR, NULL_VECTOR );
	TeleportEntity( client, ClientPos[client], NULL_VECTOR, NULL_VECTOR );
	
	bClientInDuel[ClientTarget[client]] = false;
	bClientInDuel[client] = false;
	
	War3_SetBuff( ClientTarget[client], bNoMoveMode, thisRaceID, false );
	War3_SetBuff( client, bNoMoveMode, thisRaceID, false );
}

stock GetRandomPlayer( const String:type[] = "#a", bool:check_alive = false, bool:check_immunity = true )
{
	new targettable[MaxClients];
	new target = 0;
	new x = 0;
	if( StrEqual( type, "#t" ) )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( i > 0 && i <= MaxClients && IsClientConnected( i ) && IsClientInGame( i ) && GetClientTeam( i ) == 2 )
			{
				if( check_alive && !IsPlayerAlive( i ) )
					continue;
				if( check_immunity && !HasImmunity(i))
					continue;
				targettable[x] = i;
				x++;
			}
		}
		for( new y = 0; y <= x; y++ )
		{
			if( target == 0 )
			{
				target = targettable[GetRandomInt( 0, x )];
			}
			else if( target != 0 && target > 0 )
			{
				return target;
			}
		}
	}
	if( StrEqual( type, "#ct" ) )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( i > 0 && i <= MaxClients && IsClientConnected( i ) && IsClientInGame( i ) && GetClientTeam( i ) == 3 )
			{
				if( check_alive && !IsPlayerAlive( i ) )
					continue;
				if( check_immunity && !HasImmunity(i))
					continue;
				targettable[x] = i;
				x++;
			}
		}
		for( new y = 0; y <= x; y++ )
		{
			if( target == 0 )
			{
				target = targettable[GetRandomInt( 0, x )];
			}
			else if( target != 0 && target > 0 )
			{
				return target;
			}
		}
	}
	if( StrEqual( type, "#a" ) )
	{
		for( new i = 1; i <= MaxClients; i++ )
		{
			if( i > 0 && i <= MaxClients && IsClientConnected( i ) && IsClientInGame( i ) )
			{
				if( check_alive && !IsPlayerAlive( i ) )
					continue;
				if( check_immunity && !HasImmunity(i))
					continue;
				targettable[x] = i;
				x++;
			}
		}
		for( new y = 0; y <= x; y++ )
		{
			if( target == 0 )
			{
				target = targettable[GetRandomInt( 0, x )];
			}
			else if( target != 0 && target > 0 )
			{
				return target;
			}
		}
	}
	return 0;
}*/