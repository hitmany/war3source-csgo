#pragma semicolon 1

#include "W3SIncs/War3Source_Interface"

new thisRaceID;
new SKILL_ARMOR,SKILL_CHARGE,SKILL_STRIKE,ULT_DISCHARGE;

/* Todos:
 * Clean up this messy code
 * add more effects/sounds
 * Add cvars
 */

//Lightning Armor
new Float:LightningArmor[]={1.0,0.92,0.85,0.8,0.73}; // By how much lightning armor reduces damage
new ArmorDamage[]={0,1,2,3,4}; // How much damage lightning armor does
new Float:ArmorRange=300.0; // How far the lightning emitted by lightning armor can strike
new Float:ArmorCooldownTime=6.0; // How long it takes Lightning Armor to cool down

//Static Charge
new MeleeImprovement=10; // How much percent each charge adds
new ChargeAmount[]={0,2,4,6,8}; // How much charges the player can hold
new CurrentChargeCount[MAXPLAYERS]; // How much charges each player has

//Lightning Strike
new StrikeChance = 40; // 40% chance to activate, this is rolled after the regular war3 chance
new Float:StrikeMultiplierStart[] = {0.0, 0.0625, 0.125, 0.25, 0.5}; // The multiplier for the first strike
new Float:StrikeRange = 400.0; // Maximum range the lightning can jump to
new bool:beenStruck[MAXPLAYERS]; // Has this client been struck by lightning strike?

//Static Discharge
new Float:DischargeRange = 800.0; // Range
new DischargeDamage[] = {0, 1, 4, 6, 8}; // Multiplier

// Effects
new BeamSprite,HaloSprite; 
new String:lightningSound[]="war3source/lightningbolt.wav";

public Plugin:myinfo = 
{
	name = "War3Source Race - Zeus+",
	author = "Ted Theodore Logan",
	description = "The Zeus race for War3Source.",
	version = "1.0.0.0",
};

public OnWar3PluginReady(){
	thisRaceID=War3_CreateNewRace("Zeus","zeus");
	RegConsoleCmd("charges",DisplayCharges);
	SKILL_ARMOR=War3_AddRaceSkill(thisRaceID,"Lightning Armor","Enhance your armor and strike foes around you with lightning when you're attacked",false,4);
	SKILL_CHARGE=War3_AddRaceSkill(thisRaceID,"Static Charge","When attacked you generate charge, increasing your melee damage by 10% for each charge. Higher level allows for more charge",false,4);
	SKILL_STRIKE=War3_AddRaceSkill(thisRaceID,"Lightning Strike","Your attacks become imbued with electricity, giving them a chance to emit lightning.",false,4);
	ULT_DISCHARGE=War3_AddRaceSkill(thisRaceID,"Static Discharge","Discharge all that static charge you built up",true,4); 
	War3_CreateRaceEnd(thisRaceID); ///DO NOT FORGET THE END!!!
}

public OnMapStart()
{
	BeamSprite=PrecacheModel("materials/sprites/lgtning.vmt");
	HaloSprite=PrecacheModel("materials/sprites/halo01.vmt");
	
	War3_PrecacheSound(lightningSound);
}

// Activate Lightning Armor and Static Charge when attacked
public OnW3TakeDmgBulletPre(victim,attacker,Float:damage)
{
	if(ValidPlayer(victim,true)&&ValidPlayer(attacker,false)&&GetClientTeam(victim)!=GetClientTeam(attacker))
	{	// Victim is Zeus
		if(War3_GetRace(victim)==thisRaceID)
		{
			// Lightning Armor
			new armor_level=War3_GetSkillLevel(victim,thisRaceID,SKILL_ARMOR);
			if(armor_level>0)
			{
				// Reduce inflicted damage
				War3_DamageModPercent(LightningArmor[armor_level]);
				
				// Return electric damage
				if(War3_SkillNotInCooldown(victim,thisRaceID,SKILL_ARMOR,false))
				{
					// Amount of damage to inflict
					new amount=ArmorDamage[armor_level] + RoundToFloor(CurrentChargeCount[victim] / 2.0);
					
					// Position of the player struck
					new Float:playerOrigin[3];
					GetClientAbsOrigin(victim,playerOrigin);
					
					// effect
					new Float:effectOrigin[3];
					GetClientAbsOrigin(victim,effectOrigin);
					effectOrigin[2] += 30.0;
					
					new team = GetClientTeam(victim);
					new Float:otherVec[3];
					for(new i=1;i<=MaxClients;i++){
						if(ValidPlayer(i,true)){
							GetClientAbsOrigin(i,otherVec);
							if(GetVectorDistance(playerOrigin,otherVec)<ArmorRange)
							{
								// Check if the player who is in range is on a different team
								if(GetClientTeam(i)!=team){
									otherVec[2]+=30.0;
									TE_SetupBeamPoints(effectOrigin,otherVec,BeamSprite,HaloSprite,0,35,1.0,20.0,20.0,0,20.0,{0,100,255,255},40);
									TE_SendToAll();
									EmitSoundToAll( lightningSound , attacker,_,SNDLEVEL_TRAIN);
									//War3_DealDamage(i,amount,victim,DMG_SHOCK,"lightning",W3DMGORIGIN_SKILL);
									DealDamageWrapper(i,victim,amount,"lightning");
								}
							}
						}
					}
					
					// Set a cooldown on Armor so you don't totaly wreck Scouts by just having
					// a Heavy shoot at you from across the map...
					War3_CooldownMGR(victim,ArmorCooldownTime,thisRaceID,SKILL_ARMOR,true,true);
				}				   
			}
			// Static Charge
			new charge_level=War3_GetSkillLevel(victim,thisRaceID,SKILL_CHARGE);
			if(CurrentChargeCount[victim]<ChargeAmount[charge_level])
			{
				// If I understood correctly this should give you a charge with the same
				// chance that the attacker could proc a damage modifier
				if(GetRandomInt(1, 100)<=RoundFloat(100.0*W3ChanceModifier(attacker)))
				{
					// Todo: Spark effect
					CurrentChargeCount[victim]++;
					War3_ChatMessage(victim, "Your body fills with electricity... (%d/%d)", CurrentChargeCount[victim], ChargeAmount[charge_level]);
					new Float:dir[3]={0.0,0.0,28.0};
					new Float:iVec[3];
					GetClientEyePosition(victim,iVec);
					iVec[2]+20.0;
					TE_SetupBeamRingPoint( iVec,1.0,75.0,BeamSprite,BeamSprite,0,15,5.0,10.0,12.0,{0,0,255,255},1,0);
					TE_SendToAll();
					TE_SetupBeamRingPoint( iVec,1.0,75.0,BeamSprite,BeamSprite,0,15,5.0,10.0,12.0,{60,60,255,255},1,0);
					TE_SendToAll(0.35);
					TE_SetupBeamRingPoint( iVec,1.0,75.0,BeamSprite,BeamSprite,0,15,5.0,15.0,12.0,{120,120,255,255},1,0);
					TE_SendToAll(0.65);
					TE_SetupBeamRingPoint( iVec,1.0,75.0,BeamSprite,BeamSprite,0,15,5.0,10.0,12.0,{180,190,255,255},1,0);
					TE_SendToAll(1.00);
					TE_SetupBeamRingPoint( iVec,1.0,75.0,BeamSprite,BeamSprite,0,15,5.0,15.0,12.0,{200,200,255,255},1,0);
					TE_SendToAll(1.35);
					TE_SetupSparks(iVec, dir, 500, 50);
					TE_SendToAll(0.1);
				}
			}
		}
		// Attacker is Zeus
		if(War3_GetRace(attacker)==thisRaceID)
		{
			new Float:amount = float(CurrentChargeCount[attacker] + MeleeImprovement) / 10.0;
			new String:weaponname[64];
			GetClientWeapon(attacker, weaponname, 64);
			new bool:damageFrom = W3IsDamageFromMelee(weaponname);
			
			// Holding a melee weapon?
			if(damageFrom==true)
			{   
				if(GetClientTeam(victim)!=GetClientTeam(attacker)&&(amount>0.0))
				{
					new Float:victimPos[3];
					new Float:attackerPos[3];
					GetClientAbsOrigin(victim,victimPos);
					GetClientAbsOrigin(attacker,attackerPos);
					// Probably a real melee attack if it was this close
					if(GetVectorDistance(victimPos,attackerPos)<250.0)
					{
						War3_DamageModPercent(amount);
					}
				}
			}
			else
			{
				amount = 1.0; // Reset melee multiplier for lightning strike
			}
			new strike_skill=War3_GetSkillLevel(attacker,thisRaceID,SKILL_STRIKE);	
			if(strike_skill>0)
			{
				// Roll for Lightning Strike
				if(GetRandomInt(1, 100)<=RoundFloat(100.0*W3ChanceModifier(attacker)))
				{
					if(GetRandomInt(1, 100) <= StrikeChance)
					{								
						for(new i=1;i<=MaxClients;i++)
						{
							if(ValidPlayer(i))	
							{
								beenStruck[i] = false;
							}
						}
						new Float:distance = StrikeRange;
						new calcDamage = RoundToCeil((damage * amount) * StrikeMultiplierStart[strike_skill]);
						DoChain(attacker,distance,calcDamage,true,0);
					}
				}
			}
		}
	}
}

// Generic charge removing function
public RemoveCharge(client)
{
	if(CurrentChargeCount[client]>0)
	{
		War3_ChatMessage(client, "The electricity stored in your body fades away...");
		CurrentChargeCount[client] = 0;
	}
}

//Trigger the damage dealing effect of lightning armor

// Remove charge on respawn
public OnWar3EventSpawn(client)
{	
	RemoveCharge(client);
}

// Remove charge when switching races
public OnRaceChanged( client, oldrace, newrace )
{
	if(newrace!=thisRaceID)
	{
		RemoveCharge(client);
	}
}

// Display charges cmd
public Action:DisplayCharges(client,args){
	new charge_level=War3_GetSkillLevel(client,thisRaceID,SKILL_CHARGE);
	War3_ChatMessage(client, "You have %d/%d charges", CurrentChargeCount[client], ChargeAmount[charge_level]);
	return Plugin_Handled;
}

// Taken from Orc, slightly modified :3
public DoChain(client,Float:distance,dmg,bool:first_call,last_target)
{
	new target=0;
	new Float:target_dist=distance+1.0; // just an easy way to do this
	new caster_team=GetClientTeam(client);
	new Float:start_pos[3];
	if(last_target<=0)
		GetClientAbsOrigin(client,start_pos);
	else
		GetClientAbsOrigin(last_target,start_pos);
	for(new x=1;x<=MaxClients;x++)
	{
		if(ValidPlayer(x,true)&&!beenStruck[x]&&caster_team!=GetClientTeam(x))
		{
			new Float:this_pos[3];
			GetClientAbsOrigin(x,this_pos);
			new Float:dist_check=GetVectorDistance(start_pos,this_pos);
			if(dist_check<=target_dist)
			{
				// found a candidate, whom is currently the closest
				target=x;
				target_dist=dist_check;
			}
		}
	}
	if(ValidPlayer(target))
	{
		// found someone
		beenStruck[target]=true; // don't let them get hit twice
		DealDamageWrapper(target,client,dmg,"lightning");
		//War3_DealDamage(target,dmg,client,DMG_ENERGYBEAM,"lightning");
		PrintHintText(target,"Hit by Lightning Strike -%d HP",target,War3_GetWar3DamageDealt());
		start_pos[2]+=300.0; // offset for effect
		new Float:target_pos[3];
		GetClientAbsOrigin(target,target_pos);
		target_pos[2]+=30.0;
		TE_SetupBeamPoints(start_pos,target_pos,BeamSprite,HaloSprite,0,35,1.0,20.0,28.0,0,40.0,{255,100,255,255},40);
		TE_SendToAll();
		EmitSoundToAll( lightningSound , target,_,SNDLEVEL_TRAIN);
		new new_dmg=RoundFloat(float(dmg)*0.5);
		
		DoChain(client,distance,new_dmg,false,target);
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && IsPlayerAlive(client))
	{
		new skill=War3_GetSkillLevel(client,race,ULT_DISCHARGE);
		if(skill>0)
		{
			new Float:victimPos[3];
			new Float:attackerPos[3];
			new AttackerTeam = GetClientTeam(client);
			GetClientAbsOrigin(client,attackerPos);
			
			new damage = CurrentChargeCount[client] * DischargeDamage[skill];
			
			for(new x=1;x<=MaxClients;x++)
			{
				if(ValidPlayer(x,true)&&AttackerTeam!=GetClientTeam(x))
				{
					GetClientAbsOrigin(x,victimPos);
					if(GetVectorDistance(victimPos,attackerPos)<=DischargeRange)
					{
						War3_DealDamage(x,damage,client,DMG_SHOCK,"discharge",W3DMGORIGIN_SKILL);
						new Float:iVec[3];
						GetClientEyePosition(client,iVec);
						new Float:iVec2[3];
						GetClientAbsOrigin(x,iVec2);
						iVec[2]+=50.0;
						TE_SetupBeamPoints(iVec,iVec2,BeamSprite,HaloSprite,0,41,1.6,25.0,35.0,0,7.5,{120,120,255,200},45);
						TE_SendToAll();
					}
				}
			}
			
			CurrentChargeCount[client] = 0;
		}
		else
		{
			W3MsgUltNotLeveled(client);
		}
	}
}

//dirty dealdamage workaround.. tell me if you got another idea :o
stock DealDamageWrapper(victim,attacker,damage,String:classname[32],Float:delay=0.1) {
	new Handle:pack;
	CreateDataTimer(delay, Timer_DealDamageWrapper, pack);
	WritePackCell(pack, victim);
	WritePackCell(pack, attacker);
	WritePackCell(pack, damage);
	WritePackString(pack, classname);
}
public Action:Timer_DealDamageWrapper(Handle:timer, Handle:pack)
{
	ResetPack(pack); //resolve the package...
	new victim = ReadPackCell(pack);
	new attacker = ReadPackCell(pack);
	new damage = ReadPackCell(pack);
	decl String:classname[32];
	ReadPackString(pack,classname,sizeof(classname));
	War3_DealDamage(victim,damage,attacker,DMG_BULLET,classname);
}