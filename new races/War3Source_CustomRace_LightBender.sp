/**
* File: War3Source_Light_Bender.sp
* Description: The Light Bender race for SourceCraft.[special version]
* Author(s): xDr.HaaaaaaaXx & Revan(DiscoEffect&SpawnEffect)
*/
//disco ball effect and spawn effect by Revan
#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdktools_stocks>
#include <sdktools_functions>

#include "W3SIncs/War3Source_Interface"

// War3Source stuff
new thisRaceID, SKILL_RED, SKILL_GREEN, SKILL_BLUE, SKILL_SPEED, ULT_DISCO;

new Float:LightGravity[6] = { 1.0, 0.68, 0.60, 0.52, 0.44, 0.40 };
new Float:LightSpeed[6] = { 1.0, 1.1, 1.15, 1.22, 1.25, 1.29 };
new Float:RGBChance[6] = { 0.00, 0.05, 0.10, 0.13, 0.14, 0.17 };
new Float:ClientPos[64][3];
new ClientTarget[64];

new bool:bIsDJ[MAXPLAYERS];

new HaloSprite, BeamSprite;

public Plugin:myinfo = 
{
	name = "Light Bender ++",
	author = "xDr.HaaaaaaaXx & Revan",
	description = "The Light Bender race for War3Source.[special version]",
	version = "1.0.0.0",
	url = "www.wcs-lagerhaus.de"
};

public OnMapStart()
{
	HaloSprite = PrecacheModel( "materials/sprites/halo01.vmt" );
	BeamSprite = PrecacheModel( "materials/sprites/laser.vmt" );
}

public OnPluginStart()
{
	PrintToServer("[WAR3] Race loaded : Light Bender\n[-]Enchanched Version");
	CreateTimer(0.1,discosfx,_,TIMER_REPEAT);
}

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "Light Bender", "lightbender" );
	
	SKILL_RED = War3_AddRaceSkill( thisRaceID, "Red Laser:Burn", "Burn your targets", false, 5 );	
	SKILL_GREEN = War3_AddRaceSkill( thisRaceID, "Green Laser:Shake", "Shake your targets", false, 5 );	
	SKILL_BLUE = War3_AddRaceSkill( thisRaceID, "Blue Laser:Flash", "Sens an icy beam to your target to ministun + disort him!", false, 5 );
	SKILL_SPEED = War3_AddRaceSkill( thisRaceID, "Phase Shift", "Increase your Speed and Reduce the gravity for advanced jumps", false, 5 );
	ULT_DISCO = War3_AddRaceSkill( thisRaceID, "Disco Ball", "Teleport an enemy into the air above you, prepare for a show!", true, 1 );
	
	W3SkillCooldownOnSpawn( thisRaceID, ULT_DISCO, 36.0, true );

	War3_CreateRaceEnd( thisRaceID );
}

public Action:discosfx(Handle:h,any:client){
   for(new i=1;i<=MaxClients;i++){
	if(ValidPlayer(i,true)){
		if(bIsDJ[i]&&IsPlayerAlive(i))
		{
			CreateTimer(20.0, Timer_StopUltimate,i);
			//roter effekt -big edition-
			new Float:StartPos[3];
			new Float:EndPos[3];
			
			GetClientAbsOrigin( i, StartPos );
			
			GetClientAbsOrigin( i, EndPos );

			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );

			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 255, 11, 15, 255 }, 1 );
			TE_SendToAll();
			
			GetClientAbsOrigin( i, EndPos );
			
			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );
			
			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 255, 11, 15, 255 }, 1 );
			TE_SendToAll();
			
			GetClientAbsOrigin( i, EndPos );
			
			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );
			
			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 255, 11, 15, 255 }, 1 );
			TE_SendToAll();
			
			//gruener effekt
			DoGreenDisco(i);
			//blauer effekt
			DoBlueDisco(i);
		}
	}
 }
}

public DoGreenDisco(client)
{
	if(ValidPlayer(client,true)){
			new Float:StartPos[3];
			new Float:EndPos[3];
			
			GetClientAbsOrigin( client, StartPos );
			
			GetClientAbsOrigin( client, EndPos );

			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );

			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 11, 255, 15, 255 }, 1 );
			TE_SendToAll();
			
			GetClientAbsOrigin( client, EndPos );
			
			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );
			
			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 11, 255, 15, 255 }, 1 );
			TE_SendToAll();
			
			GetClientAbsOrigin( client, EndPos );
			
			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );
			
			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 11, 255, 15, 255 }, 1 );
			TE_SendToAll();
	}
}

public DoBlueDisco(client)
{
	if(ValidPlayer(client,true)){
			new Float:StartPos[3];
			new Float:EndPos[3];
			
			GetClientAbsOrigin( client, StartPos );
			
			GetClientAbsOrigin( client, EndPos );

			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );

			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 15, 11, 255, 255 }, 1 );
			TE_SendToAll();
			
			GetClientAbsOrigin( client, EndPos );
			
			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );
			
			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 15, 11, 255, 255 }, 1 );
			TE_SendToAll();
			
			GetClientAbsOrigin( client, EndPos );
			
			EndPos[0] += GetRandomFloat( -500.0, 500.0 );
			EndPos[1] += GetRandomFloat( -500.0, 500.0 );
			EndPos[2] += GetRandomFloat( -500.0, 500.0 );
			
			TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 0.61, 20.0, 2.0, 0, 1.0, { 15, 11, 255, 255 }, 1 );
			TE_SendToAll();
	}
}

public InitPassiveSkills( client )
{
	if( War3_GetRace( client ) == thisRaceID )
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, LightSpeed[War3_GetSkillLevel( client, thisRaceID, SKILL_SPEED )] );
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, LightGravity[War3_GetSkillLevel( client, thisRaceID, SKILL_SPEED )] );
		if( War3_GetSkillLevel( client, thisRaceID, SKILL_SPEED ) > 0 )
			PrintToChat( client, "\x03You are currently in a Phase Shift" );
	}
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, 1.0 );
	}
	else
	{
		if( IsPlayerAlive( client ) )
		{
			InitPassiveSkills( client );
		}
	}
}

public OnSkillLevelChanged( client, race, skill, newskilllevel )
{
	InitPassiveSkills( client );
}

public OnWar3EventSpawn( client )
{
	bIsDJ[client]=false;
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		DoGreenDisco(client);
		InitPassiveSkills( client );
		W3FlashScreen(client,{100,120,255,180}, 0.45, 1.3, FFADE_OUT);
		new Float:iVec[ 3 ];
		GetClientAbsOrigin(client, Float:iVec);
		iVec[2]+=28;
		TE_SetupBeamRingPoint(iVec, 10.0, 350.0, BeamSprite, BeamSprite, 0, 20, 4.5, 28.0, 1.5, {120,120,120,255}, 28, 0);
		TE_SendToAll();
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_red = War3_GetSkillLevel( attacker, thisRaceID, SKILL_RED );
			if( !Hexed( attacker, false ) && skill_red > 0 && GetRandomFloat( 0.0, 1.4 ) <= RGBChance[skill_red] )
			{
				IgniteEntity( victim, 5.0 );
				
				PrintToChat( victim, "Red Laser:Burn" );
				
				new Float:StartPos[3];
				new Float:EndPos[3];
				
				GetClientAbsOrigin( victim, StartPos );
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );

				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 255, 11, 15, 255 }, 1 );
				TE_SendToAll();
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );
				
				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 255, 11, 15, 255 }, 1 );
				TE_SendToAll();
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );
				
				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 255, 11, 15, 255 }, 1 );
				TE_SendToAll();
			}
			
			new skill_green = War3_GetSkillLevel( attacker, thisRaceID, SKILL_GREEN );
			if( !Hexed( attacker, false ) && skill_green > 0 && GetRandomFloat( 0.0, 0.95 ) <= RGBChance[skill_green] )
			{
				War3_ShakeScreen( victim );
				
				PrintToChat( victim, "Green Laser:Shake" );
				
				new Float:StartPos[3];
				new Float:EndPos[3];
				
				GetClientAbsOrigin( victim, StartPos );
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );

				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 11, 255, 15, 255 }, 1 );
				TE_SendToAll();
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );
				
				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 11, 255, 15, 255 }, 1 );
				TE_SendToAll();
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );
				
				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 11, 255, 15, 255 }, 1 );
				TE_SendToAll();
			}
			
			new skill_blue = War3_GetSkillLevel( attacker, thisRaceID, SKILL_BLUE );
			if( !Hexed( attacker, false ) && skill_blue > 0 && GetRandomFloat( 0.0, 1.5 ) <= RGBChance[skill_blue] )
			{
				War3_SetBuff( victim, bStunned, thisRaceID, true );
				W3FlashScreen(victim,{100,120,255,180}, 1.20, 1.3, FFADE_OUT);
				CreateTimer( 0.10, StopFreeze, victim );
				
				PrintToChat( victim, "Blue Laser:Flash" );
				
				new Float:StartPos[3];
				new Float:EndPos[3];
				
				GetClientAbsOrigin( victim, StartPos );
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );

				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 15, 11, 255, 255 }, 1 );
				TE_SendToAll();
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );
				
				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 15, 11, 255, 255 }, 1 );
				TE_SendToAll();
				
				GetClientAbsOrigin( victim, EndPos );
				
				EndPos[0] += GetRandomFloat( -500.0, 500.0 );
				EndPos[1] += GetRandomFloat( -500.0, 500.0 );
				EndPos[2] += GetRandomFloat( -500.0, 500.0 );
				
				TE_SetupBeamPoints( StartPos, EndPos, BeamSprite, HaloSprite, 0, 1, 30.0, 20.0, 2.0, 0, 0.0, { 15, 11, 255, 255 }, 1 );
				TE_SendToAll();
			}
		}
	}
}

public Action:StopFreeze( Handle:timer, any:client )
{
	War3_SetBuff( client, bStunned, thisRaceID, false );
}

public Action:Timer_StopUltimate( Handle:timer, any:client)
{
	if(ValidPlayer(client, true) && bIsDJ[client])
	{
		bIsDJ[client] = false;
	}
}

public OnUltimateCommand( client, race, bool:pressed )
{
	if( race == thisRaceID && pressed && ValidPlayer( client, true ) )
	{
		new ult_level = War3_GetSkillLevel( client, race, ULT_DISCO );
		if( ult_level > 0 )
		{
			if( War3_SkillNotInCooldown( client, thisRaceID, ULT_DISCO, true ) )
			{
				Disco( client );
			}
		}
		else
		{
			PrintHintText( client, "Level Your Ultimate First" );
		}
	}
}

Action:Disco( client )
{
	new Float:besttargetDistance = 2800.0; 
	new Float:posVec[3];
	new Float:otherVec[3];
	ClientTarget[client] = 0;
	new team = GetClientTeam( client );
	
	GetClientAbsOrigin( client, posVec );
	
	for( new i = 1; i <= MaxClients; i++ )
	{
		if( ValidPlayer( i, true ) && GetClientTeam( i ) != team && IsPlayerAlive( i ) && !W3HasImmunity( i, Immunity_Ultimates ) )
		{
			GetClientAbsOrigin( i, otherVec );
			new Float:dist = GetVectorDistance( posVec, otherVec );
			if( dist < besttargetDistance )
			{
				ClientTarget[client] = i;
				besttargetDistance = GetVectorDistance( posVec, otherVec );
				break;
			}
		}
	}
	
	if( ClientTarget[client] == 0 )
	{
		PrintHintText( client, "No Target Found within %.1f feet", besttargetDistance / 10 );
	}
	else
	{
		GetClientAbsOrigin( client, ClientPos[client] );
		//CreateTimer( 3.0, Teleport, client );
		CreateTimer( 3.8, Freeze, client );
		CreateTimer( 4.1, UnFreeze, client );
		
		new String:NameAttacker[64];
		GetClientName( client, NameAttacker, 64 );
		
		new String:NameVictim[64];
		GetClientName( ClientTarget[client], NameVictim, 64 );
		bIsDJ[ClientTarget[client]]=true;
		PrintToChat( client, ": %s will become a Disco Ball in 3 seconds", NameVictim );
		PrintToChat( ClientTarget[client], ": %s makes you to a Disco Ball in 3 seconds", NameAttacker );
		War3_CooldownMGR( client, 36.0, thisRaceID, ULT_DISCO, _, true);
	}
}

public Action:Teleport( Handle:timer, any:client )
{
	ClientPos[client][2] += 150;
	//TeleportEntity( ClientTarget[client], ClientPos[client], NULL_VECTOR, NULL_VECTOR );
	War3_DealDamage( ClientTarget[client], GetRandomInt(1,20), client, DMG_ENERGYBEAM, "laser" );
	TE_SetupBeamRingPoint(ClientPos[client],750000.0,75.0,BeamSprite,BeamSprite,0,15,5.0,60.0,12.0,{0,0,255,255},1,0);
	TE_SendToAll();
	TE_SetupBeamRingPoint(ClientPos[client],750000.0,75.0,BeamSprite,BeamSprite,0,15,5.0,60.0,12.0,{255,0,0,255},1,0);
	TE_SendToAll(0.3);
	TE_SetupBeamRingPoint(ClientPos[client],750000.0,75.0,BeamSprite,BeamSprite,0,15,5.0,60.0,12.0,{255,0,0,255},1,0);
	TE_SendToAll(0.5);
}

public Action:Freeze( Handle:timer, any:client )
{
	War3_SetBuff( ClientTarget[client], bStunned, thisRaceID, true );
	//bIsDJ[client]=true;
}

public Action:UnFreeze( Handle:timer, any:client )
{
	War3_SetBuff( ClientTarget[client], bStunned, thisRaceID, false );
	//bIsDJ[client]=false;
}