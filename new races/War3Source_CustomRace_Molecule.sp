/**
* File: War3Source_Molecule.sp
* Description: The Molecule race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx
*/

#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"
#define MAX_BEAM_SCROLLSPEED 100

// War3Source stuff
new thisRaceID;

// Chance/Data Arrays
new Float:DamageMultiplier[6] = { 0.0, 0.15, 0.20, 0.25, 0.28, 0.32 };
new Float:EvadeChance[7] = { 0.0, 0.04, 0.08, 0.12, 0.15, 0.17, 0.20 };
new Float:MoleculeSpeed[7] = { 1.0, 1.05, 1.1,1.16, 1.18, 1.20, 1.25 };
new Float:UltDuration[9] = { 0.0, 1.0, 1.3, 1.6, 2.0, 2.4, 2.6, 2.8, 3.0 };
new Float:UltRadius[9] = { 200.0, 250.0, 300.0, 350.0, 400.0, 450.0, 500.0, 588.0};
new ShieldSprite, AttackSprite, EvadeSprite, BeamSprite, HaloSprite;

#define SOUND_SPAWN "weapons/explode3.wav"

new SKILL_SPEED, SKILL_DMG, SKILL_EVADE, ULT_FIELD;

public Plugin:myinfo = 
{
    name = "War3Source Race - Molecule",
    author = "xDr.HaaaaaaaXx&vulpone",
    description = "Molecule race for War3Source.",
    version = "1.0-balanced",
    url = "www.war3source.com"
};

public OnMapStart()
{
    PrecacheSound( SOUND_SPAWN );
    ShieldSprite = PrecacheModel( "sprites/strider_blackball.vmt" );
    AttackSprite = PrecacheModel( "particle/particle_ring_wave_12.vmt" );
    EvadeSprite = PrecacheModel( "sprites/blueglow1.vmt" );
    BeamSprite=War3_PrecacheBeamSprite();
    HaloSprite=War3_PrecacheHaloSprite();
}

public OnWar3PluginReady()
{
    thisRaceID = War3_CreateNewRace( "Electric Molecule", "molecule" );
    
    //FIRST CLASS TO USE A SKILL-TREE WOOHOHOHOHOHOHO
    //native War3_AddRaceSkill(raceid,String:tskillorultname[],String:tskillorultdescription[],bool:isult=false,maxskilllevel=DEF_MAX_SKILL_LEVEL, bool:needsotherskill=false,String:otherskillname[]="", otherskilllevel);

    
    SKILL_SPEED = War3_AddRaceSkill( thisRaceID, "Electric Agility", "Dive into your divine power and speed up your blood.\nGain 5-25% more speed!", false, 6);	
    SKILL_DMG = War3_AddRaceSkill( thisRaceID, "Electric Shock", "Use your weapons to electricute your enemy.\n13% chance.", false, 5 );	
    SKILL_EVADE = War3_AddRaceSkill( thisRaceID, "Electric Shield", "Use thunder as your guide to prevent harm!\nRegisters a 4-20% chance to distort a bullet and get less damage.", false, 6);
    ULT_FIELD = War3_AddRaceSkill( thisRaceID, "Force Field", "Gather all the electricity there is to send down a mighty rain of thunder!\nTakes three seconds to cast; will select nearest target(s) if within reach. It will stun the enemy for 1.5 seconds and do 30 damage..", true, 8 );
    
    W3SkillCooldownOnSpawn( thisRaceID, ULT_FIELD, 5.0, true );
    
    War3_CreateRaceEnd( thisRaceID );
    
    War3_SetDependency(thisRaceID, SKILL_EVADE, SKILL_SPEED, 3);
    W3Faction(thisRaceID,"Elementalists",true);

}

public InitPassiveSkills( client )
{
    if( War3_GetRace( client ) == thisRaceID )
    {
        new skill_speed = War3_GetSkillLevel( client, thisRaceID, SKILL_SPEED );
        new Float:speed = MoleculeSpeed[skill_speed];
        War3_SetBuff( client, fMaxSpeed, thisRaceID, speed );
    }
}

public OnRaceChanged( client, oldrace, newrace )
{
    if( newrace == thisRaceID )
    {
        if( IsPlayerAlive( client ) )
        {
            InitPassiveSkills( client );
        }
    }
    else if( oldrace == thisRaceID )
    {
        W3ResetPlayerColor(client,thisRaceID);
        War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
    }
}

public OnSkillLevelChanged( client, race, skill, newskilllevel )
{
    if(race == thisRaceID && newskilllevel>0)
    InitPassiveSkills( client );
}

public OnWar3EventSpawn( client )
{
    new race = War3_GetRace( client );
    if( race == thisRaceID )
    {
        InitPassiveSkills( client );
        EmitSoundToAll( SOUND_SPAWN, client );
    }
}

public OnW3TakeDmgBulletPre( victim, attacker, Float:damage )
{
    if( IS_PLAYER( victim ) && IS_PLAYER( attacker ) && victim > 0 && attacker > 0 && attacker != victim )
    {
        new vteam = GetClientTeam( victim );
        new ateam = GetClientTeam( attacker );
        if( vteam != ateam )
        {
            new race_victim = War3_GetRace( victim );
            new skill_level = War3_GetSkillLevel( victim, thisRaceID, SKILL_EVADE );
            if( race_victim == thisRaceID )
            {
                if( !W3HasImmunity( attacker, Immunity_Skills ) )
                {
                    if(skill_level > 0 && GetRandomFloat( 0.0, 1.0 ) <= EvadeChance[skill_level]) {
                        new Float:flMax = 0.20;
                        switch(W3FactionCompare(thisRaceID,War3_GetRace(victim)))
                        {
                        case (FactionBehavior:Friend):
                            flMax-=0.05;
                        case (FactionBehavior:Enemy):
                            damage+=0.03;
                        }
                        War3_DamageModPercent( GetRandomFloat(0.10,flMax) );
                        //W3MsgEvaded( victim, attacker );
                        PrintHintText(victim, "Distorted the bullet! Incoming damage reduced!");
                        
                        decl Float:startpos[3];
                        decl Float:endpos[3];
                        
                        GetClientAbsOrigin( attacker, startpos );
                        GetClientAbsOrigin( victim, endpos );
                        
                        TE_SetupBeamPoints( startpos, endpos, EvadeSprite, EvadeSprite, 0, 10,0.5, 2.0, 1.0, 0, 1.0, { 255, 255, 255, 255 }, 5 );
                        TE_SendToAll();
                    }
                }
                else
                W3MsgEnemyHasImmunity( victim, true );
                /* ultimate has a NEW EFFECT
                if( !W3HasImmunity( attacker, Immunity_Ultimates ) )
                {
                    if(GOD[victim]) {
                        War3_DamageModPercent( 0.0 );
                        W3FlashScreen( victim, RGBA_COLOR_BLUE );
                        new Float:position[ 3 ];
                        GetClientAbsOrigin( victim, Float:position );
                        new Float:endPos[3];
                        GetClientAbsOrigin(attacker, endPos);
                        endPos[2] += 120;
                        
                        TE_Start("Bubbles");
                        TE_WriteVector("m_vecMins", position);
                        TE_WriteVector("m_vecMaxs", endPos);
                        TE_WriteFloat("m_fHeight", 150.0);
                        TE_WriteNum("m_nModelIndex", ShieldSprite);
                        TE_WriteNum("m_nCount", 166);
                        TE_SendToAll();
                    }
                }
                else
                W3MsgEnemyHasImmunity( victim, true );*/
            }
            if( War3_GetRace( attacker ) == thisRaceID )
            {
                new skill_dmg = War3_GetSkillLevel( attacker, thisRaceID, SKILL_DMG );
                if( !Hexed( attacker, false ) && skill_dmg > 0 && GetRandomFloat( 0.01, 0.99 ) <= 0.20 )
                {
                    decl String:wpnstr[32];
                    GetClientWeapon( attacker, wpnstr, sizeof(wpnstr) );
                    if( !StrEqual( wpnstr, "weapon_knife" ) )
                    {
                        //War3_DealDamage( victim, RoundToFloor( damage * DamageMultiplier[skill_dmg] ), attacker, DMG_BULLET, "electric_crit" );
                        switch(W3FactionCompare(thisRaceID,War3_GetRace(victim)))
                        {
                        case (FactionBehavior:Friend):
                            damage--;
                        case (FactionBehavior:Enemy):
                            damage++;
                        }
                        DealDamageWrapper(victim,attacker,RoundToFloor( damage * DamageMultiplier[skill_dmg] ),"electric");
                        //W3PrintSkillDmgHintConsole( victim, attacker, War3_GetWar3DamageDealt(), SKILL_DMG );
                        PrintCenterText(attacker,"+%d damage dealt with electrocute",RoundToFloor(damage*DamageMultiplier[skill_dmg]));						

                        W3FlashScreen(victim,{0,0,120,80},0.2,0.1,FFADE_IN);
                        
                        decl Float:pos[3],Float:pos2[3],Float:vecles[3],Float:angles[3];			
                        
                        GetClientAbsOrigin( victim, pos );						
                        pos[2] += 15;
                        TE_SetupDynamicLight(pos, 40,40,255,4,20.0,0.45,1.0);
                        TE_SendToAll();						
                        TE_SetupGlowSprite( pos, AttackSprite, 0.5, 0.07, 255 );
                        TE_SendToAll();

                        decl Handle:pack;
                        CreateDataTimer(0.1, Timer_DrawFX, pack);
                        WritePackCell(pack, 2);//amount of repeates
                        WritePackCell(pack, GetRandomInt(0,1));
                        WritePackFloat(pack, pos[0]);
                        WritePackFloat(pack, pos[1]);
                        WritePackFloat(pack, pos[2]+10);
                        GetClientAbsOrigin( attacker, pos2 );
                        SubtractVectors( pos2, pos, vecles );
                        GetVectorAngles( vecles, angles );						
                        TE_SetupSparks(pos, angles, 10, 10);
                        TE_SendToAll();
                        TE_SetupExplosion(vecles, ShieldSprite, 0.3, 1, 1, 0, 0);
                        TE_SendToAll(); 
                    }
                    else
                    PrintHintText(attacker,"You can't electricute your enemy with your knife!");
                }
            }
        }
    }
}

public Action:Timer_DrawFX(Handle:timer, Handle:pack) {
    //data collection and other stuff...
    ResetPack(pack);
    new Float:fAngles[3]={0.0,0.0,0.0};
    decl Float:fPos[3];
    new times=ReadPackCell(pack);
    new ax=ReadPackCell(pack);
    fPos[0]=ReadPackFloat(pack);
    fPos[1]=ReadPackFloat(pack);
    fPos[2]=ReadPackFloat(pack);
    --times;
    //draw the fx - probably intresting? ;D
    fPos[ax]+=GetRandomInt(2,10);
    TE_SetupGlowSprite( fPos, AttackSprite, 0.2, 0.15, 255 );
    TE_SendToAll();
    TE_SetupSparks(fPos, fAngles, 10, 10);
    TE_SendToAll();
    //restart timer(if we got some 'times' remaining...)!
    if(times>0) {
        decl Handle:pack2;
        CreateDataTimer(0.1, Timer_DrawFX, pack2);
        WritePackCell(pack2, times);
        WritePackCell(pack2, GetRandomInt(0,1));
        WritePackFloat(pack2, fPos[0]);
        WritePackFloat(pack2, fPos[1]);
        WritePackFloat(pack2, fPos[2]);
    }
}

public OnUltimateCommand( client, race, bool:pressed )
{
    if( race == thisRaceID && pressed && IsPlayerAlive( client ) && !Silenced( client ) )
    {
        new ult_level = War3_GetSkillLevel( client, race, ULT_FIELD );
        if( ult_level > 0 )
        {
            if( War3_SkillNotInCooldown( client, thisRaceID, ULT_FIELD, true ) )
            {
                //new effect from Vulpone: summon a thunder skill that hits a target if he's within reach
                War3_SetBuff( client, bNoMoveMode, thisRaceID, true );
                CreateTimer( UltDuration[ult_level], StopGod, client );
                War3_CooldownMGR( client, UltDuration[ult_level] + 15.0, thisRaceID, ULT_FIELD, _, true);
                decl Float:pos[3];
                GetClientAbsOrigin( client, pos );
                TE_SetupGlowSprite( pos, ShieldSprite, UltDuration[ult_level], 0.5, 200 );
                TE_SendToAll();
                W3SetPlayerColor( client, thisRaceID, 80, 80, 255, 200, 1);
                W3FlashScreen(client,RGBA_COLOR_BLUE,UltDuration[ult_level], 0.5);
                PrintHintText(client, "Casting Force Field!");
                CreateTimer(1.0, TR_CountdownOne, client);
            }
        }
        else
        {
            W3MsgUltNotLeveled( client );
        }
    }
}

public Action:TR_RemoveStunVictim(Handle:timer, any: client)
{
    //buffs can safetly be removed, without checking for a valid userid since buffs are just arrays
    War3_SetBuff( client, bNoMoveMode, thisRaceID, false);
}

public Action:TR_CountdownOne(Handle:timer, any:client)
{
    if(ValidPlayer(client, true))
    {
        PrintHintText(client, "One...");
        CreateTimer(1.0, TR_CountdownTwo, client);
    }
}

public Action:TR_CountdownTwo(Handle:timer, any:client)
{
    if(ValidPlayer(client, true))
    {
        PrintHintText(client, "Two...");
        CreateTimer(1.0, TR_CountdownThree, client);
    }
}
public Action:TR_CountdownThree(Handle:timer, any:client)
{
    if(ValidPlayer(client, true))
    {
        PrintHintText(client, "Three!!");
        DoThunderAoE(client, GetRandomInt(6,12));
    }
}

stock DoThunderAoE(client, iNumberOfThunder)
{
    if(ValidPlayer(client, true))
    {
        if(War3_GetRace(client) != thisRaceID) {
            //He isn't molecule anymore!! bail out!
            return;
        }
        decl Float:fClientVec[3];
        decl Float:fEnemyVec[3];
        GetClientAbsOrigin(client, fClientVec);
        
        new CasterTeam = GetClientTeam(client);
        new ult_level = War3_GetSkillLevel( client, thisRaceID, ULT_FIELD );
        for(new i = 1; i<= MaxClients; i++)
        {
            if(ValidPlayer(i, true) && GetClientTeam(i) != CasterTeam && !W3HasImmunity(i, Immunity_Ultimates))
            {
                GetClientAbsOrigin(i, fEnemyVec);
                if(GetVectorDistance(fEnemyVec, fClientVec) < UltRadius[ult_level])
                {
                    new thunderdamage = 30;
                    switch(W3FactionCompare(thisRaceID,War3_GetRace(i)))
                    {
                    case (FactionBehavior:Friend):
                        thunderdamage-=5;
                    case (FactionBehavior:Enemy):
                        thunderdamage+=5;
                    }
                    if(War3_DealDamage(i,thunderdamage,client,DMG_BULLET,"forcefield")) {
                        W3FlashScreen(i,RGBA_COLOR_BLUE,UltDuration[ult_level], 0.5);
                        War3_SetBuff( i, bNoMoveMode, thisRaceID, true );
                        CreateTimer(1.5, TR_RemoveStunVictim, i);
                    }
                }
            }
        }
        fClientVec[2] += 300; //raise
        
        // TE_SetupBeamPoints(const Float:start[3], const Float:end[3], ModelIndex, HaloIndex, StartFrame, FrameRate, Float:Life, 
        //				Float:Width, Float:EndWidth, FadeLength, Float:Amplitude, const Color[4], Speed)
        
        decl Float:fxdelay;
        for(new counter = 0; counter <= iNumberOfThunder; counter++)
        {
            fEnemyVec[1] += 10;
            fEnemyVec[1] += 10;
            
            if(fEnemyVec[1] >= 50)
            {
                fEnemyVec[0] += 25;
                fEnemyVec[1] -= 80;
            }
            
            fxdelay += 0.1;
            TE_SetupBeamPoints(fClientVec,fEnemyVec,BeamSprite,HaloSprite,0,GetRandomInt(55,MAX_BEAM_SCROLLSPEED),0.2,2.0,4.0,0,88.0,{240,236,215,242},20);
            TE_SendToAll(fxdelay);
        }
    }
}


public Action:StopGod( Handle:timer, any:client )
{
    W3ResetPlayerColor( client, thisRaceID );
    War3_SetBuff( client, bNoMoveMode, thisRaceID, false );
}

stock TE_SetupDynamicLight(const Float:vecOrigin[3], r,g,b,iExponent,Float:fRadius,Float:fTime,Float:fDecay)
{
    TE_Start("Dynamic Light");
    TE_WriteVector("m_vecOrigin",vecOrigin);
    TE_WriteNum("r",r);
    TE_WriteNum("g",g);
    TE_WriteNum("b",b);
    TE_WriteNum("exponent",iExponent);
    TE_WriteFloat("m_fRadius",fRadius);
    TE_WriteFloat("m_fTime",fTime);
    TE_WriteFloat("m_fDecay",fDecay);
}


//dirty dealdamage workaround.. tell me if you got another idea :o
stock DealDamageWrapper(victim,attacker,damage,String:classname[32],Float:delay=0.1) {
    decl Handle:pack;
    CreateDataTimer(delay, Timer_DealDamageWrapper, pack);
    WritePackCell(pack, victim);
    WritePackCell(pack, attacker);
    WritePackCell(pack, damage);
    WritePackString(pack, classname);
}
public Action:Timer_DealDamageWrapper(Handle:timer, Handle:pack)
{
    ResetPack(pack); //resolve the package...
    new victim = ReadPackCell(pack);
    new attacker = ReadPackCell(pack);
    new damage = ReadPackCell(pack);
    decl String:classname[32];
    ReadPackString(pack,classname,sizeof(classname));
    War3_DealDamage(victim,damage,attacker,DMG_BULLET,classname);
}