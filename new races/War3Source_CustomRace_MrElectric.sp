/**
* File: War3Source_Mr_Electric.sp
* Description: The Spider Man race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx
*/

//vulp-edit: healtomaxHP removed, max HP 150 removed.

#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

// War3Source stuff
new thisRaceID;

// Chance/Data Arrays
new Float:ElectricGravity[5] = { 1.0, 0.92, 0.84, 0.76, 0.68 };
new Float:ShockChance[5] = { 0.0, 0.15, 0.20, 0.24, 0.35 };
new Float:BounceChance[5] = { 0.0, 0.15, 0.16, 0.18, 0.20 };
new Float:BounceDuration[5] = { 0.0, 1.0, 1.5, 2.0, 2.5 };
new Float:JumpMultiplier[5] = { 1.0, 1.5, 2.0, 2.3, 2.5 };
new StrikeDamage[5] = { 0, 10, 15, 20, 24 };
new m_vecBaseVelocity, m_vecVelocity_0, m_vecVelocity_1;
new Beam1, Beam2;
new SKILL_ATTACK, SKILL_LONGJUMP, SKILL_BOUNCY, ULT_STRIKE;
new Handle:enableCvar;
public Plugin:myinfo = 
{
	name = "War3Source Race - Mr Electric[Special++]",
	author = "xDr.HaaaaaaaXx & Revan",
	description = "The Mr Electric race for War3Source.[+sfx & +balanced",
	version = "1.0.0.0",
	url = ""
};

public OnPluginStart()
{
	m_vecBaseVelocity = FindSendPropOffs( "CBasePlayer", "m_vecBaseVelocity" );
	m_vecVelocity_0 = FindSendPropOffs( "CBasePlayer", "m_vecVelocity[0]" );
	m_vecVelocity_1 = FindSendPropOffs( "CBasePlayer", "m_vecVelocity[1]" );
	HookEvent( "player_jump", PlayerJumpEvent );
	enableCvar=CreateConVar("war3_electric_renderfx_enable","0","Enable Slow Pulse Wide RenderFX For MR Electric?");
}

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "Mr Electric", "electric" );
	
	SKILL_ATTACK = War3_AddRaceSkill( thisRaceID, "Shocker", "Send down a lightning that shakes your enemy with 15-35% chance.", false );	
	SKILL_LONGJUMP = War3_AddRaceSkill( thisRaceID, "Electricity Bounce", "Thanks to the lightning, your jumps gain great lenghts!", false );	
	SKILL_BOUNCY = War3_AddRaceSkill( thisRaceID, "Unstable Electric Armor", "Your armor is dangerous. With 15-20% chance, you get pushed away once you get hit.", false );
	ULT_STRIKE = War3_AddRaceSkill( thisRaceID, "Lightning Strike", "Sends a lightning from the ultimate form of natural electricty out of the sky to damage enemies with 10-24 damage.", true );
	
	W3SkillCooldownOnSpawn( thisRaceID, ULT_STRIKE, 25.0, true );
	
	War3_CreateRaceEnd( thisRaceID );
}

public InitPassiveSkills( client )
{
	if( War3_GetRace( client ) == thisRaceID )
	{
		new skilllevel_levi = War3_GetSkillLevel( client, thisRaceID, SKILL_LONGJUMP );
		new Float:gravity = ElectricGravity[skilllevel_levi];
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, gravity );
		//War3_SetMaxHP(client, 150);
		//War3_SetMaxHP( client, War3_GetMaxHP( client ) + 100 );
		if(GetConVarBool(enableCvar))
		{
			SetEntityRenderFx( client, RENDERFX_PULSE_SLOW_WIDE );
		}
		if(IsPlayerAlive(client))
		{
			new Float:iVec[3];
			GetClientEyePosition(client,iVec);
			iVec[2]+20.0;
			TE_SetupBeamRingPoint( iVec,74.0,75.0,Beam2,Beam2,0,15,5.0,60.0,12.0,{0,0,255,255},1,0);
			TE_SendToAll();
		}
	}
}

public OnRaceChanged( client,oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_SetBuff( client, fLowGravitySkill, thisRaceID, 1.0 );
		War3_SetBuff( client, fInvisibilitySkill, thisRaceID, 1.0 );
	}
	else
	{	
		if( IsPlayerAlive( client ) )
		{
			InitPassiveSkills( client );
		}
	}
}

public OnSkillLevelChanged( client, race, skill, newskilllevel )
{
	InitPassiveSkills( client );
}

public OnMapStart() {
	Beam1=PrecacheModel("effects/blueblacklargebeam.vmt");
	Beam2=PrecacheModel("materials/sprites/lgtning.vmt");
}

public OnWar3EventSpawn( client )
{
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		InitPassiveSkills( client );
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_level = War3_GetSkillLevel( attacker, thisRaceID, SKILL_ATTACK );
			if( !Hexed( attacker, false ) && GetRandomFloat( 0.0, 1.0 ) <= ShockChance[skill_level] )
			{
				new Float:velocity[3];
				velocity[0] += 10.0;
				velocity[1] += 10.0;
				velocity[2] += 150.0;
				SetEntDataVector( victim, m_vecBaseVelocity, velocity, true );
				War3_ShakeScreen( victim, 3.0, 20.0, 10.0 );
				W3FlashScreen( victim, RGBA_COLOR_RED );
				
				new Float:iVec[3];
				GetClientEyePosition(attacker,iVec);
				new Float:iVec2[3];
				GetClientAbsOrigin(victim,iVec2);
				iVec[2]+=999.0;
				TE_SetupBeamPoints(iVec,iVec2,Beam1,Beam1,0,41,1.6,25.0,35.0,0,7.5,{255,255,255,255},45);
				TE_SendToAll();
			}
		}
	}
}

public PlayerJumpEvent( Handle:event, const String:name[], bool:dontBroadcast )
{
	new client = GetClientOfUserId( GetEventInt( event, "userid" ) );
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		new skill_long = War3_GetSkillLevel( client, race, SKILL_LONGJUMP );
		if( skill_long > 0 )
		{
			new Float:velocity[3] = { 0.0, 0.0, 0.0 };
			velocity[0] = GetEntDataFloat( client, m_vecVelocity_0 );
			velocity[1] = GetEntDataFloat( client, m_vecVelocity_1 );
			velocity[0] *= JumpMultiplier[skill_long] * 0.18;
			velocity[1] *= JumpMultiplier[skill_long] * 0.24;
			SetEntDataVector( client, m_vecBaseVelocity, velocity, true );
		}
	}
}

public OnW3TakeDmgBullet( victim, attacker, Float:damage )
{
	if( IS_PLAYER( victim ) && IS_PLAYER( attacker ) && victim > 0 && attacker > 0 && attacker != victim )
	{
		new vteam = GetClientTeam( victim );
		new ateam = GetClientTeam( attacker );
		if( vteam != ateam )
		{
			new race_victim = War3_GetRace( victim );
			new skill_bouncy = War3_GetSkillLevel( victim, thisRaceID, SKILL_BOUNCY );
			if( race_victim == thisRaceID && skill_bouncy > 0 && !Hexed( victim, false ) ) 
			{
				if( GetRandomFloat( 0.0, 1.0 ) <= BounceChance[skill_bouncy] )
				{
					new Float:pos1[3];
					new Float:pos2[3];
					new Float:localvector[3];
					new Float:velocity1[3];
					new Float:velocity2[3];
					
					GetClientAbsOrigin( attacker, pos1 );
					GetClientAbsOrigin( victim, pos2 );
					
					localvector[0] = pos2[0] - pos1[0];
					localvector[1] = pos2[1] - pos1[1];
					localvector[2] = pos2[2] - pos1[2];
					
					velocity1[0] += GetRandomInt(40,60);
					velocity1[1] += GetRandomInt(40,60);
					velocity1[2] += 165;
					
					velocity2[0] = localvector[0] * (100);
					velocity2[1] = localvector[1] * (100);
					velocity2[2] = localvector[2] * (100);
					
					SetEntDataVector( victim, m_vecBaseVelocity, velocity1, true );
					SetEntDataVector( victim, m_vecBaseVelocity, velocity2, true );
					
					War3_SetBuff( victim, fInvisibilitySkill, thisRaceID, 0.0 );

					CreateTimer( BounceDuration[skill_bouncy], InvisStop, victim );
					CreateTimer( 3.0, InvisStop, victim );
					TE_SetupBeamFollow(victim,Beam1,0,1.4,30.0,40.0,20,{250,180,180,255});
					TE_SendToAll();
					W3FlashScreen( victim, RGBA_COLOR_BLUE, BounceDuration[skill_bouncy] );
				}
			}
		}
	}
}

public Action:InvisStop( Handle:timer, any:client )
{
	War3_SetBuff( client, fInvisibilitySkill, thisRaceID, 1.0 );
}

public OnUltimateCommand( client, race, bool:pressed )
{
	if( race == thisRaceID && pressed && IsPlayerAlive( client ) && !Silenced( client ) )
	{
		new ult_level = War3_GetSkillLevel( client, race, ULT_STRIKE );
		if( ult_level > 0 )
		{
			if( War3_SkillNotInCooldown( client, thisRaceID, ULT_STRIKE, true ) )
			{
				ElectricStrikeRandomPlayer( client );
			}
		}
		else
		{
			W3MsgUltNotLeveled( client );
		}
	}
}

Action:ElectricStrikeRandomPlayer( client )
{
	new Float:posVec[3];
	GetClientAbsOrigin( client, posVec );
	new Float:otherVec[3];
	new Float:bestTargetDistance = 800.0; 
	new team = GetClientTeam( client );
	new bestTarget = 0;
	
	for( new i = 1; i <= MaxClients; i++ )
	{
		if( ValidPlayer( i, true ) && GetClientTeam( i ) != team && !W3HasImmunity( i, Immunity_Ultimates ) )
		{
			GetClientAbsOrigin( i, otherVec );
			new Float:dist = GetVectorDistance( posVec, otherVec );
			if( dist < bestTargetDistance )
			{
				bestTarget = i;
				bestTargetDistance = GetVectorDistance( posVec, otherVec );
			}
		}
	}
	new ult_level = War3_GetSkillLevel( client, thisRaceID, ULT_STRIKE );
	if( bestTarget == 0 )
	{
		PrintHintText( client, "No Target Found within %.1f feet", bestTargetDistance / 10 );
	}
	else
	{
		War3_DealDamage( bestTarget, StrikeDamage[ult_level], client, DMG_ENERGYBEAM, "lightning" );
		//War3_HealToMaxHP( client, StrikeDamage[ult_level] );
		
		W3PrintSkillDmgHintConsole( bestTarget, client, War3_GetWar3DamageDealt(), ULT_STRIKE );
		W3FlashScreen( bestTarget, RGBA_COLOR_RED );
		
		new Float:iVec[3];
		GetClientEyePosition(client,iVec);
		new Float:iVec2[3];
		GetClientAbsOrigin(bestTarget,iVec2);
		iVec[2]+=1500.0;
		TE_SetupBeamPoints(iVec,iVec2,Beam2,Beam2,0,41,1.6,20.0,20.0,0,9.5,{230,230,255,200},45);
		TE_SendToAll();
		War3_CooldownMGR( client, 28.0, thisRaceID, ULT_STRIKE, _, true);
		PrintHintText( client, "You called a Lightning Strike!");
	}
}