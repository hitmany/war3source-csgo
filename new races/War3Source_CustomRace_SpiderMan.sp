/**
* File: War3Source_Spider_Man.sp
* Description: The Spider Man race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx 
*/

#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

// War3Source stuff
new thisRaceID;

// Chance/Data Arrays
new Float:WebFreezeChance[5] = { 0.0, 0.15, 0.17, 0.19, 0.21 };
new Float:EvadeChance[5] = { 0.0, 0.10, 0.125, 0.17, 0.18 };
new Float:JumpMultiplier[5] = { 1.0, 1.3, 1.4, 1.5, 2.6 };
new Float:SpiderSpeed[5] = { 1.0, 1.2, 1.25, 1.29, 1.35 };
new Float:PushForce[5] = { 0.0, 0.7, 1.1, 1.3, 1.7 };

// Sounds
new String:ult_sound[] = "weapons/357/357_spin1.wav";

// Other
new m_vecBaseVelocity, m_vecVelocity_0, m_vecVelocity_1;
new BeamSprite, pfaden;

new SKILL_EVADE, SKILL_LONGJUMP, SKILL_FREEZE, ULT_WEB;

public Plugin:myinfo = 
{
	name = "Spider Man",
	author = "xDr.HaaaaaaaXx",
	description = "The Spider Man race for War3Source.",
	version = "1.0.0.0",
	url = ""
};

public OnMapStart()
{
	War3_PrecacheSound( ult_sound );
	BeamSprite = PrecacheModel( "materials/sprites/laser.vmt" );
	pfaden = PrecacheModel( "effects/com_shield003a.vmt" );
}

public OnPluginStart()
{
	m_vecBaseVelocity = FindSendPropOffs( "CBasePlayer", "m_vecBaseVelocity" );
	m_vecVelocity_0 = FindSendPropOffs( "CBasePlayer", "m_vecVelocity[0]" );
	m_vecVelocity_1 = FindSendPropOffs( "CBasePlayer", "m_vecVelocity[1]" );
	HookEvent( "player_jump", PlayerJumpEvent );
}

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "Spider Man", "spider" );
	
	SKILL_EVADE = War3_AddRaceSkill( thisRaceID, "Spider sense", "Your spidey-senses make you see bullets and evade them with 10-18% chance.", false );
	SKILL_LONGJUMP = War3_AddRaceSkill( thisRaceID, "Agility", "Your ultra strength lets you run faster and jump further. Up to 35% more speed!", false );	
	SKILL_FREEZE = War3_AddRaceSkill( thisRaceID, "Web-shooters", "Capture your foe in your web! with 15-21% chance!", false );
	ULT_WEB = War3_AddRaceSkill( thisRaceID, "Weblines", "Use your Weblines to travel around the map", true );
	
	W3SkillCooldownOnSpawn( thisRaceID, ULT_WEB, 6.0, true);
	
	War3_CreateRaceEnd( thisRaceID );
}

public InitPassiveSkills( client )
{
	if( War3_GetRace( client ) == thisRaceID )
	{
		new skill_speed = War3_GetSkillLevel( client, thisRaceID, SKILL_LONGJUMP );
		new Float:speed = SpiderSpeed[skill_speed];
		War3_SetBuff( client, fMaxSpeed, thisRaceID, speed );
	}
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
	}
	else
	{	
		if( IsPlayerAlive( client ) )
		{
			InitPassiveSkills( client );
		}
	}
}

public OnSkillLevelChanged( client, race, skill, newskilllevel )
{
	InitPassiveSkills( client );
}

public OnWar3EventSpawn( client )
{
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		InitPassiveSkills( client );
		new Float:pos[3];
		GetClientAbsOrigin(client,pos);
		TE_SetupGlowSprite( pos, BeamSprite, 3.5 , 1.5 , 255);
		TE_SendToAll();
		W3FlashScreen( client, {255,255,255,255} );
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_level = War3_GetSkillLevel( attacker, thisRaceID, SKILL_FREEZE );
			if( !Hexed( attacker, false ) && GetRandomFloat( 0.0, 1.0 ) <= WebFreezeChance[skill_level] )
			{
				War3_SetBuff( victim, bNoMoveMode, thisRaceID, true );
				CreateTimer( 1.0, StopFreeze, victim );				
				W3FlashScreen( victim, RGBA_COLOR_RED );
				PrintHintText(victim,"You can't move!");
				new Float:startpos[3];
				new Float:endpos[3];
				GetClientAbsOrigin( attacker, startpos );
				GetClientAbsOrigin( victim, endpos );
				startpos[2]+=45;
				endpos[2]+=35;
				TE_SetupBeamPoints( startpos, endpos, BeamSprite, BeamSprite, 0, 20, 1.5, 1.0, 20.0, 0, 3.5, { 200, 200, 200, 255 }, 0 );
				TE_SendToAll();
				startpos[2]+=45;
				TE_SetupBeamPoints( startpos, endpos, BeamSprite, BeamSprite, 0, 20, 1.5, 1.0, 20.0, 0, 3.5, { 200, 200, 200, 255 }, 0 );
				TE_SendToAll();
			}
		}
	}
}

public Action:StopFreeze( Handle:timer, any:client )
{
	War3_SetBuff( client, bNoMoveMode, thisRaceID, false );
}

public PlayerJumpEvent( Handle:event, const String:name[], bool:dontBroadcast )
{
	new client = GetClientOfUserId( GetEventInt( event, "userid" ) );
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		new skill_long = War3_GetSkillLevel( client, race, SKILL_LONGJUMP );
		if( skill_long > 0 )
		{
			new Float:velocity[3] = { 0.0, 0.0, 0.0 };
			velocity[0] = GetEntDataFloat( client, m_vecVelocity_0 );
			velocity[1] = GetEntDataFloat( client, m_vecVelocity_1 );
			velocity[0] *= JumpMultiplier[skill_long] * 0.25;
			velocity[1] *= JumpMultiplier[skill_long] * 0.25;
			SetEntDataVector( client, m_vecBaseVelocity, velocity, true );
			TE_SetupBeamFollow(client,pfaden,0,0.9,0.5,17.8,15,{250,250,100,255});
			TE_SendToAll();
		}
	}
}

public OnW3TakeDmgBulletPre( victim, attacker, Float:damage )
{
	if( IS_PLAYER( victim ) && IS_PLAYER( attacker ) && victim > 0 && attacker > 0 && attacker != victim )
	{
		new vteam = GetClientTeam( victim );
		new ateam = GetClientTeam( attacker );
		if( vteam != ateam )
		{
			new race_victim = War3_GetRace( victim );
			new skill_evade = War3_GetSkillLevel( victim, thisRaceID, SKILL_EVADE );
			if( race_victim == thisRaceID && skill_evade > 0 && !Hexed( victim, false ) ) 
			{
				new Float:percent = EvadeChance[skill_evade];
				if( GetRandomFloat( 0.0, 1.0 ) <= percent )
				{
					W3FlashScreen( victim, RGBA_COLOR_BLUE );
					War3_DamageModPercent( 0.0 );
					W3MsgEvaded( victim, attacker );
					new Float:pos[3];
					PrintToConsole(victim,"[W3S]Evaded a shoot");
					GetClientAbsOrigin( victim, pos );
					TE_SetupBeamRingPoint( pos,65.0,75.0,pfaden,pfaden,0,15,6.20,100.0,2.0,{255,0,0,255},30,0);
					TE_SendToAll();
				}
			}
		}
	}
}

public OnUltimateCommand( client, race, bool:pressed )
{
	if( race == thisRaceID && pressed && IsPlayerAlive( client ) && !Silenced( client ) )
	{
		new ult_level = War3_GetSkillLevel( client, race, ULT_WEB );
		if( ult_level > 0 )
		{
			if( War3_SkillNotInCooldown( client, thisRaceID, ULT_WEB, true ) )
			{
				TeleportPlayer( client );
				EmitSoundToAll( ult_sound, client );
				//War3_CooldownMGR( client, 5, thisRaceID, ULT_WEB, _, _, false, "Webline" );
				War3_CooldownMGR(client,6.3,thisRaceID,ULT_WEB,_,_);
			}
		}
		else
		{
			W3MsgUltNotLeveled( client );
		}
	}
}

TeleportPlayer( client )
{
	if( client > 0 && IsPlayerAlive( client ) )
	{
		new ult_level = War3_GetSkillLevel( client, thisRaceID, ULT_WEB );
		new Float:startpos[3];
		new Float:endpos[3];
		new Float:localvector[3];
		new Float:velocity[3];
		
		GetClientAbsOrigin( client, startpos );
		War3_GetAimEndPoint( client, endpos );
		
		localvector[0] = endpos[0] - startpos[0];
		localvector[1] = endpos[1] - startpos[1];
		localvector[2] = endpos[2] - startpos[2];
		
		velocity[0] = localvector[0] * PushForce[ult_level];
		velocity[1] = localvector[1] * PushForce[ult_level];
		velocity[2] = localvector[2] * PushForce[ult_level];
		
		SetEntDataVector( client, m_vecBaseVelocity, velocity, true );
		
		TE_SetupBeamPoints( startpos, endpos, pfaden, pfaden, 0, 0, 1.5, 1.0, 1.0, 0, 1.0, { 200, 200, 200, 255 }, 0 );
		TE_SendToAll();
		TE_SetupBeamPoints( startpos, endpos, pfaden, pfaden, 0, 0, 1.5, 1.0, 1.0, 0, 6.0, { 255, 255, 255, 255 }, 0 );
		TE_SendToAll();
	}
}

//dirty dealdamage workaround.. tell me if you got another idea :o
stock DealDamageWrapper(victim,attacker,damage,String:classname[32],Float:delay=0.1) {
	new Handle:pack;
	CreateDataTimer(delay, Timer_DealDamageWrapper, pack);
	WritePackCell(pack, victim);
	WritePackCell(pack, attacker);
	WritePackCell(pack, damage);
	WritePackString(pack, classname);
}
public Action:Timer_DealDamageWrapper(Handle:timer, Handle:pack)
{
	ResetPack(pack); //resolve the package...
	new victim = ReadPackCell(pack);
	new attacker = ReadPackCell(pack);
	new damage = ReadPackCell(pack);
	decl String:classname[32];
	ReadPackString(pack,classname,sizeof(classname));
	War3_DealDamage(victim,damage,attacker,DMG_BULLET,classname);
}