#pragma semicolon 1    ///WE RECOMMEND THE SEMICOLON

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"

public Plugin:myinfo = 
{
	name = "War3Source Race - Rogue Knight",
	author = "Ted Theodore Logan",
	description = "The Rogue Knight race for War3Source.",
	version = "1.1",
};

new thisRaceID;
new SKILL_BOLT, SKILL_CLEAVE, SKILL_WARCRY, ULT_STRENGTH;

// Tempents
new g_BeamSprite;
new g_HaloSprite;

// Storm Bolt 
new BoltDamage[5] = {0,5,10,15,20};
new Handle:StormCooldownTime; // cooldown
new Float:BoltRange = 380.0;
new const StormCol[4] = {0, 0, 255, 255}; // Color of the beacon
new bool:bIsStunned[MAXPLAYERS];

// Cleave Multiplayer
new Float:CleaveDistance[5] = {0.0,50.0,100.0,150.0,200.0};
new Float:CleaveMultiplier[5] = {0.0,0.05,0.1,0.15,0.2};

// Warcry Buffs
new Float:WarcrySpeed[5]={1.0,1.06,1.12,1.18,1.24};
new Float:WarcryArmor[5]={1.0,0.94,0.88,0.82,0.76};

// Gods Strength
new Float:GodsStrength[5]={1.0,1.25,1.50,1.75,2.0};
new bool:bStrengthActivated[MAXPLAYERS];
new Handle:StrengthCooldown; // cooldown

// Sounds
new String:skillsnd[]="war3source/sven/cast.mp3";
new String:ultsnd[]="war3source/sven/grunt.mp3";

public OnWar3PluginReady(){
	thisRaceID=War3_CreateNewRace("Rogue Knight","sven");
	SKILL_BOLT=War3_AddRaceSkill(thisRaceID,"Storm Bolt (Ability)","Do a strong AoE and stun enemies, dealing 5-20 damage!",false,4);
	SKILL_CLEAVE=War3_AddRaceSkill(thisRaceID,"Great Cleave","Your attacks are so strong they damage foes around you with +5-20% extra damage!",false,4);
	SKILL_WARCRY=War3_AddRaceSkill(thisRaceID,"Warcry","Your warcry makes all enemies tremble while you get stronger.\nGet 6-24% damage and receive 6-24% less damage!",false,4);
	ULT_STRENGTH=War3_AddRaceSkill(thisRaceID,"Gods Strength (Ultimate)","Greatly enhance your damage for a short amount of time! Gain 25-100% additional damage!",true,4); 
	War3_CreateRaceEnd(thisRaceID); ///DO NOT FORGET THE END!!!
}

public OnPluginStart()
{
	StormCooldownTime=CreateConVar("war3_sven_bolt_cooldown","15","Cooldown timer.");
	StrengthCooldown=CreateConVar("war3_sven_strength_cooldown","30","Cooldown timer.");
}

public OnMapStart()
{
	// Precache the stuff for the beacon ring
	g_BeamSprite = PrecacheModel("materials/sprites/laser.vmt");
	g_HaloSprite = PrecacheModel("materials/sprites/halo01.vmt");   
	//Sounds
	War3_PrecacheSound(skillsnd);
	War3_PrecacheSound(ultsnd);
}

public OnWar3EventSpawn(client)
{
	InitPassiveSkills(client);
	bIsStunned[client] = false;
	bStrengthActivated[client] = false;
	W3ResetPlayerColor(client, thisRaceID);
}

public OnSkillLevelChanged(client,race,skill,newskilllevel)
{
	InitPassiveSkills(client);
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace == thisRaceID )
	if(IsPlayerAlive( client ))
	InitPassiveSkills( client );
	else
	War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
}

public InitPassiveSkills(client)
{
	if(War3_GetRace(client)==thisRaceID)
	{
		new skill = War3_GetSkillLevel(client,thisRaceID,SKILL_WARCRY);
		new Float:speed = WarcrySpeed[skill];
		War3_SetBuff(client,fMaxSpeed,thisRaceID,speed);
	}
}

public OnW3TakeDmgBulletPre(victim,attacker,Float:damage){
	if(ValidPlayer(victim,true)&&ValidPlayer(attacker,false)&&GetClientTeam(victim)!=GetClientTeam(attacker))
	{
		if(War3_GetRace(victim)==thisRaceID)
		{
			// Apply armor to victims
			new skill = War3_GetSkillLevel(victim,thisRaceID,SKILL_WARCRY);
			War3_DamageModPercent(WarcryArmor[skill]);
		}
		if(War3_GetRace(attacker)==thisRaceID)
		{
			new skill;
			if(bStrengthActivated[attacker])
			{
				// GODS STRENGTH!
				skill = War3_GetSkillLevel(attacker,thisRaceID,ULT_STRENGTH);
				War3_DamageModPercent(GodsStrength[skill]);
				// For Cleave...
				damage = damage * GodsStrength[skill];
				new Float:spos[3];
				new Float:epos[3];
				GetClientAbsOrigin(victim,epos);
				GetClientAbsOrigin(attacker,spos);
				epos[2]+=35;
				spos[2]+=35;
				TE_SetupBeamPoints(spos, epos, g_BeamSprite, g_HaloSprite, 0, 35, 1.0, 10.0, 10.0, 0, 10.0, {25,25,25,255}, 30);
				TE_SendToAll();
			}
			// Cleave
			skill = War3_GetSkillLevel(attacker,thisRaceID,SKILL_CLEAVE);
			new splashdmg = RoundToFloor(damage * CleaveMultiplier[skill]);
			// AWP? AWP!
			if(splashdmg>40)
			{
				splashdmg = 40;
			}
			new Float:dist = CleaveDistance[skill];
			new AttackerTeam = GetClientTeam(attacker);
			new Float:OriginalVictimPos[3];
			GetClientAbsOrigin(victim,OriginalVictimPos);
			new Float:VictimPos[3];
			
			if(attacker>0)
			{
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true)&&(GetClientTeam(i)!=AttackerTeam)&&(victim!=i))
					{
						GetClientAbsOrigin(i,VictimPos);
						if(GetVectorDistance(OriginalVictimPos,VictimPos)<=dist)
						{
							//War3_DealDamage(i,splashdmg,attacker,DMG_BURN,"greatcleave",W3DMGORIGIN_SKILL);
							DealDamageWrapper(i,attacker,splashdmg,"greatcleave");
							new Float:spos[3];
							new Float:epos[3];
							GetClientAbsOrigin(victim,epos);
							GetClientAbsOrigin(attacker,spos);
							epos[2]+=35;
							spos[2]+=95;
							TE_SetupBeamPoints(spos, epos, g_BeamSprite, g_HaloSprite, 0, 35, 1.2, 1.0, 45.0, 0, 10.0, {25,25,50,255}, 30);
							TE_SendToAll();
						}
					}
				}
			}
		}
	}
}

public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client))
	{
		new skill = War3_GetSkillLevel(client,thisRaceID,SKILL_BOLT);
		if(skill > 0)
		{
			
			if(!Silenced(client)&&War3_SkillNotInCooldown(client,thisRaceID,SKILL_BOLT,true))
			{
				new damage = BoltDamage[skill];
				new Float:AttackerPos[3];
				GetClientAbsOrigin(client,AttackerPos);
				new AttackerTeam = GetClientTeam(client);
				new Float:VictimPos[3];
				
				TE_SetupBeamRingPoint(AttackerPos, 10.0, BoltRange, g_BeamSprite, g_HaloSprite, 0, 15, 0.5, 5.0, 0.0, StormCol, 10, 0);
				TE_SendToAll();
				
				for(new i=1;i<=MaxClients;i++)
				{
					if(ValidPlayer(i,true)){
						GetClientAbsOrigin(i,VictimPos);
						if(GetVectorDistance(AttackerPos,VictimPos)<BoltRange && !W3HasImmunity(i,Immunity_Skills) && !W3HasImmunity(i,Immunity_Ultimates))
						{
							if(GetClientTeam(i)!=AttackerTeam)
							{
								War3_DealDamage(i,damage,client,DMG_BURN,"stormbolt",W3DMGORIGIN_SKILL);
								if(!bIsStunned[i])
								{
									bIsStunned[i]=true;
									// Nice color :3
									W3SetPlayerColor(i,thisRaceID, StormCol[0], StormCol[1], StormCol[2], StormCol[3]); 
									//War3_SetBuff(i,bNoMoveMode,thisRaceID,true);
									War3_SetBuff( i, bBashed, thisRaceID, true );
									W3FlashScreen(i,RGBA_COLOR_RED);
									CreateTimer(1.5,UnstunPlayer,i);
									
									PrintHintText(i,"You were stunned by Storm Bolt");
								}
							}
						}
					}
				}
				EmitSoundToAll(skillsnd,client);
				War3_CooldownMGR(client,GetConVarFloat(StormCooldownTime),thisRaceID,SKILL_BOLT,false,true);
			}
		}
	}
}

public Action:UnstunPlayer(Handle:timer,any:client)
{
	
	PrintHintText(client,"No longer stunned");
	//War3_SetBuff(client,bNoMoveMode,thisRaceID,false);
	War3_SetBuff( client, bBashed, thisRaceID, false );
	bIsStunned[client]=false;
	W3ResetPlayerColor(client, thisRaceID);
	
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && ValidPlayer(client,true))
	{
		new skill = War3_GetSkillLevel(client,thisRaceID,ULT_STRENGTH);
		if(skill>0)
		{    
			if(!Silenced(client)&&War3_SkillNotInCooldown(client,thisRaceID,ULT_STRENGTH,true ))
			{
				PrintHintText(client, "The gods lend you their strength");
				bStrengthActivated[client] = true;
				CreateTimer(5.0,stopUltimate,client);
				
				EmitSoundToAll(ultsnd,client);  
				War3_CooldownMGR(client,GetConVarFloat(StrengthCooldown),thisRaceID,ULT_STRENGTH,false,true);
			}
		}
	}
}


public Action:stopUltimate(Handle:t,any:client){
	bStrengthActivated[client] = false;
	if(ValidPlayer(client,true)){
		PrintHintText(client,"You feel less powerful");
	}
}

//dirty dealdamage workaround.. tell me if you got another idea :o
stock DealDamageWrapper(victim,attacker,damage,String:classname[32],Float:delay=0.1) {
	new Handle:pack;
	CreateDataTimer(delay, Timer_DealDamageWrapper, pack);
	WritePackCell(pack, victim);
	WritePackCell(pack, attacker);
	WritePackCell(pack, damage);
	WritePackString(pack, classname);
}
public Action:Timer_DealDamageWrapper(Handle:timer, Handle:pack)
{
	ResetPack(pack); //resolve the package...
	new victim = ReadPackCell(pack);
	new attacker = ReadPackCell(pack);
	new damage = ReadPackCell(pack);
	decl String:classname[32];
	ReadPackString(pack,classname,sizeof(classname));
	War3_DealDamage(victim,damage,attacker,DMG_BULLET,classname);
}