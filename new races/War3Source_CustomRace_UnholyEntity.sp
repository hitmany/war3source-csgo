/**
 * File: War3Source_Unholy_Entity.sp
 * Description: The Unholy Entity race for War3Source.
 * Author(s): xDr.HaaaaaaaXx
 */
 
#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

new thisRaceID;

//skill 1
new Float:EntSpeed[6] = { 1.0, 1.16, 1.20, 1.24, 1.28, 1.32 };

//skill 2
new Float:BuryChance[6] = { 0.0, 0.13, 0.15, 0.17, 0.19, 0.22 };

//skill 3
new Float:AvangeChance[6] = { 0.0, 0.50, 0.55, 0.60, 0.65, 0.70 };

//ultimate
new Float:UltDelay[6] = { 0.0, 45.0, 40.0, 35.0, 30.0, 25.0 };

new SKILL_SPEED, SKILL_BURY, SKILL_AVENGE, ULT_TRADE;
new Float:Ult_ClientPos[64][3];
new Float:Ult_EnemyPos[64][3];
new Float:Client_Pos[64][3];
new Ult_BestTarget[64];
new BestTarget[64];
new bool:Buryed[MAXPLAYERS];
new String:Sound[] ="ambient/atmosphere/cave_hit5.wav";
new BeamSprite, HaloSprite, Ult_BeamSprite1, Ult_BeamSprite2;

public Plugin:myinfo = 
{
	name = "War3Source Race - Unholy Entity",
	author = "xDr.HaaaaaaaXx",
	description = "The Unholy Entity race for War3Source.",
	version = "1.0.0.0",
	url = ""
};

public OnPluginStart()
{
	HookEvent( "player_death", PlayerDeathEvent );
}

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "Unholy Entity", "unholyent" );
	
	SKILL_SPEED = War3_AddRaceSkill( thisRaceID, "Swift", "Your legs have been enhanced by dark magic. Run 16-32% faster!", false, 4 );
	SKILL_BURY = War3_AddRaceSkill( thisRaceID, "Bury", "Cast a magic spell on your enemy that buries them inside the earth when shot! 13-22% chance!", false, 4 );
	SKILL_AVENGE = War3_AddRaceSkill( thisRaceID, "Avenge", "Call forth a teammate to avenge your death! 50-70% chance!", false, 4 );
	ULT_TRADE = War3_AddRaceSkill( thisRaceID,  "Possessor", "Confuse everyone by changing your position with a random enemy! You two trade places!", true, 4 );
	
	W3SkillCooldownOnSpawn( thisRaceID, ULT_TRADE, 20.0, true );
	
	War3_CreateRaceEnd( thisRaceID );
}

public OnMapStart()
{
	BeamSprite = PrecacheModel( "sprites/bluelight1.vmt" );
	HaloSprite = PrecacheModel( "materials/sprites/halo01.vmt" );
	Ult_BeamSprite1 = PrecacheModel( "materials/effects/ar2_altfire1.vmt" );
	Ult_BeamSprite2 = PrecacheModel( "materials/cs_havana/pupil_l.vmt" );
	War3_PrecacheSound( Sound );
}

public InitPassiveSkills( client )
{
	if( War3_GetRace( client ) == thisRaceID )
	{
		new skill_speed = War3_GetSkillLevel( client, thisRaceID, SKILL_SPEED );
		new Float:speed = EntSpeed[skill_speed];
		War3_SetBuff( client, fMaxSpeed, thisRaceID, speed );
	}
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
	}
	else
	{	
		if( IsPlayerAlive( client ) )
		{
			InitPassiveSkills( client );
		}
	}
}

public OnSkillLevelChanged( client, race, skill, newskilllevel )
{
	InitPassiveSkills( client );
}

public OnWar3EventSpawn( client )
{
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		InitPassiveSkills(client);
		EmitSoundToAll( Sound, client );
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_bury = War3_GetSkillLevel( attacker, thisRaceID, SKILL_BURY );
			if( !Hexed( attacker, false ) && skill_bury > 0 && GetRandomFloat( 0.0, 1.0 ) <= BuryChance[skill_bury] )
			{
				Buryed[victim]=true;
				new Float:attacker_pos[3];
				new Float:victim_pos[3];

				GetClientAbsOrigin( attacker, attacker_pos );
				GetClientAbsOrigin( victim, victim_pos );
				
				victim_pos[2] -= 40;
				
				TeleportEntity( victim, victim_pos, NULL_VECTOR, NULL_VECTOR );

				victim_pos[2] += 40;

				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 165, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[1] += 70;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[1] += 70;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				GetClientAbsOrigin( attacker, attacker_pos );
				GetClientAbsOrigin( victim, victim_pos );
				
				attacker_pos[2] += 40;
				victim_pos[2] += 40;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[1] += 50;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[0] += 50;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				GetClientAbsOrigin( attacker, attacker_pos );
				GetClientAbsOrigin( victim, victim_pos );
				
				attacker_pos[2] += 140;
				victim_pos[2] += 40;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[1] += 170;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[0] += 170;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				GetClientAbsOrigin( attacker, attacker_pos );
				GetClientAbsOrigin( victim, victim_pos );
				
				attacker_pos[2] += 1140;
				victim_pos[2] += 40;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[1] += 5;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[0] += 3;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				GetClientAbsOrigin( attacker, attacker_pos );
				GetClientAbsOrigin( victim, victim_pos );
				
				attacker_pos[2] += 20;
				victim_pos[2] += 20;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[1] += 30;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				
				attacker_pos[0] += 30;
				
				TE_SetupBeamPoints( attacker_pos, victim_pos, BeamSprite, HaloSprite, 0, 0, 5.0, 10.0, 10.0, 0, 0.0, { 215, 11, 167, 255 }, 0 );
				TE_SendToAll();
				CreateTimer(GetRandomFloat( 1.0, 5.0 ),PortUp,victim); //unbury
			}
		}
	}
}

public Action:PortUp(Handle:timer, any:target)
{
	if( ValidPlayer( target, true )) {
		Buryed[target]=false;
		new Float:victim_pos[3];
		GetClientAbsOrigin( target, victim_pos );			
		victim_pos[2] += 41;	
		TeleportEntity( target, victim_pos, NULL_VECTOR, NULL_VECTOR );
		TE_SetupGlowSprite(victim_pos,BeamSprite,1.5,2.0,200);
		TE_SendToAll();
		PrintToChat(target,"\x05Un-Buryed");
	}
}

public PlayerDeathEvent( Handle:event, const String:name[], bool:dontBroadcast )
{
	new client = GetClientOfUserId( GetEventInt( event, "userid" ) );
	if( War3_GetRace( client ) == thisRaceID )
	{
		new skill_avange = War3_GetSkillLevel( client, thisRaceID, SKILL_AVENGE );
		if( skill_avange > 0 && GetRandomFloat( 0.0, 1.0 ) <= AvangeChance[skill_avange] )
		{
			new Float:TeamMate_Pos[3];
			BestTarget[client] = 0;
			
			new targettable[MaxClients];
			new x = 0;
			for( new i = 0; i <= MaxClients-1; i++ )
			{
				targettable[i] = 0;
			}
			for( new i = 1; i <= MaxClients; i++ )
			{
				if( ValidPlayer( i, true ) && GetClientTeam( i ) == GetClientTeam( client ) && !W3HasImmunity( i, Immunity_Ultimates ) )
				{
					targettable[x] = i;
					x++;
				}
			}
			BestTarget[client] = targettable[GetRandomInt( 0, x )];
			if(ValidPlayer(BestTarget[client],true)) {
				GetClientAbsOrigin( client, Client_Pos[client] );
				GetClientAbsOrigin( BestTarget[client], TeamMate_Pos );
				
				if( BestTarget[client] == 0 )
				{
					PrintHintText( client, "No Target Found" );
				}
				else
				{
					CreateTimer( 6.0, AvangeTeleport, client );
					
					new String:Name[64];
					GetClientName( BestTarget[client], Name, 64 );
					
					PrintToChat( client, "You call apon %s to Avenge your Death", Name );
					PrintToChat( BestTarget[client], "A teammate has been slayed, you have been summuned to avenge his death" );
					
					EmitSoundToAll( Sound, client );
					EmitSoundToAll( Sound, BestTarget[client] );
				}
			}
		}
	}
}

public Action:AvangeTeleport( Handle:timer, any:client )
{
	TeleportEntity( BestTarget[client], Client_Pos[client], NULL_VECTOR, NULL_VECTOR );
	PrintToChat(client,"\x03Avenge your Mate!");
}

public OnUltimateCommand( client, race, bool:pressed )
{
	if( race == thisRaceID && pressed && IsPlayerAlive( client ) )
	{
		new ult_level = War3_GetSkillLevel( client, race, ULT_TRADE );
		if( ult_level > 0 )
		{
			if( War3_SkillNotInCooldown( client, thisRaceID, ULT_TRADE, true ) )
			{
				Trade( client );
				War3_CooldownMGR( client, UltDelay[ult_level], thisRaceID, ULT_TRADE, _, true );
			}
		}
		else
		{
			W3MsgUltNotLeveled( client );
		}
	}
}

Action:Trade( client )
{
	Ult_BestTarget[client] = 0;
	new targettable[MaxClients];
	new x = 0;
	for( new i = 0; i <= MaxClients-1; i++ )
	{
		targettable[i] = 0;
	}
	for( new i = 1; i <= MaxClients; i++ )
	{
		if( ValidPlayer( i, true ) && GetClientTeam( i ) != GetClientTeam( client ) && !W3HasImmunity( i, Immunity_Ultimates ) )
		{
			targettable[x] = i;
			x++;
		}
	}
	Ult_BestTarget[client] = targettable[GetRandomInt( 0, x )];
	
	GetClientAbsOrigin( Ult_BestTarget[client], Ult_EnemyPos[client] );
	GetClientAbsOrigin( client, Ult_ClientPos[client] );

	if( Ult_BestTarget[client] == 0 )
	{
		PrintHintText( client, "No Target Found" );
	}
	else
	{
		new String:Name[64];
		GetClientName( Ult_BestTarget[client], Name, 64 );
	
		EmitSoundToAll( Sound, client );
		EmitSoundToAll( Sound, Ult_BestTarget[client] );
		
		PrintToChat( client, ":You will trade places with %s in three seconds", Name );
		
		CreateTimer( 3.0, TradeDelay, client );
		
		new Float:BeamPos[3];
		BeamPos[0] = Ult_ClientPos[client][0];
		BeamPos[1] = Ult_ClientPos[client][1];
		BeamPos[2] = Ult_ClientPos[client][2] + 40.0;
		
		TE_SetupBeamRingPoint( BeamPos, 950.0, 190.0, Ult_BeamSprite1, HaloSprite, 0, 0, 3.0, 150.0, 0.0, { 115, 115, 100, 200 }, 1, FBEAM_ISACTIVE );
		TE_SendToAll();

		TE_SetupBeamRingPoint( BeamPos, 950.0, 190.0, Ult_BeamSprite2, HaloSprite, 0, 0, 3.0, 150.0, 0.0, { 115, 115, 100, 200 }, 1, FBEAM_ISACTIVE );
		TE_SendToAll();
	}
}

public Action:TradeDelay( Handle:timer, any:client )
{
	TeleportEntity( Ult_BestTarget[client], Ult_ClientPos[client], NULL_VECTOR, NULL_VECTOR );
	TeleportEntity( client, Ult_EnemyPos[client], NULL_VECTOR, NULL_VECTOR );
}