/**
* File: War3Source_Sniper.sp
* Description: The Sniper race for SourceCraft.
* Author(s): xDr.HaaaaaaaXx
*/

#pragma semicolon 1
#include <sourcemod>
#include <sdktools_tempents>
#include <sdktools_functions>
#include <sdktools_tempents_stocks>
#include <sdktools_entinput>
#include <sdktools_sound>

#include "W3SIncs/War3Source_Interface"

// War3Source stuff
new thisRaceID;

// Chance/Data Arrays
new Float:BeaconChance[6] = { 0.0, 0.05, 0.10, 0.15, 0.20, 0.25 };
new Float:DisgChance[6] = { 0.0, 0.10, 0.20, 0.30, 0.50, 0.75 };
new Float:UltDuration[6] = { 0.0, 2.0, 3.0, 4.0, 5.0, 6.0 };
new CashMin[6] = { 0, 20, 30, 45, 75, 100 };
new CashMax[6] = { 0, 30, 45, 60, 100, 150 };
new XPMin[6] = { 0, 10, 15, 20, 30, 35 };
new XPMax[6] = { 0, 15, 20, 25, 35, 40 };
new HaloSprite, BeamSprite;
new MoneyOffsetCS;

new SKILL_SKIN, SKILL_XP, SKILL_BEACON, SKILL_HIDE;
new bool:bInUltimate[MAXPLAYERS];

public Plugin:myinfo = 
{
	name = "Sniper",
	author = "xDr.HaaaaaaaXx",
	description = "The Sniper race for War3Source.",
	version = "1.0.0.0",
	url = ""
};

public OnMapStart()
{
	BeamSprite = PrecacheModel( "materials/sprites/lgtning.vmt" );
	HaloSprite = PrecacheModel( "materials/sprites/halo01.vmt" );
}

public OnPluginStart()
{
	MoneyOffsetCS = FindSendPropInfo( "CCSPlayer", "m_iAccount" );
}

public OnWar3PluginReady()
{
	thisRaceID = War3_CreateNewRace( "[Oddity]Sniper", "odsniper" );
	
	SKILL_SKIN = War3_AddRaceSkill( thisRaceID, "Disguise", "You may spawn and look like the enemy", false, 5 );
	SKILL_XP = War3_AddRaceSkill( thisRaceID, "Scope Master", "You get 10-40 XP and 20-150 money if you get a scout hit", false, 5 );
	SKILL_BEACON = War3_AddRaceSkill( thisRaceID, "One bullet one kill", "You beacon enemies in order to track them", false, 5 );
	SKILL_HIDE = War3_AddRaceSkill( thisRaceID, "Sniper", "Snipers must never be seen(automatic activatet if you take damage)", false, 5 );
	
	War3_CreateRaceEnd( thisRaceID );
}

public OnRaceChanged( client, oldrace, newrace )
{
	if( newrace != thisRaceID )
	{
		War3_WeaponRestrictTo( client, thisRaceID, "" );
		War3_SetBuff( client, fInvisibilitySkill, thisRaceID, 1.0 );
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
	}
	else
	{
		War3_WeaponRestrictTo( client, thisRaceID, "weapon_knife,weapon_scout" );
		if( IsPlayerAlive( client ) )
		{
			GivePlayerItem( client, "weapon_scout" );
		}
	}
}

public OnWar3EventSpawn( client )
{
	new race = War3_GetRace( client );
	if( race == thisRaceID )
	{
		GivePlayerItem( client, "weapon_scout" );
		War3_SetBuff( client, fInvisibilitySkill, thisRaceID, 1.0 );
		War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
		if( War3_GetSkillLevel( client, thisRaceID, SKILL_SKIN ) > 0 && GetRandomFloat( 0.0, 1.0 ) <= DisgChance[War3_GetSkillLevel( client, thisRaceID, SKILL_SKIN )] )
		{
			if( GetClientTeam( client ) == TEAM_T )
			{
				SetEntityModel( client, "models/player/ct_urban.mdl" );
			}
			if( GetClientTeam( client ) == TEAM_CT )
			{
				SetEntityModel( client, "models/player/t_leet.mdl" );
			}
		}
	}
}

public OnWar3EventPostHurt( victim, attacker, damage )
{
	if( W3GetDamageIsBullet() && ValidPlayer( victim, true ) && ValidPlayer( attacker, true ) && GetClientTeam( victim ) != GetClientTeam( attacker ) )
	{
		if( War3_GetRace( attacker ) == thisRaceID )
		{
			new skill_xp = War3_GetSkillLevel( attacker, thisRaceID, SKILL_XP );
			if( !Hexed( attacker, false ) && skill_xp > 0 )
			{
				new String:wpnstr[32];
				GetClientWeapon( attacker, wpnstr, 32 );
				if( StrEqual( wpnstr, "weapon_scout" ) )
				{
					new xp = GetRandomInt( XPMin[skill_xp], XPMax[skill_xp] );
					new cash = GetRandomInt( CashMin[skill_xp], CashMax[skill_xp] );
				
					SetMoney( attacker, GetMoney( attacker ) + cash );
					War3_SetXP( attacker, thisRaceID, War3_GetXP( attacker, thisRaceID ) + xp );
					
					War3_ChatMessage( attacker, "+%dXP and +%i$", xp, cash );
				
					new Float:start_pos[3];
					new Float:target_pos[3];
				
					GetClientAbsOrigin( attacker, start_pos );
					GetClientAbsOrigin( victim, target_pos );
				
					start_pos[2] += 40;
					target_pos[2] += 40;
				
					TE_SetupBeamPoints( start_pos, target_pos, BeamSprite, HaloSprite, 0, 0, 1.0, 10.0, 10.0, 0, 0.0, { 255, 200, 0, 255 }, 0 );
					TE_SendToAll();
				}
			}
			
			new skill_beacon = War3_GetSkillLevel( attacker, thisRaceID, SKILL_BEACON );
			if( !Hexed( attacker, false ) && skill_beacon > 0 && GetRandomFloat( 0.0, 1.0 ) <= BeaconChance[skill_beacon] )
			{
				ServerCommand( "sm_beacon #%d 1", GetClientUserId( victim ) );
			}
		}
	}
}

public OnW3TakeDmgBulletPre( victim, attacker, Float:damage )
{
	if( IS_PLAYER( victim ) && IS_PLAYER( attacker ) && victim > 0 && attacker > 0 && attacker != victim )
	{
		new vteam = GetClientTeam( victim );
		new ateam = GetClientTeam( attacker );
		if( vteam != ateam )
		{
			new race_victim = War3_GetRace( victim );
			new skill_hide = War3_GetSkillLevel( victim, thisRaceID, SKILL_HIDE );
			if( race_victim == thisRaceID && skill_hide > 0 && !Hexed( victim, false ) && !bInUltimate[victim]) 
			{
				W3FlashScreen( victim, RGBA_COLOR_RED );
				
				PrintHintText( victim, "You have been seen.. RELOCATE FAST!" );				
				War3_SetBuff( victim, fInvisibilitySkill, thisRaceID, 0.0 );
				War3_SetBuff( victim, fMaxSpeed, thisRaceID, 1.5 );
				DisarmTarget(victim);
				bInUltimate[victim]=true;
				CreateTimer( UltDuration[skill_hide], StopHide, victim );
			}
			else if( bInUltimate[attacker] ) //bInUltimate only counts for [Oddity]Sniper's ultimate
			{
				War3_DamageModPercent(0.0);
				DisarmTarget(attacker);
				PrintHintText(attacker,"You cannot damage other players\nwhile your ultimate is currently active!");
			}
		}
	}
}

public Action:StopHide( Handle:timer, any:client )
{
	bInUltimate[client]=false;
	War3_SetBuff( client, fInvisibilitySkill, thisRaceID, 1.0 );
	War3_SetBuff( client, fMaxSpeed, thisRaceID, 1.0 );
	GivePlayerItem( client, "weapon_scout" );
	GivePlayerItem( client, "weapon_knife" );
}

stock GetMoney( player )
{
	return GetEntData( player, MoneyOffsetCS );
}

stock SetMoney( player, money )
{
	SetEntData( player, MoneyOffsetCS, money );
}

stock DisarmTarget(client) {
	for( new slot = 0; slot < 10; slot++ )
	{		
		new wpn = GetPlayerWeaponSlot( client, slot );
		if( wpn > 0 )
		{
			RemovePlayerItem( client, wpn );
		}
	}
}