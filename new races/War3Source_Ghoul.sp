/**
* File: War3Source_Ghoul.sp
* Description: The Ghoul race for War3Source.
* Author(s): Cereal Killer, Vulpone and Revan added factions and made it AWESOME
*/

#pragma semicolon 1

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdkhooks>

//vulp-edit: defines the chance the infect skill will be toggled
#define INFECT_TRIGGERCHANCE 15

public Plugin:myinfo = 
{
	name = "War3Source Race - Ghoul",
	author = "Cereal Killer (factionized by Vulp&Rev)",
	description = "Ghoul for War3Source.",
	version = "3.14",
	url = "http://wcs-lagerhaus.de"
}

/*
* Todo:
* add sounds --> DONE
* add more effects --> 
* custom model? --> DONE
*/

new thisRaceID;

new BeamSprite,HaloSprite;
//vulp-edit: cannibalize HP regen doubled
//Cannibalize
//new String:Nom[]="war3source/nomnom.mp3";
//ew Float:corpselocation[3][MAXPLAYERS][20];
//new dietimes[MAXPLAYERS];
new cannibal[]={0,32,38,40,47,55};
//new corpsehealth[MAXPLAYERS][20];
//new bool:corpsedied[MAXPLAYERS][20];

//Frenzy
//vulp-edit: changed speed values so it doesnt OP the class with infected!
new Float:attspeed[]={1.0,1.2,1.25,1.3,1.35,1.4};
new Float:movespeed[]={1.0,1.32,1.34,1.39,1.41,1.43};

//Unholy Strength
//vulp-edit: new damage! not 30-50, but 40-60
new Float:unhsdamage[6]={1.0,1.4,1.45,1.50,1.54,1.6};

//Unholy Armor
//vulp-edit: new hp values
new unholyarmor[]={70,80,90,100,110,120};

//Unholy Virus
new Float:f_unholyVirus_shake_dur[]={0.0, 0.5, 1.0, 1.5, 2.0, 2.5};
new Float:f_unholyVirus_freeze_intensity[]={1.0,0.95,0.92,0.89,0.85,0.80}; 


//#####sounds#####
new String:snd_breatheloop[]="npc/zombie_poison/pz_breathe_loop1.wav";
new String:snd_killsound[]="npc/zombie/zombie_pain2.wav";
new String:snd_dotsound[]="npc/zombie/zombie_hit.wav";

//vulp-edit: new skill added (infect)
new CANN, FREN, UNHS, UNHA, INFECT;

/*public OnPluginStart()
{
	//CreateTimer(0.5,nomnomnom,_,TIMER_REPEAT);
}
*/

public OnMapStart()
{
	PrecacheSound(snd_breatheloop);
	PrecacheSound(snd_killsound);
	PrecacheSound(snd_dotsound);
	//BeamSprite=PrecacheModel("materials/sprites/blueshaft1.vmt");
	//HaloSprite=PrecacheModel("materials/sprites/tp_beam001.vmt");
	BeamSprite=War3_PrecacheBeamSprite();
	HaloSprite=War3_PrecacheHaloSprite();
}

public OnWar3LoadRaceOrItemOrdered2(num)
{
	if(num==192){
		thisRaceID=War3_CreateNewRace("Undead Ghoul","ghoul");
		CANN=War3_AddRaceSkill(thisRaceID,"Unholy Cravings","Your incredible lust for human flesh cannot be stopped\nGain 32/38/40/47/55 HP for a kill.!",false,5);
		FREN=War3_AddRaceSkill(thisRaceID,"Unholy Frenzy","This bloodlust is driving you crazy\nSprint towards your victim at 42%/44%/49%/51%/53% extra speed and\20%/25%/30%/35%/40% extra attack speed!",false,5);
		UNHS=War3_AddRaceSkill(thisRaceID,"Unholy Strength","Use your claws you punch your foe!\nYou have 35% chance to deal 40%-60% more damage!",false,5);
		INFECT=War3_AddRaceSkill(thisRaceID,"Unholy Virus","Infect your victim with a disease!\nWith a chance of 15% you can infect your enemy! Hitting his legs lowers speed, hitting his head shakes him\nand hitting his chest makes him bleed!\nHigher level will increase effects!",true,5);
		UNHA=War3_AddRaceSkill(thisRaceID,"Unholy Armor","The frozen blood on your skin makes bullets less effective!\nRaise your total health by 70-120 HP!",false,5);
		War3_CreateRaceEnd(thisRaceID);
		
		//faction code
		W3Faction(thisRaceID,"undead",true);
	}
}

public OnWar3EventSpawn(client)
{
	if(War3_GetRace(client)==thisRaceID){
		//War3_WeaponRestrictTo(client,thisRaceID,"weapon_knife");
		if(ValidPlayer(client,true)){
			GivePlayerItem(client,"weapon_knife");
			setbuffs(client);
			CreateTimer(3.0, Timer_SoundLoopBreathing, client);
			
			//vulp-edit: new model! :)
			//SetEntityModel( client, "models/player/slow/frazzle/frazzle.mdl" );
		}
	}
	//resetcorpses();
}

public Action:Timer_SoundLoopBreathing(Handle:timer, any:client)
{
	if(IsPlayerAlive(client))
	{
		//only actually emit the sound if the player is alive!
		W3EmitSoundToAll(snd_breatheloop,client);
	}
}

public OnRaceChanged(client, oldrace, newrace)
{
	if(newrace!=thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"");
		resetbuffs(client);
		W3ResizePlayer(client, 1.0);
	}
	if(newrace==thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"weapon_knife");
		if(ValidPlayer(client,true)){
			GivePlayerItem(client,"weapon_knife");
			setbuffs(client);
		}
		W3ResizePlayer(client, 0.80);
	}
}

/*public resetcorpses()
{
	for(new client=0;client<=MaxClients;client++){
		for(new deaths=0;deaths<=19;deaths++){
			corpselocation[0][client][deaths]=0.0;
			corpselocation[1][client][deaths]=0.0;
			corpselocation[2][client][deaths]=0.0;
			dietimes[client]=0;
			corpsehealth[client][deaths]=0;
			corpsedied[client][deaths]=false;
		}
	}
}*/

stock setbuffs(client)
{
	if(War3_GetRace(client)==thisRaceID){
		new skill_level1=War3_GetSkillLevel(client,thisRaceID,UNHA);
		new skill_level2=War3_GetSkillLevel(client,thisRaceID,FREN);
		//War3_SetMaxHP(client,GetClientHealth(client)+unholyarmor[skill_level1]);
		War3_SetBuff(client,iAdditionalMaxHealth,thisRaceID,unholyarmor[skill_level1]);
		if(skill_level2>0){
			War3_SetBuff(client,fAttackSpeed,thisRaceID,attspeed[skill_level2]);
			War3_SetBuff(client,fMaxSpeed,thisRaceID,movespeed[skill_level2]);
		}
	}
}

public resetbuffs(client)
{
	//War3_SetMaxHP(client,100);
	War3_SetBuff(client,iAdditionalMaxHealth,thisRaceID,0);
	War3_SetBuff(client,fAttackSpeed,thisRaceID,1.0);
	War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
}

new i_counter[MAXPLAYERSCUSTOM] = 0; //should be 5x dot over 10 seconds (2 secs ea wave)
public OnW3TakeDmgPre(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0){
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam	(attacker);
		if(vteam!=ateam){
			new race_attacker=War3_GetRace(attacker);
			if(race_attacker==thisRaceID){
				if(!Hexed(attacker)){
					new skill_level=War3_GetSkillLevel(attacker,thisRaceID,UNHS);
					new skill_level_infect=War3_GetSkillLevel(attacker,thisRaceID,INFECT);
					if(skill_level>0){
						War3_DamageModPercent(unhsdamage[skill_level]);
						new Float:clientpos[3];
						GetClientAbsOrigin(victim,clientpos);
						clientpos[2]+=5;
						new Float:fxdelay;
						for(new timer;timer<=5;timer++){
							clientpos[2]+=5;
							fxdelay += 0.2;
							TE_SetupBeamRingPoint(clientpos,50.0,80.0,BeamSprite,HaloSprite,0,15,0.7,30.0,4.2,{200,40,40,255},77,0);
							TE_SendToAll(fxdelay);
						}
					}
					if(skill_level_infect>0 && !W3HasImmunity(victim,Immunity_Ultimates)) 
					{
						new i_randomInt = GetRandomInt(0,100);
						if( i_randomInt <= INFECT_TRIGGERCHANCE) 
						{ //chance is n% that it triggers
							new i_randomEffect = GetRandomInt(1,3);
							switch(i_randomEffect)
							{
								case(1): //let's say he hit his legs. -> slowdown
								{
									new Float:f_shakeDur = f_unholyVirus_shake_dur[skill_level_infect];
									//native War3_ShakeScreen(client,Float:duration=1.0,Float:magnitude=40.0,Float:noise=30.0);     
									War3_ShakeScreen(victim, f_shakeDur, 45.0, 35.0);
									PrintHintText(attacker, "Infected enemy with Distortion (Shake)!");
									PrintHintText(victim, "You have been infected with Distortion (Shake)!");
									W3EmitSoundToAll(snd_dotsound,victim);
								}
								case(2):
								{
									CreateTimer(2.0,Timer_FreezeDelay,victim);
									War3_SetBuff(victim,fSlow,thisRaceID,f_unholyVirus_freeze_intensity[skill_level_infect]);
									PrintHintText(attacker, "Infected enemy with Disability (Freeze)!");
									PrintHintText(victim, "You have been infected with Disability (Freeze)!");
									W3EmitSoundToAll(snd_dotsound,victim);
								}
								case(3):
								{
									i_counter[victim] = 0;
									CreateTimer(2.0, Timer_DoTDelay, victim);
									War3_DecreaseHP(victim, 10);
									W3EmitSoundToAll(snd_dotsound,victim);
									PrintHintText(attacker, "Infected enemy with Flesh-Eating Virus (Damage over time)!");
									PrintHintText(victim, "You have been effected with Flesh-Eating Virus (Damage over time)!");
								}
							}
						}
					}
				}
			}
		}
	}
}

public Action:Timer_FreezeDelay(Handle:timer, any:client)
{
	if(ValidPlayer(client, false)) //freeze is now gone.
	{
		War3_SetBuff(client,fSlow,thisRaceID,1.0);
	}
}

public Action:Timer_DoTDelay(Handle:timer, any:client)
{
	if( ValidPlayer(client, true))
	{
		if(i_counter[client] < 5)
		{
			War3_DecreaseHP(client, 5);
			i_counter[client]++;
			CreateTimer(2.0, Timer_DoTDelay, client);
			W3EmitSoundToAll(snd_dotsound,client);
		}
	}
}

public OnWar3EventDeath(victim,attacker)
{
	if(War3_GetRace(attacker) == thisRaceID && ValidPlayer(attacker, true) && attacker != victim)
	{
		new skill_level = War3_GetSkillLevel(attacker, thisRaceID, CANN);
		if(skill_level > 0)
		{
			W3FlashScreen(attacker,{155,0,0,40},0.3);
			new addhp = cannibal[skill_level];
			War3_HealToMaxHP(attacker, addhp);
			PrintHintText(attacker, "Killed an enemy! Regained %i HP!", addhp);
			W3EmitSoundToAll(snd_killsound,attacker);
		}
	}
	/*new deaths=dietimes[victim];
	dietimes[victim]++;
	corpsedied[victim][deaths]=true;
	corpsehealth[victim][deaths]=60;
	new Float:pos[3];
	War3_CachedPosition(victim,pos);
	corpselocation[0][victim][deaths]=pos[0];
	corpselocation[1][victim][deaths]=pos[1];
	corpselocation[2][victim][deaths]=pos[2];
	for(new client=0;client<=MaxClients;client++){
		if(War3_GetRace(client)==thisRaceID&&ValidPlayer(client,true)){
			TE_SetupBeamRingPoint(pos,25.0,75.0,BeamSprite,HaloSprite,0,15,6.0,20.0,3.0,{100,100,150,255},20,0);
			TE_SendToAll();
		}
	}*/
	
}

/*
public Action:nomnomnom(Handle:timer)
{
	for(new client=0;client<=MaxClients;client++){
		if(War3_GetRace(client)==thisRaceID&&ValidPlayer(client,true)){
			new skill_level=War3_GetSkillLevel(client,thisRaceID,CANN);
			if(skill_level>0){
				for(new corpse=0;corpse<=MaxClients;corpse++){
					for(new deaths=0;deaths<=19;deaths++){
						if(corpsedied[corpse][deaths]==true){
							new Float:corpsepos[3];
							new Float:clientpos[3];
							GetClientAbsOrigin(client,clientpos);
							corpsepos[0]=corpselocation[0][corpse][deaths];
							corpsepos[1]=corpselocation[1][corpse][deaths];
							corpsepos[2]=corpselocation[2][corpse][deaths];
							if(GetVectorDistance(clientpos,corpsepos)<50){
								if(corpsehealth[corpse][deaths]>=0){
									EmitSoundToAll(Nom,client);
									W3FlashScreen(client,{155,0,0,40},0.1);
									corpsehealth[corpse][deaths]-=5;
									new addhp1=cannibal[skill_level];
									War3_HealToMaxHP(client,addhp1);
									TE_SetupBeamRingPoint(corpsepos,0.1,60.0,BeamSprite,HaloSprite,0,20,3.5,35.0,1.0,{200,130,128,255},40,0);
									TE_SendToClient(client);
								}
							}
							else
							{
								corpsehealth[corpse][deaths]-=5;
							}
						}
					}
				}
			}
		}
	}
}

stock War3_SetMaxHP(client,maxhp){
	W3SetPlayerProp(client,iMaxHP,maxhp);
}*/