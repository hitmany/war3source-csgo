
#pragma semicolon 1

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
new thisRaceID;
new MoneyOffsetCS;
new SKILL_ORB,SKILL_PRISON,SKILL_AURA,ULT;
public Plugin:myinfo = 
{
	name = "War3Source Race - Obsidian Destroyer",
	author = "DonRevan",
	description = "Obsidian Destroyer War3Source Race",
	version = "1.0",
	url = "http://www.wcs-lagerhaus.de"
};

public OnPluginStart()
{
	if(War3_GetGame()==Game_CS) {
		MoneyOffsetCS=FindSendPropInfo("CCSPlayer","m_iAccount");
	}
	else
	SetFailState("This race only works with cstrike - Sorry");
}

new String:howl[]="war3source/howlofterror.wav";
new String:arcane[]="weapons/physcannon/energy_sing_explosion2.wav";
new String:ultsnd[]="npc/antlion/attack_single2.wav";
new BeamSprite, HydraSprite, HaloSprite, Glow, Orb;
public OnMapStart()
{
	BeamSprite=PrecacheModel("materials/effects/ar2_altfire1b.vmt"); //hawhaw, sprites/lgtning owned :D
	HydraSprite=PrecacheModel("materials/sprites/hydragutbeam.vmt");
	HaloSprite=PrecacheModel("materials/sprites/halo01.vmt"); //effects often looks better with halo01.vmt ;)
	Glow=PrecacheModel("materials/sprites/orangeflare1.vmt");
	Orb=PrecacheModel("materials/sprites/purpleglow1.vmt");
	War3_PrecacheSound(howl);
	War3_PrecacheSound(arcane);
	War3_PrecacheSound(ultsnd);
}

public OnWar3LoadRaceOrItemOrdered2(num) if(num==292)
{
	thisRaceID=War3_CreateNewRace("Harbinger - Obsidian Destroyer","harbinger");
	SKILL_ORB=War3_AddRaceSkill(thisRaceID,"Arcane Orb","Gives the destroyer extra power to damage his enemies on cost of  mana!",false,4);
	SKILL_PRISON=War3_AddRaceSkill(thisRaceID,"Astral Imprisonment","Teleports a target allied or enemy hero into an astral prison.\nThe target remains separated from the real world for the duration of the spell(ability)",false,4);
	SKILL_AURA=War3_AddRaceSkill(thisRaceID,"Essence Aura","Whenever allies kills enemy units harbinger will regenerate mana"/*Whenever harbinger's is near allied hero's he will regenerate mana slowly*/,false,4);
	ULT=War3_AddRaceSkill(thisRaceID,"Sanity's Eclipse","Unleash a psionic storm to greatly damage weak enemyes.\nStronger enemyes will resist the most of the damage, but they'll lose some mana!"/*"Harbinger unleashes a psionic storm able to penetrate lesser minds\n, dealing massive damage to them.More crafty minds resist most of the damage, but they'll lose some mana!"*/,true,4);
	War3_CreateRaceEnd(thisRaceID);
}

stock GetMoney(player) return GetEntData(player,MoneyOffsetCS);
stock SetMoney(player,money) SetEntData(player,MoneyOffsetCS,money);

new Float:OrbChance[5] = {0.0,0.15,0.20,0.25,0.32};
new OrbManaValue[5] = {0,300,250,150,100};
new OrbDMGArray[5] = {0,7,9,12,14};
public OnW3TakeDmgBullet(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new race_attacker=War3_GetRace(attacker);
			new skill_level=War3_GetSkillLevel(attacker,thisRaceID,SKILL_ORB);
			if(race_attacker==thisRaceID && skill_level>0)
			{
				if(GetRandomFloat(0.0,1.0)<=OrbChance[skill_level] && !W3HasImmunity(victim,Immunity_Skills))
				{
					new old_money = GetMoney(attacker);
					if(old_money-OrbManaValue[skill_level]>0) {
						SetMoney(attacker,old_money-OrbManaValue[skill_level]);
						War3_DealDamage(victim, OrbDMGArray[skill_level], attacker, DMG_ENERGYBEAM, "Arcane Orb" );
						new Float:spos[3];
						new Float:epos[3];
						GetClientAbsOrigin(victim,epos);
						GetClientAbsOrigin(attacker,spos);
						epos[2]+=40;
						spos[2]+=40;
						TE_SetupBeamPoints(spos, epos, HydraSprite, HaloSprite, 0, 35, 1.2, 30.0, 30.0, 0, 8.0, {200,255,200,255}, 30);
						TE_SendToAll();
						TE_SetupGlowSprite(epos, Orb, 1.4, 1.0, 255);
						TE_SendToAll();
						new Float:fx_delay = 0.0;
						for(new x=1;x<=6;x++) {
							TE_SetupBeamRingPoint(epos, 200.0, 160.0, Glow, HaloSprite, 0, 20, 0.4, 45.0, 1.0, {255,255,255,255}, 28, 0);
							TE_SendToAll(fx_delay);
							spos[2]+=20;
							fx_delay += 0.3;
						}					
						EmitSoundToAll(arcane,attacker);
					}
				}
			}
		}
	}
}

//too much hp chance :S -> new Float:EssenceDivider[5]={0,5,3,2,1};
new EssenceManaMin[]={0,10,30,40,60};
new EssenceManaMax[]={0,30,40,60,80};
public OnWar3EventDeath(victim,attacker)
{
	War3_SetBuff(victim,fInvisibilitySkill,thisRaceID,1.0);
	War3_SetBuff(victim,bBashed,thisRaceID,false);
	new team=GetClientTeam(attacker);
	if(!W3HasImmunity(victim,Immunity_Ultimates)) {
		for(new i=1;i<=MaxClients;i++)
		{
			if(War3_GetRace(i)==thisRaceID)
			{
				if(ValidPlayer(i,true)&&GetClientTeam(i)==team)
				{
					new skill=War3_GetSkillLevel(i,thisRaceID,SKILL_AURA);
					if(skill>0 && !Silenced(i))
					{
						PrintCenterText(i,"Essence Aura gives you mana");
						new min = EssenceManaMin[skill];
						new max = EssenceManaMax[skill];
						new add_money = GetRandomInt(min,max);
						add_money *= 10;
						
						new old_money = GetMoney(i);
						if(old_money+add_money<16000) {
							SetMoney(i,old_money+add_money);
							W3FlashScreen(i,RGBA_COLOR_YELLOW,0.3,0.2,FFADE_OUT);
							new Float:TargetPos[3];
							GetClientAbsOrigin(i, TargetPos);
							new Float:SecondPos[3];
							SecondPos[0]=TargetPos[0],SecondPos[1]=TargetPos[1],SecondPos[2]=TargetPos[2];
							SecondPos[2]+=120;
							TE_Start("Bubbles");
							TE_WriteVector("m_vecMins", TargetPos);
							TE_WriteVector("m_vecMaxs", SecondPos);
							TE_WriteFloat("m_fHeight", 400.0);
							TE_WriteNum("m_nModelIndex", BeamSprite);
							TE_WriteNum("m_nCount", 40);
							TE_SendToAll();							
							PrintToChat(i,"\x04[Essence Aura]\x01 You got %i mana because one of your allies kills a enemy!",add_money);
						}
						else
						PrintToConsole(i,"[Essence Aura] You've reached max mana count!"); //mostly debugging purpose :S
					}
				}
			}
		}
	}
}

new Float:PrisonRadius[5] = {0.0,400.0,600.0,850.0,900.0}; //skill is not the world, lets give him a high radius :S
public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID && IsPlayerAlive(client)){
		if(!Silenced(client)){			
			if(pressed){
				if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_PRISON,true)){
					new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_PRISON);
					new Float:range = PrisonRadius[skill_level];
					if(skill_level>0){
						new target=War3_GetTargetInViewCone(client,range,false,15.0);
						if(target>0){
							War3_CooldownMGR(client,20.0,thisRaceID,SKILL_PRISON);
							new Float:client_pos[3];
							GetClientAbsOrigin(target,client_pos);
							new Float:new_pos[3] = {0.0,0.0,900.0};
							TeleportEntity(target, new_pos, NULL_VECTOR, NULL_VECTOR );
							War3_SetBuff(target,bBashed,thisRaceID,true);
							War3_SetBuff(target,fInvisibilitySkill,thisRaceID,0.0);
							//screenoverlay's do not need a precache :S = LET'S SPAM THAM >:D
							W3Screenoverlay(target,0.0,"models/props_lab/cornerunit_cloud.vmt");
							W3Screenoverlay(target,0.4,"models/props_combine/masterinterface01c.vmt");
							W3Screenoverlay(target,0.6,"effects/alyxmonitor_talk.vmt");
							W3Screenoverlay(target,0.8,"decals/offaddress.vmt");//overlays will override themself :p (huh, logical ?^^)
							W3Screenoverlay(target,0.9,"effects/tvscreen_noise001a.vmt");
							W3Screenoverlay(target,1.0,"effects/breenscreen_static01_.vmt");
							W3Screenoverlay(target,1.2,"effects/c17_07camera.vmt");
							W3Screenoverlay(target,1.4,"effects/prisonmap_disp.vmt");
							W3Screenoverlay(target,1.6,"models/props_lab/warp_sheet.vmt");
							W3Screenoverlay(target,1.8,"0"); //reset overlay's
							new Handle:pack;
							CreateDataTimer(2.0, Timer_FinishAstral, pack);
							WritePackCell(pack, target);
							WritePackFloat(pack,client_pos[0]);
							WritePackFloat(pack,client_pos[1]);
							WritePackFloat(pack,client_pos[2]);
							EmitSoundToAll(howl,target);
						}
						else
						{
							new Float:feets = range/10.0;
							PrintHintText(client, "No target in %i feets found!",RoundToNearest(feets));
						}
					}
					else
					{
						PrintHintText(client, "Level Astral Imprisonment Form first");
					}	
				}
			}
		}
	}
}

public Action:Timer_FinishAstral(Handle:timer, Handle:pack)
{
	ResetPack(pack);
	new client = ReadPackCell(pack);
	if(ValidPlayer(client,false)) {
		new Float:pos[3];
		pos[0] = ReadPackFloat(pack);
		pos[1] = ReadPackFloat(pack);
		pos[2] = ReadPackFloat(pack);
		War3_SetBuff(client,bBashed,thisRaceID,false);
		War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
		TeleportEntity(client, pos, NULL_VECTOR, NULL_VECTOR );		
	}
}

public W3Screenoverlay(target,Float:delay,String:material[]) {
	new Handle:pack;
	CreateDataTimer(delay, Timer_DoOverlay, pack);
	WritePackCell(pack, target);
	WritePackString(pack, material);
}

public Action:Timer_DoOverlay(Handle:timer, Handle:pack)
{
	ResetPack(pack);
	new target = ReadPackCell(pack);
	if(ValidPlayer(target,false)) {
		new String:buffer[64];
		ReadPackString(pack, buffer, sizeof(buffer)); 
		ClientCommand(target, "r_screenoverlay %s",buffer);
	}
}

new Float:SanityRadius[5]={0.0,200.0,350.0,400.0,450.0};
new SanityDamage[5]={0,50,60,70,80};
new SanityManaValue[5]={0,500,600,700,800};

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && IsPlayerAlive(client))
	{
		new skill=War3_GetSkillLevel(client,race,ULT);
		if(skill>0)
		{
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT,true))
			{
				new caster_team = GetClientTeam(client);
				new Float:start_pos[3];
				GetClientAbsOrigin(client,start_pos);
				new count = 0;
				for(new x=1;x<=MaxClients;x++)
				{
					if(ValidPlayer(x,true)&&caster_team!=GetClientTeam(x)&&!W3HasImmunity(x,Immunity_Ultimates))
					{
						new Float:this_pos[3];
						GetClientAbsOrigin(x,this_pos);
						if(GetVectorDistance(start_pos,this_pos)<=SanityRadius[skill])
						{
							if(DoSanityEclipse(client,x,skill,start_pos,this_pos))
							count++;
						}
					}
				}
				if(count>0) {
					War3_CooldownMGR(client,30.0,thisRaceID,ULT,true);
					PrintCenterText(client,"Sanity's Eclipse found %i targets!",count);
				}
				else {
					War3_CooldownMGR(client,3.0,thisRaceID,ULT,true);
					PrintCenterText(client,"Sanity's Eclipse found no target!");
				}
				EmitSoundToAll(ultsnd,client,SNDCHAN_AUTO);
			}
		}
		else
		PrintHintText(client,"%T","No Ultimate Leveled",client);
	}
}

public bool:DoSanityEclipse(client,target,ult_level,Float:pos1[3],Float:pos2[3]) {
	if(ult_level>0) {
		new target_hp = GetClientHealth(target);
		new client_hp = GetClientHealth(client);
		new damage = SanityDamage[ult_level];
		if(target_hp - client_hp>0) {//enemy is stronger...
			//don't touch damage :p
		}
		else { //enemy is weaker!
			damage /= 3; //lower the damage :S
			new old_money = GetMoney(target);
			if(old_money-SanityManaValue[ult_level]>0) SetMoney(target,old_money-SanityManaValue[ult_level]);
		} // lol >.< .. idk why im doing it this way o�
		if(damage>0) {
			new String:target_name[64];
			GetClientName(target,target_name,sizeof(target_name));
			War3_DealDamage(target,damage,client,DMG_BULLET,"Sanity's Eclipse");
			pos1[2]+=40,pos2[2]+=35;
			TE_Start("Large Funnel");
			
			TE_WriteFloat("m_vecOrigin[0]", pos1[0]);
			TE_WriteFloat("m_vecOrigin[1]", pos1[1]);
			TE_WriteFloat("m_vecOrigin[2]", pos1[2]);
			TE_WriteNum("m_nModelIndex", HaloSprite);//always purple-back, never found any working :S.. anywaaaay it's ok
			TE_WriteNum("m_nReversed", 5);
			TE_SendToAll();
			TE_SetupBubbles(pos1,pos2,Glow,20.0,0.2,12);
			TE_SendToAll();
			TE_SetupBeamPoints(pos1,pos2, HaloSprite, HaloSprite, 0, 35, 0.5, 1.0, 1.0, 0, 2.0, {255,255,255,255}, 10);
			TE_SendToAll();
			PrintToConsole(client,"[Sanity's Eclipse] dealt %i damage to %s",damage,target_name);
			return true;
		}
	}
	return false;
}

stock TE_SetupBubbles(Float:mins[3],Float:max[3],ModelIndex,Float:fHeight,Float:fSpeed,nCount){
	TE_Start("Bubbles");
	TE_WriteVector("m_vecMins", mins);
	TE_WriteVector("m_vecMaxs", max);
	TE_WriteNum("m_nModelIndex", ModelIndex);
	TE_WriteFloat("m_fHeight", fHeight);
	TE_WriteNum("m_nCount", nCount);
	TE_WriteFloat("m_fSpeed", fSpeed);
}