/**
* File: War3Source_PriestessOfTheMoon.sp
* Description: The Priestess Of The Moon race for War3Source.
* Author(s): Cereal Killer + Anthony Iacono + Vulpone
*/
 
#pragma semicolon 1
#include <sourcemod>
//#undef REQUIRE_EXTENSIONS
//#include "W3SIncs/mana"
//#define REQUIRE_EXTENSIONS
#include <sdktools>
#include "sdkhooks"
#include "W3SIncs/War3Source_Interface"
public W3ONLY(){}



new thisRaceID;
new Float:TrueshotDamagePercent[6]={0.0,0.04,0.08,0.12,0.14, 0.16};
new Float:HideAlpha[7]={1.0,0.95,0.90,0.85,0.80, 0.75, 0.70}; //array used as percentage points for the SKILL_HIDE (invis) skill.




//new BurnChance[5]={11,9,8,7,6};
new Float:BurnTime[6]={0.0,0.3,0.8,0.9,1.10};
new BurnArrow[6]={0,3,5,7,8,10};

//new BurnDamage[5]={0,2,3,4,5};
new Float:scoutcooldown[3]={60.0,12.0,8.0};
new playerisbeingburned[MAXPLAYERS];
new Float:starfallstar[MAXPLAYERS][3];
new Float:StarFallCoolDown[7]={100.0,8.0,10.0,13.0,17.0,20.0, 22.0};
new starfallbool[MAXPLAYERS];
new BeamSprite,HaloSprite;
new StarSprite,BurnSprite,g_iExplosionModel,g_iSmokeModel;
new String:incoming[]="npc/env_headcrabcanister/incoming.wav";
new String:boom[]="npc/env_headcrabcanister/explosion.wav";

public Plugin:myinfo = 
{
	name = "War3Source Race - Priestess Of The Moon",
	author = "Cereal Killer & Vulpone",
	description = "The Priestess Of The Moon for War3Source.. Re-made for War3Source v2 by Vulpone",
	version = "1.0.6.3",
	url = "http://war3source.com/"
};


new SKILL_HIDE, SKILL_SCOUT, SKILL_SEARING, SKILL_TRUESHOT, ULT_STARFALL, SKILL_HIDE2;
public OnPluginStart()
{
	CreateTimer(0.2,loop11,_,TIMER_REPEAT);
}
public OnMapStart()
{
	War3_PrecacheSound(incoming);
	War3_PrecacheSound(boom);
	BeamSprite=PrecacheModel("materials/sprites/plasmabeam.vmt");
	HaloSprite=PrecacheModel("materials/sprites/halo01.vmt");
	StarSprite=PrecacheModel("materials/effects/fluttercore.vmt");
//	TSprite=PrecacheModel("VGUI/gfx/VGUI/guerilla.vmt");
//	CTSprite=PrecacheModel("VGUI/gfx/VGUI/gign.vmt");
	BurnSprite=PrecacheModel("materials/sprites/fire1.vmt");
	g_iExplosionModel = PrecacheModel("materials/effects/fire_cloud1.vmt");
	g_iSmokeModel     = PrecacheModel("materials/effects/fire_cloud2.vmt");
}
public OnWar3LoadRaceOrItemOrdered2(num)
{
	if(num==150)
	{
		thisRaceID=War3_CreateNewRace("Rockarz The Moon King","potm");
		SKILL_HIDE=War3_AddRaceSkill(thisRaceID,"Rockarz' Cloak","Cloak yourself within the mighty Rockarz Cloak!\nGain a permanent 5-30% bonus to invisibility.",false,6);
		SKILL_HIDE2=War3_AddRaceSkill(thisRaceID,"Rockarz' Illusion","Upon absorbing an Orb of Invisibility, you enhance your Cloak.\nDoubles the effect of the invisibility and adds 10% extra invisibility when crouching!",false,1);
		SKILL_SCOUT=War3_AddRaceSkill(thisRaceID,"Light Orb","Let light be your guidance to your foes!\nEmits a beam for every enemy indicating their direction!(+ability)",false,2);
		SKILL_SEARING=War3_AddRaceSkill(thisRaceID,"Burning Arrow","Enhance your arrows with the element of fire!\n3-10% chance to burn the area you shoot towards. Can ignite enemies.",false,5);
		SKILL_TRUESHOT=War3_AddRaceSkill(thisRaceID,"Dance of the Rose","Use the terrestrial power that surrounds you to enhance your shots!\nGain a passive 4-16% chance of enhacing your damage by 10%.",false,5);
		ULT_STARFALL=War3_AddRaceSkill(thisRaceID,"Heaven's Rain","Let the firmament tremble as you shoot down starts from the heavens!!\nShoot down 1-6 stars where you aim at to damage your enemy that do 12 damage each!(+ultimate)",true,6);
		War3_CreateRaceEnd(thisRaceID);
		War3_SetDependency( thisRaceID, SKILL_HIDE2, SKILL_HIDE, 6);
		War3_SetDependency( thisRaceID, SKILL_TRUESHOT, SKILL_SEARING, 3);
		W3Faction(thisRaceID,"Elementalists",true);

	}
}
public OnRaceChanged(client, oldrace, newrace)
{
	if(newrace != thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"");
		War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
		War3_SetBuff(client,fAttackSpeed,thisRaceID,1.0);
		//W3ToggleMana(client,thisRaceID,false);
		//W3ManaSystem(thisRaceID,false);
		
	}
	if(newrace == thisRaceID)
	{
		//W3ToggleMana(client,thisRaceID,true);
		//W3ManaSystem(thisRaceID,true);
		War3_WeaponRestrictTo(client,thisRaceID,"weapon_m3;weapon_knife");
	}
}
public OnWeaponFired(index)
{
	if(War3_GetRace(index)==thisRaceID)
	{
		new skill=War3_GetSkillLevel(index,thisRaceID,SKILL_SEARING);
		if(skill>0)
		{
			if(BurnArrow[skill] < 100) //3%-7% chance 
			{
				decl Float:pos[3]; 
				GetClientAbsOrigin(index,pos);
				pos[2]+=30;
				new target = War3_GetTargetInViewCone(index,9000.0,false,5.0);
				if(target>0 && !W3HasImmunity(target,Immunity_Skills))
				{
					
					decl Float:targpos[3];
					GetClientAbsOrigin(target,targpos);
					TE_SetupBeamPoints(pos, targpos, BurnSprite, HaloSprite, 0, 8, 0.5, 10.0, 10.0, 10, 10.0, {255,255,255,255}, 70); 
					TE_SendToAll();
					new Float:flBurnTime = BurnTime[skill];
					switch(W3FactionCompare(thisRaceID,War3_GetRace(target)))
					{
						case (FactionBehavior:Friend):
							flBurnTime-=0.15;
						case (FactionBehavior:Enemy):
							flBurnTime+=0.1;
					}
					IgniteEntity(target,flBurnTime);
					targpos[2]+=10;
					TE_SetupGlowSprite(targpos,BurnSprite,1.0,1.9,200);
					TE_SendToAll();
					targpos[1] -=10;
					TE_SetupGlowSprite(targpos,BurnSprite,2.0,1.9,200);
					TE_SendToAll();
					targpos[1] +=20;
					TE_SetupGlowSprite(targpos,BurnSprite,2.0,1.9,200);
					TE_SendToAll();
					
					PrintHintText(target,"Rockarz burned you with Fire Arrow!");
					//sprites/640_logo.vmt server_var(wcs_x1) server_var(wcs_y1) server_var(wcs_z1) server_var(wcs_x2) server_var(wcs_y2) server_var(wcs_z2) 1 2 2 255 225 255 255
				}
				else
				{
					decl Float:targpos[3];
					War3_GetAimEndPoint(index,targpos);
					TE_SetupBeamPoints(pos, targpos, BurnSprite, HaloSprite, 0, 8, 0.5, 10.0, 10.0, 10, 10.0, {255,255,255,255}, 70); 
					TE_SendToAll();
					targpos[2]+=10;
					TE_SetupGlowSprite(targpos,BurnSprite,1.0,1.9,255);
					TE_SendToAll();
				}
			}
		}
	}
}

public OnWar3EventSpawn(client)
{
	playerisbeingburned[client]=0;
	new skill_level_hide=War3_GetSkillLevel(client,thisRaceID,SKILL_HIDE);
	new skill_level_hide2=War3_GetSkillLevel(client, thisRaceID, SKILL_HIDE2);
	if(War3_GetRace(client)==thisRaceID)
	{
		new Float:alpha=HideAlpha[skill_level_hide];
		if(skill_level_hide > 0 &&  skill_level_hide2 == 0)
		{
			
			War3_SetBuff(client,fInvisibilitySkill,thisRaceID,alpha);	
		}
		else if(skill_level_hide2 > 0) //double invis takes effect...
		{
			War3_SetBuff(client,fInvisibilitySkill,thisRaceID,alpha/2); //alpha/2 is double alpha
		}
		GiveDefaultWeapon(client);
	}
}

//only gives weapon if there is no primary
#define CS_SLOT_PRIMARY 0
GiveDefaultWeapon(client)
{
	new Ent = GetPlayerWeaponSlot(client, CS_SLOT_PRIMARY); 
	if (Ent==-1) {
		GivePlayerItem(client,"weapon_m3");
	}
}

public OnW3TakeDmgBulletPre(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new race_attacker=War3_GetRace(attacker);
			new skill_level_trueshot=War3_GetSkillLevel(attacker,thisRaceID,SKILL_TRUESHOT);
			if(race_attacker==thisRaceID && skill_level_trueshot>0 && IsPlayerAlive(attacker))
			{
				if(GetRandomFloat(0.0,1.0)< TrueshotDamagePercent[skill_level_trueshot] && !W3HasImmunity(victim,Immunity_Skills))
				{
					decl Float:spos[3];
					decl Float:epos[3];
					GetClientAbsOrigin(attacker,epos);
					GetClientAbsOrigin(victim,spos);
					spos[2]+=15;
					epos[2]+=15;
					TE_SetupBeamPoints(epos,spos,g_iExplosionModel,HaloSprite,0,80,1.8,10.0,35.0,1,8.5,{255,182,193,220},45);
					TE_SendToAll();
					War3_DamageModPercent(1.1);   
					W3FlashScreen(attacker,RGBA_COLOR_PINK);
					W3FlashScreen(victim,RGBA_COLOR_RED);
					PrintHintText(attacker,"Dance of the Rose!");
					PrintHintText(victim,"Hit by Rockarz' Dance of the Rose!!");
				}
			}
		}
	}
}



new StarAmountCounter[MAXPLAYERSCUSTOM]=0;

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && IsPlayerAlive(client) && pressed)
	{
		new skill_level=War3_GetSkillLevel(client,race,ULT_STARFALL);
		if(skill_level>0){
			if(!Silenced(client))
			{
				if(War3_SkillNotInCooldown(client,thisRaceID,ULT_STARFALL,false))
				{
					War3_CooldownMGR(client,StarFallCoolDown[skill_level],thisRaceID,ULT_STARFALL,_,true);	
					decl Float:position[3];
					War3_GetAimEndPoint(client,position);
					position[0]+=50;
					position[1]+=50;
					position[2]+=400;
					starfallstar[client][0]=position[0];
					starfallstar[client][1]=position[1];
					starfallstar[client][2]=position[2];
					
					StarAmountCounter[client]=0; //reset the counter
					
					CreateTimer(0.4, CallStarLoop,client); //start the amount of times loop!
					
					CreateTimer(0.4, StarExplode,client); //LEARN TO USE PARAMETERS, CEREAL!
					//CreateTimer(1.0, StarExplode);
					//CreateTimer(1.5, StarExplode);
					CreateTimer(0.5, BoomSound,client);
					PrintHintText(client,"Star Fall!");
				}
			}
		}
	}
}

public Action:CallStarLoop(Handle:timer, any:client)
{
	StarAmountCounter[client]++; //every time it is called.
	if(StarAmountCounter[client] < War3_GetSkillLevel(client, thisRaceID, ULT_STARFALL))
	{
		CreateTimer(0.4, CallStarLoop, client);
		CreateTimer(0.4, StarExplode, client);
		CreateTimer(0.5, BoomSound,client);
		
		
		//re-do aim calculations
		decl Float:position[3];
		War3_GetAimEndPoint(client,position);
		position[0]+=50;
		position[1]+=50;
		position[2]+=400;
		starfallstar[client][0]=position[0];
		starfallstar[client][1]=position[1];
		starfallstar[client][2]=position[2];
		
	}
}

public Action:loop11(Handle:timer,any:a)
{
	for(new client=1;client<=MaxClients;client++)
	{
		if(ValidPlayer(client,true) &&War3_GetRace(client)==thisRaceID)
		{
			if(starfallbool[client]==1){
				TE_SetupGlowSprite(starfallstar[client],StarSprite,0.15,1.2,100);
				TE_SendToAll();
				starfallstar[client][0]-=16;
				starfallstar[client][1]-=16;
				starfallstar[client][2]-=128;
			}
		}
	}
}

public Action:OnPlayerRunCmd(client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{
	if(ValidPlayer(client,true) && War3_GetRace(client)==thisRaceID) 
	{
		new skill_level_hide=War3_GetSkillLevel(client,thisRaceID,SKILL_HIDE);
		if(skill_level_hide>0 && War3_GetSkillLevel(client,thisRaceID, SKILL_HIDE2) > 0)
		{
			new Float:alpha=HideAlpha[skill_level_hide];
			if((buttons & IN_DUCK)) 
			{
				War3_SetBuff(client,fInvisibilitySkill,thisRaceID,alpha/2-0.1); //this means: double invis +10%
			}
		}
	}
}
			

public Action:BoomSound(Handle:timer,any:client)
{
	EmitSoundToAll(boom,client);
}
public Action:StarExplode(Handle:timer,any:client)
{
	if(ValidPlayer(client,true) && War3_GetRace(client)==thisRaceID)
	{
		TE_SetupExplosion(starfallstar[client], g_iExplosionModel, 10.0, 10, TE_EXPLFLAG_NONE, 200, 255);
		TE_SendToAll();
		TE_SetupSmoke(starfallstar[client],     g_iExplosionModel, 50.0, 2);
		TE_SendToAll();
		TE_SetupSmoke(starfallstar[client],     g_iSmokeModel,     50.0, 2);
		TE_SendToAll();
		TE_SetupBeamRingPoint(starfallstar[client],0.0,400.0,BeamSprite,HaloSprite,0,15,0.8,10.0,6.0,{255,10,0,255},10,0);
		TE_SendToAll(); 
		starfallbool[client]=0;
		EmitSoundToAll(incoming,client);
		for (new i=1;i<=MaxClients;i++)
		{
			new ownerteam=GetClientTeam(client);	
			if(ValidPlayer(i,true)&& GetClientTeam(i)!=ownerteam )
			{
				decl Float:VictimPos[3];
				GetClientAbsOrigin(i,VictimPos);
				if(GetVectorDistance(starfallstar[client],VictimPos)<200)
				{
					EmitSoundToAll(incoming,i);
					if(!W3HasImmunity(i,Immunity_Ultimates))
					{
						new iStarDamage = 12;
						switch(W3FactionCompare(thisRaceID,War3_GetRace(i)))
						{
							case (FactionBehavior:Friend):
								iStarDamage-=GetRandomInt(2,4);
							case (FactionBehavior:Enemy):
								iStarDamage+=GetRandomInt(2,4);
						}
						War3_DealDamage(i,iStarDamage,client,DMG_CRUSH,"Falling Star",_,W3DMGTYPE_MAGIC);
						PrintHintText(i,"Star Fall: 12 damage!");
					}
				}
			}
		}
	}
}


public OnAbilityCommand(client,ability,bool:pressed)
{
	if (War3_GetRace(client)==thisRaceID && pressed && IsPlayerAlive(client))
	{
		if(ability == 0)
		{
			new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_SCOUT);
			if(skill_level>0)
			{
				if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_SCOUT,false))
				{
					War3_CooldownMGR(client,scoutcooldown[skill_level],thisRaceID,SKILL_SCOUT,_,true);
					for (new i=1;i<=MaxClients;i++)
					{
						if(ValidPlayer(i,true)&&IsPlayerAlive(i))
						{
							decl Float:position[3];
							decl Float:positionclient[3];
							War3_CachedPosition(i,position);
							position[2]+=60;
							War3_CachedPosition(client,positionclient);
							positionclient[2]+=60;
							if(GetClientTeam(client)==2)
							{							
								if(GetClientTeam(i)==3)
								{
									TE_SetupBeamPoints(position, positionclient, BeamSprite,HaloSprite, 0, 8, 2.0, 3.0, 20.0, 15, 1.0, {0,0,222,200}, 100); 
									TE_SendToClient(client,0.0);
								}
							}
							else if(GetClientTeam(client)==3)
							{
								if(GetClientTeam(i)==2)
								{
									TE_SetupBeamPoints(position, positionclient, BeamSprite,HaloSprite, 0, 8, 2.0, 3.0, 20.0, 15, 1.0, {233,0,0,200}, 100); 
									TE_SendToClient(client,0.0);
								}
							}
						}
					}
				}
			}
		}
	}
}