/* ============================================================================ */
/*										                                        */
/*   WcsL_Race_Razor.sp								                            */
/*   (c) 2010 Revan								                                */
/*	www.wcs-lagerhaus.de						                                */
/*										                                        */
/* ============================================================================	*/


#include <sourcemod>
#include <sdktools_functions>
#include <sdktools_sound>
#include "W3SIncs/War3Source_Interface"

new Float:AttackSpeed[5]={1.0, 0.90,  0.85,  0.80,  0.75};
new Float:Range[5]={0.0,300.0,450.0,550.0,650.0};
new Float:Duration[5]={0.0, 5.0,  8.0,   10.0,  12.0 };
new Float:Zeit[5]={0.0, 3.5,  5.0,   6.5,  8.0 };
new Float:Distance[5]={0.0,200.0,250.0,300.0,350.0};
new Float:Chance[5]={0.0,0.05,0.10,0.15,0.20};
new Float:Speedy[5]={1.00,1.20,1.35,1.45,1.50};

new Float:Static[5]={0.0,4.0,6.0,8.0,10.0};

new Float:LinkUntil[MAXPLAYERS];
new bool:bLinked[MAXPLAYERS];
new LinkedBy[MAXPLAYERS];
new bool:bLinker[MAXPLAYERS];

new Handle:ultCooldownCvar;
new Handle:abilityCooldownCvar;

new thisRaceID, SKILL_PF, SKILL_SL, SKILL_UC, ULT_STORM;

new String:explode[]="war3source/thunder_clap.wav";
new String:announce[]="npc/overwatch/radiovoice/stormsystem.wav";
new String:storm[]="npc/combine_soldier/vo/storm.wav";
//new String:damagesnd[]="hl1/fvox/shock_damage.wav";
new String:thunder1[]="ambient/levels/labs/teleport_postblast_thunder1.wav";
new String:plasma[]="weapons/physcannon/energy_sing_flyby1.wav";
new String:done[]="weapons/physcannon/energy_sing_explosion2.wav";
new String:link1[]="ambient/levels/citadel/portal_beam_loop1.wav";
new String:linkdmg[]="ambient/levels/citadel/portal_beam_shoot1.wav";
new BeamSprite, HaloSprite, Ice, Beam, IceBurn, Particle, Plasma;

new bool:bStormEye[MAXPLAYERS];
new bool:bSupernatural[MAXPLAYERS];

public Plugin:myinfo = 
{
	name = "War3Source Race - Razor",
	author = "Revan",
	description = "War3Source Race Razor is lightning incarnate",
	version = "1.0.0.0",
	url = "www.wcs-lagerhaus.de"
};


public OnPluginStart()
{
	ultCooldownCvar=CreateConVar("war3_razor_ult_cooldown","28","Ultimate(Eye of the Storm) Cooldown for Razor.");
	abilityCooldownCvar=CreateConVar("war3_razor_ability_cooldown","10","Ability(Plasma Field) Cooldown for Razor.");
	CreateTimer(1.0,dropdown,_,TIMER_REPEAT);
	CreateTimer(1.1,StaticLinkLoop,_,TIMER_REPEAT);
}
public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==280)
	{
		thisRaceID=War3_CreateNewRace("Razor - The Lightning Revenant","razor");
		SKILL_PF=War3_AddRaceSkill(thisRaceID,"Plasma Field","Razor releases a wave energetic plasma which grows in power as it extends outwards!(ability)",false,4);
		SKILL_SL=War3_AddRaceSkill(thisRaceID,"Static Link","Create a Link with your target hero\nAs long this link is maintained\nyou will raise your damage percent and lower enemys",false,4);
		SKILL_UC=War3_AddRaceSkill(thisRaceID,"Unstable Current","Get supernatural to raise your agility and get Ultimate Immunity for a few of Seconds",false,4);
		ULT_STORM=War3_AddRaceSkill(thisRaceID,"Eye of the Storm","Call upon a powerful storm of cracking energy, wich will damage the nearest enemy",true,4); 
		War3_CreateRaceEnd(thisRaceID);
	}
}

stock bool:Razor(client) {
	return War3_GetRace(client)==thisRaceID;
}


public OnMapStart() { //some precaches
	War3_PrecacheSound(explode);
	War3_PrecacheSound(announce);
	//War3_PrecacheSound(damagesnd);
	War3_PrecacheSound(storm);
	War3_PrecacheSound(thunder1);
	War3_PrecacheSound(plasma);
	War3_PrecacheSound(done);
	War3_PrecacheSound(link1);
	War3_PrecacheSound(linkdmg);
	BeamSprite=PrecacheModel("materials/sprites/lgtning.vmt");
	HaloSprite=PrecacheModel("materials/sprites/halo01.vmt"); 
	Ice=PrecacheModel("materials/sprites/plasmabeam.vmt");
	Beam=PrecacheModel("effects/blueblacklargebeam.vmt");
	IceBurn=PrecacheModel("effects/strider_muzzle.vmt");
	Particle=PrecacheModel("particle/fire.vmt")
	Plasma=PrecacheModel("sprites/bluelight1.vmt");
}

public OnWar3EventSpawn(client){
	bStormEye[client]=false;
	bLinked[client]=false;
	bLinker[client]=false;
	bSupernatural[client]=false;
	if(Razor(client)){
		new Float:dir[3]={0.0,0.0,900.0};
		new Float:iVec[ 3 ];
		GetClientAbsOrigin( client, Float:iVec );
		TE_SetupSparks(iVec, dir, 500, 50);
		TE_SendToAll();
		TE_SetupBeamRingPoint(iVec, 10.0, 350.0, Beam, HaloSprite, 0, 15, 0.5, 5.0, 0.0, {120,120,255,255}, 10, 0);
		TE_SendToAll();
		TE_SetupBeamRingPoint(iVec, 10.0, 350.0, Beam, HaloSprite, 0, 15, 0.5, 5.0, 0.0, {120,120,255,255}, 10, 0);
		TE_SendToAll(0.2);
		TE_SetupBeamRingPoint(iVec, 10.0, 350.0, Beam, HaloSprite, 0, 15, 0.5, 5.0, 0.0, {120,120,255,255}, 10, 0);
		TE_SendToAll(0.4);
		//spawn effect
		new SmokeEnt = CreateEntityByName("env_smokestack");
		new String:originData[64];
		Format(originData, sizeof(originData), "%f %f %f", iVec[0], iVec[1], iVec[2]);
		if(SmokeEnt)
		{
			EmitSoundToAll( announce , client,_,SNDLEVEL_TRAIN);
			new String:SmokeColor[128];
			new red = GetRandomInt(100, 120);
			new green = GetRandomInt(100, 120);
			new blue = GetRandomInt(200, 255);
			Format(SmokeColor, sizeof(SmokeColor), "%i %i %i", red, green, blue);
			new String:SName[128];
			Format(SName, sizeof(SName), "Smoke%i", client);
			DispatchKeyValue(SmokeEnt,"targetname", SName);
			DispatchKeyValue(SmokeEnt,"Origin", originData);
			DispatchKeyValue(SmokeEnt,"Spreadspeed", "120");
			DispatchKeyValue(SmokeEnt,"speed", "60");
			DispatchKeyValue(SmokeEnt,"startsize", "20");
			DispatchKeyValue(SmokeEnt,"endsize", "1");
			DispatchKeyValue(SmokeEnt,"rate", "200");
			DispatchKeyValue(SmokeEnt,"jetlength", "175");
			DispatchKeyValue(SmokeEnt,"twist", "175");
			DispatchKeyValue(SmokeEnt,"RenderAmt", "255");
			DispatchKeyValue(SmokeEnt,"RenderColor", SmokeColor);
			DispatchKeyValue(SmokeEnt,"SmokeMaterial", "particle/fire.vmt");
			DispatchSpawn(SmokeEnt);
			AcceptEntityInput(SmokeEnt, "TurnOn");
			new Float:delay = 12.0;
			new Handle:pack;
			CreateDataTimer(delay, Timer_KillSmoke, pack);
			WritePackCell(pack, SmokeEnt);
			new Float:longerdelay = 5.0 + delay;
			new Handle:pack2;
			CreateDataTimer(longerdelay, Timer_StopSmoke, pack2);
			WritePackCell(pack2, SmokeEnt);
			iVec[2]+=120.0;
			TE_SetupGlowSprite( iVec, IceBurn, 3.5 , 1.5 , 150);
			TE_SendToAll(1.0);
			EmitSoundToAll(thunder1, SOUND_FROM_WORLD, SNDCHAN_AUTO, SNDLEVEL_NORMAL, SND_NOFLAGS, SNDVOL_NORMAL, 500, -1, iVec, NULL_VECTOR, true, 0.0);
		}
	}
}

//Example Syntax : WL_SmokeStack(iVec, "15", "100", "50", "15", "15", "25", "200", "100", "5", "particle\fire.vmt", "25", "75", "255", "255", "4.5");
/*stock WL_SmokeStack(vec, basespread, spreadspeed, speed, startsize, endsize, anzahl, jet, drehung, material, red, green, blue, alpha, duration)
{
	new WLEnt = CreateEntityByName("env_smokestack");
		new String:originData[64];
		Format(originData, sizeof(originData), "%f %f %f", vec[0], vec[1], vec[2]);
		if(WLEnt)
		{
			EmitSoundToAll( announce , client,_,SNDLEVEL_TRAIN);
			new String:WLColor[128];
			Format(WLColor, sizeof(WLColor), "%i %i %i", red, green, blue);
			new String:SName[128];
			Format(SName, sizeof(SName), "warlancer%i", client);
			DispatchKeyValue(WLEnt,"targetname", SName);
			DispatchKeyValue(WLEnt,"Origin", originData);
			DispatchKeyValue(WLEnt,"Spreadspeed", spreadspeed);
			DispatchKeyValue(WLEnt,"speed", basespread);
			DispatchKeyValue(WLEnt,"Basespread", basespread);
			DispatchKeyValue(WLEnt,"startsize", startsize);
			DispatchKeyValue(WLEnt,"endsize", endsize);
			DispatchKeyValue(WLEnt,"rate", anzahl);
			DispatchKeyValue(WLEnt,"jetlength", jet);
			DispatchKeyValue(WLEnt,"twist", drehung);
			DispatchKeyValue(WLEnt,"RenderAmt", alpha);
			DispatchKeyValue(WLEnt,"RenderColor", WLColor);
			DispatchKeyValue(WLEnt,"SmokeMaterial", "particle/fire.vmt");
			DispatchSpawn(WLEnt);
			AcceptEntityInput(WLEnt, "TurnOn");
			new Float:delay = 12.0;
			new Handle:pack;
			CreateDataTimer(delay, Timer_KillSmoke, pack);
			WritePackCell(pack, WLEnt);
			new Float:longerdelay = 5.0 + delay;
			new Handle:pack2;
			CreateDataTimer(longerdelay, Timer_StopSmoke, pack2);
			WritePackCell(pack2, WLEnt);
		}
}*/

public Action:dropdown(Handle:h,any:client){
	//new attacker;
	for(new i=1;i<=MaxClients;i++){
		if(ValidPlayer(i,true)){
			if(bStormEye[i]&&IsPlayerAlive(i)){
				new Float:ang[3];
				new Float:pos[3];
				GetClientEyeAngles(i,ang);
				GetClientAbsOrigin(i,pos);
				pos[2]+=100;
				TE_SetupSmoke( pos, Particle, 20.0, 5 );
				TE_SendToAll()
				TE_SetupSparks( pos, ang, 500, 50);
				TE_SendToAll();
				//eye of the storm	
				pos[2]+=20;
				TE_SetupGlowSprite( pos, IceBurn, 1.0 , 1.5 , 150);
				TE_SendToAll();
				//overtime smoke
				/*CreateTimer(0.10, Smoke, i);
				CreateTimer(0.20, Smoke, i);
				CreateTimer(0.30, Smoke, i);
				CreateTimer(0.40, Smoke, i);
				CreateTimer(0.50, Smoke, i);
				CreateTimer(0.60, Smoke, i);
				CreateTimer(0.70, Smoke, i);
				CreateTimer(0.80, Smoke, i);
				CreateTimer(0.90, Smoke, i);
				CreateTimer(0.99, Smoke, i);*/
				new Float:fx_delay = 0.10;
				for(new smoke=1;smoke<=8;smoke++){
					CreateTimer(0.10, Smoke, i);
					fx_delay += 0.15;
				}
				//effekt erstellen
				/*new SmokeEnt = CreateEntityByName("env_smokestack");
				new String:originData[64];
				Format(originData, sizeof(originData), "%f %f %f", pos[0], pos[1], pos[2]);
				if(SmokeEnt)
				{
					//einstellen
					new String:SmokeColor[128];
					new red = GetRandomInt(0, 40);
					new green = GetRandomInt(0, 80);
					new blue = GetRandomInt(210, 255);
					Format(SmokeColor, sizeof(SmokeColor), "%i %i %i", red, green, blue);
					new String:SName[128];
					Format(SName, sizeof(SName), "WcsL%i", i);
					DispatchKeyValue(SmokeEnt,"targetname", SName);
					DispatchKeyValue(SmokeEnt,"Origin", originData);
					DispatchKeyValue(SmokeEnt,"Spreadspeed", "35");
					DispatchKeyValue(SmokeEnt,"Basespread", "28");
					DispatchKeyValue(SmokeEnt,"speed", "90");
					//DispatchKeyValue(SmokeEnt,"startsize", "13");
					DispatchKeyValue(SmokeEnt,"startsize", "15")
					DispatchKeyValue(SmokeEnt,"endsize", "5");
					DispatchKeyValue(SmokeEnt,"rate", "600");
					DispatchKeyValue(SmokeEnt,"jetlength", "19");
					DispatchKeyValue(SmokeEnt,"twist", "600");
					DispatchKeyValue(SmokeEnt,"roll", "320");
					DispatchKeyValue(SmokeEnt,"RenderAmt", "255");
					DispatchKeyValue(SmokeEnt,"RenderColor", SmokeColor);
					DispatchKeyValue(SmokeEnt,"SmokeMaterial", "effects/strider_muzzle.vmt");
					DispatchSpawn(SmokeEnt);
					//anschalten
					AcceptEntityInput(SmokeEnt, "TurnOn");
					//effekt entfernen
					new Float:delay = 0.95;
					new Handle:pack;
					CreateDataTimer(delay, Timer_StopSmoke, pack);
					WritePackCell(pack, SmokeEnt);
									
					CreateTimer(0.2, twist0, SmokeEnt);
					CreateTimer(0.7, twist1, SmokeEnt);
				}*/
				//skill function
				new skill=War3_GetSkillLevel(i,thisRaceID,ULT_STORM);
				new Float:distance=Distance[skill];
				new zufallschaden = GetRandomInt(10,30);
				DoStorm(i,distance,zufallschaden); 
				PrintCenterText(i,"Eye of the Storm!");
			}
		}
	}
}

public Action:Smoke(Handle:Timer, any:client)
{
	new Float:iVec[ 3 ];
	GetClientAbsOrigin( client, Float:iVec );
	iVec[2]+=100;
	TE_SetupSmoke( iVec, Particle, 10.0, 3 );
	TE_SendToAll();
	//twist effect
}

/*public Action:twist0(Handle:Timer, any:ent)
{
	new String:Angles[128];
	Format(Angles, sizeof(Angles), "0 0 90");
	DispatchKeyValue(ent,"twist", "800");
	DispatchKeyValue(ent,"speed", "200");
	DispatchKeyValue(ent,"Spreadspeed", "120");
	DispatchKeyValue(ent,"WindSpeed", "40");
	DispatchKeyValue(ent,"WindAngles", Angles);
}

public Action:twist1(Handle:Timer, any:ent)
{
	new String:Angles[128];
	Format(Angles, sizeof(Angles), "0 0 0");
	DispatchKeyValue(ent,"twist", "800");
	DispatchKeyValue(ent,"speed", "200");
	DispatchKeyValue(ent,"Spreadspeed", "40");
	DispatchKeyValue(ent,"WindSpeed", "0");
	DispatchKeyValue(ent,"WindAngles", Angles);
}*/

//env_smokestack handler
public Action:Timer_KillSmoke(Handle:timer, Handle:pack)
{      
	ResetPack(pack);
	new SmokeEnt = ReadPackCell(pack); 
	StopSmokeEnt(SmokeEnt);
}
StopSmokeEnt(target)
{
	if (IsValidEntity(target))
	{
		AcceptEntityInput(target, "TurnOff");
	}
} 
public Action:Timer_StopSmoke(Handle:timer, Handle:pack)
{      
	ResetPack(pack);
	new SmokeEnt = ReadPackCell(pack);
	RemoveSmokeEnt(SmokeEnt);

}
RemoveSmokeEnt(target)
{
	if (IsValidEntity(target))
	{
		AcceptEntityInput(target, "Kill");
	}
}

public DoStorm(client,Float:distance,dmg)
{
	new target=0;
	new Float:target_dist=distance+1.0;
	new caster_team=GetClientTeam(client);
	new Float:start_pos[3];
	GetClientAbsOrigin(client,start_pos);
	for(new x=1;x<=MaxClients;x++)
	{
		if(ValidPlayer(x,true)&&caster_team!=GetClientTeam(x)&&!W3HasImmunity(x,Immunity_Ultimates))
		{
			new Float:this_pos[3];
			GetClientAbsOrigin(x,this_pos);
			new Float:dist_check=GetVectorDistance(start_pos,this_pos);
			if(dist_check<=target_dist)
			{
				target=x;
				target_dist=dist_check;
			}
		}
	}
	if(target<=0)
	{
		PrintHintText(client,"No Target Found");
		//W3MsgNoTargetFound(client,distance);
	}
	else
	{
		War3_DealDamage(target,dmg,client,DMG_ENERGYBEAM,"storm");
		PrintHintText(target,"Eye of The Storm caused %d damage to you",War3_GetWar3DamageDealt());
		PrintHintText(client,"Eye of The Strom:\n Dealing %d damage to the nearest victim!",dmg);
		//PrintCenterText(client,"Eye of the Storm hit target!");
		PrintCenterText(target,"Got hit by the Eye of The Storm");
		start_pos[2]+=100.0;
		new Float:target_pos[3];
		GetClientAbsOrigin(target,target_pos);
		target_pos[2]+=30.0;
		TE_SetupBeamPoints(start_pos,target_pos,Beam,HaloSprite,0,35,1.0,20.0,60.0,0,45.0,{120,120,255,255},40);
		TE_SendToAll();
		TE_SetupBeamPoints(start_pos,target_pos,BeamSprite,HaloSprite,0,35,1.0,1.0,5.0,0,120.0,{255,100,255,255},60);
		TE_SendToAll();
		new Float:direction[3]={0.0,0.0,120.0};
		TE_SetupEnergySplash(target_pos, direction, true)
		TE_SendToAll();
		EmitSoundToAll( explode , target,_,SNDLEVEL_TRAIN);
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(Razor(client) && pressed && IsPlayerAlive(client))
	{
		new skill=War3_GetSkillLevel(client,race,ULT_STORM);
		if(skill>0)
		{
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT_STORM,true)&&!Silenced(client))
			{
				new Float:timetill=Duration[skill];
				//enable cookies, for the hungry one
				bStormEye[client]=true;
				CreateTimer(timetill, RemoveMarkStorm, client);
				EmitSoundToAll( storm , client,_,SNDLEVEL_TRAIN);
				PrintCenterText(client,"Eye of the Storm!");
				PrintToChat(client,"\x03[\x01Eye Of The Storm\x03]\x05You Sucessfully called the Cloud to damage the nearest enemy!");
				new Float:cooldown=GetConVarFloat(ultCooldownCvar);
				War3_CooldownMGR(client,cooldown,thisRaceID,ULT_STORM,_,_);
			}
		}
		else
		{
			W3MsgUltNotLeveled(client);
		}
	}
}

public Action:RemoveMarkStorm(Handle:Timer, any:client)
{
	//disable cookie, get back to the dark side!
	bStormEye[client]=false;
	PrintToChat(client,"\x03[\x01Eye Of The Storm\x03]\x05Storm is fading out...");
	new Float:iVec[ 3 ];
	GetClientAbsOrigin( client, Float:iVec );
	TE_SetupSmoke( iVec, Particle, 10.0, 3 );
	TE_SendToAll();
}


public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client))
	{
		new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_PF);
		if(skill_level>0)
		{
			if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_PF,true))
			{
				if(!Silenced(client))
				{
					new target=0;
					new Float:distance=Range[skill_level];
					new Float:target_dist=distance+1.0;
					new caster_team=GetClientTeam(client);
					new Float:start_pos[3];
					GetClientAbsOrigin(client,start_pos);
					//effects
					PrintCenterText(client,"Plasma Field!");
					start_pos[2]+=20.0;
					new Float:direction[3]={0.0,0.0,120.0};
					for(new it=1;it<=4;it++){
						TE_SetupEnergySplash(start_pos, direction, true)
						TE_SendToAll();
						TE_SetupBeamRingPoint(start_pos, 10.0, target_dist, Plasma, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {20,250,255,255}, 50, 0);
						TE_SendToAll();
						TE_SetupBeamRingPoint(start_pos, 10.0, 280.0, Plasma, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {20,250,255,255}, 50, 0);
						TE_SendToAll(0.5);
						TE_SetupBeamRingPoint(start_pos, target_dist, 10.0, Plasma, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {20,250,255,255}, 50, 0);
						TE_SendToAll(4.0);
						start_pos[2]+=20.0;
					}
					start_pos[2]+=20.0;
					TE_SetupExplosion(start_pos, Plasma, 1.5, 1, 4, 0, 0);
					TE_SendToAll(); 
					TE_SetupExplosion(start_pos, Plasma, 1.5, 1, 4, 0, 0);
					TE_SendToAll(1.8); 
					start_pos[2]-=80.0;
					EmitSoundToAll( plasma , client,_,SNDLEVEL_TRAIN);
					CreateTimer(0.75,PlasmaFlybye,client);
					EmitSoundToAll( plasma , target,_,SNDLEVEL_TRAIN);
					new Float:cooldown=GetConVarFloat(abilityCooldownCvar);
					War3_CooldownMGR(client,cooldown,thisRaceID,SKILL_PF,_,_);
					PrintToChat(client,"\x05You Released a field of energetic plasma!");
					//effects
					for(new x=1;x<=MaxClients;x++)
					{
						if(ValidPlayer(x,true)&&caster_team!=GetClientTeam(x)&&!W3HasImmunity(x,Immunity_Ultimates))
						{
							new Float:this_pos[3];
							GetClientAbsOrigin(x,this_pos);
							new Float:dist_check=GetVectorDistance(start_pos,this_pos);
							if(dist_check<=target_dist)
							{
								target=x;
								//target_dist=dist_check;
								War3_DealDamage(target,GetRandomInt(10,30),client,DMG_ENERGYBEAM,"plasmafield");
								PrintHintText(target,"Losed %d Health due a Plasma Field",War3_GetWar3DamageDealt());
								
								new Float:target_pos[3];
								GetClientAbsOrigin(target,target_pos);
								TE_SetupBeamPoints(start_pos,target_pos,Ice,HaloSprite,0,41,1.6,6.0,15.0,0,10.5,{255,255,255,222},45);
								TE_SendToAll();
								
								CreateTimer(1.8,RemoveSlow,target);
								War3_SetBuff(target,fSlow,thisRaceID,0.79);
							}
						}
					}
					/*if(client!=0)
					{
						PrintToServer("[SM] Sourcemod Error, cant use 'Plasma Field' on nobody!");
					}
					else
					{
						//> bigger then
						//< smaller then
						// learn it at school
						
					}*/
				}
				else
				{
					PrintHintText(client,"Silenced: Can not cast!");
				}
			}
		}
	}
}

public Action:over(Handle:h,any:index){
	if(ValidPlayer(index,false)) {
		bLinked[index]=false;
		new attacker=LinkedBy[index];
		bLinker[attacker]=false;
		StopSound(index, SNDCHAN_AUTO,link1);
		PrintCenterText(index,"Static Link deactivatet!");
	}
}

public Action:StaticLinkLoop(Handle:h,any:data){
	new attacker;
	for(new i=1;i<=MaxClients;i++){
		if(ValidPlayer(i,true)){
			if(bLinked[i]){
				attacker=LinkedBy[i];
				if(ValidPlayer(attacker)&&IsPlayerAlive(attacker)){
					new Float:iVec[ 3 ];
					GetClientAbsOrigin( i, Float:iVec );
					iVec[2]+=50.0;
					new Float:iVec2[ 3 ];
					GetClientAbsOrigin( attacker, Float:iVec2 );
					iVec2[2]+=56.0;
					TE_SetupExplosion(iVec, BeamSprite, 0.3, 1, 4, 0, 0);
					TE_SendToAll(); 
					TE_SetupBeamPoints(iVec,iVec2,BeamSprite,HaloSprite,0,41,1.6,6.0,15.0,0,20.5,{111,111,255,222},45);
					TE_SendToAll();
					new Float:direction[3]={-50.0,50.0,120.0};
					TE_SetupEnergySplash(iVec2, direction, true)
					TE_SendToAll();
					EmitSoundToAll(linkdmg,i,SNDCHAN_AUTO);
					War3_DealDamage(i,1,attacker,_,"staticlink");
					War3_HealToBuffHP(attacker,GetRandomInt(2,4));
					PrintToConsole(attacker,"Static Link is affecting your victim...");
					PrintToConsole(i,"Affected by a Static Link!");
				}
				
			}
		}
		if(GetGameTime()>LinkUntil[i]){
			attacker=LinkedBy[i];
			bLinked[i]=false;
			bLinker[attacker]=false;
		}
	}
}

public OnW3TakeDmgBulletPre(victim,attacker,Float:damages){
	if(ValidPlayer(victim)&&ValidPlayer(attacker)&&victim!=attacker&&GetClientTeam(victim)!=GetClientTeam(attacker)){
		if(War3_GetRace(victim)==thisRaceID&&!Hexed(victim,false)){
			new level=War3_GetSkillLevel(victim,thisRaceID,SKILL_UC);
			if( level>0 &&!Hexed(victim,false) && GetRandomFloat(0.0,1.0)<=Chance[level] && !bSupernatural[victim])
			{
				new Float:duration=Zeit[level];
				new Float:speed=Speedy[level];
				new Float:agility=AttackSpeed[level];
				War3_SetBuff(victim,fMaxSpeed,thisRaceID,speed);
				War3_SetBuff(victim,fLowGravitySkill,thisRaceID,0.25);
				War3_SetBuff(victim,bImmunityUltimates,thisRaceID,true);
				War3_SetBuff(victim,fAttackSpeed,thisRaceID,agility);
				CreateTimer(duration,NormalSettings,victim);
				PrintToChat(victim,"\x05You transformet to a unnatural state!");
				W3FlashScreen(victim,RGBA_COLOR_BLUE);
				bSupernatural[victim]=true;
				decl Float:pos[3];
				GetClientAbsOrigin( victim, Float:pos );
				new SmokeEnt = CreateEntityByName("env_smokestack");
				new String:originData[64];
				Format(originData, sizeof(originData), "%f %f %f", pos[0], pos[1], pos[2]);
				if(SmokeEnt)
				{
					new String:SmokeColor[128];
					Format(SmokeColor, sizeof(SmokeColor), "100 100 220");
					new String:SName[128];
					Format(SName, sizeof(SName), "Smoke%i", victim);
					DispatchKeyValue(SmokeEnt,"targetname", SName);
					//DispatchKeyValue(SmokeEnt,"parentname", victim );
					DispatchKeyValue(SmokeEnt,"Origin", originData);
					DispatchKeyValue(SmokeEnt,"Spreadspeed", "10");
					DispatchKeyValue(SmokeEnt,"Basespread", "5");
					DispatchKeyValue(SmokeEnt,"speed", "10");
					DispatchKeyValue(SmokeEnt,"startsize", "5")
					DispatchKeyValue(SmokeEnt,"endsize", "1");
					DispatchKeyValue(SmokeEnt,"rate", "200");
					DispatchKeyValue(SmokeEnt,"jetlength", "100");
					DispatchKeyValue(SmokeEnt,"twist", "30");
					DispatchKeyValue(SmokeEnt,"RenderAmt", "100");
					DispatchKeyValue(SmokeEnt,"RenderAmt", "18");
					DispatchKeyValue(SmokeEnt,"RenderColor", SmokeColor);
					DispatchKeyValue(SmokeEnt,"SmokeMaterial", "particle/fire.vmt");
					DispatchSpawn(SmokeEnt);
					//SetVariantString(victim);
					AcceptEntityInput(SmokeEnt, "SetParent");
					AcceptEntityInput(SmokeEnt, "TurnOn");
					//new Float:delay = 12.0;
					new Handle:pack;
					CreateDataTimer(duration, Timer_KillSmoke, pack);
					WritePackCell(pack, SmokeEnt);
					new Float:longerdelay = 8.0 + duration;
					new Handle:pack2;
					CreateDataTimer(longerdelay, Timer_StopSmoke, pack2);
					WritePackCell(pack2, SmokeEnt);
					pos[2]+=20;
					new Float:direction[3]={80.0,80.0,80.0};
					TE_SetupEnergySplash(pos, direction, true)
					TE_SendToAll();
					new Float:fxdelay = 0.0;
					for(new i=1;i<=4;i++){
					TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
					TE_SendToAll(fxdelay);
					fxdelay += 0.2;
					/*TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
					TE_SendToAll(0.2);
					TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
					TE_SendToAll(0.4);
					TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
					TE_SendToAll(0.6);*/
					}
					TE_SetupBeamFollow(victim, Plasma, HaloSprite, 3.5, 2.0, 20.0, 1, {222,222,255,200});
					TE_SendToAll();
				}
			}
			
		}
		if(War3_GetRace(attacker)==thisRaceID&&!Hexed(attacker,false)){
			//add a chance, that better for balancing stuff!
			if(GetRandomFloat(0.0,1.1)<=0.16 && !W3HasImmunity(victim,Immunity_Skills))
			{
				new skilllevel=War3_GetSkillLevel(attacker,thisRaceID,SKILL_SL);
				if(skilllevel>0){
					bLinked[victim]=true;
					bLinker[attacker]=true;
					LinkedBy[victim]=attacker;
					LinkUntil[victim]=GetGameTime()+Static[skilllevel]+6.5;
					CreateTimer(Static[skilllevel],over,attacker);
					new Float:iVec[ 3 ];
					GetClientAbsOrigin( victim, Float:iVec );
					new Float:iVec2[ 3 ];
					GetClientAbsOrigin( attacker, Float:iVec2 );
					iVec[2]+=40;
					iVec2[2]+=45;
					TE_SetupBeamPoints(iVec,iVec2,BeamSprite,HaloSprite,0,35,1.0,20.0,60.0,0,45.0,{125,125,255,255},40);
					TE_SendToAll();
					//War3_DealDamage(victim,GetRandomInt(2,skilllevel),attacker,DMG_ENERGYBEAM,"staticlink");
					DealDamageWrapper(victim,attacker,GetRandomInt(2,skilllevel),"staticlink");
					EmitSoundToAll(link1,attacker,SNDCHAN_AUTO);
					/*PrintToConsole(attacker,"[Static Link] Status - Createt : Reason - Damage on current victim");
					PrintToConsole(attacker,"[Static Link] you got static linked... damage percent 0.85%");
					PrintToConsole(victim,"[Static Link] linked enemy... damage percent 1.35%");*/
					PrintToChat(attacker,"\x02[\x01Static Link\x02]\x05You linked yourself with your enemy!");
					PrintToChat(victim,"\x03You got Linked to a Razor with a Static Link, damage will be reduced");
					//WL_SmokeStack(iVec2, 15, 100, 50, 15, 15, 25, 200, 100, 5, "particle\fire.vmt", 25, 75, 255, 255, 4.5);
					//WL_SmokeStack(iVec, "15", "100", "50", "15", "15", "25", "200", "100", "5", "particle\fire.vmt", "25", "75", "255", "255", "4.5");
					new WLEnt = CreateEntityByName("env_smokestack");
					new String:originData[64];
					Format(originData, sizeof(originData), "%f %f %f",iVec2[0], iVec2[1], iVec2[2]);
					if(WLEnt)
					{
						new String:SColor[128];
						Format(SColor, sizeof(SColor), "100 100 220");
						new String:Angles[128];
						Format(Angles, sizeof(Angles), "90 90 90");
						new String:SName[128];
						Format(SName, sizeof(SName), "warlancer%i", attacker);
						DispatchKeyValue(WLEnt,"targetname", SName);
						DispatchKeyValue(WLEnt,"Origin", originData);
						DispatchKeyValue(WLEnt,"Spreadspeed", "700");
						DispatchKeyValue(WLEnt,"speed", "450");
						DispatchKeyValue(WLEnt,"Basespread", "50");
						DispatchKeyValue(WLEnt,"startsize", "20");
						DispatchKeyValue(WLEnt,"endsize", "1");
						DispatchKeyValue(WLEnt,"rate", "80");
						DispatchKeyValue(WLEnt,"jetlength", "150");
						DispatchKeyValue(WLEnt,"twist", "12");
						DispatchKeyValue(WLEnt,"RenderAmt", "200");
						DispatchKeyValue(WLEnt,"RenderColor", SColor);
						DispatchKeyValue(WLEnt,"SmokeMaterial", "particle/fire.vmt");
						DispatchSpawn(WLEnt);
						AcceptEntityInput(WLEnt, "TurnOn");
						new Float:delay = 12.0;
						new Handle:pack;
						CreateDataTimer(delay, Timer_KillSmoke, pack);
						WritePackCell(pack, WLEnt);
						new Float:longerdelay = 5.0 + delay;
						new Handle:pack2;
						CreateDataTimer(longerdelay, Timer_StopSmoke, pack2);
						WritePackCell(pack2, WLEnt);
					}
				}
			}
		}
		
		//Static Link Powerup
		if(bLinker[attacker]){
			PrintCenterText(attacker,"Raised Damage due the Static Link");
			War3_DamageModPercent(1.35);
			new Float:direction[3]={0.0,0.0,20.0};
			new Float:iVec[ 3 ];
			GetClientAbsOrigin( victim, Float:iVec );
			TE_SetupEnergySplash(iVec, direction, true)
			TE_SendToAll();
			TE_SetupEnergySplash(iVec, direction, false)
			TE_SendToAll();
		}
		if(bLinked[attacker]){
			PrintCenterText(attacker,"Reduced Damage due the Static Link");
			War3_DamageModPercent(0.85);
			new Float:iVec[ 3 ];
			GetClientAbsOrigin( attacker, Float:iVec );
			TE_SetupExplosion(iVec, BeamSprite, 1.0, 1, 4, 0, 0);
			TE_SendToAll();
		}
		if(bLinker[attacker]&&bLinked[victim]){
			//PrintCenterText(attacker,"Extra Damage to linked target");
			new randomdmg = GetRandomInt(2,6);
			PrintToChat(attacker,"\x05You caused %i extra damage to your victim", randomdmg);
			//War3_DealDamage(victim,randomdmg,attacker,DMG_ENERGYBEAM,"staticlink");
			DealDamageWrapper(victim,attacker,randomdmg,"staticlink");
		}
		if(bLinker[victim]&&bLinked[attacker]){
			//PrintCenterText(attacker,"Damage to your linker is reduced!!!!");
			PrintToChat(attacker,"\x05Recvived damage because damage target is linked withyourself");
			//War3_DealDamage(attacker,GetRandomInt(1,2),attacker,DMG_ENERGYBEAM,"staticlink");
			DealDamageWrapper(attacker,victim,GetRandomInt(1,2),"staticlink");
		}
		/*if(damage>=15)
				{
					PrintToChat(attacker,"\x05You haven taken too much damage, Static Link deactivatet!");
					PrintToConsole(attacker,"[Static Link] Status - Disappeared : Reason - Too much damage recvived");
				}*/
	}
}

public OnWar3EventDeath(victim,attacker){
	bStormEye[victim]=false;
	bLinked[victim]=false;
	bLinker[attacker]=false;
	bLinker[victim]=false;
	bSupernatural[victim]=false;
}

public Action:PlasmaFlybye(Handle:t,any:client){
	EmitSoundToAll( done , client,_,SNDLEVEL_TRAIN);
}

public Action:NormalSettings(Handle:t,any:client){
	War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
	War3_SetBuff(client,fLowGravitySkill,thisRaceID,1.0);
	War3_SetBuff(client,fAttackSpeed,thisRaceID,1.0);
	War3_SetBuffItem(client,bImmunityUltimates,thisRaceID,false);
	PrintToChat(client,"\x05Transformed back to the normal state...");
	new Float:pos[ 3 ];
	GetClientAbsOrigin( client, Float:pos );
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll();
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.1);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.2);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.3);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.4);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.5);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.6);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.8);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(1.0);
	pos[2]+=40;
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll();
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.1);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.2);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.3);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.4);
	TE_SetupBeamRingPoint(pos, 10.0, 280.0, BeamSprite, HaloSprite, 0, 4, 1.0, 50.0, 1.0, {100,100,220,255}, 50, 0);
	TE_SendToAll(0.5);
}

public Action:RemoveSlow(Handle:t,any:client){
	War3_SetBuff(client,fSlow,thisRaceID,1.0);
	PrintToConsole(client,"slow-down disappears");
}
//WcsL | Revan
//Razor Version 1 = Release 2