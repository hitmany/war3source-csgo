#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include "W3SIncs/War3Source_Interface"
#define OnUsr1 "OnUser1 !self:Kill::1.7:1"
#define KNIFE_MDL "models/weapons/w_knife_ct.mdl"
#define KNIFEHIT_SOUND "weapons/knife/knife_hit3.wav"
new thisRaceID;
new Handle:runCooldownCvar;
new Float:g_fVelocity;
new Handle:g_hLethalArray;
new g_iKnifeMI;
new MaxKnivesPerUserArray[5]={0,3,6,8,10};
new MaxStaminaArray[5]={0,35,40,66,100};
new Float:InvisArray[5]={1.0,0.8,0.65,0.50,0.32};
new HasKnives[MAXPLAYERS];
new currentstamina[MAXPLAYERS];
new maxstamina[MAXPLAYERS];
new Handle:g_CVarFF;
new SKILL_1,SKILL_2,S_ULTIMATE;
//new g_iPointHurt;
new g_iEnvBlood;
new bool:bRunning[MAXPLAYERS];
new BeamSprite,Tracer; 
new const Float:g_fSpin[3] = {1877.4, 0.0, 0.0};
new const Float:g_fMinS[3] = {-16.0, -16.0, -16.0};
new const Float:g_fMaxS[3] = {16.0, 16.0, 16.0};
public Plugin:myinfo = 
{
	name = "War3Source Race - Shadow Ninja",
	author = "Meng, DonRevan",
	description = "Ninja race for War3Source.",
	version = "1.0.0.0",
	url = "http://www.wcs-lagerhaus.de"
};

public OnPluginStart()
{
	runCooldownCvar=CreateConVar("war3_shadownin_training_cooldown","5","Cooldown time for Shadow Ninja's training skill.");
	g_hLethalArray = CreateArray();
	AddNormalSoundHook(NormalSHook:SoundsHook);
	g_fVelocity = (1000.0 + (250.0 * 5.0));
	g_CVarFF = FindConVar("mp_friendlyfire");
	HookEvent("round_start", EventRoundStart, EventHookMode_PostNoCopy);
	CreateTimer(1.0,StaminaTimer,_,TIMER_REPEAT);
	CreateTimer(0.8,StaminaInfo,_,TIMER_REPEAT);
}

public OnMapStart() 
{
	BeamSprite = PrecacheModel("materials/sprites/orangeglow1.vmt");
	Tracer = PrecacheModel("materials/sprites/smoke.vmt");
	PrecacheSound(KNIFEHIT_SOUND);
	g_iKnifeMI = PrecacheModel(KNIFE_MDL);
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==270)
	{
		thisRaceID=War3_CreateNewRace("Shadow Ninja","shadownin");
		SKILL_1=War3_AddRaceSkill(thisRaceID,"Sprint","After solving hard training you are able to run much faster then normal humans on cost of your Stamina!(+ability)",false,4);
		SKILL_2=War3_AddRaceSkill(thisRaceID,"Cloaking","Turns yourself invisible (20-78%)",false,4);
		S_ULTIMATE=War3_AddRaceSkill(thisRaceID,"Throwing Knives","Ability to throw knives! ",true,2);
		W3SkillCooldownOnSpawn(thisRaceID,SKILL_1,10.0,true);
		War3_CreateRaceEnd(thisRaceID);
	}
}

public EventRoundStart(Handle:event, const String:name[], bool:dontBroadcast) {

	//g_iPointHurt = -1;
	g_iEnvBlood = -1;
	ClearArray(g_hLethalArray);
	CreateEnts();
}

public OnWar3EventSpawn(client)
{
	if(War3_GetRace(client)==thisRaceID){
		new throw_level=War3_GetSkillLevel(client,thisRaceID,S_ULTIMATE);
		new level=War3_GetSkillLevel(client,thisRaceID,SKILL_1);
		new knives = MaxKnivesPerUserArray[throw_level];
		HasKnives[client]=knives;
		currentstamina[client]=10;
		bRunning[client]=false;
		maxstamina[client]=MaxStaminaArray[level];
		War3_ChatMessage(client,"You have %i Knives!",knives);
		new skill=War3_GetSkillLevel(client,thisRaceID,SKILL_2);
		if(skill>0)
		{
			PrintToChat(client,"\x04[Cloaking] \x03You have equipped your cloak!");
			War3_SetBuff( client, fInvisibilitySkill, thisRaceID, InvisArray[skill] );
			new Float:iVec[ 3 ];
			GetClientAbsOrigin( client, Float:iVec );
			iVec[2]+=45;
			TE_SetupBeamRingPoint(iVec,20.0,380.0,BeamSprite,BeamSprite,0,10,3.5,28.0,0.0,{200,200,255,255},8,0);
			TE_SendToAll();
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && IsPlayerAlive(client))
	{	
		new skill=War3_GetSkillLevel(client,race,S_ULTIMATE);
		if(skill>0)
		{
			if(HasKnives[client]>0)
			{
				//knive throw do not need any cooldown because of the limitations
				KnifeThrow(client,race);
				new Float:iVec[ 3 ];
				GetClientAbsOrigin( client, Float:iVec );
				TE_SetupBeamRingPoint(iVec,10.0,20.0,BeamSprite,BeamSprite,0,10,1.5,25.0,0.0,{200,200,255,255},8,0);
				TE_SendToAll();
			}
		}
	}
}

KnifeThrow(client,race) {
	static Float:fPos[3], Float:fAng[3], Float:fVel[3];
	GetClientEyePosition(client, fPos);
	/* create & spawn entity. set model & owner. set to kill itself OnUser1 */
	new kunai = CreateEntityByName("flashbang_projectile");
	if ((kunai != -1) && DispatchSpawn(kunai)) {
		new throw_level=War3_GetSkillLevel(client,race,S_ULTIMATE);
		if(throw_level>0)
		{
			SetEntityModel(kunai, KNIFE_MDL);
			SetEntPropEnt(kunai, Prop_Send, "m_hOwnerEntity", client);
			SetVariantString(OnUsr1);
			AcceptEntityInput(kunai, "AddOutput");
			
			GetClientEyeAngles(client, fAng);
			GetAngleVectors(fAng, fVel, NULL_VECTOR, NULL_VECTOR);
			ScaleVector(fVel, g_fVelocity);
			SetEntPropVector(kunai, Prop_Data, "m_vecAngVelocity", g_fSpin);
			SetEntPropFloat(kunai, Prop_Send, "m_flElasticity", 0.2);

			PushArrayCell(g_hLethalArray, kunai);
			TeleportEntity(kunai, fPos, fAng, fVel);
			--HasKnives[client];
			PrintHintText(client, "Knives(%i/%i)", HasKnives[client],MaxKnivesPerUserArray[throw_level]);
			TE_SetupBeamFollow(kunai, Tracer, 0, 0.7, 7.7, 7.7, 3, {20,20,60,180});
			TE_SendToAll();
		}
	}
}

stock W3DissolveClassname( const String:target[] )
{
	new dissolver = CreateEntityByName("env_entity_dissolver");
	if(dissolver != 0)
	{
		new String:SName[128];
		Format(SName, sizeof(SName), "dissolver_%i", target);
		DispatchKeyValue(dissolver,"magnitude", "12000");
		DispatchKeyValue(dissolver,"dissolvetype", "2");
		DispatchKeyValue(dissolver,"target", target);
		DispatchSpawn(dissolver);
		AcceptEntityInput(dissolver, "Dissolve");
		CreateTimer( 1.40, Timer_RemoveEntity, dissolver );
	}
}

public Action:Timer_RemoveEntity( Handle:timer, any:ent )
{	
	AcceptEntityInput(ent,"Kill");
}

public Action:SoundsHook(clients[64], &numClients, String:sample[PLATFORM_MAX_PATH], &entity, &channel, &Float:volume, &level, &pitch, &flags) {

	if (StrEqual(sample, "weapons/flashbang/grenade_hit1.wav", false)) {
		new index = FindValueInArray(g_hLethalArray, entity);
		//PrintToChatAll("[debug]flashbang detonade");
		if (index != -1) {
			//PrintToChatAll("[ignore-this]knife found");
			volume = 0.2;
			RemoveFromArray(g_hLethalArray, index); /* delethalize on first "hit" */
			new attacker = GetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity");
			static Float:fKnifePos[3], Float:fAttPos[3], Float:fVicEyePos[3];
			GetEntPropVector(entity, Prop_Data, "m_vecOrigin", fKnifePos);
			new victim = GetTraceHullEntityIndex(fKnifePos, attacker);
			//if (IsClientIndex(victim) && IsClientInGame(attacker)) {
			RemoveEdict(entity);
			//DispatchKeyValue(entity,classname, "war3knife");
			//W3DissolveClassname("war3knife");
			//PrintToChatAll("[debug]removing entinty...");
			if (GetConVarBool(g_CVarFF) || (GetClientTeam(victim) != GetClientTeam(attacker))) {
				//PrintToChatAll("[debug]client found, did he took any dmg?");
				GetClientAbsOrigin(attacker, fAttPos);
				GetClientEyePosition(victim, fVicEyePos);
				EmitAmbientSound(KNIFEHIT_SOUND, fKnifePos, victim, SNDLEVEL_NORMAL, _, 0.7);
				// lets use the war3dealdamage function ... Hurt(victim, attacker, fAttPos, (FloatAbs(fKnifePos[2] - fVicEyePos[2]) < 4.7) ? "100" : "60");
				War3_DealDamage(victim,GetRandomInt(58,102),attacker,_,"weapon_knife",W3DMGORIGIN_SKILL,W3DMGTYPE_TRUEDMG);
				if((FloatAbs(fKnifePos[2] - fVicEyePos[2]) < 4.7))
				{
					//PrintToChatAll("headshot?");
					//War3_DealDamage(victim,GetRandomInt(20,30),attacker,_,"weapon_knife",W3DMGORIGIN_SKILL,W3DMGTYPE_TRUEDMG);
				}
				Bleed(victim);
			}
			//}
			else
			AcceptEntityInput(entity, "FireUser1");
			return Plugin_Changed;
		}
		else if (GetEntProp(entity, Prop_Send, "m_nModelIndex") == g_iKnifeMI) {
			volume = 0.2;
			return Plugin_Changed;
		}
	}
	return Plugin_Continue;
}

GetTraceHullEntityIndex(Float:pos[3], xindex) {
	TR_TraceHullFilter(pos, pos, g_fMinS, g_fMaxS, MASK_SHOT, THFilter, xindex);
	return TR_GetEntityIndex();
}

public bool:THFilter(entity, contentsMask, any:data) {

	return IsClientIndex(entity) && (entity != data);
}

bool:IsClientIndex(index) {

	return (index > 0) && (index <= MaxClients);
}


CreateEnts() {

	//if (((g_iPointHurt = CreateEntityByName("point_hurt")) != -1) && DispatchSpawn(g_iPointHurt)) {
	//	DispatchKeyValue(g_iPointHurt, "DamageTarget", "hurt");
	//	DispatchKeyValue(g_iPointHurt, "DamageType", "0");
	//}
	if (((g_iEnvBlood = CreateEntityByName("env_blood")) != -1) && DispatchSpawn(g_iEnvBlood)) {
		DispatchKeyValue(g_iEnvBlood, "targetname", "shadowninja_bleed");
		DispatchKeyValue(g_iEnvBlood, "spawnflags", "13");
		DispatchKeyValue(g_iEnvBlood, "amount", "1000");
	}
}

/*Hurt(victim, attacker, Float:attackerPos[3], String:damage[]) {

	if (IsValidEntity(g_iPointHurt)) {
		DispatchKeyValue(victim, "targetname", "hurt");
		DispatchKeyValue(g_iPointHurt, "Damage", damage);
		DispatchKeyValue(g_iPointHurt, "classname", "weapon_knife");
		TeleportEntity(g_iPointHurt, attackerPos, NULL_VECTOR, NULL_VECTOR);
		AcceptEntityInput(g_iPointHurt, "Hurt", attacker);
		DispatchKeyValue(g_iPointHurt, "classname", "point_hurt");
		DispatchKeyValue(victim, "targetname", "nohurt");
	}
}*/

Bleed(client) {
	if (IsValidEntity(g_iEnvBlood))
	AcceptEntityInput(g_iEnvBlood, "EmitBlood", client);
}

/*public NativeSetClientThrowingKnives(Handle:plugin, numParams) {

	new client = GetNativeCell(1);
	new num = GetNativeCell(2);
	HasKnives[client] = num;
	if (IsClientInGame(client))
		new throw_level=War3_GetSkillLevel(client,race,ULTIMATE);
		PrintHintText(client, "Knives(%i/%i)", num,MaxKnivesPerUserArray[throw_level]);
}*/

public OnRaceChanged(client, oldrace, newrace)
{
	if(newrace!=thisRaceID){
		War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
		War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
		War3_WeaponRestrictTo(client,thisRaceID,"");
		//ClientCommand(client, "r_screenoverlay 0");
	}
	if(newrace==thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"weapon_knife;weapon_c4;weapon_usp;weapon_glock;weapon_p228;weapon_deagle;weapon_elite;weapon_fiveseven;weapon_ump45;weapon_mac10;weapon_tmp;weapon_p90;weapon_scout");
		PrintToChat(client,"\x03Shadow Ninja has some restrictions, allowed weapons are \x05knife, pistols, smgs, c4, scout");
	}
}

/*public OnWar3EventSpawn(client)
{
	new race = War3_GetRace(client);
	if (race == thisRaceID)
	{
		new throw_level=War3_GetSkillLevel(client,race,ULTIMATE);
		new level=War3_GetSkillLevel(client,race,SKILL_1);
		new knives = MaxKnivesPerUserArray[throw_level];
		HasKnives[client]=knives;
		currentstamina[client]=10;
		bRunning[client]=false;
		maxstamina[client]=MaxStaminaArray[level];
		War3_ChatMessage(client,"You have %i Knives!",knives);
		//ClientCommand(client, "r_screenoverlay effects/tp_refract");
	}
}*/

public OnWar3EventDeath(client,attacker)
{
	new race = War3_GetRace(client);
	if (race == thisRaceID)
	{
		//ClientCommand(client, "r_screenoverlay 0");
		War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
	}
}

public Action:StaminaTimer(Handle:timer)
{
	new shnin;
	for(new i=1;i<=MaxClients;i++)
	{
		if(ValidPlayer(i,true))
		{
			new race=War3_GetRace(i);
			if(race==thisRaceID)
			{
				shnin = i;
				if(!bRunning[shnin])
				{ 
					new level=War3_GetSkillLevel(shnin,race,SKILL_1);
					if(currentstamina[shnin]<MaxStaminaArray[level])
					{
						currentstamina[shnin]++;
					}
				}
			}
		}
	}
}

public Action:StaminaInfo(Handle:timer) 
{
	for(new i=1;i<=MaxClients;i++) 
	{
		//if (bRunning[i] && IsPlayerAlive(i)) 
		if (War3_GetRace(i)==thisRaceID && IsPlayerAlive(i)) 
		{
			switch(currentstamina[i] / 10)
			{
				case(0):
				{
					bRunning[i]=false;
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.0);
				}
				case(1):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.08);
				}
				case(2):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.1);
				}
				case(3):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.16);
				}
				case(4):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.30);
				}
				case(5):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.25);
				}
				case(6):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.30);
				}
				case(7):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.35);
				}
				case(8):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.40);
				}
				case(9):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.45);
				}
				case(10):
				{
					War3_SetBuff(i,fMaxSpeed,thisRaceID,1.50);
				}
			}
			PrintCenterText(i,"Stamina : %i%/%i%",currentstamina[i],maxstamina[i]);
		}
	}
}

/*if(ability==0 && !pressed && IsPlayerAlive(client))
{
War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
bRunning[client]=false;
War3_CooldownMGR(client,GetConVarFloat(runCooldownCvar),thisRaceID,SKILL_1,true,true,true,"Sprint");
PrintToConsole(client,"[W3S] Ability key released, skill deactivated...");
}*/
public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID)
	{
		new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_1);
		if(skill_level>0&&!Silenced(client))
		{
			if(ability==0 && pressed && IsPlayerAlive(client))
			{
				if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_1,true))
				{
					if(bRunning[client])
					{
						War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
						bRunning[client]=false;
						War3_CooldownMGR(client,GetConVarFloat(runCooldownCvar),thisRaceID,SKILL_1,true,true);
						PrintToConsole(client,"[W3S] Ability key released, skill deactivated...");
					}
					else
					{
						bRunning[client]=true;
						PrintToConsole(client,"[W3S] Ability key pressed, skill activated...");
					}
				}
			}
		}
	}
}