

#pragma semicolon 1

#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
#include <sdktools_functions>
#include <sdktools_tempents>
#include <sdktools_tempents_stocks>


/*
* GetPlayerWeaponSlot(client, slot); Entity index on success
* native bool:RemovePlayerItem(client, item);  Entity index 
* native GivePlayerItem(client, const String:item[], iSubType=0);  classname
* */


new thisRaceID;
new bool:bFlying[66];
new Handle:ultCooldownCvar;

//skill 1
new Float:this_pos[3];
new GlowSprite,GlowSprite2;
new bool:bFaerie[66];
new Float:AbilityCooldownTime=15.0;
new Float:FaerieMaxDistance[]={0.0,650.0,700.0,750.0,800.0,850.0,900.0,950.0,1000.0}; //max distance u can target your ultimate

//skill 2
new m_vecBaseVelocity; //offsets
new Float:CycloneVec[9]={0.0,380.0,390.0,400.0,410.0,420.0,430.0,440.0,450.0};
//skill 3
new String:crow[]="npc/crow/alert3.wav";


new SKILL_FAERIE, SKILL_CYCLONE, ULT_CROW;

public Plugin:myinfo = 
{
	name = "War3Source Race - FormChangerDruid",
	author = "[Oddity]TeacherCreature",
	description = "The Druid race for War3Source.",
	version = "1.0.1.0",
	url = "warcraft-source.net"
};

public OnPluginStart()
{
	HookEvent("weapon_fire", WeaponFire);
	ultCooldownCvar=CreateConVar("war3_druidt_flying_cooldown","1.0","Cooldown for Flying");
	HookEvent("round_start",RoundStartEvent);
	m_vecBaseVelocity = FindSendPropOffs("CBasePlayer","m_vecBaseVelocity");
}

public Action:WeaponFire(Handle:event, const String:name[], bool:dontBroadcast)
{
	new userid=GetEventInt(event,"userid");
	new index=GetClientOfUserId(userid);
	if(index>0)
	{
		new race=War3_GetRace(index);
		if(race==thisRaceID&&bFlying[index])
		{
			EmitSoundToAll(crow,index);
		}
	}
}

public OnRaceChanged(client,oldrace, newrace)
{
	if(newrace!=thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"");
		War3_SetBuff(client,bFlyMode,thisRaceID,false);
	}
	else
	{
		War3_WeaponRestrictTo(client,thisRaceID,"weapon_knife");
		if(IsPlayerAlive(client)){
			bFlying[client]=false;
		}
	}
}

public OnWar3LoadRaceOrItemOrdered(num)
{
	if(num==48)
	{
		thisRaceID=War3_CreateNewRace("Druid of the Talon","druidt");
		SKILL_FAERIE=War3_AddRaceSkill(thisRaceID,"Faerie Fire","Mark your target (press ability)",false,8);
		SKILL_CYCLONE=War3_AddRaceSkill(thisRaceID,"Cyclone","Lift your enemy in the air",false,8);
		ULT_CROW=War3_AddRaceSkill(thisRaceID,"Crow Form","You transform into a Crow",false,1); 
		War3_CreateRaceEnd(thisRaceID);
	}
}

public OnGameFrame()
{
	for(new i=1;i<=MaxClients;i++){
		if(ValidPlayer(i,true))
		{
			new tteam=GetClientTeam(i);
			if(bFaerie[i]==true)
			{
				GetClientAbsOrigin(i,this_pos);
				this_pos[2]+=20;//offset for effect
				if(tteam==2)
				{
					TE_SetupGlowSprite(this_pos,GlowSprite,0.1,0.6,80);
					TE_SendToAll();
					//TE_SendToClient(client, Float:delay=0.0) 
				}
				else
				{
					this_pos[2]+=20;
					TE_SetupGlowSprite(this_pos,GlowSprite2,0.1,0.1,150);
					TE_SendToAll();	
				}
			}
		}
	}
}

public OnMapStart()
{
	PrecacheModel("models/crow.mdl", true);
	PrecacheModel("models/pigeon.mdl", true);
	GlowSprite=PrecacheModel("effects/redflare.vmt");
	//GlowSprite2=PrecacheModel("effects/bluespark.vmt");
	//GlowSprite=PrecacheModel("VGUI/gfx/VGUI/arctic.vmt");
	GlowSprite2=PrecacheModel("VGUI/gfx/VGUI/gign.vmt");
	War3_PrecacheSound(crow);
}

public OnAbilityCommand(client,ability,bool:pressed)
{
	if(War3_GetRace(client)==thisRaceID && ability==0 && pressed && IsPlayerAlive(client))
	{
		new skill_level=War3_GetSkillLevel(client,thisRaceID,SKILL_FAERIE);
		if(skill_level>0)
		{
			if(War3_SkillNotInCooldown(client,thisRaceID,SKILL_FAERIE,true))
			{
				new target = War3_GetTargetInViewCone(client,FaerieMaxDistance[skill_level],false,23.0);
				if(target>0 && !W3HasImmunity(target,Immunity_Skills))
				{
					PrintHintText(client,"Faerie Fire: Marked Target");
					bFaerie[target]=true;
					War3_CooldownMGR(client,AbilityCooldownTime,thisRaceID,SKILL_FAERIE,_,true);
				}
				else
				{
					PrintHintText(client,"NO VALID TARGETS WITHIN %.1f FEET",FaerieMaxDistance[skill_level]/10.0);
				}
			}
			
		}
		else
		{
			PrintHintText(client,"Level Your Ability First");
		}
	}
}

public OnW3TakeDmgBullet(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			new race_attacker=War3_GetRace(attacker);
			new skill_level_cyclone=War3_GetSkillLevel(attacker,thisRaceID,SKILL_CYCLONE);
			// Cyclone
			if(race_attacker==thisRaceID && skill_level_cyclone>0)
			{
				if(GetRandomFloat(0.0,1.0)<=0.8 && !W3HasImmunity(victim,Immunity_Skills))
				{
					new Float:velocity[3];
					velocity[2]=CycloneVec[skill_level_cyclone];
					SetEntDataVector(victim,m_vecBaseVelocity,velocity,true);
					PrintToConsole(attacker,"Cyclone");
					PrintToConsole(victim,"Cyclone");
					W3FlashScreen(victim,RGBA_COLOR_RED, 1.2,0.9);
					W3FlashScreen(attacker,RGBA_COLOR_RED, 0.3,0.4);
				}
			}
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(race==thisRaceID && pressed && ValidPlayer(client,true))
	{
		new ult_level=War3_GetSkillLevel(client,race,ULT_CROW);
		if(ult_level>0)		
		{
			new Float:cooldown=GetConVarFloat(ultCooldownCvar);
			if(War3_SkillNotInCooldown(client,thisRaceID,ULT_CROW,true)) 
			{
				if(!bFlying[client])
				{
					bFlying[client]=true;
					War3_SetBuff(client,bFlyMode,thisRaceID,true);
					PrintHintText(client,"Crow Form!");
					SetEntityModel(client, "models/crow.mdl");
					EmitSoundToAll(crow,client);
					
					new weapon=GetPlayerWeaponSlot(client, 2);
					if(weapon>0){
						RemovePlayerItem(client, weapon); 
					}
					/*if(GetClientTeam(client)==3)
					{
						SetEntityModel(client, "models/pigeon.mdl");
					}
					if(GetClientTeam(client)==2)
					{
						SetEntityModel(client, "models/crow.mdl");
					}*/
				}	
				else
				{
					CreateTimer(0.1,returnform,client);
				}
				War3_CooldownMGR(client,cooldown,thisRaceID,ULT_CROW,_,true);
			}
			
		}
		else
		{
			PrintHintText(client,"Level Your Ultimate First");
		}
	}
}

public Action:returnform(Handle:h, any:client)
{
	if(IS_PLAYER(client)&&IS_PLAYER(client))
	{
		bFlying[client]=false;
		War3_SetBuff(client,bFlyMode,thisRaceID,false);
		PrintHintText(client,"Elf Form!");
		GivePlayerItem(client, "weapon_knife");
		if(GetClientTeam(client)==3)
		{
			SetEntityModel(client, "models/player/ct_urban.mdl");
		}
		if(GetClientTeam(client)==2)
		{
			SetEntityModel(client, "models/player/t_leet.mdl");
		}
	}
}

public OnWar3EventDeath(victim,attacker)
{
	bFaerie[victim]=false;
}

public OnClientPutInServer(client)
{
	War3_SetBuff(client,bFlyMode,thisRaceID,false);
}

public RoundStartEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	for(new i=1;i<=MaxClients;i++)
	{
		bFaerie[i]=false;
	}
}

public OnWar3EventSpawn(client)
{
	new race=War3_GetRace(client);
	if(race==thisRaceID)
	{
		UTIL_Remove( GetPlayerWeaponSlot(client, 0));  
		UTIL_Remove( GetPlayerWeaponSlot(client, 1));  
		bFlying[client]=false;
		War3_SetBuff(client,bFlyMode,thisRaceID,false);
		GivePlayerItem(client, "weapon_knife");
		War3_SetBuff(client,fMaxSpeed,thisRaceID,1.3);
	}
	else{
		//bFlying[client]=true; //kludge, not to allow some other race switch to this race and explode on death (ultimate)
	}
}