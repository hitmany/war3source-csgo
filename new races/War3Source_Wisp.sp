/*** File: War3Source_Wisp.sp* Description: The Wisp for War3Source.
* Author(s): [Oddity]TeacherCreature
*/
 
#pragma semicolon 1
 
#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
#include <sdkhooks>

new thisRaceID;

public Plugin:myinfo = 
{
	name = "War3Source Race - Wisp",
	author = "Teacher Creature",
	description = "The Wisp race for War3Source.",
	version = "1.0.7.6",
	url = "http://warcraft-source.net/"
};

new SKILL_WISP, SKILL_GHOSTLY, SKILL_SPIRIT, SKILL_ANCIENT, SKILL_SUICIDE;

new Float:WispSpeed[]={1.0,1.4,1.45,1.50,1.55,1.6};
new Float:InvisArr[]={1.0, 0.8, 0.6, 0.4, 0.2, 0.01};
new HealthArr[]={100,80,60,40,30,25};
new Float:AncientChance[]={0.0, 0.1, 0.15, 0.2, 0.25, 0.3};

new Float:SuicideBomberRadius[5]={0.0,200.0,233.0,275.0,333.0}; 
new Float:SuicideBomberDamage[5]={0.0,166.0,200.0,233.0,266.0};
new Float:SuicideBomberDamageTF[5]={0.0,133.0,175.0,250.0,300.0};

new ExplosionModel;
new BeamSprite;
new HaloSprite;

// use weapon_usp for cs:s
// p250 for cs:go
#define	WISPGUN "weapon_p250"
#define CSGO_WISP_HP 35

stock TE_SetupDynamicLight(const Float:vecOrigin[3], r,g,b,iExponent,Float:fRadius,Float:fTime,Float:fDecay)
{
	TE_Start("Dynamic Light");
	TE_WriteVector("m_vecOrigin",vecOrigin);
	TE_WriteNum("r",r);
	TE_WriteNum("g",g);
	TE_WriteNum("b",b);
	TE_WriteNum("exponent",iExponent);
	TE_WriteFloat("m_fRadius",fRadius);
	TE_WriteFloat("m_fTime",fTime);
	TE_WriteFloat("m_fDecay",fDecay);
}

public OnPluginStart()
{
	HookEvent("weapon_fire",Event_OnWeaponFire);
}

new Float:direction[3]={-50.0,50.0,120.0};
public Action:Event_OnWeaponFire(Handle:event , String:name[] , bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	if(War3_GetRace(client) == thisRaceID) {
		decl Float:pos[3];
		GetClientAbsOrigin(client, pos);
		pos[2]+=GetRandomFloat(15.0, 42.0);
		TE_SetupEnergySplash(pos, direction, true);
		TE_SendToAll();
	}
}

public OnMapStart()
{
	if(GAMETF)
	{
		ExplosionModel=PrecacheModel("materials/particles/explosion/explosionfiresmoke.vmt",false);
	}
	else
	{
		ExplosionModel=PrecacheModel("materials/sprites/zerogxplode.vmt",false);
	}
	BeamSprite=PrecacheModel("materials/sprites/lgtning.vmt");
	HaloSprite=PrecacheModel("materials/sprites/halo01.vmt");
}

public OnWar3LoadRaceOrItemOrdered2(num)
{
	if(num==121)
	{
		thisRaceID=War3_CreateNewRace("Wisp","wisp");
		SKILL_WISP=War3_AddRaceSkill(thisRaceID,"Wisp","Gain more Speed",false,5);
		new blah = 5;
		if(GAMECSGO) {
			blah = 1;
		}
		SKILL_GHOSTLY=War3_AddRaceSkill(thisRaceID,"Ghostly","Get invisible but less health",false,blah);
		SKILL_SPIRIT=War3_AddRaceSkill(thisRaceID,"Spirit","Skill Immunity",false,1);
		SKILL_ANCIENT=War3_AddRaceSkill(thisRaceID,"Ancient Wisp","Shocks an enemy, inflicting Nature damage",false,5);
		SKILL_SUICIDE=War3_AddRaceSkill(thisRaceID,"Detonate","Explode and do damage, will not activate near teamates",true,4);
		War3_CreateRaceEnd(thisRaceID);
		W3Faction(thisRaceID,"Elves",true);
	}
}
public OnRaceChanged( client, oldrace, newrace )
{
	if(oldrace == thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,"");
		War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
		War3_SetBuff(client,fMaxSpeed,thisRaceID,1.0);
		War3_SetBuff(client,bImmunitySkills,thisRaceID,false);
		War3_SetBuff(client,bDoNotInvisWeapon,thisRaceID,false);
		//SetEntityRenderState(client,true);
		SetEntityRenderMode(client,RENDER_TRANSCOLOR);
	}
	else if(newrace == thisRaceID){
		War3_WeaponRestrictTo(client,thisRaceID,WISPGUN);
		War3_SetBuff(client,bDoNotInvisWeapon,thisRaceID,true);
		if(IsPlayerAlive(client)){
			GivePlayerItem(client, WISPGUN);
			PassiveSkills(client);
		}
	}
}

public PassiveSkills(client){
	if(ValidPlayer(client,true)&&War3_GetRace(client)==thisRaceID)
	{
		new Float:client_location[3];
		new skill_speed=War3_GetSkillLevel(client,thisRaceID,SKILL_WISP);
		if(skill_speed)
		{
			new Float:speed=WispSpeed[skill_speed];
			War3_SetBuff(client,fMaxSpeed,thisRaceID,speed);
			GetClientAbsOrigin(client,client_location);
			client_location[2]+=60;
			TE_SetupBeamRingPoint(client_location, 10.0, 120.0, BeamSprite, HaloSprite, 0, 15, 0.5, 10.0, 10.0, {255,255,255,255}, 120, 0);
			TE_SendToAll();
		}
		new skill_ghost=War3_GetSkillLevel(client,thisRaceID,SKILL_GHOSTLY);
		if(skill_ghost)
		{
			if(GAMECSGO) {
				PrintToChat(client,"Invisibility: You are now 100% invisible.");
				SetEntityRenderMode(client,RENDER_NONE);
				SetEntityHealth(client,CSGO_WISP_HP);
				War3_SetBuff(client,iAdditionalMaxHealthNoHPChange,thisRaceID,-65);
			}
			else {
				War3_SetBuff(client,fInvisibilitySkill,thisRaceID,InvisArr[skill_ghost]);
				SetEntityHealth(client,HealthArr[skill_ghost]);
				War3_SetBuff(client,iAdditionalMaxHealthNoHPChange,thisRaceID,-HealthArr[skill_ghost]); // we need to negate the value because its ADDITIONAL max health.
			}
			//War3_SetMaxHP(client,HealthArr[skill_ghost]);
			GetClientAbsOrigin(client,client_location);
			client_location[2]+=40;
			TE_SetupBeamRingPoint(client_location, 10.0, 250.0, HaloSprite, HaloSprite, 0, 15, 0.5, 10.0, 10.0, {0,255,0,255}, 120, 0);
			TE_SendToAll();
		}
		new skill_spirit=War3_GetSkillLevel(client,thisRaceID,SKILL_SPIRIT);
		if(skill_spirit)
		{
			War3_SetBuff(client,bImmunitySkills,thisRaceID,true);
			GetClientAbsOrigin(client,client_location);
			client_location[2]+=30;
			TE_SetupBeamRingPoint(client_location, 10.0, 300.0, ExplosionModel, HaloSprite, 0, 15, 0.5, 10.0, 10.0, {10,10,10,255}, 120, 0);
			TE_SendToAll();
		}
	}
}

public OnWar3EventSpawn(client)
{
	if(War3_GetRace(client)==thisRaceID)
	{
		//SetEntityRenderState(client,true);
		SetEntityRenderMode(client,RENDER_TRANSCOLOR);
		War3_SetBuff(client,fInvisibilitySkill,thisRaceID,1.0);
		GivePlayerItem(client, WISPGUN);
		PassiveSkills(client);
	}
}

public OnW3TakeDmgBulletPre(victim,attacker,Float:damage)
{
	if(IS_PLAYER(victim)&&IS_PLAYER(attacker)&&victim>0&&attacker>0&&attacker!=victim)
	{
		new vteam=GetClientTeam(victim);
		new ateam=GetClientTeam(attacker);
		if(vteam!=ateam)
		{
			// dirty hack.
			if(War3_GetRace(victim) == thisRaceID) {
				if(GetClientHealth(victim) > CSGO_WISP_HP) {
					SetEntityHealth(victim,CSGO_WISP_HP);
					PrintHintText(victim,"You've lost HP due to your unstable current!");
				}
			}

			new race_attacker=War3_GetRace(attacker);
			new level_ancient=War3_GetSkillLevel(attacker,thisRaceID,SKILL_ANCIENT);
			if(race_attacker==thisRaceID && level_ancient>0)
			{
				if(W3Chance(AncientChance[level_ancient]) && !W3HasImmunity(victim,Immunity_Skills))
				{  
					War3_DamageModPercent(1.2);   
					W3FlashScreen(victim,RGBA_COLOR_RED);
					decl Float:target_location[3];
					GetClientAbsOrigin(victim,target_location);
					target_location[2] += 20.0;
					TE_SetupExplosion(target_location,ExplosionModel,10.0,1,0,0,0);
					TE_SendToAll();
					PrintToConsole(attacker,"[WAR3] You've shocked your enemy!");
				}
			}
		}
	}
}

public OnUltimateCommand(client,race,bool:pressed)
{
	if(pressed)
	{
		if(race==thisRaceID&&IsPlayerAlive(client)&&!Silenced(client))
		{
			new ult_level=War3_GetSkillLevel(client,race,SKILL_SUICIDE);
			if(ult_level>0)
			{
				decl Float:location[3];
				GetClientAbsOrigin(client,location);
				if(War3_GetGame()==Game_TF)
				{
					War3_SuicideBomber(client, location, SuicideBomberDamageTF[ult_level], SKILL_SUICIDE, SuicideBomberRadius[ult_level]);
				} else {
					War3_SuicideBomber(client, location, SuicideBomberDamage[ult_level], SKILL_SUICIDE, SuicideBomberRadius[ult_level]);
				}
				ForcePlayerSuicide(client); //this causes them to die...
			}
			else
			{
				PrintHintText(client,"Level Your Ultimate First");
			}
		}
	}
}




