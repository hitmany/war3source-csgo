#include <sourcemod>
#include "W3SIncs/War3Source_Interface"
#include <sdktools>
#include <santa>

public Plugin:myinfo = 
{
    name = "War3Source Santa Restrict",
    author = "hitmany",
    description = "for event"
};

public OnPluginStart()
{
	HookEvent("round_start",RoundStartEvent);
}

public RoundStartEvent(Handle:event,const String:name[],bool:dontBroadcast)
{
	if((WhoIsSanta() != -1) && (WhoIsSanta() != 0))
	{
		for(new player=1;player<=MaxClients;player++)
		{
			if(IsClientInGame(player) && !IsFakeClient(player) && IsClientAuthorized(player))
			{
				new raceid = War3_GetRace(player);
				new humanid = War3_GetRaceIDByShortname("human");
				if(raceid == War3_GetRaceIDByShortname("naix"))
				{
					War3_SetRace(player, humanid);
				}
				else if(raceid == War3_GetRaceIDByShortname("nightelf"))
				{
					War3_SetRace(player, humanid);
				}
				else if(raceid == War3_GetRaceIDByShortname("stealth"))
				{
					War3_SetRace(player, humanid);
				}
				else if(raceid == War3_GetRaceIDByShortname("undead"))
				{
					War3_SetRace(player, humanid);
				}
				else if(raceid == War3_GetRaceIDByShortname("crypt"))
				{
					War3_SetRace(player, humanid);
				}
			}
		}
	}
}

public OnRaceChanged(client,oldrace,newrace)
{
    if((WhoIsSanta() != -1) && (WhoIsSanta() != 0))
	{
		new humanid = War3_GetRaceIDByShortname("human");
		if(newrace==War3_GetRaceIDByShortname("naix"))
		{
			War3_SetRace(client, humanid);
		}
		else if(newrace == War3_GetRaceIDByShortname("nightelf"))
		{
			War3_SetRace(client, humanid);
		}
		else if(newrace == War3_GetRaceIDByShortname("stealth"))
		{
			War3_SetRace(client, humanid);
		}
		else if(newrace == War3_GetRaceIDByShortname("undead"))
		{
			War3_SetRace(client, humanid);
		}
		else if(newrace == War3_GetRaceIDByShortname("crypt"))
		{
			War3_SetRace(client, humanid);
		}
	}
}