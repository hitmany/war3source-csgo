#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include "W3SIncs/War3Source_Interface"

#define PLUGIN_VERSION   "1.0"
#define SPECTATOR_NONE 				0
#define SPECTATOR_FIRSTPERSON 		4
#define SPECTATOR_3RDPERSON 		5
#define SPECTATOR_FREELOOK	 		6

public Plugin:myinfo =
{
	name = "Show race on HUD",
	author = "hitmany",
	description = "Show race on HUD",
	version = PLUGIN_VERSION,
	url = "http://hitmany.net/"
};

public OnPluginStart()
{
	CreateTimer(0.5, Timer, _, TIMER_REPEAT);
}

public Action:Timer(Handle:timer)
{
	decl String:str[200],String:racename[64];
	new race, level;
	for(new i = 1; i <= MaxClients; i++)
	{
			
			if(IsClientInGame(i))
			{
				new observer_mode = GetEntProp(i, Prop_Send, "m_iObserverMode");
				if((!IsFakeClient(i)) && ((observer_mode == SPECTATOR_FIRSTPERSON) || (observer_mode == SPECTATOR_3RDPERSON)))
				{
					new target = GetEntPropEnt(i, Prop_Send, "m_hObserverTarget");
					race = War3_GetRace(target);
					if(target > 0 && target <= MaxClients && IsClientInGame(target) && IsPlayerAlive(target))
					{
						War3_GetRaceName(race,racename,sizeof(racename));
						level =  War3_GetLevel(target, race);
						Format(str,sizeof(str),"<font color='#abff00'>%s</font><br>Level: %d",racename,level);
						new Handle: message_handle = StartMessageOne("KeyHintText", i);
						if (message_handle != INVALID_HANDLE) 
						{
							PbAddString(message_handle, "hints", str);
							EndMessage();
						}
					}
				}
			}
	}
	return Plugin_Continue; 
}